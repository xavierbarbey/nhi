const rollup = require('rollup');
const rollupConfig = require('./rollup.config');
const jsCompileHelper = require('./jsCompileHelper');
const path = require('path');

const rollupPlugins = rollupConfig.plugins;

const rollupJSModuleListForReCompile = [];
jsCompileHelper.getCacheModuleList();

const rollupJSModuleList = jsCompileHelper
	.getBundleList()
	.map(async (rollupJSModule) => {
		const cacheModule = jsCompileHelper.cacheModuleList.find((cacheModule) => {
			const srcPath = path.normalize(cacheModule.srcPath);
			const srcPathRollupModule = path.normalize(rollupJSModule.input);
			return srcPath.indexOf(srcPathRollupModule) > -1 && rollupJSModule.name === cacheModule.name;
		});

		if (!cacheModule) {
			rollupJSModuleListForReCompile.push(rollupJSModule);
			return;
		};

		const currentXXHash = await jsCompileHelper.getCacheModuleXXHash(cacheModule);
		const currentTimeHash = await jsCompileHelper.getCacheModuleTimeHash(cacheModule);

		if (currentXXHash !== cacheModule.moduleHash && currentTimeHash !== cacheModule.moduleHash) {
			rollupJSModuleListForReCompile.push(rollupJSModule);
		} else if (currentXXHash === cacheModule.moduleHash) {
			cacheModule.moduleHash = currentTimeHash;
		}
	});

(async function main() {
	await Promise.all(rollupJSModuleList);
	if (rollupJSModuleListForReCompile.length > 0) {
		rollupPlugins.push({
			generateBundle: jsCompileHelper.getGenerateHookHandler()
		});
		return rollupJSModuleListForReCompile.map(async (rollupJSModule) => {
			const bundleResult = await rollup.rollup({
				input: path.normalize(rollupJSModule.input),
				plugins: rollupPlugins,
				external: rollupConfig.external
			});
			return bundleResult.write({
				file: path.normalize(rollupJSModule.file),
				format: rollupConfig.output[0].format,
				sourcemap: rollupConfig.output[0].sourcemap,
				name: rollupJSModule.name,
				globals: rollupConfig.output[0].globals
			});
		});
	}
	return await jsCompileHelper.writeCacheConfigFile();
})();
