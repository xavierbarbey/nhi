define([
	'dojo/_base/declare',
	'dijit/_WidgetBase',
	'dijit/_TemplatedMixin',
	'dijit/Tooltip',
	'dojo/_base/lang',
	'ES/Scripts/Constants',
	'ES/Scripts/Controls/FilterDialog2',
	'ES/Scripts/Classes/Utils',
	'dojo/text!./../../Views/Templates/FilterItem.html',
	'dojo/text!./../../Views/Templates/FilterOption.html',
	'dojo/text!./../../Views/Templates/FilterTooltipItem.html'
], function(
	declare,
	_WidgetBase,
	_TemplatedMixin,
	Tooltip,
	lang,
	Constants,
	FilterDialog2,
	Utils,
	filterItemTemplate,
	filterOptionTemplate,
	filterTooltipItemTemplate
) {
	return declare([_WidgetBase, _TemplatedMixin], {
		_arasObj: null,
		_utils: null,
		_constants: null,

		_moreLabel: '',
		_selectAllTooltip: '',
		_sortAlphaModeTooltip: '',
		_sortFreqModeTooltip: '',

		_isInitiallySelected: false,

		id: '',
		item: null,
		showCounts: true,
		allItemsLoaded: false,

		_filterDialog2: null,

		_disableScroll: null,
		_onFilterChange: null,
		_onFilterReset: null,
		_onFilterLoadOptions: null,

		templateString: '',

		constructor: function(args) {
			this._arasObj = args.arasObj;
			this._utils = new Utils({
				arasObj: this._arasObj
			});
			this._constants = new Constants();

			this._moreLabel = args.moreLabel;
			this._selectAllTooltip = args.selectAllTooltip;
			this._sortAlphaModeTooltip = args.sortAlphaModeTooltip;
			this._sortFreqModeTooltip = args.sortFreqModeTooltip;

			this.item = args.item;
			this.showCounts = args.showCounts;

			this._disableScroll = args.disableScroll;
			this._onFilterChange = args.onFilterChange;
			this._onFilterReset = args.onFilterReset;
			this._onFilterLoadOptions = args.onFilterLoadOptions;

			this._isInitiallySelected = this.item.isAnyOptionSelected();

			this.templateString = lang.replace(filterItemTemplate, {
				filterId: this.item.id,
				moreLabel: this._moreLabel,
				filterLabel: this.item.title,
				clearTooltip: args.clearTooltip,
				orange: args.topFilter ? 'Orange' : '',
				clearVisible: this._isInitiallySelected ? '' : 'hidden',
				topFilter: args.topFilter ? 'topFilterItem' : '',
				filterOptionsMarkup: this._getFilterOptionsMarkup(),
				selectedFilter: this._isInitiallySelected ? 'selectedFilter' : ''
			});
		},

		postCreate: function() {
			var options = this.item.getOptions();

			if (options.length > 1 && ((options.length > this._constants.maxVisibleOptionsCount) || this._isInitiallySelected)) {
				//We need to show "more" link
				this._utils.setNodeVisibility(this.filterItemMoreOptionsLinkContainer, true);
			}

			new Tooltip({
				connectId: [this.filterItemLabel],
				label: this._getTooltipMarkup(),
				position: ['after']
			});
		},

		/**
		 * Get filter tooltip markup
		 *
		 * @private
		 */
		_getTooltipMarkup: function() {
			var maxItemTypesCount = 7;
			var indexedTypes = this.item.getIndexedTypes();

			return indexedTypes.length > maxItemTypesCount ?
				lang.replace(filterTooltipItemTemplate, {
					text: lang.replace(this._utils.getResourceValueByKey('hint.item_types_count'), [indexedTypes.length]),
					iconUrl: '',
					iconAttr: 'hidden'
				}) :
				indexedTypes.reduce(function(acc, data) {
					return acc + lang.replace(filterTooltipItemTemplate, {
						text: data.labelPlural,
						iconUrl: this._utils.getImageUrl(data.iconPath),
						iconAttr: ''
					});
				}.bind(this), '');
		},

		/**
		 * Get markup for filter options
		 *
		 * @private
		 */
		_getFilterOptionsMarkup: function() {
			var renderedOptionsCount = 0;
			var iteratedOptionsCount = 0;
			var res = '';
			var options = this._getOptions();

			while ((renderedOptionsCount < this._constants.maxVisibleOptionsCount) && (iteratedOptionsCount < options.length)) {
				var option = options[iteratedOptionsCount];
				var isOptionHidden = this._isInitiallySelected && !option.isSelected;

				if (!isOptionHidden) {
					res += lang.replace(filterOptionTemplate, {
						index: iteratedOptionsCount,
						name: option.name,
						label: option.label,
						count: option.count,
						state: option.isSelected ? 'checked' : '',
						visibility: this.showCounts ? 'visible' : 'hidden'
					});
					renderedOptionsCount++;
				}

				iteratedOptionsCount++;
			}

			return res;
		},

		/**
		 * Open filters dialog
		 *
		 * @param {object} ev Event object
		 * @private
		 */
		_openFilterDialog: function(ev) {
			this._filterDialog2 = new FilterDialog2({
				arasObj: this._arasObj,
				showCounts: this.showCounts,
				facet: this.item,
				moreLabel: this._moreLabel,
				selectAllTooltip: this._selectAllTooltip,
				sortAlphaModeTooltip: this._sortAlphaModeTooltip,
				sortFreqModeTooltip: this._sortFreqModeTooltip,
				onPopupClose: this._onPopupClose.bind(this)
			});
			this._filterDialog2.show(ev.target);
			this._disableScroll(true);
			ev.stopPropagation();
		},

		/**
		 * Updates filter item
		 *
		 * @public
		 */
		update: function() {
			var options = this._getOptions();

			for (var i = 0; i < options.length; i++) {
				var optionInput = this.domNode.querySelector('[data-filter-option-name="' + options[i].name + '"]');
				if (!this._utils.isNullOrUndefined(optionInput)) {
					optionInput.checked = options[i].isSelected;
				}
			}
		},

		/**
		 * Gets sorted options depending on current facet name
		 *
		 * @private
		 */
		_getOptions: function() {
			var options = [];

			if (this._constants.alphabeticallySortedFacets.indexOf(this.item.name) === -1) {
				options = this.item.getSortedOptionsByFrequency(this._constants.sortOrders.descending);
			} else {
				options = this.item.getSortedOptionsByAlphabet(this._constants.sortOrders.ascending);
			}

			return options;
		},

		/*------------------------------------------------------------------------------------------------------------*/
		//Event handlers

		_onPopupClose: function() {
			if (!this._utils.isNullOrUndefined(this._onFilterChange)) {
				this._onFilterChange(this.item);
			}

			if (!this._utils.isNullOrUndefined(this._disableScroll)) {
				this._disableScroll(false);
			}

			this.update();
		},

		_onFilterExpandCollapseEventHandler: function() {
			this._utils.toggleClass(this.filterItemContainer, 'filterItemCollapsed');
		},

		_onFilterItemClearOptionsClickEventHandler: function() {
			if (!this._utils.isNullOrUndefined(this._onFilterReset)) {
				this._onFilterReset(this.item);
			}
		},

		_onFilterOptionChangeEventHandler: function(ev) {
			var selected = ev.target.checked;
			var name = ev.target.getAttribute('data-filter-option-name');
			var option = this.item.getOption(name);

			option.isSelected = selected;

			if (!this._utils.isNullOrUndefined(this._onFilterChange)) {
				this._onFilterChange(this.item);
			}
		},

		_onMoreLinkClickEventHandler: function(ev) {
			var self = this;

			//Destroy old dialog
			if (!this._utils.isNullOrUndefined(this._filterDialog2)) {
				this._filterDialog2.destroy();
			}

			if (!self.allItemsLoaded) {
				this._utils.toggleSpinner(true, function() {
					try {
						if (!self._utils.isNullOrUndefined(self._onFilterLoadOptions)) {
							self.resultItem = self._onFilterLoadOptions(self.item);
						}

						var newOptions = self.resultItem.getOptions();
						self.item.getOptions().forEach(function(option) {
							option.count = 0;
						});
						for (var i = 0; i < newOptions.length; i++) {
							var newOption = newOptions[i];
							var curOption = self.item.getOption(newOption.name);
							if (self._utils.isNullOrUndefined(curOption)) {
								self.item.addOption(newOption.name, newOption.label, newOption.count, newOption.isSelected);
							} else {
								curOption.count = newOption.count;
							}
						}

						self.allItemsLoaded = true;
					} catch (ex) {
						self._arasObj.AlertError(ex.message);
					} finally {
						self._utils.toggleSpinner(false, null);
						self._openFilterDialog(ev);
					}
				});
			} else {
				self._openFilterDialog(ev);
			}
		}
	});
});
