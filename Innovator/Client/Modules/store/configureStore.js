﻿import {createStore} from '../../vendors/redux';

export default function configureStore(reducer, initialState) {
	const store = createStore(reducer, initialState);
	return store;
}
