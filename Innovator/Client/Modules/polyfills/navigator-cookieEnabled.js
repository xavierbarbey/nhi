﻿(function() {
	// cookieEnabled always is true in IE11. Try to set cookie to test it
	if ('ActiveXObject' in window && navigator.cookieEnabled) {
		const testCookieName = 'testCookieEnabled=';
		document.cookie = testCookieName;
		const cookieEnabled = document.cookie.indexOf(testCookieName) !== -1;
		document.cookie = testCookieName + ';expires=Thu, 01 Jan 1970 00:00:01 GMT;';

		if (!cookieEnabled) {
			Object.defineProperty(navigator, 'cookieEnabled', {
				get: function() {
					return false;
				}
			});
		}
	}
})();
