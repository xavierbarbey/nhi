﻿(function(window) {
	'use strict';

	const getIconNode = function(metadata) {
		if (!metadata || !metadata.iconUrl) {
			return null;
		}

		const img = {
			tag: 'img',
			attrs: {
				src: metadata.iconUrl
			}
		};

		if (metadata.iconStyle) {
			img.style = metadata.iconStyle;
		}

		img.className = metadata.iconClass || 'dynamicTreeGridIcon';
		return img;
	};

	window.Grid.formatters.dynamicTreeGrid_iconText = function(columnName, rowId, value, grid, metadata) {
		return {
			children: [
				getIconNode(metadata),
				{
					tag: 'span',
					children: [value]
				}
			]
		};
	};
}(window));
