﻿export default async function fetch(requestURL) {
	const aras = window.aras || window.opener.aras;
	const oAuthClient = aras.OAuthClient;
	const authHeaders = oAuthClient.getAuthorizationHeader();
	const headers = Object.assign({ Accept: 'application/json' }, authHeaders);

	try {
		const response = await window.fetch(requestURL, {
			headers: headers,
			credentials: 'same-origin'
		});

		const isUnauthorized =
			response.status === oAuthClient.unauthorizedStatusCode;
		const responseObject = isUnauthorized
			? {
					error: {
						code: 'unauthorized_error'
					}
			  }
			: await response.json();

		if (responseObject.error) {
			throw responseObject;
		}

		return responseObject;
	} catch (errorObject) {
		const error = errorObject.error;

		if (error && error.code) {
			const soap = window.SOAP || window.opener.SOAP;
			let message;

			switch (error.code) {
				case 'SOAP-ENV:Server.Authentication.SessionTimeout':
					message = aras.getResource('', 'soap_object.session_has_expired');
					break;
				case 'unauthorized_error':
					message = aras.getResource('', 'soap_object.unauthorized_error');
					break;
				default:
					return Promise.reject(errorObject);
			}
			try {
				await soap.handleSessionTimeout({
					message: message
				});

				return fetch(requestURL);
			} catch (e) {
				return Promise.reject(errorObject);
			}
		}
	}
}
