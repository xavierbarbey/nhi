﻿const SvgManager = {
	loadedUrls: new Set(),
	vaultImages: new Map(),
	_salt: 'svg-',
	_vaultRegExp: /vault:\/\/\/\?fileid=/i,
	_vaultPromises: new Map(),

	init: function() {
		if (this.dom) {
			return;
		}

		const svgNS = 'http://www.w3.org/2000/svg';
		const symbolContainer = document.createElementNS(svgNS, 'svg');
		symbolContainer.setAttribute('id', 'svg-symbols');
		symbolContainer.setAttribute('style', 'display:none');
		const defNode = document.createElementNS(svgNS, 'def');
		const filterNode = document.createElementNS(svgNS, 'filter');
		const feColorMatrixNode = document.createElementNS(svgNS, 'feColorMatrix');
		filterNode.setAttribute('id', 'GrayFilter');
		feColorMatrixNode.setAttribute('type', 'saturate');
		feColorMatrixNode.setAttribute('values', '0');
		filterNode.appendChild(feColorMatrixNode);
		defNode.appendChild(filterNode);

		symbolContainer.appendChild(defNode);
		document.body.appendChild(symbolContainer);
		this.dom = symbolContainer;
	},

	createHyperHTMLNode: function(icon, options = {}) {
		if (!icon) {
			return null;
		}

		const cssClasses = options.class || '';
		const alt = options.alt || '';
		const overlayIconUrls = options.overlayIcons || [];
		let iconUrlsToDisplay = [icon, ...overlayIconUrls];
		const allSvgIconsLoaded = iconUrlsToDisplay.every(iconUrl =>
			this.loadedUrls.has(iconUrl)
		);
		if (allSvgIconsLoaded) {
			const baseUrl = window.location.href.replace(window.location.hash, '');
			const useNodesTemplate = iconUrlsToDisplay
				.map(
					iconUrl =>
						`<use xlink:href="${baseUrl + '#' + this.getSymbolId(iconUrl)}" />`
				)
				.join('');
			const altNode = alt ? `<title>${alt}</title>` : '';
			const svgTemplate = `
				<svg class="${cssClasses}" focusable="false">
					${altNode}
					${useNodesTemplate}
				</svg>`;
			return HyperHTMLElement.wire(null, 'svg')([svgTemplate]);
		}

		const fileId = icon.replace(this._vaultRegExp, '');
		const imageUrl = this.vaultImages.get(fileId);
		if (!icon.toLowerCase().startsWith('vault:///?fileid=') || imageUrl) {
			iconUrlsToDisplay = [imageUrl || icon, ...overlayIconUrls];
			const topImage = iconUrlsToDisplay.pop();
			const backgroundImages = iconUrlsToDisplay
				.map(image => `url('${image}')`)
				.join(',');
			const styleAttribute = backgroundImages
				? `background-image:${backgroundImages};background-size:cover;`
				: '';
			const imgTemplate = `
				<img
					class="${cssClasses}"
					src="${topImage}"
					alt="${alt}"
					style="${styleAttribute}"
				/>
			`;

			return HyperHTMLElement.wire()([imgTemplate]);
		}

		const imageNode = HyperHTMLElement.wire()`<img class="${cssClasses}" alt="${alt}" />`;
		if (!this._vaultPromises.has(fileId)) {
			this.load([icon]);
		}
		this._vaultPromises.get(fileId).then(iconSrc => {
			iconUrlsToDisplay = [iconSrc, ...overlayIconUrls];
			imageNode.src = iconUrlsToDisplay.pop();

			const backgroundImages = iconUrlsToDisplay
				.map(image => `url('${image}')`)
				.join(',');
			if (backgroundImages) {
				imageNode.style.backgroundImage = backgroundImages;
				imageNode.style.backgroundSize = 'cover';
			}
		});

		return imageNode;
	},

	createInfernoVNode: function(icon, options = {}) {
		if (!icon) {
			return null;
		}

		const cssClasses = options.class || null;
		const alt = options.alt || null;
		const symbolId = this.getSymbolId(icon);
		if (symbolId) {
			const baseUrl = window.location.href.replace(window.location.hash, '');
			return (
				<svg className={cssClasses} focusable="false">
					{alt ? <title>{alt}</title> : null}
					<use xlinkHref={baseUrl + '#' + symbolId} />
				</svg>
			);
		}

		const setAsBackground = options.setAsBackground;
		let imageUrl = icon;
		const fileId = icon.replace(this._vaultRegExp, '');
		imageUrl = this.vaultImages.get(fileId);
		if (!icon.toLowerCase().startsWith('vault:///?fileid=') || imageUrl) {
			return <img className={cssClasses} src={imageUrl || icon} alt={alt} />;
		}

		let ref = null;
		const refPromise = new Promise(resolve => {
			ref = function(dom) {
				if (dom) {
					resolve(dom);
				}
			};
		});

		if (!this._vaultPromises.has(fileId)) {
			this.load([icon]);
		}
		Promise.all([this._vaultPromises.get(fileId), refPromise]).then(data => {
			const iconSrc = data[0];
			const imageNode = data[1];
			if (setAsBackground) {
				imageNode.style.backgroundImage = 'url(' + iconSrc + ')';
			} else {
				imageNode.src = iconSrc;
			}
		});

		return (
			<img
				className={cssClasses}
				src="../images/DefaultItemType.svg"
				alt={alt}
				ref={ref}
			/>
		);
	},

	getSymbolId: function(url) {
		const id = this._urlToId(url);
		return this.loadedUrls.has(url) ? this._salt + id : null;
	},

	load: function(symbolUrlsArr) {
		const self = this;
		const requiredUrls = symbolUrlsArr.filter(function(url) {
			return (
				url.match(/\.\.\/images\/(?!.+\/).+\.svg/i) && !self.loadedUrls.has(url)
			);
		});
		const vaultUrls = new Set();
		symbolUrlsArr
			.filter(function(url) {
				const fileId = url.replace(self._vaultRegExp, '');
				return url.match(self._vaultRegExp) && !self.vaultImages.has(fileId);
			})
			.forEach(url => {
				vaultUrls.add(url);
			});
		if (!requiredUrls.length && !vaultUrls.size) {
			return Promise.resolve();
		}
		this.init();
		requiredUrls.forEach(function(url) {
			self.loadedUrls.add(url);
		});
		const requiredPromise = new Promise(function(resolve, reject) {
			const query = requiredUrls
				.map(function(url) {
					return encodeURI(self._urlToId(url));
				})
				.join(',');
			const request = new XMLHttpRequest();
			request.onload = function() {
				if (request.responseText) {
					const parser = new DOMParser();
					const tempDocument = parser.parseFromString(
						request.responseText,
						'text/html'
					);
					const tempFragment = document.createDocumentFragment();
					const symbols = tempDocument.firstChild.childNodes;
					for (let i = 0; i < symbols.length; i++) {
						tempFragment.appendChild(symbols[i].cloneNode(true));
					}
					self.dom.appendChild(tempFragment);
				}
				resolve();
			};
			request.onerror = function() {
				reject();
			};
			request.open(
				'GET',
				aras.getBaseURL() + '/javascript/include.aspx?svg=' + query,
				true
			);
			request.send();
		});

		if (!vaultUrls.size) {
			return requiredPromise;
		}
		const headers = aras.OAuthClient.getAuthorizationHeader();
		vaultUrls.forEach(url => {
			const fileId = url.replace(self._vaultRegExp, '');
			url = aras.IomInnovator.getFileUrl(fileId, aras.Enums.UrlType.None);
			const promise = fetch(url, { headers: headers }).then(response =>
				response.blob().then(blob => {
					return new Promise((resolve, reject) => {
						const reader = new FileReader();
						reader.onloadend = () => resolve(reader.result);
						reader.onerror = reject;
						reader.readAsDataURL(blob);
					}).then(dataUrl => {
						self.vaultImages.set(fileId, dataUrl);
						self._vaultPromises.delete(fileId);
						return dataUrl;
					});
				})
			);

			self._vaultPromises.set(fileId, promise);
		});

		const vaultPromise = Promise.all(this._vaultPromises);

		return Promise.all([requiredPromise, vaultPromise]);
	},

	_urlToId: function(url) {
		return url
			.substring(url.lastIndexOf('/') + 1, url.lastIndexOf('.'))
			.toLowerCase();
	}
};

export default SvgManager;
