var Communicator;
(function (Communicator) {
    var HWF;
    (function (HWF) {
        var Internal;
        (function (Internal) {
            var BPack = /** @class */ (function () {
                function BPack() {
                    this.data = []; /*!< payload */
                    this.allocated = 0; /*!< amount of data allocated */
                    this.used = 0; /*!< current int being written */
                    this.bit = 0; /*!< current bit getting written */
                    this.rused = 0; /*!< next int to be read */
                    this.rbit = 0; /*!< next bit to be read */
                    this.can_reallocate = 0; /*!< if we don't own the pointer, we can't reallocate it */
                    this.status = 0; /*!< a flag indicating an error condition */
                    this.mask = []; /*!< precalculated values to efficiently mask reads */
                    this.range = []; /*!< precalculated values maximum possible value given bit length  */
                }
                BPack.prototype.Reset = function () {
                    this.data = []; /*!< payload */
                    this.allocated = 0; /*!< amount of data allocated */
                    this.used = 0; /*!< current int being written */
                    this.bit = 0; /*!< current bit getting written */
                    this.rused = 0; /*!< next int to be read */
                    this.rbit = 0; /*!< next bit to be read */
                    this.can_reallocate = 0; /*!< if we don't own the pointer, we can't reallocate it */
                    this.status = 0; /*!< a flag indicating an error condition */
                    this.mask[0] = 0;
                    this.range[0] = 0;
                    var i = 0;
                    for (i = 1; i <= 32; i++) {
                        this.mask[i] = ((-1) >>> 0) >>> (32 - i);
                        this.range[i] = ((1 << (i - 1)) >>> 0) - 1;
                    }
                };
                BPack.prototype.InitRead = function (size, pointer) {
                    this.Reset();
                    this.allocated = size / 4;
                    this.data = pointer;
                    this.can_reallocate = 0;
                };
                BPack.prototype.Get = function (numbits) {
                    var return_val;
                    if (this.rbit + numbits <= 32) {
                        var temp = (this.data[this.rused]) >>> 0;
                        return_val = (temp >>> (32 - this.rbit - numbits)) & this.mask[numbits];
                        this.rbit += numbits;
                    }
                    else {
                        var shift = (numbits + this.rbit - 32);
                        var temp = (this.data[this.rused++]) >>> 0;
                        return_val = ((temp << shift) >>> 0) & this.mask[numbits];
                        temp = (this.data[this.rused]) >>> 0;
                        return_val |= temp >>> (32 - shift);
                        this.rbit += numbits - 32;
                    }
                    return return_val;
                };
                BPack.prototype.NumBytes = function () {
                    return (this.used + (this.bit ? 1 : 0)) * 4;
                };
                return BPack;
            }());
            Internal.BPack = BPack;
        })(Internal = HWF.Internal || (HWF.Internal = {}));
    })(HWF = Communicator.HWF || (Communicator.HWF = {}));
})(Communicator || (Communicator = {}));
var Communicator;
(function (Communicator) {
    var HWF;
    (function (HWF) {
        var Internal;
        (function (Internal) {
            var CaptureParser = /** @class */ (function () {
                function CaptureParser(viewer, hwfParser) {
                    this._captureNames = [];
                    this._toCaptureTag = {};
                    this._toCamera = {};
                    this._toCuttingPlane = {};
                    this._toStyleTags = {};
                    this._toStyleMatrix = {};
                    this._toStyleVis = {};
                    this._principleStyleSegs = {};
                    this._principleNonStyleSegs = {};
                    //private _markupSegs: { [key: string]: boolean | undefined } = {};
                    this._toFrameMeshInstanceData = {};
                    this._viewer = viewer;
                    this._hwfParser = hwfParser;
                }
                CaptureParser.prototype.checkParseCapture = function (captureStr, segId) {
                    // Parse and store the data
                    var opt = Internal.Util.extractUserOpt(captureStr, "cad_cap_defns");
                    if (opt)
                        this.parseCaptureDefinitions(opt);
                    opt = Internal.Util.extractUserOpt(captureStr, "cad_cap_style_defns");
                    if (opt)
                        this.parseCaptureStyleDefinitions(opt);
                    opt = Internal.Util.extractUserOpt(captureStr, "cad_default_capture");
                    if (opt)
                        this.parseDefaultCapture(opt);
                    if (segId !== null) {
                        opt = Internal.Util.extractUserOpt(captureStr, "cm");
                        if (opt)
                            this.parseCaptureMarkup(opt, segId);
                        opt = Internal.Util.extractUserOpt(captureStr, "cn");
                        if (opt)
                            this.parseCaptureNonStyles(opt, segId);
                        opt = Internal.Util.extractUserOpt(captureStr, "cs");
                        if (opt)
                            this.parseCaptureStyles(opt, segId);
                    }
                };
                /*
                    Export to model structure
                */
                CaptureParser.prototype.exportToModelStructure = function () {
                    var defaultCaptureActivated = false;
                    var rootId = this._hwfParser.getHwfRootNode();
                    for (var i = 0; i < this._captureNames.length; i++) {
                        var viewName = this._captureNames[i];
                        var viewTag = this._toCaptureTag[viewName];
                        if (viewTag === undefined)
                            continue;
                        var camera = this._toCamera[viewTag];
                        if (camera === undefined)
                            continue;
                        var cuttingPlane = void 0;
                        var cuttingPlaneData = this._toCuttingPlane[viewTag];
                        if (cuttingPlaneData !== undefined && cuttingPlaneData.length === 4) {
                            cuttingPlane = new Communicator.Plane();
                            cuttingPlane.normal.x = cuttingPlaneData[0];
                            cuttingPlane.normal.y = cuttingPlaneData[1];
                            cuttingPlane.normal.z = cuttingPlaneData[2];
                            cuttingPlane.d = cuttingPlaneData[3];
                        }
                        var viewFrameMeshInstance = this._toFrameMeshInstanceData[viewTag];
                        var pmiToShow = [];
                        var nonStyleSeg = this._principleNonStyleSegs[viewTag];
                        if (nonStyleSeg !== undefined) {
                            for (var j = 0; j < nonStyleSeg.length; j++) {
                                var pmiSegId = nonStyleSeg[j];
                                var pmiSeg = this._hwfParser.getSegmentPropertyManager().getFromID(pmiSegId);
                                if (!!pmiSeg.assemblyNodeId)
                                    pmiToShow.push(pmiSeg.assemblyNodeId);
                            }
                        }
                        var styles = this._toStyleTags[viewTag];
                        var matrices = [];
                        if (styles !== undefined) {
                            for (var j = 0; j < styles.length; j++) {
                                var viz = this._toStyleVis[styles[j]];
                                var segs = this._principleStyleSegs[styles[j]];
                                if (viz !== undefined && !viz.everythingOff() && segs !== undefined) {
                                    for (var k = 0; k < segs.length; k++) {
                                        var segId = segs[k];
                                        var seg = this._hwfParser.getSegmentPropertyManager().getFromID(segId);
                                        if (!!seg.assemblyNodeId) {
                                            pmiToShow.push(seg.assemblyNodeId);
                                        }
                                    }
                                }
                            }
                            // matrices
                            for (var j = 0; j < styles.length; j++) {
                                var matrix = this._toStyleMatrix[styles[j]];
                                var segs = this._principleStyleSegs[styles[j]];
                                if (matrix !== undefined && segs !== undefined) {
                                    for (var k = 0; k < segs.length; k++) {
                                        var segId = segs[k];
                                        var seg = this._hwfParser.getSegmentPropertyManager().getFromID(segId);
                                        if (!!seg.assemblyNodeId) {
                                            var bodyInstanceNodeIds = [];
                                            this.collectBodyInstance(seg.assemblyNodeId, bodyInstanceNodeIds);
                                            for (var l = 0; l < bodyInstanceNodeIds.length; l++) {
                                                var curlocalMatrix = this._viewer.model.getNodeMatrix(bodyInstanceNodeIds[l]);
                                                var deltaMatrix = Communicator.Matrix.createFromArray(matrix);
                                                matrices.push([bodyInstanceNodeIds[l], Communicator.Matrix.multiply(curlocalMatrix, deltaMatrix)]); // The matrix to apply to the node seems to be a post multiply matrix
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        // setup view
                        var cadViewId = this._viewer.model.createCadView(rootId, this._captureNames[i], camera, pmiToShow, [], [], matrices, cuttingPlane, viewFrameMeshInstance);
                        //Activate the default view as soon as possible so the user can have a meaningful view as the model is loaded.
                        if (cadViewId !== null) {
                            if (this._captureNames[i] === "cad_default") {
                                this._viewer.model.activateCadView(cadViewId, 0);
                                defaultCaptureActivated = true;
                            }
                        }
                    }
                    return defaultCaptureActivated;
                };
                CaptureParser.prototype.collectBodyInstance = function (currNodeId, collectedNodesId) {
                    if (this._viewer.model.getNodeType(currNodeId) === Communicator.NodeType.BodyInstance)
                        collectedNodesId.push(currNodeId);
                    var children = this._viewer.model.getNodeChildren(currNodeId);
                    for (var i = 0; i < children.length; i++) {
                        this.collectBodyInstance(children[i], collectedNodesId);
                    }
                };
                CaptureParser.prototype.addFrame = function (captureTag, captureMesh) {
                    this._toFrameMeshInstanceData[captureTag] = captureMesh;
                };
                /*
                    Parse Camera and cutting planes
                */
                CaptureParser.prototype.parseCaptureDefinitions = function (captureStr) {
                    var parts = captureStr.split(";");
                    var adder;
                    if (this._hwfParser.getHwfFileVersionNumber() > 1.91)
                        adder = 5;
                    else
                        adder = 4;
                    for (var i = 0; i < parts.length; i += adder) {
                        var captureName = parts[i];
                        var cam = this.parseCamStr(parts[i + 1]);
                        var cuttingPlane = null;
                        var capTag = void 0;
                        var styleTags = void 0;
                        if (this._hwfParser.getHwfFileVersionNumber() > 1.91) {
                            cuttingPlane = this.parseCuttingPlaneStr(parts[i + 2]);
                            capTag = parts[i + 3];
                            styleTags = parts[i + 4].split("&");
                        }
                        else {
                            capTag = parts[i + 2];
                            styleTags = parts[i + 3].split("&");
                        }
                        this._captureNames.push(captureName);
                        this._toCaptureTag[captureName] = capTag;
                        if (cam !== null) {
                            this._toCamera[capTag] = cam;
                        }
                        if (cuttingPlane !== null) {
                            this._toCuttingPlane[capTag] = cuttingPlane;
                        }
                        this._toStyleTags[capTag] = styleTags;
                    }
                    return parts.length / adder;
                };
                /*
                    Capture style definition
                */
                CaptureParser.prototype.parseCaptureStyleDefinitions = function (captureStr) {
                    var parts = captureStr.split(";");
                    for (var i = 0; i < parts.length; i += 3) {
                        var styleTag = parts[i];
                        var vis = Internal.Visibility.deserialize(parts[i + 1]);
                        this._toStyleVis[styleTag] = vis;
                        var matrix = this.parseMatrix(parts[i + 2]);
                        if (matrix !== null) {
                            this._toStyleMatrix[styleTag] = matrix;
                        }
                    }
                };
                /*
                    Default capture
                */
                CaptureParser.prototype.parseDefaultCapture = function (captureStr) {
                    this._defaultCapture = captureStr;
                };
                CaptureParser.prototype.parseCaptureMarkup = function (defnsStr, segId) {
                    defnsStr;
                    segId;
                };
                CaptureParser.prototype.parseCaptureNonStyles = function (capTagsStr, segId) {
                    var capTags = capTagsStr.split(";");
                    for (var i = 0; i < capTags.length; ++i) {
                        var capTag = capTags[i];
                        if (!this._principleNonStyleSegs.hasOwnProperty(capTag)) {
                            this._principleNonStyleSegs[capTag] = [];
                        }
                        this._principleNonStyleSegs[capTag].push(segId);
                    }
                };
                CaptureParser.prototype.parseCaptureStyles = function (styleTagsStr, segId) {
                    var baseVis = Internal.Visibility.EMPTY();
                    var styleTags = styleTagsStr.split(";");
                    for (var i = 0; i < styleTags.length; ++i) {
                        var styleTag = styleTags[i];
                        if (!this._principleStyleSegs.hasOwnProperty(styleTag)) {
                            this._principleStyleSegs[styleTag] = [];
                        }
                        this._principleStyleSegs[styleTag].push(segId);
                        var vis = this._toStyleVis[styleTag];
                        if (vis !== undefined && !vis.empty()) {
                            var newBaseVis = void 0;
                            if (true) {
                                newBaseVis = vis.everythingOff()
                                    ? Internal.Visibility.EVERYTHING()
                                    : Internal.Visibility.EVERYTHING().not();
                            }
                            /*else {
                                newBaseVis = vis.not();
                            }*/
                            if (!baseVis.equals(newBaseVis) && !baseVis.empty()) {
                                throw new Communicator.CommunicatorError("Incompatible style visibilities");
                            }
                            baseVis = newBaseVis;
                        }
                    }
                    return baseVis;
                };
                /*
                    HWF camera paring
                */
                CaptureParser.prototype.parseCamStr = function (camStr) {
                    if (camStr === "?") {
                        return null;
                    }
                    var parts = camStr.split(" ");
                    var cam = new Communicator.Camera();
                    var point = Communicator.Point3.zero();
                    var i = 0;
                    point.x = Number(parts[i++]);
                    point.y = Number(parts[i++]);
                    point.z = Number(parts[i++]);
                    cam.setPosition(point);
                    point.x = Number(parts[i++]);
                    point.y = Number(parts[i++]);
                    point.z = Number(parts[i++]);
                    cam.setTarget(point);
                    point.x = Number(parts[i++]);
                    point.y = Number(parts[i++]);
                    point.z = Number(parts[i++]);
                    cam.setUp(point);
                    cam.setWidth(Number(parts[i++]));
                    cam.setHeight(Number(parts[i++]));
                    switch (parts[i++]) {
                        case "perspective":
                            cam.setProjection(Communicator.Projection.Perspective);
                            break;
                        default:
                        case "orthographic":
                            cam.setProjection(Communicator.Projection.Orthographic);
                            break;
                    }
                    return cam;
                };
                /*
                    HWF cutting plane parsing
                */
                CaptureParser.prototype.parseCuttingPlaneStr = function (cutStr) {
                    if (cutStr === "?") {
                        return null;
                    }
                    var parts = cutStr.split(" ");
                    var cutting = [];
                    cutting[0] = parseFloat(parts[0]);
                    cutting[1] = parseFloat(parts[1]);
                    cutting[2] = parseFloat(parts[2]);
                    cutting[3] = parseFloat(parts[3]);
                    return cutting;
                };
                /*
                    HWF matrix parsing
                */
                CaptureParser.prototype.parseMatrix = function (str) {
                    if (str === "?") {
                        return null;
                    }
                    var nums = str.split("_");
                    for (var i = 0; i < nums.length; ++i) {
                        nums[i] = Number(nums[i]);
                    }
                    return nums;
                };
                return CaptureParser;
            }());
            Internal.CaptureParser = CaptureParser;
        })(Internal = HWF.Internal || (HWF.Internal = {}));
    })(HWF = Communicator.HWF || (Communicator.HWF = {}));
})(Communicator || (Communicator = {}));
var Communicator;
(function (Communicator) {
    var HWF;
    (function (HWF) {
        var Internal;
        (function (Internal) {
            var Data;
            (function (Data) {
                function generateLineDataFromPointsAndIndices(points, indices, lineColor, transparency, meshData, measurementBitsOnEdge) {
                    var _genVertexColor = function (polyline) {
                        var rgba32;
                        if (lineColor !== null) {
                            rgba32 = new Uint8Array((polyline.length / 3) * 4);
                            for (var i = 0; i < polyline.length / 3; i++) {
                                var outputIndex = 4 * i;
                                rgba32[outputIndex] = lineColor.r;
                                rgba32[outputIndex + 1] = lineColor.g;
                                rgba32[outputIndex + 2] = lineColor.b;
                                rgba32[outputIndex + 3] = transparency;
                            }
                        }
                        return rgba32;
                    };
                    var _addPointToPolyline = function (pointNum, polyline) {
                        var index = 3 * pointNum;
                        polyline.push(points[index]);
                        polyline.push(points[index + 1]);
                        polyline.push(points[index + 2]);
                    };
                    var _registerPolyline = function (polyline, vertexColor, meshData) {
                        var measurementBits = 0;
                        if (measurementBitsOnEdge !== undefined && measurementBitsOnEdge[lineIndex] !== undefined)
                            measurementBits = measurementBitsOnEdge[lineIndex];
                        meshData.addPolyline(polyline, vertexColor, measurementBits);
                        lineIndex++;
                    };
                    var lineIndex = 0;
                    var segmentCount = indices.length / 2;
                    var prevPt1 = -1;
                    var polyline;
                    for (var i = 0; i < segmentCount; i++) {
                        var pt0 = indices[i * 2];
                        var pt1 = indices[i * 2 + 1];
                        //this is the case of indices like: 0,1 1,2 2,3 ....
                        if (pt0 === prevPt1) {
                            _addPointToPolyline(pt1, polyline);
                        }
                        else {
                            if (polyline)
                                _registerPolyline(polyline, _genVertexColor(polyline), meshData);
                            polyline = [];
                            _addPointToPolyline(pt0, polyline);
                            _addPointToPolyline(pt1, polyline);
                        }
                        prevPt1 = pt1;
                    }
                    if (polyline)
                        _registerPolyline(polyline, _genVertexColor(polyline), meshData);
                }
                Data.generateLineDataFromPointsAndIndices = generateLineDataFromPointsAndIndices;
                function generateFaceData(points, indices, normals, vertexColors, uvs, faceColor, transparency, meshData, measurementBitsOnFace) {
                    var _unindexPoint = function (pointNum, outputNum, vertexList, normalList, rgbaList, uvList) {
                        var index = 3 * pointNum;
                        vertexList.push(points[index], points[index + 1], points[index + 2]);
                        if (normals !== undefined) {
                            normalList.push(normals[index], normals[index + 1], normals[index + 2]);
                        }
                        else {
                            normalList.push(0, 1, 0);
                        }
                        var uvIndex = 2 * pointNum;
                        if (uvs) {
                            uvList.push(uvs[uvIndex], uvs[uvIndex + 1]);
                        }
                        var rgbaInputIndex = 3 * pointNum;
                        var rgbaOutputIndex = 4 * outputNum;
                        if (vertexColors !== undefined) {
                            rgbaList[rgbaOutputIndex] = vertexColors[rgbaInputIndex] * 255;
                            rgbaList[rgbaOutputIndex + 1] = vertexColors[rgbaInputIndex + 1] * 255;
                            rgbaList[rgbaOutputIndex + 2] = vertexColors[rgbaInputIndex + 2] * 255;
                            rgbaList[rgbaOutputIndex + 3] = transparency;
                        }
                        else {
                            rgbaList[rgbaOutputIndex] = faceColor.r;
                            rgbaList[rgbaOutputIndex + 1] = faceColor.g;
                            rgbaList[rgbaOutputIndex + 2] = faceColor.b;
                            rgbaList[rgbaOutputIndex + 3] = transparency;
                        }
                    };
                    for (var i = 0; i < indices.length; i++) {
                        var vertexList = [];
                        var normalList = [];
                        var uvList = [];
                        var rgba32List = void 0;
                        rgba32List = new Uint8Array(4 * indices[i].length);
                        for (var j = 0; j < indices[i].length; j++) {
                            _unindexPoint(indices[i][j], j, vertexList, normalList, rgba32List, uvList);
                        }
                        var measurementBits = 0;
                        if (measurementBitsOnFace !== undefined && measurementBitsOnFace[i] !== undefined)
                            measurementBits = measurementBitsOnFace[i];
                        meshData.addFaces(vertexList, normalList.length > 0 ? normalList : undefined, rgba32List !== undefined && rgba32List.length > 0 ? rgba32List : undefined, uvList.length > 0 ? uvList : undefined, measurementBits);
                    }
                }
                Data.generateFaceData = generateFaceData;
            })(Data = Internal.Data || (Internal.Data = {}));
        })(Internal = HWF.Internal || (HWF.Internal = {}));
    })(HWF = Communicator.HWF || (Communicator.HWF = {}));
})(Communicator || (Communicator = {}));
var Communicator;
(function (Communicator) {
    var HWF;
    (function (HWF) {
        var Internal;
        (function (Internal) {
            var StreamOpcode;
            (function (StreamOpcode) {
                StreamOpcode[StreamOpcode["DoneValue1"] = 0] = "DoneValue1";
                StreamOpcode[StreamOpcode["DoneValue2"] = 1] = "DoneValue2";
                StreamOpcode[StreamOpcode["ColorMap"] = 12] = "ColorMap";
                StreamOpcode[StreamOpcode["PolyPolypoint"] = 16] = "PolyPolypoint";
                StreamOpcode[StreamOpcode["UnicodeOptions"] = 22] = "UnicodeOptions";
                StreamOpcode[StreamOpcode["Matrix"] = 37] = "Matrix";
                StreamOpcode[StreamOpcode["Color"] = 34] = "Color";
                StreamOpcode[StreamOpcode["OpenSegment"] = 40] = "OpenSegment";
                StreamOpcode[StreamOpcode["CloseSegment"] = 41] = "CloseSegment";
                StreamOpcode[StreamOpcode["Comment"] = 59] = "Comment";
                StreamOpcode[StreamOpcode["BoundingType1"] = 66] = "BoundingType1";
                StreamOpcode[StreamOpcode["Heuristics"] = 72] = "Heuristics";
                StreamOpcode[StreamOpcode["FileInfo"] = 73] = "FileInfo";
                StreamOpcode[StreamOpcode["Polypoint"] = 76] = "Polypoint";
                StreamOpcode[StreamOpcode["ClipRegion"] = 79] = "ClipRegion";
                StreamOpcode[StreamOpcode["Shell"] = 83] = "Shell";
                StreamOpcode[StreamOpcode["UserOptions"] = 85] = "UserOptions";
                StreamOpcode[StreamOpcode["Visibility"] = 86] = "Visibility";
                StreamOpcode[StreamOpcode["Compression"] = 90] = "Compression";
                StreamOpcode[StreamOpcode["BoundingType2"] = 98] = "BoundingType2";
                StreamOpcode[StreamOpcode["Image"] = 105] = "Image";
                StreamOpcode[StreamOpcode["Line"] = 108] = "Line";
                StreamOpcode[StreamOpcode["UserIndexData"] = 109] = "UserIndexData";
                StreamOpcode[StreamOpcode["Tag"] = 113] = "Tag";
                StreamOpcode[StreamOpcode["Reference"] = 114] = "Reference";
                StreamOpcode[StreamOpcode["Texture"] = 116] = "Texture";
                StreamOpcode[StreamOpcode["Camera"] = 125] = "Camera";
                StreamOpcode[StreamOpcode["ColorRgb"] = 126] = "ColorRgb";
                StreamOpcode[StreamOpcode["UserValue"] = 118] = "UserValue";
            })(StreamOpcode = Internal.StreamOpcode || (Internal.StreamOpcode = {}));
            var StreamTK;
            (function (StreamTK) {
                StreamTK[StreamTK["CameraOrtho"] = 0] = "CameraOrtho";
                StreamTK[StreamTK["CameraPersp"] = 1] = "CameraPersp";
                StreamTK[StreamTK["CommentEnd"] = 10] = "CommentEnd";
            })(StreamTK = Internal.StreamTK || (Internal.StreamTK = {}));
            var StreamTKHeuristic;
            (function (StreamTKHeuristic) {
                StreamTKHeuristic[StreamTKHeuristic["Extended"] = 32768] = "Extended";
                StreamTKHeuristic[StreamTKHeuristic["Extras"] = 12] = "Extras";
                StreamTKHeuristic[StreamTKHeuristic["ExtendedMask"] = 4294901760] = "ExtendedMask";
                StreamTKHeuristic[StreamTKHeuristic["ExtendedShift"] = 16] = "ExtendedShift";
            })(StreamTKHeuristic = Internal.StreamTKHeuristic || (Internal.StreamTKHeuristic = {}));
            var UserIndexData;
            (function (UserIndexData) {
                UserIndexData[UserIndexData["Bounding"] = 1] = "Bounding";
                UserIndexData[UserIndexData["BrepInfo"] = 22] = "BrepInfo";
                UserIndexData[UserIndexData["PmiData"] = 23] = "PmiData";
                UserIndexData[UserIndexData["Unused1"] = 24] = "Unused1";
                UserIndexData[UserIndexData["Unused2"] = 25] = "Unused2";
                UserIndexData[UserIndexData["MapId"] = 26] = "MapId";
                UserIndexData[UserIndexData["PrototypeMap"] = 27] = "PrototypeMap";
                UserIndexData[UserIndexData["SecurityMark"] = 1802331507] = "SecurityMark";
            })(UserIndexData = Internal.UserIndexData || (Internal.UserIndexData = {}));
            var BrepType;
            (function (BrepType) {
                BrepType[BrepType["Cylinder"] = 0] = "Cylinder";
                BrepType[BrepType["Cone"] = 1] = "Cone";
                BrepType[BrepType["Plane"] = 2] = "Plane";
                BrepType[BrepType["Circle"] = 3] = "Circle";
            })(BrepType = Internal.BrepType || (Internal.BrepType = {}));
            var TKSH;
            (function (TKSH) {
                TKSH[TKSH["EXPANDED"] = 128] = "EXPANDED";
                TKSH[TKSH["FIRSTPASS"] = 16] = "FIRSTPASS";
                TKSH[TKSH["HAS_OPTIONALS"] = 8] = "HAS_OPTIONALS";
            })(TKSH = Internal.TKSH || (Internal.TKSH = {}));
            var TKO_Image;
            (function (TKO_Image) {
                TKO_Image[TKO_Image["TKO_Image_RGB"] = 2] = "TKO_Image_RGB";
                TKO_Image[TKO_Image["TKO_Image_RGBA"] = 3] = "TKO_Image_RGBA";
                TKO_Image[TKO_Image["TKO_Image_JPEG"] = 7] = "TKO_Image_JPEG";
                TKO_Image[TKO_Image["TKO_Image_PNG"] = 12] = "TKO_Image_PNG";
                TKO_Image[TKO_Image["Format_Mask"] = 15] = "Format_Mask";
                TKO_Image[TKO_Image["Options_Mask"] = 4294967280] = "Options_Mask";
                TKO_Image[TKO_Image["Is_Named"] = 128] = "Is_Named";
            })(TKO_Image = Internal.TKO_Image || (Internal.TKO_Image = {}));
            var OPT;
            (function (OPT) {
                OPT[OPT["TERMINATE"] = 0] = "TERMINATE";
                OPT[OPT["ALL_NORMALS_POLAR"] = 19] = "ALL_NORMALS_POLAR";
                OPT[OPT["FACE_REGIONS"] = 51] = "FACE_REGIONS";
                OPT[OPT["ALL_VFINDICES"] = 11] = "ALL_VFINDICES";
                OPT[OPT["ALL_PARAMETERS"] = 28] = "ALL_PARAMETERS";
                OPT[OPT["ALL_VFCOLORS"] = 5] = "ALL_VFCOLORS";
            })(OPT = Internal.OPT || (Internal.OPT = {}));
            var TKO_Channel;
            (function (TKO_Channel) {
                TKO_Channel[TKO_Channel["Diffuse"] = 0] = "Diffuse";
                TKO_Channel[TKO_Channel["Transmission"] = 3] = "Transmission";
            })(TKO_Channel = Internal.TKO_Channel || (Internal.TKO_Channel = {}));
            var TKO_Compression;
            (function (TKO_Compression) {
                TKO_Compression[TKO_Compression["None"] = 0] = "None";
                TKO_Compression[TKO_Compression["PNG"] = 5] = "PNG";
            })(TKO_Compression = Internal.TKO_Compression || (Internal.TKO_Compression = {}));
            var TKO_Geo;
            (function (TKO_Geo) {
                TKO_Geo[TKO_Geo["Extended"] = 128] = "Extended";
                TKO_Geo[TKO_Geo["Extended_Colors"] = 32768] = "Extended_Colors";
                TKO_Geo[TKO_Geo["Extended2"] = 8388608] = "Extended2";
                TKO_Geo[TKO_Geo["Extended_Shift"] = 8] = "Extended_Shift";
                TKO_Geo[TKO_Geo["Extended_Colors_Shift"] = 16] = "Extended_Colors_Shift";
                TKO_Geo[TKO_Geo["Extended2_Shift"] = 24] = "Extended2_Shift";
            })(TKO_Geo = Internal.TKO_Geo || (Internal.TKO_Geo = {}));
            var TKO_Texture;
            (function (TKO_Texture) {
                TKO_Texture[TKO_Texture["Param_Source"] = 1] = "Param_Source";
                TKO_Texture[TKO_Texture["Tiling"] = 2] = "Tiling";
                TKO_Texture[TKO_Texture["Extended"] = 32768] = "Extended";
                TKO_Texture[TKO_Texture["Extended_Shift"] = 16] = "Extended_Shift";
                TKO_Texture[TKO_Texture["Interpolation"] = 4] = "Interpolation";
                TKO_Texture[TKO_Texture["Decimation"] = 8] = "Decimation";
                TKO_Texture[TKO_Texture["Param_Offset"] = 262144] = "Param_Offset";
            })(TKO_Texture = Internal.TKO_Texture || (Internal.TKO_Texture = {}));
            var TKPP;
            (function (TKPP) {
                TKPP[TKPP["COMPRESSED"] = 1] = "COMPRESSED";
                TKPP[TKPP["HAS_EXPLICIT_PRIMITIVE_COUNT"] = 2] = "HAS_EXPLICIT_PRIMITIVE_COUNT";
                TKPP[TKPP["ONE_PRIMITIVE_ONLY"] = 4] = "ONE_PRIMITIVE_ONLY";
                TKPP[TKPP["EXPLICIT_PRIMITIVE_MASK"] = 6] = "EXPLICIT_PRIMITIVE_MASK";
                TKPP[TKPP["GLOBAL_QUANTIZATION"] = 8] = "GLOBAL_QUANTIZATION";
                TKPP[TKPP["ZERO_X"] = 256] = "ZERO_X";
                TKPP[TKPP["SAME_X"] = 512] = "SAME_X";
                TKPP[TKPP["PER_PRIMITIVE_X"] = 768] = "PER_PRIMITIVE_X";
                TKPP[TKPP["ZERO_Y"] = 1024] = "ZERO_Y";
                TKPP[TKPP["SAME_Y"] = 2048] = "SAME_Y";
                TKPP[TKPP["PER_PRIMITIVE_Y"] = 3072] = "PER_PRIMITIVE_Y";
                TKPP[TKPP["ZERO_Z"] = 4096] = "ZERO_Z";
                TKPP[TKPP["SAME_Z"] = 8192] = "SAME_Z";
                TKPP[TKPP["PER_PRIMITIVE_Z"] = 12288] = "PER_PRIMITIVE_Z";
                TKPP[TKPP["EXPANDED"] = 32768] = "EXPANDED";
                TKPP[TKPP["X_2D_MASK"] = 768] = "X_2D_MASK";
                TKPP[TKPP["Y_2D_MASK"] = 3072] = "Y_2D_MASK";
                TKPP[TKPP["Z_2D_MASK"] = 12288] = "Z_2D_MASK";
                TKPP[TKPP["ANY_2D_MASK"] = 16128] = "ANY_2D_MASK";
            })(TKPP = Internal.TKPP || (Internal.TKPP = {}));
            var GeometryElementType;
            (function (GeometryElementType) {
                GeometryElementType[GeometryElementType["UNDEFINED"] = 0] = "UNDEFINED";
                GeometryElementType[GeometryElementType["STRAIGHTLINE"] = 1] = "STRAIGHTLINE";
                GeometryElementType[GeometryElementType["CIRCLE"] = 2] = "CIRCLE";
                GeometryElementType[GeometryElementType["CYLINDER"] = 3] = "CYLINDER";
                GeometryElementType[GeometryElementType["PLANE"] = 4] = "PLANE";
                GeometryElementType[GeometryElementType["CONE"] = 5] = "CONE";
                GeometryElementType[GeometryElementType["OTHERLINE"] = 6] = "OTHERLINE";
            })(GeometryElementType = Internal.GeometryElementType || (Internal.GeometryElementType = {}));
            var RegionInfo = /** @class */ (function () {
                function RegionInfo(item, region) {
                    this.item = item;
                    this.region = region;
                }
                return RegionInfo;
            }());
            Internal.RegionInfo = RegionInfo;
            var PmiInfo = /** @class */ (function () {
                function PmiInfo() {
                    this.refsToBrep = [];
                    this.pmiType = Communicator.PmiType.Unknown;
                    this.pmiSubtype = Communicator.PmiSubType.Unknown;
                }
                return PmiInfo;
            }());
            Internal.PmiInfo = PmiInfo;
        })(Internal = HWF.Internal || (HWF.Internal = {}));
    })(HWF = Communicator.HWF || (Communicator.HWF = {}));
})(Communicator || (Communicator = {}));
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var Communicator;
(function (Communicator) {
    var HWF;
    (function (HWF) {
        var Internal;
        (function (Internal) {
            var HWFParser = /** @class */ (function () {
                //private _hwfRootNode?: NodeId;
                function HWFParser(viewer) {
                    this._colorMap = [];
                    this._currentBytePosition = 0;
                    this._currentTag = 0;
                    this._tagBase = 0;
                    this._currentlyParsingMarkup = false;
                    this._currentlyReadingFrames = false;
                    this._pendingMeshData = [];
                    this._pendingInstanceData = [];
                    this._makingMeshes = false;
                    /* This maps include tags to Communicator Mesh IDs. This is used to instance the correct mesh when parsing reference opcodes */
                    this._modelTagToMeshData = {};
                    /* This is true when we are parsing guido's zzz_new_includes segment */
                    this._currentlyParsingIncludes = false;
                    /* This count is zero when when we are current at an include segment, it is greater than zero when we are parsing included geometry segments */
                    this._currentIncludeDepth = 0;
                    // Needed when a PMI refers to a brep body(representation item)
                    this._nodeAdditionalIdToSegmentProps = {};
                    this._modelUnits = 1.0;
                    this._faceWinding = Communicator.FaceWinding.CounterClockwise; // XXX: This should probably be localized on a segment level (with inheritance) and not be a global parser value.
                    this._segmentPropertyManager = new Internal.SegmentPropertyManager();
                    this._InstancedDataManager = new Internal.InstancedDataManager();
                    this._imageResources = {};
                    this._textureImageNames = {};
                    this._viewer = viewer;
                    this._captureParser = new Internal.CaptureParser(viewer, this);
                }
                HWFParser.prototype._log = function (s) {
                    console.log(s);
                };
                HWFParser.prototype.getHwfFileVersion = function () {
                    return this._hwfFileVersion;
                };
                HWFParser.prototype.getHwfFileVersionNumber = function () {
                    return parseFloat(this._hwfFileVersion);
                };
                HWFParser.prototype.getOriginalFilename = function () {
                    return this._originalFilename;
                };
                HWFParser.prototype.getModelUnits = function () {
                    return this._modelUnits;
                };
                HWFParser.prototype.getSegmentPropertyManager = function () {
                    return this._segmentPropertyManager;
                };
                HWFParser.prototype.parse = function (data) {
                    this._data = new Uint8Array(data);
                    this._dataView = new DataView(this._data.buffer);
                    this._currentBytePosition = 0;
                    return this._parseChunk();
                };
                HWFParser.prototype._parseChunk = function () {
                    var _this = this;
                    var chunkStartTime = new Date().getMilliseconds();
                    var currentParseTime = chunkStartTime;
                    while (!this._makingMeshes) {
                        var opcode = this._data[this._currentBytePosition++];
                        /*  if parsing an opcode results in an operation that triggers a promise, it may be necessary to let that operation execute before continuing parsing the file.
                            e.g. creating mesh data */
                        switch (opcode) {
                            case Internal.StreamOpcode.Comment:
                                this._parseComment();
                                break;
                            case Internal.StreamOpcode.FileInfo:
                                this._parseFileInfo();
                                break;
                            case Internal.StreamOpcode.BoundingType1:
                            case Internal.StreamOpcode.BoundingType2:
                                this._parseBounding();
                                break;
                            case Internal.StreamOpcode.Camera:
                                this._parseCamera();
                                break;
                            case Internal.StreamOpcode.UserValue:
                                this._parseUserValue();
                                break;
                            case Internal.StreamOpcode.UserIndexData:
                                this._parseUserIndexData();
                                break;
                            case Internal.StreamOpcode.UserOptions:
                                this._parseUserOptions();
                                break;
                            case Internal.StreamOpcode.UnicodeOptions:
                                this._parseUnicodeOptions();
                                break;
                            case Internal.StreamOpcode.Heuristics:
                                this._parseHeuristics();
                                break;
                            case Internal.StreamOpcode.Visibility:
                                this._parseVisibility();
                                break;
                            case Internal.StreamOpcode.OpenSegment:
                                this._parseOpenSegment();
                                break;
                            case Internal.StreamOpcode.Reference:
                                this._parseReference();
                                break;
                            case Internal.StreamOpcode.Polypoint:
                                this._parsePolyPoint();
                                break;
                            case Internal.StreamOpcode.Line:
                                this._parseLine();
                                break;
                            case Internal.StreamOpcode.CloseSegment:
                                this._parseCloseSegment();
                                break;
                            case Internal.StreamOpcode.Shell:
                                this._parseShell();
                                break;
                            case Internal.StreamOpcode.Matrix:
                                this._parseMatrix();
                                break;
                            case Internal.StreamOpcode.Tag:
                                this._parseTag();
                                break;
                            case Internal.StreamOpcode.ColorRgb:
                                this._parseColorRgb();
                                break;
                            case Internal.StreamOpcode.PolyPolypoint:
                                this._parsePolyPolyPoint();
                                break;
                            case Internal.StreamOpcode.Color:
                                this._parseColor();
                                break;
                            case Internal.StreamOpcode.Texture:
                                this._parseTexture();
                                break;
                            case Internal.StreamOpcode.Image:
                                this._parseImage();
                                break;
                            case Internal.StreamOpcode.ColorMap:
                                this._parseColorMap();
                                break;
                            case Internal.StreamOpcode.Compression:
                                this._parseCompression();
                                break;
                            case Internal.StreamOpcode.ClipRegion:
                                this._parseClipRegion();
                                break;
                            case Internal.StreamOpcode.DoneValue1:
                            case Internal.StreamOpcode.DoneValue2:
                                // Export views to model structure
                                return Promise.all(this._pendingMeshData).then(function () {
                                    return Promise.all(_this._pendingInstanceData).then(function () {
                                        var defaultCaptureActivated = _this._captureParser.exportToModelStructure();
                                        if (!defaultCaptureActivated && _this._defaultCamera !== undefined) {
                                            // In case there wasn't the default capture activated, then we use the default camera
                                            _this._viewer.view.setCamera(_this._defaultCamera, 0);
                                        }
                                        _this._parsingDone();
                                    });
                                });
                        }
                        currentParseTime = new Date().getMilliseconds();
                    }
                    return Communicator.Internal.sleep(0).then(function () {
                        return _this._parseChunk();
                    });
                };
                HWFParser.prototype._parseComment = function () {
                    // comments not used in HWF, simply consume data and move on
                    while (this._data[this._currentBytePosition++] !== Internal.StreamTK.CommentEnd) {
                        // do nothing
                    }
                };
                HWFParser.prototype._parseFileInfo = function () {
                    // file info flags are not used by HWF
                    this._currentBytePosition += 4;
                };
                HWFParser.prototype._parseBounding = function () {
                    var boundingType = this._data[this._currentBytePosition++];
                    // this type stores bounding as 6 floats min, max
                    if (boundingType == 0) {
                        this._currentBytePosition += 24;
                    }
                    else {
                        this._currentBytePosition += 16;
                    }
                };
                HWFParser.prototype._parseCamera = function () {
                    var point = Communicator.Point3.zero();
                    this._defaultCamera = new Communicator.Camera();
                    var projection = this._data[this._currentBytePosition++];
                    this._defaultCamera.setProjection(projection === Internal.StreamTK.CameraOrtho ? Communicator.Projection.Orthographic : Communicator.Projection.Perspective);
                    this._readPoint3(point);
                    this._defaultCamera.setPosition(point);
                    this._readPoint3(point);
                    this._defaultCamera.setTarget(point);
                    this._readPoint3(point);
                    this._defaultCamera.setUp(point);
                    this._defaultCamera.setWidth(this._readFloat());
                    this._defaultCamera.setHeight(this._readFloat());
                    // consume view name which is not used in HWF
                    var viewNameLength = this._data[this._currentBytePosition++];
                    this._currentBytePosition += viewNameLength;
                    // TODO: Set Camera?
                };
                HWFParser.prototype._parseUserValue = function () {
                    // in HC2015 this indicated "Drawing mode"
                    this._readUnsignedInt();
                };
                HWFParser.prototype.getHwfRootNode = function () {
                    return this._viewer.model.getAbsoluteRootNode();
                };
                HWFParser.prototype._parseUserIndexData = function () {
                    var count = this._readInt();
                    var segment = this._segmentPropertyManager.current();
                    var currentValue = 0;
                    var index = 0;
                    var size = 0;
                    var assemblyTreeName = "(null)";
                    var forcedNodeId;
                    var geometryText;
                    for (var i = 0; i < count; i++) {
                        if (currentValue === 0) {
                            index = this._readInt();
                            currentValue++;
                        }
                        if (currentValue === 1) {
                            size = this._readInt();
                            currentValue++;
                        }
                        if (currentValue === 2) {
                            if (index === Internal.UserIndexData.Bounding) {
                                this._readString(size);
                            }
                            else if (index === Internal.UserIndexData.PmiData) {
                                this._parsePmiData(size);
                            }
                            else if (index === Internal.UserIndexData.Unused1 || index === Internal.UserIndexData.Unused2) {
                                this._currentBytePosition += size;
                            }
                            else if (index === Internal.UserIndexData.MapId) {
                                forcedNodeId = this._readUnsignedInt();
                                //this._getOpenSegment().mapIds.push(mapId);
                            }
                            else if (index === Internal.UserIndexData.PrototypeMap) {
                                this._parsePrototypeMap();
                            }
                            else if (index === Internal.UserIndexData.BrepInfo) {
                                geometryText = this._readString(size);
                            }
                            else if (index === Internal.UserIndexData.SecurityMark) {
                                this._readString(size);
                            }
                            else {
                                assemblyTreeName = Internal.Util.Utf8.decode(this._readString(size));
                                assemblyTreeName = assemblyTreeName.replace(/"/gi, ' ');
                                this._log("parse assembly name: " + assemblyTreeName);
                                segment.assemblyTreeName = assemblyTreeName;
                            }
                            currentValue = 0;
                        }
                    }
                    var model = this._viewer.model;
                    // Creates part def and rep item nodes
                    if (segment.segmentName !== undefined) {
                        if (segment.segmentName.indexOf("part") !== -1) {
                            var parentId = segment.resolveParentNodeId(model, Communicator.NodeType.AssemblyNode);
                            segment.assemblyNodeId = model.createPart(forcedNodeId); // TODO: don't create a new part if its used multiple times
                            model.setPart(parentId, segment.assemblyNodeId);
                            var additionalId = parseInt(segment.segmentName.split("part ")[1], 10);
                            if (!isNaN(additionalId))
                                this._nodeAdditionalIdToSegmentProps[additionalId] = segment;
                        }
                        else if (segment.segmentName.indexOf("representation item") !== -1) {
                            var parentId = segment.resolveParentNodeId(model, Communicator.NodeType.Part);
                            segment.assemblyNodeId = model.createAndAddRepresentationItem(parentId, forcedNodeId); // TODO: manage the case of nested RI
                            var additionalId = parseInt(segment.segmentName.split("representation item ")[1], 10);
                            if (!isNaN(additionalId))
                                this._nodeAdditionalIdToSegmentProps[additionalId] = segment;
                        }
                        else if (segment.segmentName.indexOf("product occurrence") !== -1) {
                            var parentId = segment.resolveParentNodeId(model, Communicator.NodeType.AssemblyNode);
                            segment.assemblyNodeId = model.createNode(parentId, assemblyTreeName, forcedNodeId);
                            var additionalId = parseInt(segment.segmentName.split("product occurrence ")[1], 10);
                            if (!isNaN(additionalId))
                                this._nodeAdditionalIdToSegmentProps[additionalId] = segment;
                        }
                        /*else
                            this._log(`Unknown segment: ${segment.segmentName}`);*/
                    }
                    // Now that we build assembly tree nodes, we ca n now parse and register brep measurement info
                    if (geometryText !== undefined)
                        this._parseBrepInfo(geometryText);
                    // Now that the assembly tree node is created we can now register saved attributes/properties
                    if (segment.userOptions !== undefined && segment.userOptions.length > 0) {
                        for (var i = 0; i < segment.userOptions.length; i++) {
                            if (segment.userOptions[i].indexOf("brep_properties") === -1 && segment.userOptions[i].indexOf("hx3d_type") === -1) {
                                var property = segment.userOptions[i].replace(/\`+/g, '');
                                property = property.replace(/\\/g, "sLaSh");
                                var splitTest = property.split('=');
                                var nodeId = !!segment.assemblyNodeId ? segment.assemblyNodeId : this.getHwfRootNode();
                                model.addPropertyToNode(nodeId, splitTest[0], splitTest[1]);
                                /*if (userOptionsArray[j].indexOf("globalid") !== -1) {
                                    sp.cadId = (userOptionsArray[j].split("="))[1];
                                    var uniqueId = this.open_segments[this.segmentLevel];
                                    this.idConverter.insertCadIdMapping(sp.cadId, uniqueId);
                                }*/
                            }
                        }
                    }
                    if (!!segment.brepProperties && !!segment.assemblyNodeId) {
                        if (model.getNodeType(segment.assemblyNodeId) === Communicator.NodeType.Body) {
                            var props = segment.brepProperties.split(" ");
                            var gravityCenter = void 0;
                            var surfaceArea = void 0;
                            var volume = void 0;
                            if (props[2] !== "-1.#IND00")
                                gravityCenter = new Communicator.Point3(parseFloat(props[2]), parseFloat(props[3]), parseFloat(props[4]));
                            else {
                                gravityCenter = new Communicator.Point3(0, 0, 0);
                            }
                            surfaceArea = parseFloat(props[0]);
                            volume = parseFloat(props[1]);
                            model.setPhysicalProperties(segment.assemblyNodeId, gravityCenter, surfaceArea, volume);
                        }
                    }
                };
                HWFParser.prototype._parseUserOptions = function () {
                    var openSegment = this._segmentPropertyManager.current();
                    var length = this._readUnsignedShort();
                    var userOptionsString = this._readString(length);
                    if (userOptionsString.indexOf("lights_off") >= 0) {
                        //openSegment.ignoreLighting = true;
                    }
                    // Store properties/attributes/physical-properties to create them into assembly tree after, when assembly tree node will be created
                    openSegment.userOptions = this._parseUserOptionsString(userOptionsString);
                    openSegment.brepProperties = Internal.Util.extractUserOpt(userOptionsString, "brep_properties");
                    // markup segments will have PMI info that we need to extract
                    if (this._currentlyParsingMarkup) {
                        var pmiType = Internal.Util.extractUserOpt(userOptionsString, "pmi_type");
                        if (pmiType && openSegment) {
                            openSegment.pmiInfo = new Internal.PmiInfo();
                            if (pmiType === "dimension") {
                                openSegment.pmiInfo.pmiType = Communicator.PmiType.Dimension;
                                var pmiSubType = Internal.Util.extractUserOpt(userOptionsString, "dimension_subtype");
                                if (pmiSubType === "distance")
                                    openSegment.pmiInfo.pmiSubtype = Communicator.PmiSubType.DimensionDistance;
                                else if (pmiSubType === "distanceoffset")
                                    openSegment.pmiInfo.pmiSubtype = Communicator.PmiSubType.DimensionDistanceOffset;
                                else if (pmiSubType === "distancecumulate")
                                    openSegment.pmiInfo.pmiSubtype = Communicator.PmiSubType.DimensionDistanceCumulate;
                                else if (pmiSubType === "chamfer")
                                    openSegment.pmiInfo.pmiSubtype = Communicator.PmiSubType.DimensionChamfer;
                                else if (pmiSubType === "slope")
                                    openSegment.pmiInfo.pmiSubtype = Communicator.PmiSubType.DimensionSlope;
                                else if (pmiSubType === "ordinate")
                                    openSegment.pmiInfo.pmiSubtype = Communicator.PmiSubType.DimensionOrdinate;
                                else if (pmiSubType === "radius")
                                    openSegment.pmiInfo.pmiSubtype = Communicator.PmiSubType.DimensionRadius;
                                else if (pmiSubType === "radiustangent")
                                    openSegment.pmiInfo.pmiSubtype = Communicator.PmiSubType.DimensionRadiusTangent;
                                else if (pmiSubType === "radiuscylinder")
                                    openSegment.pmiInfo.pmiSubtype = Communicator.PmiSubType.DimensionRadiusCylinder;
                                else if (pmiSubType === "radiusedge")
                                    openSegment.pmiInfo.pmiSubtype = Communicator.PmiSubType.DimensionRadiusEdge;
                                else if (pmiSubType === "diameter")
                                    openSegment.pmiInfo.pmiSubtype = Communicator.PmiSubType.DimensionDiameter;
                                else if (pmiSubType === "diametertangent")
                                    openSegment.pmiInfo.pmiSubtype = Communicator.PmiSubType.DimensionDiameterTangent;
                                else if (pmiSubType === "diametercylinder")
                                    openSegment.pmiInfo.pmiSubtype = Communicator.PmiSubType.DimensionDiameterCylinder;
                                else if (pmiSubType === "diameteredge")
                                    openSegment.pmiInfo.pmiSubtype = Communicator.PmiSubType.DimensionDiameterEdge;
                                else if (pmiSubType === "diametercone")
                                    openSegment.pmiInfo.pmiSubtype = Communicator.PmiSubType.DimensionDiameterCone;
                                else if (pmiSubType === "length")
                                    openSegment.pmiInfo.pmiSubtype = Communicator.PmiSubType.DimensionLength;
                                else if (pmiSubType === "lengthcurvilinear")
                                    openSegment.pmiInfo.pmiSubtype = Communicator.PmiSubType.DimensionLengthCurvilinear;
                                else if (pmiSubType === "lengthcircular")
                                    openSegment.pmiInfo.pmiSubtype = Communicator.PmiSubType.DimensionLengthCircular;
                                else if (pmiSubType === "angle")
                                    openSegment.pmiInfo.pmiSubtype = Communicator.PmiSubType.DimensionAngle;
                            }
                            else if (pmiType === "note")
                                openSegment.pmiInfo.pmiType = Communicator.PmiType.Text;
                            else if (pmiType === "arrow")
                                openSegment.pmiInfo.pmiType = Communicator.PmiType.Arrow;
                            else if (pmiType === "balloon")
                                openSegment.pmiInfo.pmiType = Communicator.PmiType.Balloon;
                            else if (pmiType === "balloon")
                                openSegment.pmiInfo.pmiType = Communicator.PmiType.Balloon;
                            else if (pmiType === "balloon")
                                openSegment.pmiInfo.pmiType = Communicator.PmiType.Balloon;
                            else if (pmiType === "circlecenter")
                                openSegment.pmiInfo.pmiType = Communicator.PmiType.CircleCenter;
                            else if (pmiType === "coordinate")
                                openSegment.pmiInfo.pmiType = Communicator.PmiType.Coordinate;
                            else if (pmiType === "datum") {
                                openSegment.pmiInfo.pmiType = Communicator.PmiType.Datum;
                                var pmiSubType = Internal.Util.extractUserOpt(userOptionsString, "datum_type");
                                if (pmiSubType === "identifier")
                                    openSegment.pmiInfo.pmiSubtype = Communicator.PmiSubType.DatumIdent;
                                else if (pmiSubType === "target")
                                    openSegment.pmiInfo.pmiSubtype = Communicator.PmiSubType.DatumTarget;
                            }
                            else if (pmiType === "fastener")
                                openSegment.pmiInfo.pmiType = Communicator.PmiType.Fastener;
                            else if (pmiType === "fcf") {
                                openSegment.pmiInfo.pmiType = Communicator.PmiType.Gdt;
                                openSegment.pmiInfo.pmiSubtype = Communicator.PmiSubType.GdtFcf;
                            }
                            else if (pmiType === "gdt") {
                                openSegment.pmiInfo.pmiType = Communicator.PmiType.Gdt;
                                var pmiSubType = Internal.Util.extractUserOpt(userOptionsString, "gdt_subtype");
                                if (pmiSubType === "fcf")
                                    openSegment.pmiInfo.pmiSubtype = Communicator.PmiSubType.GdtFcf;
                            }
                            else if (pmiType === "locator")
                                openSegment.pmiInfo.pmiType = Communicator.PmiType.Locator;
                            else if (pmiType === "measurementpoint")
                                openSegment.pmiInfo.pmiType = Communicator.PmiType.MeasurementPoint;
                            else if (pmiType === "roughness")
                                openSegment.pmiInfo.pmiType = Communicator.PmiType.Roughness;
                            else if (pmiType === "welding") {
                                openSegment.pmiInfo.pmiType = Communicator.PmiType.Welding;
                                var pmiSubType = Internal.Util.extractUserOpt(userOptionsString, "welding_subtype");
                                if (pmiSubType === "line")
                                    openSegment.pmiInfo.pmiSubtype = Communicator.PmiSubType.WeldingLine;
                                else if (pmiSubType === "spot")
                                    openSegment.pmiInfo.pmiSubtype = Communicator.PmiSubType.WeldingSpot;
                            }
                            else if (pmiType === "table")
                                openSegment.pmiInfo.pmiType = Communicator.PmiType.Table;
                            else if (pmiType === "other") {
                                openSegment.pmiInfo.pmiType = Communicator.PmiType.Other;
                                var pmiSubType = Internal.Util.extractUserOpt(userOptionsString, "other_subtype");
                                if (pmiSubType === "symboluser")
                                    openSegment.pmiInfo.pmiSubtype = Communicator.PmiSubType.OtherSymbolUser;
                                else if (pmiSubType === "symbolutility")
                                    openSegment.pmiInfo.pmiSubtype = Communicator.PmiSubType.OtherSymbolUtility;
                                else if (pmiSubType === "symbolcustom")
                                    openSegment.pmiInfo.pmiSubtype = Communicator.PmiSubType.OtherSymbolCustom;
                                else if (pmiSubType === "geometricreference")
                                    openSegment.pmiInfo.pmiSubtype = Communicator.PmiSubType.OtherGeometricReference;
                            }
                            else if (pmiType === "generic") {
                                openSegment.pmiInfo.pmiType = Communicator.PmiType.GeometricalTolerance;
                            }
                        }
                    }
                    // default visibility can be stored in two different ways
                    var visibility = Internal.Util.extractUserOpt(userOptionsString, "prc_hidden");
                    if (visibility) {
                        openSegment.visible = false;
                    }
                    visibility = Internal.Util.extractUserOpt(userOptionsString, "default_visibility");
                    if (visibility) {
                        openSegment.visible = visibility !== "off";
                    }
                    var hwfVersion = Internal.Util.extractUserOpt(userOptionsString, "hwf_version");
                    if (hwfVersion !== null) {
                        this._hwfFileVersion = hwfVersion;
                    }
                    var filename = Internal.Util.extractUserOpt(userOptionsString, "filename");
                    if (filename !== null) {
                        this._originalFilename = filename;
                        if (filename.indexOf(".dwg") !== -1 || filename.indexOf(".dwf") !== -1 || filename.indexOf(".skp") !== -1 || filename.indexOf(".hsf") !== -1)
                            this._faceWinding = Communicator.FaceWinding.Unknown; // On files which doesn't come from exchange we experienced some badly setup handedness
                    }
                    var modelUnits = Internal.Util.extractUserOpt(userOptionsString, "model_units");
                    if (modelUnits !== null) {
                        this._modelUnits = parseFloat(modelUnits);
                    }
                    // capture data is also stored in user options, delegated to capture parser
                    this._captureParser.checkParseCapture(userOptionsString, openSegment.segmentId);
                };
                /*
                    Data parsed in this function does not appear to be used anywhere in the file parsing.
                    Its purpose appears to be to consume data that could be present as a report of HSF export
                */
                HWFParser.prototype._parseUnicodeOptions = function () {
                    var length = this._readUnsignedShort();
                    this._readString(2 * length);
                };
                HWFParser.prototype._parseHeuristics = function () {
                    var mask = this._readUnsignedShort();
                    if (mask & Internal.StreamTKHeuristic.Extended) {
                        var word = this._readUnsignedShort();
                        mask |= word << Internal.StreamTKHeuristic.ExtendedShift;
                    }
                    var value = this._readUnsignedShort();
                    if (mask | Internal.StreamTKHeuristic.Extended) {
                        var word = this._readUnsignedShort();
                        value |= word << Internal.StreamTKHeuristic.ExtendedShift;
                    }
                    var extras = 0;
                    if ((mask & value & Internal.StreamTKHeuristic.Extras) !== 0) {
                        extras = this._readByte();
                        this._faceWinding = Communicator.FaceWinding.Clockwise;
                    }
                };
                /*
                    Data parsed in this function does not appear to be used anywhere in the file parsing.
                    Its purpose appears to be to consume data that could be present as a report of HSF export
                */
                HWFParser.prototype._parseVisibility = function () {
                    var mask = this._data[this._currentBytePosition++];
                    /* var value = this._data[ */ this._currentBytePosition++ /* ] */;
                    if (mask & Internal.TKO_Geo.Extended) {
                        var word = this._readUnsignedShort();
                        mask |= word << Internal.TKO_Geo.Extended_Shift;
                    }
                    if (mask & Internal.TKO_Geo.Extended) {
                        var word = this._readUnsignedShort();
                        mask |= word << Internal.TKO_Geo.Extended_Shift;
                    }
                    if (mask & Internal.TKO_Geo.Extended2) {
                        var bbyte = this._readByte();
                        mask |= bbyte << Internal.TKO_Geo.Extended2_Shift;
                    }
                    if (mask & Internal.TKO_Geo.Extended2) {
                        var bbyte = this._readByte();
                        mask |= bbyte << Internal.TKO_Geo.Extended2_Shift;
                    }
                };
                HWFParser.prototype._parseOpenSegment = function () {
                    var _this = this;
                    var segment = this._segmentPropertyManager.open();
                    var length = this._data[this._currentBytePosition++];
                    if (length === 255)
                        length = this._readUnsignedInt();
                    segment.segmentName = this._readString(length);
                    // If opening PMI segment, we have to wait, PMI cross highlight needs to references the instaned bodies!
                    if (segment.segmentName === "all_markup" && this._pendingMeshData.length > 0) {
                        this._makingMeshes = true;
                        Promise.all(this._pendingMeshData).then(function () {
                            Promise.all(_this._pendingInstanceData).then(function () {
                                _this._makingMeshes = false;
                                _this._pendingMeshData.length = 0;
                            });
                        });
                    }
                    if (this._currentlyParsingIncludes) {
                        ++this._currentIncludeDepth; // We will start parsing an include segment geometry node.
                    }
                    if (segment.segmentName === "new_includes" || segment.segmentName === "zzz_new_includes") {
                        this._currentlyParsingIncludes = true;
                        this._log("begin parsing includes");
                    }
                    if (segment.segmentName === "all_markup") {
                        this._currentlyParsingMarkup = true;
                    }
                    if (segment.segmentName === "new_frames") {
                        this._currentlyReadingFrames = true;
                    }
                    if (!this._currentlyParsingIncludes && !this._currentlyParsingMarkup && !this._currentlyReadingFrames) {
                        this._log("Open segment: " + segment.segmentName);
                    }
                };
                HWFParser.prototype._parseReference = function () {
                    var index = this._readUnsignedInt() + this._tagBase;
                    /* var condLength = */ this._readByte();
                    if (this._modelTagToMeshData.hasOwnProperty(index.toString())) {
                        this._segmentPropertyManager.current().geometryReferenceTag = index;
                    }
                    else {
                        this._log("unable to lookup reference: " + index);
                    }
                };
                HWFParser.prototype._parsePolyPoint = function () {
                    var count = this._readUnsignedInt();
                    var points = [];
                    this._readFloatArray(points, 3 * count);
                    var indices = [];
                    for (var i = 0; i < count; ++i) {
                        indices.push(i);
                    }
                    this._addPendingMeshDataToCurrentSegment(true, [indices], points);
                };
                HWFParser.prototype._parseLine = function () {
                    var points = [];
                    this._readFloatArray(points, 6);
                    var indices = [0, 1];
                    this._addPendingMeshDataToCurrentSegment(true, [indices], points);
                };
                HWFParser.prototype._registerCurrentMeshDataToTag = function (tag, meshData) {
                    this._modelTagToMeshData[tag] = meshData;
                };
                HWFParser.prototype._parseCloseSegment = function () {
                    var _this = this;
                    var segment = this._segmentPropertyManager.close();
                    if (segment === undefined) {
                        console.assert(false, "segment closed without opening");
                        return;
                    }
                    var model = this._viewer.model;
                    var defaultColor = new Communicator.Color(200, 200, 200);
                    var createMeshInstance = function (meshId, segment, matrix, textureName) {
                        var meshInstanceData = new Communicator.MeshInstanceData(meshId);
                        meshInstanceData.setFaceColor(defaultColor); // Have to do that, or else the objects colors (vertex color) are all washed out
                        meshInstanceData.setMatrix(matrix);
                        var parentId = segment.resolveParentNodeId(model, Communicator.NodeType.AssemblyNode);
                        var instance = model.createMeshInstance(meshInstanceData, parentId).then(function (nodeId) {
                            segment.assemblyNodeId = nodeId;
                            // create and set texture
                            if (textureName !== undefined) {
                                var imageName = _this._textureImageNames[textureName];
                                if (imageName !== undefined) {
                                    var imagePromise = _this._imageResources[imageName];
                                    if (imagePromise !== undefined) {
                                        imagePromise.then(function (imageId) {
                                            model.setNodesTexture([nodeId], { imageId: imageId });
                                        });
                                    }
                                }
                            }
                            return nodeId;
                        });
                        _this._pendingInstanceData.push(instance);
                        return instance;
                    };
                    if (this._currentlyParsingIncludes) {
                        --this._currentIncludeDepth;
                        if (this._currentIncludeDepth === 0 && segment.meshDataToCreate !== undefined) {
                            this._registerCurrentMeshDataToTag(this._currentTag, segment.meshDataToCreate);
                        }
                    }
                    if (segment.segmentName === "new_includes" || segment.segmentName === "zzz_new_includes") {
                        this._log("done parsing includes");
                        this._currentlyParsingIncludes = false;
                    }
                    if (segment.segmentName === "all_markup") {
                        this._currentlyParsingMarkup = false;
                    }
                    if (segment.segmentName === "new_frames") {
                        this._currentlyReadingFrames = false;
                    }
                    if (!this._currentlyParsingIncludes && !this._currentlyParsingMarkup && !this._currentlyReadingFrames) {
                        this._log("Close segment: " + segment.segmentName);
                        if (segment.geometryReferenceTag !== undefined) {
                            var meshDataTagsToCreate = segment.geometryReferenceTag;
                            var mergedDataSegement = segment;
                            var partOrRISegment = segment.resolvePartOrRepItemSegment();
                            if (partOrRISegment !== undefined)
                                mergedDataSegement = partOrRISegment;
                            if (mergedDataSegement.meshDataTagsToCreate === undefined) {
                                mergedDataSegement.meshDataTagsToCreate = [];
                            }
                            if (mergedDataSegement.meshDataTagsAssociatedSegments === undefined) {
                                mergedDataSegement.meshDataTagsAssociatedSegments = [];
                            }
                            mergedDataSegement.meshDataTagsToCreate.push(meshDataTagsToCreate);
                            mergedDataSegement.meshDataTagsAssociatedSegments.push(segment);
                        }
                    }
                    // "mesh instance + merging" management
                    var instanceCurrentlyRegistering;
                    if (segment.meshDataTagsToCreate !== undefined) {
                        var registrationIndex = this._InstancedDataManager.getRegistrationIndex(segment.meshDataTagsToCreate);
                        if (registrationIndex === undefined) {
                            // first time we instantiate that merged mesh
                            // build merged mesh data to create
                            for (var i = 0; i < segment.meshDataTagsToCreate.length; i++) {
                                var meshDataToCreate = this._modelTagToMeshData[segment.meshDataTagsToCreate[i]];
                                if (meshDataToCreate === undefined || meshDataToCreate.length < 1
                                    || segment.meshDataTagsAssociatedSegments === undefined) {
                                    console.assert(false, "internal logic error");
                                    continue;
                                }
                                if (segment.meshDataToCreate === undefined)
                                    segment.meshDataToCreate = [];
                                meshDataToCreate[0].segmentDataCameFrom = segment.meshDataTagsAssociatedSegments[i];
                                for (var j = 0; j < meshDataToCreate.length; j++) {
                                    segment.meshDataToCreate.push(meshDataToCreate[j]);
                                }
                            }
                            // get instance manager entry
                            instanceCurrentlyRegistering = this._InstancedDataManager.Register(segment.meshDataTagsToCreate);
                        }
                        else if (segment.meshDataTagsAssociatedSegments !== undefined) {
                            var segSearchMaterialAndMatrix_1 = segment.meshDataTagsAssociatedSegments[0];
                            // we have already instantiated that mesh
                            // do we already got the mesh ID ?
                            var meshId = this._InstancedDataManager.registrationIndexToMeshID[registrationIndex];
                            if (meshId !== undefined) {
                                // Then we can create the instance
                                createMeshInstance(meshId, segment, segSearchMaterialAndMatrix_1.computeNetMatrix(), segSearchMaterialAndMatrix_1.resolveTextureName());
                            }
                            else {
                                // We have to wait for the mesh creation promise to end
                                this._InstancedDataManager.registrationIndexToMeshCreatePromise[registrationIndex].then(function (meshId) {
                                    if (meshId !== null) {
                                        // Then we can create the instance
                                        createMeshInstance(meshId, segment, segSearchMaterialAndMatrix_1.computeNetMatrix(), segSearchMaterialAndMatrix_1.resolveTextureName());
                                    }
                                });
                            }
                        }
                        else {
                            console.assert(false, "internal logic error");
                        }
                    }
                    // mesh creation + instantiation
                    if (segment.meshDataToCreate !== undefined) {
                        var meshDataToCreate_1 = segment.meshDataToCreate;
                        // Problem: on many PMI shell data we get the vertices before the segment matrix, so we have to postpone the mesh creation here is the segmentClose
                        var pmiInfoSeg = segment.resolvePmiInfoSegment();
                        if (pmiInfoSeg !== undefined) {
                            if (segment.pmiInfo === undefined) {
                                console.assert(false, "internal logic error");
                            }
                            else {
                                var pmiInfo_1 = segment.pmiInfo;
                                var pmiMeshData = segment.buildMergedMeshData(pmiInfo_1.meshData, true);
                                pmiInfo_1.meshData = pmiMeshData;
                                if (pmiMeshData !== undefined) {
                                    pmiMeshData.setFaceWinding(this._faceWinding);
                                    pmiMeshData.setBackfacesEnabled(true);
                                    this._pendingMeshData.push(model.createMesh(pmiMeshData).then(function (meshId) {
                                        var flags = Communicator.MeshInstanceCreationFlags.DoNotLight
                                            | Communicator.MeshInstanceCreationFlags.DoNotExplode
                                            | Communicator.MeshInstanceCreationFlags.DoNotCut
                                            | Communicator.MeshInstanceCreationFlags.DoNotOutlineHighlight
                                            | Communicator.MeshInstanceCreationFlags.DoNotXRay;
                                        var meshInstanceData = new Communicator.MeshInstanceData(meshId, null, null, null, null, null, flags);
                                        meshInstanceData.setFaceColor(defaultColor);
                                        var refOnTopoItems = [];
                                        for (var i = 0; i < pmiInfo_1.refsToBrep.length; i++) {
                                            var ref = pmiInfo_1.refsToBrep[i];
                                            var correspondingSegProp = _this._nodeAdditionalIdToSegmentProps[ref.item];
                                            if (correspondingSegProp !== undefined) {
                                                var bodyId = correspondingSegProp.resolveParentNodeId(model, Communicator.NodeType.BodyInstance);
                                                if (bodyId !== undefined) {
                                                    var newRef = new Communicator.RefOnTopoItem();
                                                    newRef.bodyId = bodyId;
                                                    newRef.subElementType = Communicator.PmiTopoRef.Face;
                                                    newRef.subElementIndex = ref.region;
                                                    refOnTopoItems.push(newRef);
                                                }
                                            }
                                        }
                                        _this._pendingInstanceData.push(model.createPmiInstance(meshInstanceData, pmiInfo_1.pmiType, pmiInfo_1.pmiSubtype, refOnTopoItems, segment.resolveParentNodeId(model, Communicator.NodeType.AssemblyNode)).then(function (nodeId) {
                                            segment.assemblyNodeId = nodeId;
                                            return nodeId;
                                        }));
                                        return meshId;
                                    }));
                                }
                            }
                        }
                        else if (!this._currentlyParsingIncludes) {
                            var meshData = segment.buildMergedMeshData(undefined, false);
                            if (meshData !== undefined) {
                                meshData.setManifold(true);
                                meshData.setFaceWinding(this._faceWinding);
                                meshData.setBackfacesEnabled(false);
                                this._pendingMeshData.push(model.createMesh(meshData).then(function (meshId) {
                                    if (instanceCurrentlyRegistering !== undefined) {
                                        // the first mesh data of a mesh instance is created, save mesh id in instance manager
                                        _this._InstancedDataManager.registrationIndexToMeshID[instanceCurrentlyRegistering] = meshId;
                                    }
                                    var segSearchMaterialAndMatrix = meshDataToCreate_1[0].segmentDataCameFrom;
                                    // instantiate mesh
                                    createMeshInstance(meshId, segment, segSearchMaterialAndMatrix.computeNetMatrix(), segSearchMaterialAndMatrix.resolveTextureName());
                                    return meshId;
                                }));
                                if (instanceCurrentlyRegistering !== undefined) {
                                    // the first mesh data of a mesh instance is to be created, save mesh data promise in instance manager (so that other instance up to come can link to it)
                                    this._InstancedDataManager.registrationIndexToMeshCreatePromise[instanceCurrentlyRegistering] = this._pendingMeshData[this._pendingMeshData.length - 1];
                                }
                            }
                        }
                    }
                };
                HWFParser.prototype._parseShell = function () {
                    var subop = this._readByte();
                    var subop2 = 0;
                    var index = 0;
                    if (subop & Internal.TKSH.EXPANDED) {
                        subop2 = this._readUnsignedShort();
                    }
                    if (!(subop & Internal.TKSH.FIRSTPASS)) {
                        index = this._readUnsignedInt();
                    }
                    /* var lodLevel = */ this._readByte();
                    var pointCount = this._readUnsignedInt();
                    var points = [];
                    this._readFloatArray(points, 3 * pointCount);
                    var compressionScheme = this._readByte();
                    var workspaceUsed = this._readUnsignedInt();
                    var bitsPerSample = this._readByte();
                    var faceList = [];
                    var faceListLength = 0;
                    if (bitsPerSample === 32) {
                        faceListLength = (workspaceUsed - 1) / 4;
                        this._readIntArray(faceList, faceListLength);
                    }
                    else if (bitsPerSample === 16) {
                        faceListLength = (workspaceUsed - 1) / 2;
                        this._readUnsignedShortArray(faceList, faceListLength);
                    }
                    else if (bitsPerSample === 8) {
                        faceListLength = workspaceUsed - 1;
                        this._readByteArray(faceList, faceListLength);
                    }
                    var normals;
                    var floatColors;
                    var vfcolors = false;
                    var extraShellData = [];
                    var uvs;
                    if (subop & Internal.TKSH.HAS_OPTIONALS) {
                        normals = [];
                        while (true) {
                            var optcode = this._readByte();
                            if (optcode === Internal.OPT.TERMINATE) {
                                break;
                            }
                            else if (optcode === Internal.OPT.ALL_NORMALS_POLAR) {
                                var normalspolar = [];
                                this._readFloatArray(normalspolar, pointCount * 2);
                                Internal.Util.normalsPolarToCartesian(pointCount, normalspolar, normals);
                            }
                            else if (optcode === Internal.OPT.ALL_PARAMETERS) {
                                var uvWidth = this._readByte();
                                var uvDataCount = uvWidth * pointCount;
                                uvs = [];
                                if (uvWidth >= 3) {
                                    this._parseUVPairsFromTriples(uvs, uvDataCount, uvWidth);
                                }
                                else {
                                    this._readFloatArray(uvs, uvDataCount);
                                }
                            }
                            else if (optcode === Internal.OPT.ALL_VFINDICES) {
                                compressionScheme = this._readByte();
                                var bbox = [];
                                this._readFloatArray(bbox, 6);
                                var bitsPerSample_1 = this._readByte();
                                var workspaceUsed_1 = this._readUnsignedInt();
                                var workspace = [];
                                floatColors = [];
                                this._readIntArray(workspace, workspaceUsed_1 / 4);
                                Internal.Util.unquantizeAndUnpackFloats(pointCount, 1, bitsPerSample_1, bbox, workspace, floatColors);
                            }
                            else if (optcode === Internal.OPT.ALL_VFCOLORS) {
                                vfcolors = true;
                                var colorCube = [0, 0, 0, 1, 1, 1];
                                compressionScheme = this._readByte();
                                bitsPerSample = this._readByte();
                                var workspaceUsed_2 = this._readUnsignedInt();
                                var workspace = [];
                                floatColors = [];
                                this._readByteArray(workspace, workspaceUsed_2);
                                Internal.Util.unquantizeAndUnpackFloats(pointCount, 3, bitsPerSample, colorCube, workspace, floatColors);
                            }
                            else if (optcode === Internal.OPT.FACE_REGIONS) {
                                var workspaceUsed_3 = 0;
                                compressionScheme = this._readByte();
                                if ((compressionScheme & 0x03) !== 0) {
                                    switch (compressionScheme & 0x18) {
                                        case 0x00:
                                            {
                                                workspaceUsed_3 = this._readInt();
                                            }
                                            break;
                                        case 0x08:
                                            {
                                                workspaceUsed_3 = this._readByte();
                                            }
                                            break;
                                        case 0x10:
                                            {
                                                workspaceUsed_3 = this._readUnsignedShort();
                                            }
                                            break;
                                    }
                                }
                                // mp_workspace_used is remaining amount of data items
                                switch (compressionScheme & 0x03) {
                                    case 0:
                                        {
                                            var i = 0;
                                            var len = 0;
                                            var faceCount = 0;
                                            while (i < faceListLength) {
                                                if (faceList[i] > 0)
                                                    len = faceList[i];
                                                else
                                                    len = -faceList[i];
                                                faceCount += len - 2;
                                                i += len + 1;
                                            }
                                            workspaceUsed_3 = faceCount;
                                        }
                                        break;
                                    case 1:
                                        {
                                        }
                                        break;
                                    case 2:
                                        {
                                            ++workspaceUsed_3;
                                        }
                                        break;
                                    case 3:
                                        {
                                            workspaceUsed_3 *= 2;
                                        }
                                        break;
                                }
                                workspaceUsed_3 *= 4;
                                var tsd = [];
                                var count = workspaceUsed_3 / 4;
                                switch (compressionScheme & 0x18) {
                                    case 0x00:
                                        {
                                            this._readIntArray(tsd, count);
                                        }
                                        break;
                                    case 0x08:
                                        {
                                            this._readByteArray(tsd, count);
                                        }
                                        break;
                                    case 0x10:
                                        {
                                            this._readUnsignedShortArray(tsd, count);
                                        }
                                        break;
                                }
                                var intCount = workspaceUsed_3 / 4;
                                switch (compressionScheme & 0x03) {
                                    case 0:
                                        {
                                            for (var i = 0; i < intCount; i++) {
                                                extraShellData[i] = tsd[i];
                                            }
                                        }
                                        break;
                                    case 3:
                                        {
                                            var cc = 0;
                                            for (var i = 0; i < intCount; i += 2) {
                                                for (var j = 0; j < tsd[i + 1]; j++) {
                                                    extraShellData[cc++] = tsd[i];
                                                }
                                            }
                                        }
                                        break;
                                }
                            }
                        }
                    }
                    // Build strips
                    var stage = 0;
                    var stripLen = 0;
                    var cache = [0, 0, 0];
                    var backorder = 0;
                    var triangleIndex = 0;
                    var indices = [];
                    if (extraShellData.length > 0) {
                        // Get the number of faces
                        var faceCount = 0;
                        for (var i = 0; i < extraShellData.length; ++i) {
                            if (faceCount < extraShellData[i] + 1)
                                faceCount = extraShellData[i] + 1;
                        }
                        // build rooms for triangle indices per face
                        for (var i = 0; i < faceCount; ++i) {
                            indices.push([]);
                        }
                    }
                    for (var i = 0; i < faceListLength; ++i) {
                        switch (stage) {
                            case 0:
                                if (extraShellData.length === 0) {
                                    indices.push([]);
                                }
                                stripLen = faceList[i];
                                if (stripLen > 0)
                                    stage = 1;
                                break;
                            case 1:
                                /* cache the first point */
                                cache[0] = faceList[i];
                                --stripLen;
                                if (stripLen === 0)
                                    stage = 0;
                                else
                                    stage = 2;
                                break;
                            case 2:
                                /* cache the second point */
                                cache[1] = faceList[i];
                                --stripLen;
                                if (stripLen === 0)
                                    stage = 0;
                                else
                                    stage = 3;
                                break;
                            case 3:
                                /* cache the third point, write a triangle */
                                cache[2] = faceList[i];
                                if (extraShellData.length === 0)
                                    indices[indices.length - 1].push(cache[0], cache[1], cache[2]);
                                else
                                    indices[extraShellData[triangleIndex]].push(cache[0], cache[1], cache[2]);
                                --stripLen;
                                if (stripLen === 0)
                                    stage = 0;
                                else
                                    stage = 4;
                                backorder = 1;
                                triangleIndex++;
                                break;
                            case 4:
                                /* move everything down one, cache the next point, draw a triangle */
                                cache[0] = cache[1];
                                cache[1] = cache[2];
                                cache[2] = faceList[i];
                                if (extraShellData.length === 0) {
                                    if (backorder == 1)
                                        indices[indices.length - 1].push(cache[2], cache[1], cache[0]);
                                    else
                                        indices[indices.length - 1].push(cache[0], cache[1], cache[2]);
                                }
                                else {
                                    if (backorder == 1)
                                        indices[extraShellData[triangleIndex]].push(cache[2], cache[1], cache[0]);
                                    else
                                        indices[extraShellData[triangleIndex]].push(cache[0], cache[1], cache[2]);
                                }
                                backorder = -backorder;
                                --stripLen;
                                if (stripLen === 0)
                                    stage = 0;
                                triangleIndex++;
                                break;
                        }
                    }
                    var vertexColors;
                    if (floatColors !== undefined) {
                        vertexColors = [];
                        if (vfcolors) {
                            for (var i = 0; i < pointCount; i++) {
                                vertexColors[i * 3] = floatColors[i * 3];
                                vertexColors[i * 3 + 1] = floatColors[i * 3 + 1];
                                vertexColors[i * 3 + 2] = floatColors[i * 3 + 2];
                            }
                        }
                        else {
                            for (var i = 0; i < pointCount; i++) {
                                var cp1 = Math.floor(floatColors[i]);
                                var cp2 = Math.ceil(floatColors[i]);
                                var p = cp2 - floatColors[i];
                                var c1 = new Communicator.Point3(this._colorMap[cp1 * 3], this._colorMap[cp1 * 3 + 1], this._colorMap[cp1 * 3 + 2]);
                                var c2 = new Communicator.Point3(this._colorMap[cp2 * 3], this._colorMap[cp2 * 3 + 1], this._colorMap[cp2 * 3 + 2]);
                                var delta = new Communicator.Point3(c2.x - c1.x, c2.y - c1.y, c2.z - c1.z);
                                vertexColors[i * 3] = c1.x + p * delta.x;
                                vertexColors[i * 3 + 1] = c1.y + p * delta.y;
                                vertexColors[i * 3 + 2] = c1.z + p * delta.z;
                            }
                        }
                    }
                    if (bitsPerSample != 32) {
                        if (this._currentlyParsingMarkup) {
                            /*
                            var ihobj = new Object();
                            ihobj.tag = myModel.currentTag;
                            ihobj.isEdge = false;
                            this.tempIncludeList[this.tempIncludeListLength++] = ihobj;
        
                            this.libraryNode.add("node", {
                                type: "geometry",
                                id: `sh-${myModel.currentTag}`,
                                coreId: `cid${myModel.currentTag}`,
                                primitive: "triangles",
                                positions: points,
                                normals: normals,
                                indices: indices,
                                colors: vertexColors,
                                uv: uvs
                            });
                            */
                        }
                        else {
                            /*
                            var backfaces = myhwv.m_showBackfaces;
                            if (this.readingMarkup)
                                backfaces = true;
                            if (!this.current_lightson[this.segmentLevel])
                                backfaces = true;
                            var geometryNode = {
                                type: "geometry",
                                id: `sh-${myModel.currentTag}`,
                                primitive: "triangles",
                                positions: points,
                                normals: normals,
                                indices: indices,
                                colors: vertexColors,
                                uv: uvs
                            };
        
        
                            var flagsNode = {
                                type: "flags",
                                id: `fv${myModel.currentTag}`,
                                flags: {
                                    enabled: myModel.faceVisibility,
                                    visibleAfterCulling: true,
                                    backfaces: backfaces,
                                    frontface: myModel.faceWinding,
                                    transparent: isTransparent
                                },
                                nodes: [geometryNode]
                            };
        
        
                            this.activeSubnode[this.segmentLevel].add("node", flagsNode);
                            */
                        }
                    }
                    this._addPendingMeshDataToCurrentSegment(false, indices, points, normals, uvs, vertexColors);
                };
                HWFParser.prototype._addPendingMeshDataToCurrentSegment = function (isWireframe, indices, points, normals, uvs, vertexColors) {
                    // Have to reconstruct the shattered shells of a body into a single one
                    var segment = this._segmentPropertyManager.current();
                    if (this._currentlyParsingMarkup) {
                        var pmiSegment = segment.resolvePmiInfoSegment();
                        if (pmiSegment !== undefined)
                            segment = pmiSegment;
                    }
                    else {
                        var partOrRISegment = segment.resolvePartOrRepItemSegment();
                        if (partOrRISegment !== undefined)
                            segment = partOrRISegment;
                    }
                    var newMeshData = {
                        indices: indices,
                        points: points,
                        wireframe: isWireframe,
                        segmentDataCameFrom: this._segmentPropertyManager.current()
                    };
                    if (normals !== null) {
                        newMeshData.normals = normals;
                    }
                    if (uvs !== null) {
                        newMeshData.uvs = uvs;
                    }
                    if (vertexColors !== null) {
                        newMeshData.vertexColors = vertexColors;
                    }
                    if (segment.meshDataToCreate === undefined)
                        segment.meshDataToCreate = [];
                    segment.meshDataToCreate.push(newMeshData);
                };
                HWFParser.prototype._parseMatrix = function () {
                    var m = new Communicator.Matrix();
                    m.m[0] = this._readFloat();
                    m.m[1] = this._readFloat();
                    m.m[2] = this._readFloat();
                    m.m[3] = 0;
                    m.m[4] = this._readFloat();
                    m.m[5] = this._readFloat();
                    m.m[6] = this._readFloat();
                    m.m[7] = 0;
                    m.m[8] = this._readFloat();
                    m.m[9] = this._readFloat();
                    m.m[10] = this._readFloat();
                    m.m[11] = 0;
                    m.m[12] = this._readFloat();
                    m.m[13] = this._readFloat();
                    m.m[14] = this._readFloat();
                    m.m[15] = 1.0;
                    this._segmentPropertyManager.current().matrix = m;
                };
                HWFParser.prototype._parseTag = function () {
                    this._currentTag++;
                };
                HWFParser.prototype._parseColorRgb = function () {
                    var mask = this._readByte();
                    if ((mask & Internal.TKO_Geo.Extended) !== 0) {
                        var bbyte = this._readByte();
                        mask |= bbyte << Internal.TKO_Geo.Extended_Shift;
                    }
                    if ((mask & Internal.TKO_Geo.Extended_Colors) !== 0) {
                        var bbyte = this._readByte();
                        mask |= bbyte << Internal.TKO_Geo.Extended_Colors_Shift;
                    }
                    if ((mask & Internal.TKO_Geo.Extended2) !== 0) {
                        var bbyte = this._readByte();
                        mask |= bbyte << Internal.TKO_Geo.Extended2_Shift;
                    }
                    var rgb = [];
                    this._readByteArray(rgb, 3);
                    this._segmentPropertyManager.current().color = new Communicator.Color(rgb[0], rgb[1], rgb[2]);
                };
                HWFParser.prototype._parsePolyPolyPoint = function () {
                    var _this = this;
                    var subop = this._readUnsignedShort();
                    // var pointsNumDimensions = Util.figureNumDimensions(subop);
                    var primitiveCount = 0;
                    var pointCount = this._readUnsignedInt();
                    if (subop & Internal.TKPP.EXPLICIT_PRIMITIVE_MASK) {
                        if (subop & Internal.TKPP.ONE_PRIMITIVE_ONLY) {
                            primitiveCount = 1;
                        }
                        else if (subop & Internal.TKPP.HAS_EXPLICIT_PRIMITIVE_COUNT) {
                            primitiveCount = this._readInt();
                        }
                    }
                    var lengths = [];
                    if (subop & Internal.TKPP.EXPLICIT_PRIMITIVE_MASK) {
                        this._readIntArray(lengths, primitiveCount);
                    }
                    else {
                        var progress = 0;
                        while (progress < pointCount) {
                            lengths[primitiveCount] = this._readUnsignedInt();
                            progress += lengths[primitiveCount];
                            primitiveCount++;
                        }
                        this._currentTag += primitiveCount - 1;
                    }
                    var pointsNumFloats = Internal.Util.figureNumFromFloats(subop, primitiveCount, pointCount);
                    if (pointsNumFloats === 0)
                        return;
                    var points = [];
                    this._readFloatArray(points, pointsNumFloats);
                    if (subop & Internal.TKPP.ANY_2D_MASK) {
                        var newPoints = [];
                        Internal.Util.unmangleFloats(pointCount, points, primitiveCount, lengths, subop, newPoints);
                        points = newPoints;
                    }
                    var indices = [];
                    var ii = 0;
                    for (var i = 0; i < primitiveCount; i++) {
                        for (var j = 0; j < lengths[i] - 1; j++) {
                            indices.push(ii, ii + 1);
                            ii++;
                        }
                        ii++;
                    }
                    if (this._currentlyReadingFrames) {
                        var meshData = new Communicator.MeshData();
                        Internal.Data.generateLineDataFromPointsAndIndices(points, indices, null, 255, meshData, undefined);
                        var segment_1 = this._segmentPropertyManager.current();
                        this._viewer.model.createMesh(meshData).then(function (meshId) {
                            var flags = Communicator.MeshInstanceCreationFlags.DoNotSelect
                                | Communicator.MeshInstanceCreationFlags.DoNotExplode
                                | Communicator.MeshInstanceCreationFlags.DoNotCut
                                | Communicator.MeshInstanceCreationFlags.DoNotXRay;
                            var meshInstanceData = new Communicator.MeshInstanceData(meshId, null, null, null, null, null, flags);
                            meshInstanceData.setMatrix(segment_1.computeNetMatrix());
                            var lineColor = segment_1.resolveColor();
                            if (lineColor !== undefined) {
                                meshInstanceData.setLineColor(lineColor);
                            }
                            var name = segment_1.resolveName();
                            if (name !== undefined) {
                                _this._captureParser.addFrame(name, meshInstanceData);
                            }
                        });
                    }
                    else {
                        this._addPendingMeshDataToCurrentSegment(true, [indices], points);
                    }
                };
                HWFParser.prototype._parseColor = function () {
                    var mask = this._readByte();
                    if ((mask & Internal.TKO_Geo.Extended) !== 0) {
                        var bbyte = this._readByte();
                        mask |= bbyte << Internal.TKO_Geo.Extended_Shift;
                    }
                    if ((mask & Internal.TKO_Geo.Extended_Colors) !== 0) {
                        var bbyte = this._readByte();
                        mask |= bbyte << Internal.TKO_Geo.Extended_Colors_Shift;
                    }
                    if ((mask & Internal.TKO_Geo.Extended2) !== 0) {
                        var bbyte = this._readByte();
                        mask |= bbyte << Internal.TKO_Geo.Extended2_Shift;
                    }
                    var channels = this._readByte();
                    var progress = 0;
                    if ((channels & (1 << Internal.TKO_Channel.Diffuse)) !== 0) {
                        progress = this._readByte();
                    }
                    if ((channels & (1 << Internal.TKO_Channel.Diffuse)) !== 0) {
                        // if progress is not zero then a string is encoded (of length  = progress)
                        if (progress !== 0) {
                            var diffuseName = this._readString(progress);
                            if (diffuseName.indexOf("r=") !== -1) {
                                var values = /r=([\.\d]*) g=([\.\d]*) b=([\.\d]*) (\w*)/.exec(diffuseName);
                                if (values !== null) {
                                    var seg = this._segmentPropertyManager.current();
                                    seg.color = Communicator.Color.createFromFloat(parseFloat(values[1]), parseFloat(values[2]), parseFloat(values[3]));
                                    seg.textureName = values[4];
                                }
                            }
                            else {
                                //this.texture_name[this.segmentLevel] = diffuseName;
                                //this.colors_set[this.segmentLevel] |= TEXTURE_SET;
                                this._segmentPropertyManager.current().color = Communicator.Color.white();
                            }
                        }
                        else {
                            var rgb = [];
                            this._readByteArray(rgb, 3);
                            this._segmentPropertyManager.current().color = new Communicator.Color(rgb[0], rgb[1], rgb[2]);
                            //this.colors_set[this.segmentLevel] |= DIFFUSE_SET;
                        }
                    }
                    if ((channels & (1 << Internal.TKO_Channel.Transmission)) !== 0) {
                        this._readByte();
                    }
                    if ((channels & (1 << Internal.TKO_Channel.Transmission)) !== 0) {
                        var transmissionRgb = [];
                        this._readByteArray(transmissionRgb, 3);
                        if (transmissionRgb[0] !== 0) {
                            this._segmentPropertyManager.current().transparency = 255 - transmissionRgb[0];
                            //this.colors_set[this.segmentLevel] |= TRANSMISSION_SET;
                        }
                    }
                };
                HWFParser.prototype._parseTexture = function () {
                    var textureName;
                    var length = this._data[this._currentBytePosition++];
                    if (length === 255)
                        length = this._readUnsignedInt();
                    textureName = this._readString(length);
                    var imageName; // links up to actual image data loaded in parse_image
                    length = this._data[this._currentBytePosition++];
                    if (length === 255)
                        length = this._readUnsignedInt();
                    imageName = this._readString(length);
                    var flags = this._readUnsignedShort();
                    if ((flags & Internal.TKO_Texture.Extended) !== 0) {
                        var word = this._readUnsignedShort();
                        flags |= word << Internal.TKO_Texture.Extended_Shift;
                    }
                    //none of these flags seem to be used
                    if ((flags & Internal.TKO_Texture.Param_Source) !== 0) {
                        this._readByte();
                    }
                    if ((flags & Internal.TKO_Texture.Tiling) !== 0) {
                        this._readByte();
                    }
                    if ((flags & Internal.TKO_Texture.Interpolation) !== 0) {
                        this._readByte();
                    }
                    if ((flags & Internal.TKO_Texture.Decimation) !== 0) {
                        this._readByte();
                    }
                    if ((flags & Internal.TKO_Texture.Param_Offset) !== 0) {
                        this._readByte();
                    }
                    this._textureImageNames[textureName] = imageName;
                };
                HWFParser.prototype._parseImage = function () {
                    // stage 0
                    var position = Communicator.Point3.zero();
                    this._readPoint3(position);
                    // stage 1  -> read options
                    var options = this._readUnsignedInt();
                    var format = options & Internal.TKO_Image.Format_Mask; // rgb or rgba, etc
                    options = options & Internal.TKO_Image.Options_Mask;
                    // stage2 -> read image name length
                    var nameLength = 0;
                    if ((options & Internal.TKO_Image.Is_Named) !== 0) {
                        nameLength = this._data[this._currentBytePosition++];
                        if (nameLength === 255) {
                            nameLength = this._readUnsignedInt();
                        }
                    }
                    // stage 3 -> read image name
                    var imageName = "";
                    if ((options & Internal.TKO_Image.Is_Named) !== 0) {
                        imageName = this._readString(nameLength);
                    }
                    // stage 4 -> read image size (W x H)
                    var imageSize = Communicator.Point2.zero();
                    imageSize.x = this._readUnsignedInt();
                    imageSize.y = this._readUnsignedInt();
                    // stage 5 -> read compression
                    var compression = this._data[this._currentBytePosition++];
                    // stage 6 -> read image data size
                    var dataSize;
                    if (compression === Internal.TKO_Compression.None) {
                        dataSize = imageSize.x * imageSize.y;
                    }
                    else {
                        dataSize = this._readUnsignedInt();
                    }
                    // stage 7 -> read the image data blob and create resource in browser
                    var bytes = new Uint8Array(dataSize);
                    this._readByteArray(bytes, dataSize);
                    var scFormat;
                    if (compression === Internal.TKO_Compression.None) {
                        var n = 4 * dataSize;
                        var bytes2 = new Uint8Array(n);
                        var j = 0;
                        for (var i = 0; i < n; i += 4) {
                            bytes2[i] = bytes[j];
                            bytes2[i + 1] = bytes[j];
                            bytes2[i + 2] = bytes[j];
                            bytes2[i + 3] = 1;
                            j++;
                        }
                        bytes = bytes2;
                        scFormat = Communicator.ImageFormat.Rgba32;
                    }
                    else {
                        scFormat = Communicator.ImageFormat.Jpeg;
                    }
                    var imageId = this._viewer.model.createImage({
                        format: scFormat,
                        data: bytes,
                        width: imageSize.x,
                        height: imageSize.y
                    });
                    this._imageResources[imageName] = imageId;
                    if (format !== Internal.TKO_Image.TKO_Image_JPEG && compression !== Internal.TKO_Compression.None) {
                        // stage 8 -> reald alpha type byte (this should be 0)
                        var byte = this._readByte();
                        if (byte != 0) {
                            var size = this._readUnsignedInt();
                            var byteArray = new Uint8Array(size);
                            this._readByteArray(byteArray, size);
                        }
                        else {
                            this._readByte();
                        }
                    }
                };
                HWFParser.prototype._parseColorMap = function () {
                    var format = this._readByte();
                    var length = this._readInt();
                    if (format === 0) {
                        this._readFloatArray(this._colorMap, 3 * length);
                    }
                };
                HWFParser.prototype._parseCompression = function () {
                    var remainingData = this._data.subarray(this._currentBytePosition);
                    var decompressedBuffer = jz.zlib.decompress(remainingData);
                    this._data = decompressedBuffer;
                    this._dataView = new DataView(this._data.buffer);
                    this._currentBytePosition = 0;
                };
                HWFParser.prototype._parseClipRegion = function () {
                    var options = this._readByte();
                    options;
                    var pointCount = this._readInt();
                    var points = [];
                    this._readFloatArray(points, pointCount);
                };
                HWFParser.prototype._parsingDone = function () {
                    return __awaiter(this, void 0, void 0, function () {
                        var model, callbackManager, attachType, absoluteRootId, loadRootIds, isHwf;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    this._log("parsing complete!");
                                    model = this._viewer.model;
                                    return [4 /*yield*/, model._hwfAwaitAssemblyTreeReady()];
                                case 1:
                                    _a.sent();
                                    callbackManager = this._viewer._getCallbackManager();
                                    attachType = Communicator.Internal.AttachType.Indirect;
                                    return [4 /*yield*/, callbackManager.promiseTrigger("_firstAttachment", null, attachType)];
                                case 2:
                                    _a.sent();
                                    absoluteRootId = model.getAbsoluteRootNode();
                                    loadRootIds = model.getNodeChildren(absoluteRootId);
                                    isHwf = true;
                                    return [4 /*yield*/, callbackManager.promiseTrigger("_firstModelLoaded", "firstModelLoaded", loadRootIds, attachType, isHwf)];
                                case 3:
                                    _a.sent();
                                    callbackManager.trigger("hwfParseComplete");
                                    return [2 /*return*/];
                            }
                        });
                    });
                };
                HWFParser.prototype._readByte = function () {
                    return this._data[this._currentBytePosition++];
                };
                HWFParser.prototype._readPoint3 = function (point) {
                    point.x = this._dataView.getFloat32(this._currentBytePosition, true);
                    this._currentBytePosition += 4;
                    point.y = this._dataView.getFloat32(this._currentBytePosition, true);
                    this._currentBytePosition += 4;
                    point.z = this._dataView.getFloat32(this._currentBytePosition, true);
                    this._currentBytePosition += 4;
                };
                /*
                private _readPoint2(point: Point2): void {
                    point.x = this._dataView.getFloat32(this._currentBytePosition, true);
                    this._currentBytePosition += 4;
                    point.y = this._dataView.getFloat32(this._currentBytePosition, true);
                    this._currentBytePosition += 4;
                }
                */
                HWFParser.prototype._readFloat = function () {
                    var val = this._dataView.getFloat32(this._currentBytePosition, true);
                    this._currentBytePosition += 4;
                    return val;
                };
                HWFParser.prototype._readUnsignedInt = function () {
                    var val = this._dataView.getUint32(this._currentBytePosition, true);
                    this._currentBytePosition += 4;
                    return val;
                };
                HWFParser.prototype._readUnsignedShort = function () {
                    var val = this._dataView.getUint16(this._currentBytePosition, true);
                    this._currentBytePosition += 2;
                    return val;
                };
                HWFParser.prototype._readString = function (length) {
                    var resultString = "";
                    for (var i = 0; i < length; i++) {
                        var r = this._data[this._currentBytePosition++];
                        if (r > 0)
                            resultString += String.fromCharCode(r);
                    }
                    return resultString;
                };
                HWFParser.prototype._parsePmiData = function (size) {
                    var pmiDataString = this._readString(size);
                    var pmiPieces = pmiDataString.split(',');
                    for (var j = 0; j < pmiPieces.length; j++) {
                        if (pmiPieces[j] === "")
                            break;
                        var pieceData = pmiPieces[j].split(' ');
                        var regionInfo = new Internal.RegionInfo(parseInt(pieceData[0]), parseInt(pieceData[1]));
                        var pmiInfoSeg = this._segmentPropertyManager.current().resolvePmiInfoSegment();
                        if (pmiInfoSeg !== undefined && pmiInfoSeg.pmiInfo !== undefined)
                            pmiInfoSeg.pmiInfo.refsToBrep.push(regionInfo);
                    }
                };
                HWFParser.prototype._parsePrototypeMap = function () {
                    // first skip over unused ID -> prototype map
                    var idCount = this._readUnsignedInt();
                    for (var j = 0; j < idCount; j++) {
                        var aidLength = this._readUnsignedInt();
                        if (aidLength === 0)
                            break;
                        /* var id = */ this._readUnsignedInt();
                        for (var k = 0; k < aidLength; k++)
                            this._readUnsignedInt();
                    }
                    // read protoype -> mapId array that we currently use
                    for (var j = 0; j < idCount; j++) {
                        var aidLength = this._readUnsignedInt();
                        if (aidLength === 0)
                            break;
                        /* var id = */ this._readUnsignedInt();
                        /*
                        var mappingArray = new Array();
                        for (var ii=0;ii<aidlength;ii++)
                            mappingArray.push(this.toIntFromArray(byteArray));
                        this.idConverter.insertAssociativity(this.currentModelId, id, mappingArray);
                        */
                        for (var k = 0; k < aidLength; k++)
                            this._readUnsignedInt();
                    }
                };
                HWFParser.prototype._parseBrepInfo = function (geometryText) {
                    var model = this._viewer.model;
                    var sp = this._segmentPropertyManager.current();
                    var bodyId = sp.resolveParentNodeId(model, Communicator.NodeType.Body);
                    if (bodyId === undefined) {
                        console.assert(false, "internal logic error");
                        return;
                    }
                    var temp = geometryText.split(",");
                    var measurementBitsOnEdge;
                    var measurementBitsOnFace;
                    if (temp.length > 0) {
                        var partOrRISegment = sp.resolvePartOrRepItemSegment();
                        if (partOrRISegment) {
                            partOrRISegment.measurementBitsOnEdge = [];
                            partOrRISegment.measurementBitsOnFace = [];
                            measurementBitsOnEdge = partOrRISegment.measurementBitsOnEdge;
                            measurementBitsOnFace = partOrRISegment.measurementBitsOnFace;
                        }
                    }
                    for (var j = 0; j < temp.length; j++) {
                        // MRL 04/02/2018: geometryText text above may contain a trailing ',' character. This can cause an empty string to appear in the
                        // temp array that is created by splitting geometry text on ','  In this case we simply want to continue on our way as the code as written
                        // below assumes a valid text format.
                        if (temp[j].length == 0) {
                            continue;
                        }
                        var temp2 = temp[j].split(" ");
                        var elementIndex = parseInt(temp2[0]);
                        var type = parseInt(temp2[1]);
                        if (type === Internal.GeometryElementType.CYLINDER) {
                            var radius = parseFloat(temp2[2]);
                            var origin = new Communicator.Point3(parseFloat(temp2[3]), parseFloat(temp2[4]), parseFloat(temp2[5]));
                            var normal = new Communicator.Point3(parseFloat(temp2[6]), parseFloat(temp2[7]), parseFloat(temp2[8]));
                            var cylinder = new Communicator.SubentityProperties.CylinderElement(radius, origin, normal);
                            model.setFaceProperty(bodyId, elementIndex, cylinder);
                            if (measurementBitsOnFace)
                                measurementBitsOnFace[elementIndex] = 1 /* SelectionBitsFaceHasMeasurementData */;
                        }
                        else if (type === Internal.GeometryElementType.PLANE) {
                            var origin = new Communicator.Point3(parseFloat(temp2[2]), parseFloat(temp2[3]), parseFloat(temp2[4]));
                            var normal = new Communicator.Point3(parseFloat(temp2[5]), parseFloat(temp2[6]), parseFloat(temp2[7]));
                            var plane = new Communicator.SubentityProperties.PlaneElement(origin, normal);
                            model.setFaceProperty(bodyId, elementIndex, plane);
                            if (measurementBitsOnFace)
                                measurementBitsOnFace[elementIndex] = 2 /* SelectionBitsFacePlanar */ | 1 /* SelectionBitsFaceHasMeasurementData */;
                        }
                        else if (type === Internal.GeometryElementType.CONE) {
                            var radius = parseFloat(temp2[2]);
                            var origin = new Communicator.Point3(parseFloat(temp2[3]), parseFloat(temp2[4]), parseFloat(temp2[5]));
                            var normal = new Communicator.Point3(parseFloat(temp2[6]), parseFloat(temp2[7]), parseFloat(temp2[8]));
                            var halfAngle = parseFloat(temp2[9]);
                            var cone = new Communicator.SubentityProperties.ConeElement(radius, origin, normal, halfAngle);
                            model.setFaceProperty(bodyId, elementIndex, cone);
                            if (measurementBitsOnFace)
                                measurementBitsOnFace[elementIndex] = 1 /* SelectionBitsFaceHasMeasurementData */;
                        }
                        else if (type === Internal.GeometryElementType.CIRCLE) {
                            var radius = parseFloat(temp2[2]);
                            var origin = new Communicator.Point3(parseFloat(temp2[3]), parseFloat(temp2[4]), parseFloat(temp2[5]));
                            var normal = new Communicator.Point3(parseFloat(temp2[6]), parseFloat(temp2[7]), parseFloat(temp2[8]));
                            var circle = new Communicator.SubentityProperties.CircleElement(radius, origin, normal);
                            model.setEdgeProperty(bodyId, elementIndex, circle);
                            if (measurementBitsOnEdge)
                                measurementBitsOnEdge[elementIndex] = 4 /* SelectionBitsEdgeHasMeasurementData */;
                        }
                        else if (temp2[2] !== undefined) {
                            var length_1 = parseFloat(temp2[2]);
                            var line = new Communicator.SubentityProperties.LineElement(length_1);
                            model.setEdgeProperty(bodyId, elementIndex, line);
                            if (measurementBitsOnEdge)
                                measurementBitsOnEdge[elementIndex] = 4 /* SelectionBitsEdgeHasMeasurementData */;
                        }
                        else {
                            var length_2 = -1; // On HC 2015, we were measuring straight edges by directly getting tessellation points... so have to do the same on HC 2016 (so "-1" will trigger measurement using tess point)
                            var line = new Communicator.SubentityProperties.LineElement(length_2);
                            model.setEdgeProperty(bodyId, elementIndex, line);
                            if (measurementBitsOnEdge)
                                measurementBitsOnEdge[elementIndex] = 4 /* SelectionBitsEdgeHasMeasurementData */;
                        }
                    }
                };
                HWFParser.prototype._parseUserOptionsString = function (allUserOptions) {
                    var userOptionsParts = allUserOptions.split(',');
                    var userOptionsArray = [];
                    for (var i = 0; i < userOptionsParts.length; i++) {
                        var userOption = userOptionsParts[i];
                        if (userOption.indexOf("brep_properties") === -1 && userOption.indexOf("hx3d_type") === -1) {
                            var parsedOption = Internal.Util.Utf8.decode(userOption.replace(/\`+/g, '')).replace(/\\/g, '/');
                            userOptionsArray.push(parsedOption);
                        }
                    }
                    return userOptionsArray;
                };
                HWFParser.prototype._readByteArray = function (byteArray, length) {
                    for (var i = 0; i < length; i++) {
                        byteArray[i] = this._readByte();
                    }
                };
                HWFParser.prototype._readFloatArray = function (floatArray, count) {
                    for (var i = 0; i < count; i++) {
                        floatArray.push(this._readFloat());
                    }
                };
                HWFParser.prototype._readIntArray = function (intArray, count) {
                    for (var i = 0; i < count; i++) {
                        intArray.push(this._readInt());
                    }
                };
                HWFParser.prototype._readUnsignedShortArray = function (shortArray, count) {
                    for (var i = 0; i < count; i++) {
                        shortArray.push(this._readUnsignedShort());
                    }
                };
                HWFParser.prototype._readInt = function () {
                    var result = this._dataView.getInt32(this._currentBytePosition, true);
                    this._currentBytePosition += 4;
                    return result;
                };
                HWFParser.prototype._parseUVPairsFromTriples = function (dest, paramDataLength, paramWidth) {
                    for (var i = 0; i < paramDataLength; i += paramWidth) {
                        dest.push(this._readFloat());
                        dest.push(this._readFloat());
                        this._currentBytePosition += 4 * (paramWidth - 2);
                    }
                };
                return HWFParser;
            }());
            Internal.HWFParser = HWFParser;
        })(Internal = HWF.Internal || (HWF.Internal = {}));
    })(HWF = Communicator.HWF || (Communicator.HWF = {}));
})(Communicator || (Communicator = {}));
/// <reference path="js/hoops_web_viewer.d.ts"/>
var Communicator;
(function (Communicator) {
    var HWF;
    (function (HWF) {
        var Importer = /** @class */ (function () {
            function Importer(viewer) {
                this._viewer = viewer;
            }
            Importer.prototype.import = function (importOptions) {
                var _this = this;
                var importCompletePromise = Communicator.Internal.OpenPromise.create();
                var request = new XMLHttpRequest();
                request.open("GET", importOptions.url, true);
                request.responseType = "arraybuffer";
                if (importOptions.XHRonprogress) {
                    request.onprogress = importOptions.XHRonprogress;
                }
                if (importOptions.XHRonerror) {
                    request.onerror = importOptions.XHRonerror;
                }
                if (importOptions.XHRonloadend) {
                    var onloadend_1 = importOptions.XHRonloadend;
                    request.onloadend = function (e) {
                        onloadend_1(e, request.status);
                    };
                }
                request.onreadystatechange = function () {
                    if (request.readyState == 4) {
                        if (request.status == 404) {
                            _this._fileLoadError().then(function () {
                                importCompletePromise.reject(new Communicator.LoadError("Failed to import HWF file."));
                            });
                        }
                        else {
                            _this._fileLoadSucess(request.response).then(function () {
                                importCompletePromise.resolve();
                            });
                        }
                    }
                };
                request.send(null);
                return importCompletePromise;
            };
            Importer.prototype._fileLoadSucess = function (arrayBuffer) {
                var parser = new HWF.Internal.HWFParser(this._viewer);
                return parser.parse(arrayBuffer);
            };
            Importer.prototype._fileLoadError = function () {
                return Promise.resolve();
            };
            return Importer;
        }());
        HWF.Importer = Importer;
    })(HWF = Communicator.HWF || (Communicator.HWF = {}));
})(Communicator || (Communicator = {}));
var Communicator;
(function (Communicator) {
    var HWF;
    (function (HWF) {
        var Internal;
        (function (Internal) {
            var nullPromise = Promise.resolve(null);
            var InstancedDataManager = /** @class */ (function () {
                function InstancedDataManager() {
                    this.tagsOfAlreadyCreatedMergedMesh = [];
                    this.registrationIndexToMeshCreatePromise = [];
                    this.registrationIndexToMeshID = [];
                }
                InstancedDataManager.prototype.Register = function (tagsList) {
                    this.registrationIndexToMeshCreatePromise.push(nullPromise);
                    this.registrationIndexToMeshID.push(undefined);
                    return this.tagsOfAlreadyCreatedMergedMesh.push(tagsList) - 1;
                };
                InstancedDataManager.prototype.getRegistrationIndex = function (inputTags) {
                    for (var i = 0; i < this.tagsOfAlreadyCreatedMergedMesh.length; i++) {
                        var tags = this.tagsOfAlreadyCreatedMergedMesh[i];
                        var match = true;
                        if (tags.length !== inputTags.length)
                            match = false;
                        for (var j = 0; match && (j < tags.length); j++) {
                            // Is the tag present in the inputTags?
                            match = inputTags.indexOf(tags[j]) !== -1;
                        }
                        if (match)
                            return i;
                    }
                    return undefined;
                };
                return InstancedDataManager;
            }());
            Internal.InstancedDataManager = InstancedDataManager;
        })(Internal = HWF.Internal || (HWF.Internal = {}));
    })(HWF = Communicator.HWF || (Communicator.HWF = {}));
})(Communicator || (Communicator = {}));
var Communicator;
(function (Communicator) {
    var HWF;
    (function (HWF) {
        var ImportOptions = /** @class */ (function () {
            function ImportOptions() {
            }
            return ImportOptions;
        }());
        HWF.ImportOptions = ImportOptions;
    })(HWF = Communicator.HWF || (Communicator.HWF = {}));
})(Communicator || (Communicator = {}));
var Communicator;
(function (Communicator) {
    var HWF;
    (function (HWF) {
        var Internal;
        (function (Internal) {
            ;
            var SegmentProperty = /** @class */ (function () {
                function SegmentProperty(parent, id) {
                    this.children = [];
                    this.visible = true;
                    this.parent = parent;
                    this.segmentId = id;
                    if (parent) {
                        parent.children.push(this);
                    }
                }
                SegmentProperty.prototype.resolveParentNodeId = function (model, nodeType) {
                    var node = this;
                    do {
                        if (!!node.assemblyNodeId && model.getNodeType(node.assemblyNodeId) === nodeType)
                            return node.assemblyNodeId;
                        node = node.parent;
                    } while (node);
                    return undefined;
                };
                SegmentProperty.prototype.resolveColor = function () {
                    var node = this;
                    do {
                        if (node.color !== undefined)
                            return node.color;
                        node = node.parent;
                    } while (node);
                    return undefined;
                };
                SegmentProperty.prototype.resolveTransparency = function () {
                    var node = this;
                    do {
                        if (node.transparency !== undefined)
                            return node.transparency;
                        node = node.parent;
                    } while (node);
                    return 255;
                };
                SegmentProperty.prototype.resolveTextureName = function () {
                    var node = this;
                    do {
                        if (node.textureName !== undefined)
                            return node.textureName;
                        node = node.parent;
                    } while (node);
                    return undefined;
                };
                SegmentProperty.prototype.resolveName = function () {
                    var node = this;
                    do {
                        if (node.segmentName !== undefined && node.segmentName.length > 0)
                            return node.segmentName;
                        node = node.parent;
                    } while (node);
                    return undefined;
                };
                SegmentProperty.prototype.resolveMatrix = function () {
                    var node = this;
                    do {
                        if (node.matrix !== undefined)
                            return node.matrix;
                        node = node.parent;
                    } while (node);
                    return undefined;
                };
                SegmentProperty.prototype.resolvePmiInfoSegment = function () {
                    var node = this;
                    do {
                        if (node.pmiInfo !== undefined)
                            return node;
                        node = node.parent;
                    } while (node);
                    return undefined;
                };
                SegmentProperty.prototype.resolvePartOrRepItemSegment = function () {
                    var node = this;
                    do {
                        if (node.segmentName !== undefined) {
                            if (node.segmentName.indexOf("part") !== -1)
                                return node;
                            else if (node.segmentName.indexOf("representation item") !== -1)
                                return node;
                        }
                        node = node.parent;
                    } while (node);
                    return undefined;
                };
                SegmentProperty.prototype.computeNetMatrix = function () {
                    var matrix = new Communicator.Matrix;
                    var segment = this;
                    while (segment !== null) {
                        if (segment.matrix !== undefined)
                            matrix = Communicator.Matrix.multiply(matrix, segment.matrix);
                        segment = segment.parent;
                    }
                    return matrix;
                };
                /** build one body from the multiples entries stored in "meshDataToCreate" */
                SegmentProperty.prototype.buildMergedMeshData = function (meshDataToAppendOnto, transformVertices) {
                    var meshData = meshDataToAppendOnto;
                    if (this.meshDataToCreate === undefined) {
                        return meshData;
                    }
                    for (var i = 0; i < this.meshDataToCreate.length; i++) {
                        var data = this.meshDataToCreate[i];
                        var points = data.points;
                        var normals = data.normals;
                        var indices = data.indices;
                        var vertexColors = data.vertexColors;
                        var uvs = data.uvs;
                        var transparency = data.segmentDataCameFrom.resolveTransparency();
                        var color = data.segmentDataCameFrom.resolveColor();
                        if (color === undefined)
                            color = new Communicator.Color(192, 192, 192);
                        // transform points to world space
                        var matrix = data.segmentDataCameFrom.computeNetMatrix();
                        if (transformVertices) {
                            // Transform vertices
                            var vertex = new Communicator.Point3(0, 0, 0);
                            for (var j = 0; j < points.length; j += 3) {
                                vertex.set(points[j], points[j + 1], points[j + 2]);
                                matrix.transform(vertex, vertex);
                                points[j] = vertex.x;
                                points[j + 1] = vertex.y;
                                points[j + 2] = vertex.z;
                            }
                            // Transform normals
                            if (normals !== undefined) {
                                var normalMatrix = matrix.normalMatrix();
                                for (var j = 0; j < normals.length; j += 3) {
                                    vertex.set(normals[j], normals[j + 1], normals[j + 2]);
                                    if (normalMatrix !== null) {
                                        normalMatrix.transform(vertex, vertex);
                                    }
                                    vertex.normalize();
                                    normals[j] = vertex.x;
                                    normals[j + 1] = vertex.y;
                                    normals[j + 2] = vertex.z;
                                }
                            }
                        }
                        // Create mesh data
                        if (meshData === undefined)
                            meshData = new Communicator.MeshData();
                        if (this.meshDataToCreate[i].wireframe)
                            Internal.Data.generateLineDataFromPointsAndIndices(points, indices[0], color, transparency, meshData, this.measurementBitsOnEdge);
                        else
                            Internal.Data.generateFaceData(points, indices, normals, vertexColors, uvs, color, transparency, meshData, this.measurementBitsOnFace);
                    }
                    return meshData;
                };
                return SegmentProperty;
            }());
            Internal.SegmentProperty = SegmentProperty;
            var SegmentPropertyManager = /** @class */ (function () {
                function SegmentPropertyManager() {
                    this._segmentStack = [];
                    this._allSegments = [];
                    this._idToSegmentMap = {};
                    this._segmentIDGenerator = 0;
                }
                SegmentPropertyManager.prototype.open = function () {
                    var parent = null;
                    if (this._segmentStack.length > 0)
                        parent = this._segmentStack[this._segmentStack.length - 1];
                    var segmentProperty = new SegmentProperty(parent, this._segmentIDGenerator++);
                    this._segmentStack.push(segmentProperty);
                    this._idToSegmentMap[segmentProperty.segmentId] = segmentProperty;
                    this._allSegments.push(segmentProperty);
                    return segmentProperty;
                };
                SegmentPropertyManager.prototype.close = function () {
                    return this._segmentStack.pop();
                };
                SegmentPropertyManager.prototype.current = function () {
                    if (this._segmentStack.length > 0) {
                        return this._segmentStack[this._segmentStack.length - 1];
                    }
                    else {
                        return this.open();
                    }
                };
                SegmentPropertyManager.prototype.getFromID = function (id) {
                    return this._idToSegmentMap[id];
                };
                SegmentPropertyManager.prototype.getSegments = function () {
                    return this._allSegments;
                };
                return SegmentPropertyManager;
            }());
            Internal.SegmentPropertyManager = SegmentPropertyManager;
        })(Internal = HWF.Internal || (HWF.Internal = {}));
    })(HWF = Communicator.HWF || (Communicator.HWF = {}));
})(Communicator || (Communicator = {}));
var Communicator;
(function (Communicator) {
    var HWF;
    (function (HWF) {
        var Internal;
        (function (Internal) {
            var Util;
            (function (Util) {
                var Utf8;
                (function (Utf8) {
                    function decode(utftext) {
                        var utf8str = "";
                        var i = 0;
                        var c = 0, c2 = 0, c3 = 0;
                        while (i < utftext.length) {
                            c = utftext.charCodeAt(i);
                            if (c < 128) {
                                utf8str += String.fromCharCode(c);
                                i++;
                            }
                            else if (191 < c && c < 224) {
                                c2 = utftext.charCodeAt(i + 1);
                                utf8str += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
                                i += 2;
                            }
                            else {
                                c2 = utftext.charCodeAt(i + 1);
                                c3 = utftext.charCodeAt(i + 2);
                                utf8str += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
                                i += 3;
                            }
                        }
                        return utf8str;
                    }
                    Utf8.decode = decode;
                })(Utf8 = Util.Utf8 || (Util.Utf8 = {}));
                function capitaliseFirst(str) {
                    return str.charAt(0).toUpperCase() + str.slice(1);
                }
                Util.capitaliseFirst = capitaliseFirst;
                function extractUserOpt(userOptionsString, optName) {
                    var index = userOptionsString.indexOf(optName + "=");
                    if (index >= 0) {
                        var opt = userOptionsString.substr(index + optName.length + 1);
                        index = opt.indexOf(',');
                        if (index >= 0) {
                            opt = opt.substr(0, index);
                        }
                        return opt;
                    }
                    else {
                        return null;
                    }
                }
                Util.extractUserOpt = extractUserOpt;
                function normalsPolarToCartesian(count, polarNormals, cartesianNormals) {
                    for (var i = 0; i < count; i++) {
                        var theta = polarNormals[i * 2];
                        var phi = polarNormals[i * 2 + 1];
                        var sinPhi = Math.sin(phi);
                        cartesianNormals[i * 3] = sinPhi * Math.cos(theta);
                        cartesianNormals[i * 3 + 1] = Math.cos(phi);
                        cartesianNormals[i * 3 + 2] = sinPhi * Math.sin(theta);
                    }
                }
                Util.normalsPolarToCartesian = normalsPolarToCartesian;
                function unquantizeAndUnpackFloats(count, size, bitsPerSample, bounding, buf, outFloats) {
                    var range = [];
                    var fptr = outFloats;
                    var pack = new Internal.BPack();
                    if (bitsPerSample === 8 && size === 3) {
                        trivialDecompressPoints(count, buf, outFloats, bounding);
                        return;
                    }
                    var max = (1 << bitsPerSample) - 1;
                    var inverseMax = 1.0 / max;
                    for (var i = 0; i < size; i++) {
                        range[i] = (bounding[size + i] - bounding[i]) * inverseMax;
                    }
                    var length = Math.floor((size * count * bitsPerSample + 31) / 32) * 4;
                    pack.InitRead(length, buf);
                    var cpos = 0;
                    while (count--) {
                        for (var i = 0; i < size; i++) {
                            var val = pack.Get(bitsPerSample);
                            if (val != max) {
                                fptr[i + cpos] = val * range[i] + bounding[i];
                            }
                            else
                                fptr[i + cpos] = bounding[i + size]; // directly set to max -- avoid fp rounding error
                        }
                        cpos += size;
                    }
                }
                Util.unquantizeAndUnpackFloats = unquantizeAndUnpackFloats;
                function trivialDecompressPoints(pointcount, buf, outPoints, bounding) {
                    var fptr = outPoints;
                    var cptr = buf;
                    var bitsPerSample = 8;
                    var inverseMax = 1.0 / ((1 << bitsPerSample) - 1);
                    var min = [
                        bounding[0],
                        bounding[1],
                        bounding[2],
                    ];
                    var range = [
                        (bounding[3] - min[0]) * inverseMax,
                        (bounding[4] - min[1]) * inverseMax,
                        (bounding[5] - min[2]) * inverseMax,
                    ];
                    var fp = 0;
                    var cp = 0;
                    while (pointcount--) {
                        if (cptr[0 + cp] === 255)
                            fptr[0 + fp] = bounding[3]; // avoid fp rounding error
                        else
                            fptr[0 + fp] = cptr[0 + cp] * range[0] + min[0];
                        if (cptr[1 + cp] === 255)
                            fptr[1 + fp] = bounding[4]; // avoid fp rounding error
                        else
                            fptr[1 + fp] = cptr[1 + cp] * range[1] + min[1];
                        if (cptr[2 + cp] === 255)
                            fptr[2 + fp] = bounding[5]; // avoid fp rounding error
                        else
                            fptr[2 + fp] = cptr[2 + cp] * range[2] + min[2];
                        fp += 3;
                        cp += 3;
                    }
                }
                Util.trivialDecompressPoints = trivialDecompressPoints;
                function X_2D(subop) {
                    return subop & Internal.TKPP.X_2D_MASK;
                }
                function Y_2D(subop) {
                    return subop & Internal.TKPP.Y_2D_MASK;
                }
                function Z_2D(subop) {
                    return subop & Internal.TKPP.Z_2D_MASK;
                }
                function figureNumDimensions(hints) {
                    var numDimensions = 0;
                    if (!X_2D(hints))
                        numDimensions++;
                    if (!Y_2D(hints))
                        numDimensions++;
                    if (!Z_2D(hints))
                        numDimensions++;
                    return numDimensions;
                }
                Util.figureNumDimensions = figureNumDimensions;
                function figureNumFromFloats(subop, primitiveCount, pointCount) {
                    var pointsNumFloats = 0;
                    if (primitiveCount === 0)
                        return 0;
                    // note that it is ok to use bitwise AND here on m_subop because we account for the possibility that multiple bits are set
                    switch (subop & Internal.TKPP.X_2D_MASK) {
                        case Internal.TKPP.ZERO_X:
                            break;
                        case Internal.TKPP.SAME_X:
                            ++pointsNumFloats;
                            break;
                        case Internal.TKPP.PER_PRIMITIVE_X:
                            pointsNumFloats += primitiveCount;
                            break;
                        case 0:
                            pointsNumFloats += pointCount;
                            break;
                    }
                    switch (subop & Internal.TKPP.Y_2D_MASK) {
                        case Internal.TKPP.ZERO_Y:
                            break;
                        case Internal.TKPP.SAME_Y:
                            ++pointsNumFloats;
                            break;
                        case Internal.TKPP.PER_PRIMITIVE_Y:
                            pointsNumFloats += primitiveCount;
                            break;
                        case 0:
                            pointsNumFloats += pointCount;
                            break;
                    }
                    switch (subop & Internal.TKPP.Z_2D_MASK) {
                        case Internal.TKPP.ZERO_Z:
                            break;
                        case Internal.TKPP.SAME_Z:
                            ++pointsNumFloats;
                            break;
                        case Internal.TKPP.PER_PRIMITIVE_Z:
                            pointsNumFloats += primitiveCount;
                            break;
                        case 0:
                            pointsNumFloats += pointCount;
                            break;
                    }
                    return pointsNumFloats;
                }
                Util.figureNumFromFloats = figureNumFromFloats;
                function PER_PRIMITIVE_X(subop) {
                    return (subop & Internal.TKPP.X_2D_MASK) === Internal.TKPP.PER_PRIMITIVE_X;
                }
                function ZERO_X(subop) {
                    return (subop & Internal.TKPP.X_2D_MASK) === Internal.TKPP.ZERO_X;
                }
                function SAME_X(subop) {
                    return (subop & Internal.TKPP.X_2D_MASK) === Internal.TKPP.SAME_X;
                }
                function PER_PRIMITIVE_Y(subop) {
                    return (subop & Internal.TKPP.Y_2D_MASK) === Internal.TKPP.PER_PRIMITIVE_Y;
                }
                function ZERO_Y(subop) {
                    return (subop & Internal.TKPP.Y_2D_MASK) === Internal.TKPP.ZERO_Y;
                }
                function SAME_Y(subop) {
                    return (subop & Internal.TKPP.Y_2D_MASK) === Internal.TKPP.SAME_Y;
                }
                function PER_PRIMITIVE_Z(subop) {
                    return (subop & Internal.TKPP.Z_2D_MASK) === Internal.TKPP.PER_PRIMITIVE_Z;
                }
                function ZERO_Z(subop) {
                    return (subop & Internal.TKPP.Z_2D_MASK) === Internal.TKPP.ZERO_Z;
                }
                function SAME_Z(subop) {
                    return (subop & Internal.TKPP.Z_2D_MASK) === Internal.TKPP.SAME_Z;
                }
                //inverts the reorganization done by mangle_floats
                function unmangleFloats(pointCount, points, primitiveCount, lengths, hintMask, outPoints) {
                    var pp = 0;
                    for (var i = 0; i < pointCount; i++) {
                        if (!X_2D(hintMask))
                            outPoints[i * 3 + 0] = points[pp++];
                        if (!Y_2D(hintMask))
                            outPoints[i * 3 + 1] = points[pp++];
                        if (!Z_2D(hintMask))
                            outPoints[i * 3 + 2] = points[pp++];
                    }
                    var ppout = 0;
                    if (X_2D(hintMask)) {
                        ppout = 0;
                        if (PER_PRIMITIVE_X(hintMask)) {
                            for (var i = 0; i < primitiveCount; i++) {
                                for (var j = 0; j < lengths[i]; j++) {
                                    outPoints[ppout] = points[pp];
                                    ppout += 3;
                                }
                                pp++;
                            }
                        }
                        else if (SAME_X(hintMask)) {
                            for (var i = 0; i < pointCount; i++) {
                                outPoints[ppout] = points[pp];
                                ppout += 3;
                            }
                            pp++;
                        }
                        else if (ZERO_X(hintMask)) {
                            for (var i = 0; i < pointCount; i++) {
                                outPoints[ppout] = 0;
                                ppout += 3;
                            }
                        }
                    }
                    if (Y_2D(hintMask)) {
                        ppout = 1;
                        if (PER_PRIMITIVE_Y(hintMask)) {
                            for (var i = 0; i < primitiveCount; i++) {
                                for (var j = 0; j < lengths[i]; j++) {
                                    outPoints[ppout] = points[pp];
                                    ppout += 3;
                                }
                                pp++;
                            }
                        }
                        else if (SAME_Y(hintMask)) {
                            for (var i = 0; i < pointCount; i++) {
                                outPoints[ppout] = points[pp];
                                ppout += 3;
                            }
                            pp++;
                        }
                        else if (ZERO_Y(hintMask)) {
                            for (var i = 0; i < pointCount; i++) {
                                outPoints[ppout] = 0;
                                ppout += 3;
                            }
                        }
                    }
                    if (Z_2D(hintMask)) {
                        ppout = 2;
                        if (PER_PRIMITIVE_Z(hintMask)) {
                            for (var i = 0; i < primitiveCount; i++) {
                                for (var j = 0; j < lengths[i]; j++) {
                                    outPoints[ppout] = points[pp];
                                    ppout += 3;
                                }
                                pp++;
                            }
                        }
                        else if (SAME_Z(hintMask)) {
                            for (var i = 0; i < pointCount; i++) {
                                outPoints[ppout] = points[pp];
                                ppout += 3;
                            }
                            pp++;
                        }
                        else if (ZERO_Z(hintMask)) {
                            for (var i = 0; i < pointCount; i++) {
                                outPoints[ppout] = 0;
                                ppout += 3;
                            }
                        }
                    }
                }
                Util.unmangleFloats = unmangleFloats;
            })(Util = Internal.Util || (Internal.Util = {}));
        })(Internal = HWF.Internal || (HWF.Internal = {}));
    })(HWF = Communicator.HWF || (Communicator.HWF = {}));
})(Communicator || (Communicator = {}));
var Communicator;
(function (Communicator) {
    var HWF;
    (function (HWF) {
        var Internal;
        (function (Internal) {
            var VisibilitySettings = /** @class */ (function () {
                function VisibilitySettings() {
                }
                return VisibilitySettings;
            }());
            Internal.VisibilitySettings = VisibilitySettings;
            var Visibility = /** @class */ (function () {
                function Visibility(setting) {
                    this._settings = new VisibilitySettings();
                    if (setting !== undefined) {
                        this._settings[setting] = true;
                    }
                }
                Visibility.EMPTY = function () {
                    return new Visibility();
                };
                Visibility.EVERYTHING = function () {
                    return new Visibility("everything");
                };
                Visibility.GEOMETRY = function () {
                    return new Visibility("geometry");
                };
                Visibility.FACES = function () {
                    return new Visibility("faces");
                };
                Visibility.LINES = function () {
                    return new Visibility("lines");
                };
                Visibility.TEXT = function () {
                    return new Visibility("text");
                };
                Visibility.deserialize = function (visStr) {
                    var v = new Visibility();
                    var cs = visStr.split("");
                    v._settings.everything = v.deserialize_(cs);
                    v._settings.geometry = v.deserialize_(cs);
                    v.skipToNextGrouping(cs);
                    v._settings.faces = v.deserialize_(cs);
                    v._settings.lines = v.deserialize_(cs);
                    v._settings.text = v.deserialize_(cs);
                    return v;
                };
                Visibility.prototype.skipToNextGrouping = function (cs) {
                    var SERIALIZE_DELIM = '-';
                    while (cs[0] !== SERIALIZE_DELIM) {
                        cs.shift();
                    }
                    cs.shift();
                };
                Visibility.prototype.deserialize_ = function (cs) {
                    var c = cs.shift();
                    if (c === '1')
                        return true;
                    if (c === '0')
                        return false;
                    return undefined;
                };
                Visibility.prototype.everythingOn = function () {
                    return this.equals(Visibility.EVERYTHING());
                };
                Visibility.prototype.everythingOff = function () {
                    return this.equals(Visibility.EVERYTHING().not());
                };
                Visibility.prototype.empty = function () {
                    return this.equals(Visibility.EMPTY());
                };
                Visibility.prototype.not = function () {
                    var v = new Visibility();
                    for (var i = 0; i < Visibility.allSettings.length; ++i) {
                        var mem = Visibility.allSettings[i];
                        v._settings[mem] = this.not_(this._settings[mem]);
                    }
                    return v;
                };
                Visibility.prototype.not_ = function (setting) {
                    if (setting === true)
                        return false;
                    if (setting === false)
                        return true;
                    return undefined;
                };
                Visibility.prototype.toHoopsStr = function () {
                    var str = "";
                    str += this.toStr("everything");
                    str += this.toStr("geometry");
                    str += this.toStr("faces");
                    str += this.toStr("lines");
                    str += this.toStr("text");
                    return str;
                };
                ;
                Visibility.prototype.toStr = function (setting) {
                    if (this._settings[setting] === true) {
                        return setting.substr(1) + "=on";
                    }
                    if (this._settings[setting] === false) {
                        return setting.substr(1) + "=off";
                    }
                    return "";
                };
                Visibility.prototype.toString = function () {
                    return this.toHoopsStr();
                };
                Visibility.prototype.equals = function (other) {
                    for (var i = 0; i < Visibility.allSettings.length; ++i) {
                        var mem = Visibility.allSettings[i];
                        if (this._settings[mem] !== other._settings[mem]) {
                            return false;
                        }
                    }
                    return true;
                };
                Visibility.aggregateSettings = ["everything", "geometry"];
                Visibility.simpleSettings = ["faces", "lines", "text"];
                Visibility.allSettings = Visibility.aggregateSettings.concat(Visibility.simpleSettings);
                return Visibility;
            }());
            Internal.Visibility = Visibility;
        })(Internal = HWF.Internal || (HWF.Internal = {}));
    })(HWF = Communicator.HWF || (Communicator.HWF = {}));
})(Communicator || (Communicator = {}));
