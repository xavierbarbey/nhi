function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\?&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(window.location.search);
    if (results === null)
        return null;
    else
        return decodeURIComponent(results[1]);
}

var Sample = {

    applyExtraProperties: function (obj) {
        if (typeof SC_EXTRA_PROPERTIES === "undefined") {
            SC_EXTRA_PROPERTIES = {};
        }

        var extras = SC_EXTRA_PROPERTIES;
        var props = Object.keys(extras);
        for (var i = 0; i < props.length; ++i) {
            var prop = props[i];
            obj[prop] = extras[prop];
        }
    },

    getStreamingMode: function () {
        if (typeof SC_STREAMING_MODE !== "undefined")
            return SC_STREAMING_MODE;

        var streamingMode = getParameterByName("streamingMode");

        switch (streamingMode) {
            case "interactive":
                return Communicator.StreamingMode.Interactive;

            case "all":
                return Communicator.StreamingMode.All;

            case "ondemand":
                return Communicator.StreamingMode.OnDemand;
        }

        return Communicator.StreamingMode.Interactive;
    },

    getRendererType: function () {
        if (typeof SC_RENDERER_TYPE !== "undefined")
            return SC_RENDERER_TYPE;

        var val = getParameterByName("viewer");

        if (val == "ssr")
            return Communicator.RendererType.Server;
        else
            return Communicator.RendererType.Client;
    },

    getLayout: function (){
        var layout = null;

        if (typeof LAYOUT !== "undefined") {
            layout = LAYOUT;
        }
        else {
            layout = getParameterByName("layout");
        }

        return layout;
    },

    getModel: function () {
        var modelName = null;

        if (typeof SC_MODEL !== "undefined") {
            modelName = SC_MODEL;
        }
        else {
            modelName = getParameterByName("instance");
            if (!modelName)
                modelName = getParameterByName("model");
        }
        
        return modelName;
    },

    getMemoryLimit: function () {
        if (typeof SC_MEMORY_LIMIT !== "undefined")
            return SC_MEMORY_LIMIT;

        var memoryLimit = getParameterByName("memoryLimit");

        if (memoryLimit === null)
            return 0;
        else
            return parseInt(memoryLimit);
    },

    _getProxy: function () {
        var proxy = getParameterByName("proxy");
        if (proxy === null) {
            return false;
        }
        else {
            return true;
        }
    },

    _rewrite: function(endpoint){
        var regex = /([ws]+):\/\/(.*):([0-9]+)/;
        var matches = regex.exec(endpoint);

        var protocol = matches[1];
        var host = matches[2];
        var port = matches[3];

        return protocol + "://" + host + "/" + protocol + "proxy/" + port;
    },

    _createStreamingViewer: function (serviceBrokerUri) {
        var rendererType = Sample.getRendererType();

        var serviceBroker = new Communicator.ServiceBroker(serviceBrokerUri);
        var serviceRequest = new Communicator.ServiceRequest(
            rendererType == Communicator.RendererType.Client ? Communicator.ServiceClass.CSR_Session : Communicator.ServiceClass.SSR_Session);

        return new Promise(function (resolve, reject) {
            serviceBroker.request(serviceRequest).then(function (serviceResponse) {
                if (serviceResponse.getIsOk()) {
                    var serviceProtocol = serviceResponse.getEndpoints().hasOwnProperty(Communicator.ServiceProtocol.WS) ? Communicator.ServiceProtocol.WS : Communicator.ServiceProtocol.WSS;
                    var clientEndpoint = serviceResponse.getEndpoints()[serviceProtocol];

                    if (Sample._getProxy()) {
                        clientEndpoint = Sample._rewrite(clientEndpoint);
                    }

                    var config = {
                        containerId: "viewerContainer",
                        endpointUri: clientEndpoint,
                        model: Sample.getModel(),
                        rendererType: rendererType,
                        streamingMode: Sample.getStreamingMode(),
                        memoryLimit: Sample.getMemoryLimit()
                    };
                    Sample.applyExtraProperties(config);
                    var viewer = new Communicator.WebViewer(config);

                    resolve(viewer);
                }
                else {
                    reject(serviceResponse.getReason());
                }
            },
            function (serviceResponse) {
                reject("Unable to connect to Service Broker at: " + serviceBrokerUri);
            });
        });
    },

    _createScsViewer: function (scsFile) {
        return new Promise(function (resolve, reject) {
            var config = {
                containerId: "viewerContainer",
                endpointUri: scsFile,
            };
            Sample.applyExtraProperties(config);
            var viewer = new Communicator.WebViewer(config);

            resolve(viewer);
        });
    },

    _createLocalServer: function(uri){
        return new Promise(function (resolve, reject) {
            var config = {
                  containerId: "viewerContainer",
                  endpointUri: uri,
                  model: Sample.getModel(),
                  rendererType: Sample.getRendererType(),
                  streamingMode: Sample.getStreamingMode(),
                  memoryLimit: Sample.getMemoryLimit()
            };
            Sample.applyExtraProperties(config);
            var viewer = new Communicator.WebViewer(config);

            resolve(viewer);
        });
    },

    _addStylesheet: function (stylesheetUrl) {
        var link = document.createElement("link");
        link.setAttribute("rel", "stylesheet");
        link.setAttribute("type", "text/css");
        link.setAttribute("href", stylesheetUrl);
        document.getElementsByTagName('head')[0].appendChild(link);
    },

    _checkforMobile: function(){
        var layout = Sample.getLayout();

        if (layout == "mobile") {
            Sample._addStylesheet("css/Mobile.css");
            Sample["screenConfiguration"] = Communicator.ScreenConfiguration.Mobile;
        }
        else {
            Sample["screenConfiguration"] = Communicator.ScreenConfiguration.Desktop;
        }
        
    },

    createViewer: function () {
        Sample._checkforMobile();

        var scsFile = getParameterByName("scs");
        if (!scsFile && typeof SC_SCS_FILE !== "undefined") {
            scsFile = SC_SCS_FILE;
        }

        var scHost = getParameterByName("scHost");
        var scPort = getParameterByName("scPort") || "9999";

        if (scsFile) {
            return Sample._createScsViewer(scsFile)
        }
        else if ((typeof SERVER_LOCALHOST !== "undefined") && SERVER_LOCALHOST) {
            return Sample._createLocalServer("ws://localhost:55555");
        }
        else if (scHost) {
            return Sample._createLocalServer("ws://" + scHost + ":" + scPort);
        }
        else {
            var serviceBrokerEndpoint = window.location.protocol + "//" + window.location.hostname + ":11182";
            return Sample._createStreamingViewer(serviceBrokerEndpoint)
        }
    }
};