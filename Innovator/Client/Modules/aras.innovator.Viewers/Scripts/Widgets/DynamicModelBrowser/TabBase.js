﻿require([
	'dojo/_base/declare',
	'dijit/layout/ContentPane'],
	function(declare, ContentPane) {
		return dojo.setObject('VC.DynamicModelBrowser.Tabs.TabBase', declare(ContentPane,
		{
			name: null,
			title: null,

			setTabVisibility: function(doVisible) {
				this.domNode.style.display = doVisible ? '' : 'none';
			}
		}));
	});
