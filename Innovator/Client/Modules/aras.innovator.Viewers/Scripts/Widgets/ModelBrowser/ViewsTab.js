﻿VC.Utils.Page.LoadWidgets(['ModelBrowser/TabBase']);

require([
	'dojo/_base/declare',
	'dojo/_base/lang',
	'dojo/aspect',
	'dojo/text!../xml/views_tree template.xml'],
function(declare, lang, aspect, viewsTreeTemplate) {
	return dojo.setObject('VC.ModelBrowser.Tabs.ViewsTab', declare(VC.ModelBrowser.Tabs.TabBase,
		{
			tabID: 'views',
			viewsTypes: {
				standardViews: 'standardViews',
				cadViews: 'cadViews',
				annotationViews: 'annotationViews'
			},

			onSelectStandardView: function(viewDirectionType) { },
			onSelectCustomView: function(cadViewId) { },
			onGetCustomViews: function() { },

			constructor: function(args) {
				args = args || {};
				args.id = 'viewsTab';
				this.title = VC.Utils.GetResource('viewsTabTitle');
			},

			postCreate: function() {
				this.inherited(arguments);

				var self = this;
				clientControlsFactory.createControl('Aras.Client.Controls.Experimental.Tree',
				{
					id: 'viewsTree',
					IconPath: '../cbin/',
					allowEmptyIcon: true
				},
				function(control) {
					self.tree = control;

					clientControlsFactory.on(self.tree, {
						'itemSelect': lang.hitch(self, self.onItemSelect)
					});

					self.setTreeStyle(self.tree, self.domNode);
				});
			},

			setTreeStyle: function(tree, domNode) {
				domNode.appendChild(tree._tree.domNode);
				VC.Utils.addClass(tree._tree.domNode, 'ViewsTree');
				tree._tree.getRowClass = this.getRowClass;
				aspect.after(tree._tree, 'getIconStyle', this.hideBackgroundImage.bind(tree));
			},

			getRowClass: function(item) {
				if (item.userdata && item.userdata.className) {
					return item.userdata.className;
				}
			},

			hideBackgroundImage: function(style) {
				if (style && style.backgroundImage === 'url(' + this.IconPath + 'null)') {
					style.backgroundImage = 'none';
				}

				return style;
			},

			onItemSelect: function(itemId, multi, userdata) {
				if (userdata) {
					if (userdata.viewDirectionType) {
						this.onSelectStandardView(userdata.viewDirectionType);
					} else if (userdata.cadViewId) {
						this.onSelectCustomView(userdata.cadViewId);
					}
				}
			},

			loadViews: function() {
				var trNodeName = 'tr';
				var xmlDocument = VC.Utils.createXMLDocument();
				var customViews = this.onGetCustomViews();

				xmlDocument.loadXML(viewsTreeTemplate);

				var cadViewsNode = xmlDocument.selectSingleNode('./table/tr[@id=\'cadViews\']');
				var annotationViewsNode = xmlDocument.selectSingleNode('./table/tr[@id=\'annotationViews\']');
				var initialCadViewsCount = cadViewsNode.selectNodes(trNodeName).length;
				var initialAnnotationViewsCount = annotationViewsNode.selectNodes(trNodeName).length;

				if (customViews.length > 0) {
					var view = null;
					var node = null;

					for (var i = 0; i < customViews.length; i++) {
						view = customViews[i];
						node = this.createTreeNode(view);

						if (view.isAnnotationView) {
							annotationViewsNode.appendChild(node);
						} else {
							cadViewsNode.appendChild(node);
						}
					}
				}

				this.tree.initXML(xmlDocument.documentElement.xml);
				this.tree.ExpandAll();

				var actualCadViewsCount = cadViewsNode.selectNodes(trNodeName).length;
				var actualAnnotationViewsCount = annotationViewsNode.selectNodes(trNodeName).length;

				if (initialCadViewsCount === actualCadViewsCount) {
					var cadViewsTreeNode = this.tree._tree.getNodesByItem(this.viewsTypes.cadViews)[0];
					cadViewsTreeNode.domNode.style.display = 'none';
				}

				if (initialAnnotationViewsCount === actualAnnotationViewsCount) {
					var annotationViewsTreeNode = this.tree._tree.getNodesByItem(this.viewsTypes.annotationViews)[0];
					annotationViewsTreeNode.domNode.style.display = 'none';
				}
			},

			createTreeNode: function(view) {
				var xmlDocument = VC.Utils.createXMLDocument();
				var node = xmlDocument.createElement('tr');
				var userdataNode = xmlDocument.createElement('userdata');
				var labelNode = xmlDocument.createElement('td');

				node.setAttribute('id', view.id);
				if (view.isAnnotationView) {
					node.setAttribute('icon0', '../../../images/AnnotationView.svg');
					node.setAttribute('icon1', '../../../images/AnnotationView.svg');
				} else {
					node.setAttribute('icon0', '../../../images/CADView.svg');
					node.setAttribute('icon1', '../../../images/CADView.svg');
				}
				node.setAttribute('level', 1);
				userdataNode.setAttribute('key', 'cadViewId');
				userdataNode.setAttribute('value', view.id);
				labelNode.text = view.label;

				node.appendChild(userdataNode);
				node.appendChild(labelNode);

				return node;
			}
		}));
});

