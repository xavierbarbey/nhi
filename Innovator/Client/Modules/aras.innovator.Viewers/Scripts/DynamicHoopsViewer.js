﻿VC.Utils.Page.LoadModules(['HoopsViewerBase']);

require([
	'dijit/layout/BorderContainer',
	'dojo/_base/declare',
	'dojo/_base/lang'],
function(BorderContainer, declare, lang) {
	return dojo.setObject('VC.DynamicHoopsViewer', (function() {
		var isHoopsViewerSupported = (VC.Utils.isIE() && VC.Utils.isIE11()) || VC.Utils.isFirefox() || VC.Utils.isChrome();
		var defaultResolution = 'Current';
		return declare('DynamicHoopsViewer', VC.HoopsViewerBase, {
			isStarted: false,
			isSelectionFromTree: false,
			scsFileBackgroundColor: null,
			modelXml: '',
			isModelStructureReady: false,
			resetViewInProgress: false,
			pathNodeDictionary: null,
			onShowSidebarViewerButton: function() { },
			isFirstOpen: true,
			isModelReadyForSelection: false,
			isModelBrowserHidden: false,

			constructor: function(args) {
				this.type = 'dynamic';
				this.scsFileBackgroundColor = new Communicator.Color(255, 255, 255);
			},

			setViewerCallbacks: function() {
				var self = this;

				this.viewer.setCallbacks({
					modelStructureParseBegin: function() {
						self.setLoadingProgress(25);
					},

					modelStructureReady: function() {
						self.modelStructureReadyHandler();
					},

					selectionArray: function(selectionEvents) {
						var selectionEvent = selectionEvents && selectionEvents[0] ? selectionEvents[0] : null;

						// "Orient To Face" button's state depends on the current selection's type
						self.setBtnOrientToFaceAccessibility(selectionEvent);

						if (!self.isSelectionFromTree) {
							// get path to selected model and select related node in the tree
							if (VC.Utils.isNotNullOrUndefined(selectionEvent) && !self.isModelBrowserHidden) {
								var selection = selectionEvent.getSelection();
								self.prepareOnSelectItemOnModel(selection.getNodeId());
							}
						}
					},

					measurementCreated: function(measurement) {
						var measureDialog = self.DialogManager.getExistingDialog(VC.Utils.Enums.Dialogs.Measure);

						if (measureDialog) {
							measureDialog.btnDeleteAll.Enable();
						}
					}
				});
			},

			onAfterShowModelBrowserHandler: function() {
				if (this.isModelBrowserHidden && this.viewer && this.viewer.selectionManager) {
					var last = this.viewer.selectionManager.getLast();
					var nodeId = last && last.getNodeId();
					if (nodeId !== undefined && nodeId !== null) {
						this.prepareOnSelectItemOnModel(nodeId);
					}
				}

				this.isModelBrowserHidden = false;
				return this.inherited(arguments);
			},

			onAfterHideModelBrowserHandler: function() {
				this.isModelBrowserHidden = true;
				return this.inherited(arguments);
			},

			isToSkipModelLoading: function() {
				const execQueryOnOpeningViewer = aras.getPreferenceItemProperty('SSVC_Preferences', null, 'execute_query_on_opening_viewer');
				return (execQueryOnOpeningViewer === '1') ? false : this.isFirstOpen;
			},

			prepareOnSelectItemOnModel: function(nodeId) {
				var self = this;
				var resultCallback = function(parentNodeIds) {
					if (parentNodeIds && parentNodeIds !== '') {
						self.onSelectItemOnModel(parentNodeIds);
					}
				};
				self.getParentsPath(nodeId, resultCallback);
			},

			getParentsPath: function(nodeId, resultCallback) {
				var self = this;
				return new Promise(function(resolve) {
					var promises = [];
					var parentNodeIds = [];
					self.getItemPropertiesFromModel(nodeId, parentNodeIds, self.generateParentsPath.bind(self), promises, resultCallback);
					resolve(parentNodeIds.join('/'));
				});
			},

			generateParentsPath: function(props, parentNodeIds, promises, resultCallback) {
				if (props.itemId !== null && props.queryItemRefId !== null) {
					parentNodeIds.unshift(props.queryItemRefId + ':' + props.itemId);
				}
				var parentId = this.viewer.getModel().getNodeParent(props.modelNodeId);
				if (parentId !== null) {
					this.getItemPropertiesFromModel(parentId, parentNodeIds, this.generateParentsPath.bind(this), promises, resultCallback);
				} else {
					Promise.all(promises).then(function(resolve) {
						var result = new Promise(function(resolve) {
							resolve(parentNodeIds.join('/'));
							resultCallback(parentNodeIds.join('/'));
						});
					});
				}
			},

			getItemPropertiesFromModel: function(nodeId, parentNodeIds, generateParentsPath, promises, resultCallback) {
				var self = this;

				if (VC.Utils.isNotNullOrUndefined(nodeId)) {
					var model = this.viewer.getModel();

					var promise = model.getNodeProperties(nodeId).then(function(nodeProperties) {
						if (nodeProperties) {
							var properties = self.getProperties(nodeId, nodeProperties);

							if (generateParentsPath) {
								generateParentsPath(properties, parentNodeIds, promises, resultCallback);
							}
						}
					});

					promises.push(promise);
				}
			},

			getProperties: function(nodeId, nodeProperties) {
				var queryItemRefIdPropertyName = 'QUERY ITEM REF ID';
				var itemIdPropertyName = 'ITEM ID';
				var itemTypePropertyName = 'TYPE NAME';
				var instRefPropertyName = 'INSTANCE REF';
				var instIdPropertyName = 'INSTANCE ID';

				var properties = {
					modelNodeId: nodeId,
					queryItemRefId: null,
					itemId: null,
					itemType: null,
					treeNodeId: null,
					instanceId: null
				};

				if (nodeProperties.hasOwnProperty(queryItemRefIdPropertyName)) {
					properties.queryItemRefId = nodeProperties[queryItemRefIdPropertyName];
				}

				if (nodeProperties.hasOwnProperty(itemIdPropertyName)) {
					properties.itemId = nodeProperties[itemIdPropertyName];
				}

				if (nodeProperties.hasOwnProperty(itemTypePropertyName)) {
					properties.itemType = nodeProperties[itemTypePropertyName];
				}

				if (nodeProperties.hasOwnProperty(instRefPropertyName)) {
					properties.treeNodeId = nodeProperties[instRefPropertyName];
				} else {
					properties.treeNodeId = nodeId + '';
				}

				if (nodeProperties.hasOwnProperty(instIdPropertyName)) {
					properties.instanceId = nodeProperties[instIdPropertyName];
				}

				return properties;
			},

			modelStructureReadyHandler: function() {
				this.isModelStructureReady = true;
				this.onViewerLoaded();
				this.createAxisTriad();

				var modelBrowserPanel = this.DialogManager.getExistingOrNewDialog(VC.Utils.Enums.Dialogs.DynamicModelBrowser);
				modelBrowserPanel.viewsTab.loadViews();

				this.setLoadingProgress(101);

				// T-000932 Temporary disable markupMode in Dynamic
				this.toolbarContainer.btnMarkup.enable(false);

				if (!this.isToSkipModelLoading()) {
					this.refresh3DView();
				}
			},

			displayToolbarButtons: function(flag) {
			},

			afterSubtreeLoadedHandler: function() {
				var orbitOperator = this.viewer.getOperatorManager().getOperator(Communicator.OperatorId.Orbit);
				orbitOperator.setOrbitFallbackMode(Communicator.OrbitFallbackMode.ModelCenter);
			},

			initializeViewer: function(args) {
				if (isHoopsViewerSupported) {
					this.viewer = new Communicator.WebViewer({
						containerId: 'viewerContainer',
						streamingMode: Communicator.StreamingMode.OnDemand,
						boundingPreviewMode: Communicator.BoundingPreviewMode.None,
						empty: true
					});
					this.viewer.start();
					this.isStarted = true;
					this.setViewerCallbacks();
					this.initSplitter();

					VC.Utils.Page.LoadModules(['Controls/CuttingPlaneControl']);
					this.cuttingPlaneControl = new VC.Widgets.CuttingPlaneControl(new Communicator.Ui.CuttingPlaneController(this.viewer));

					var zoomOperator = this.viewer.getOperatorManager().getOperator(Communicator.OperatorId.Zoom);
					zoomOperator.setZoomToMousePosition(false);
				}

				this.isViewerInitialized = true;
			},

			_loadFile: function() {
				if (!isHoopsViewerSupported) {
					return;
				}
			},

			loadShatteredModel: function() {
				aras.browserHelper.toggleSpinner(this.viewerContainer, true);
				this.isModelReadyForSelection = false;

				var inn = aras.newIOMInnovator();
				var methodTemplate = '<Item type=\'Method\' action=\'CreateDynamicModelXml\'>' +
					'<tgvDefId>{0}</tgvDefId>' +
					'<itemId>{1}</itemId>' +
				'</Item>';
				var itemData = this.getItemData();
				var tgvdId = this.args.tgvdId;
				var method = methodTemplate.Format(tgvdId, itemData.itemId);

				var inDom = inn.newXMLDocument();
				inDom.loadXML(method);

				aras.IomInnovator.getItemInDom(inDom).applyAsync().then(function(results) {
					var xmlDocument = inn.newXMLDocument();
					xmlDocument.loadXML(results.getResult());

					this.modelXml = this.getXmlByXPath(xmlDocument, 'XMLResult/ForModel/Root');

					if (this.modelXml !== '') {
						this.modelXml = '<!--HC 6.3-->\n' + this.modelXml;
						this.loadSubtreeFromXML();
					} else {
						aras.browserHelper.toggleSpinner(this.viewerContainer, false);
					}
				}.bind(this))
				.catch(function(soapResponse) {
					aras.browserHelper.toggleSpinner(this.viewerContainer, false);
					this.isModelReadyForSelection = false;

					const errorItem = aras.newIOMItem();
					errorItem.loadAML(soapResponse.responseText);
					VC.Utils.AlertError(VC.Utils.GetResource('err_server_side'), errorItem.getErrorString(), errorItem.getErrorDetail(), null);
				});
			},

			getXmlByXPath: function(xmlDocument, xpathExpression) {
				var xmlNode = xmlDocument.selectSingleNode(xpathExpression);
				return (xmlNode !== null) ? xmlNode.xml : '';
			},

			loadSubtreeFromXML: function() {
				var self = this;
				var model = this.viewer.getModel();
				var rootNodeID = model.getAbsoluteRootNode();

				// After model is loaded used 'fitAllNodes' function as workaround,
				// because in dynamic viewer model is opened as zoomed in too much
				model.loadSubtreeFromScsXmlBuffer(rootNodeID, this.modelXml, this.getFileUrlByFileId.bind(this))
					.then(function() {
						self.fitAllNodes();
						self.fillPathNodeDictionary();
						aras.browserHelper.toggleSpinner(self.viewerContainer, false);
						self.isModelReadyForSelection = true;
					});
				this.setBtnExplodedViewAccessibility();
			},

			getFileUrlByFileId: function(fileId) {
				return aras.IomInnovator.getFileUrl(fileId, aras.Enums.UrlType.SecurityToken);
			},

			getItemData: function() {
				var returnedValue = {};

				if (!this.args || !this.args.markupHolderId || !this.args.markupHolderItemtypeName) {
					throw new Error('Item id or item type name are not specified for the hoops viewer');
				}

				returnedValue.itemId = this.args.markupHolderId;
				returnedValue.itemTypeName = this.args.markupHolderItemtypeName;

				return returnedValue;
			},

			setBtnExplodedViewAccessibility: function() {
				if (this.viewerToolbar.btnExplodedView) {
					if (VC.Utils.isNullOrUndefined(this.modelXml) || (this.modelXml === '')) {
						this.viewerToolbar.btnExplodedView.Disable();
					}

					var xmlDocument = VC.Utils.createXMLDocument();
					xmlDocument.loadXML(this.modelXml);

					var productOccurences = xmlDocument.getElementsByTagName('ProductOccurence');
					//assembly contains more than 3 'productOccurences'
					var minLength = 3;
					if (productOccurences.length > minLength) {
						this.viewerToolbar.btnExplodedView.Enable();
					} else {
						this.viewerToolbar.btnExplodedView.Disable();
					}
				}
			},

			crossSectionClick: function() {
				var self = this;

				var crossSectionArgs = {
					cappingGeometryState: this.viewer.getCuttingManager().getCappingGeometryVisibility()
				};

				var crossSectionDialog = self.DialogManager.getExistingOrNewDialog(VC.Utils.Enums.Dialogs.CrossSection, crossSectionArgs);

				if (!crossSectionDialog.isInitHandlers) {
					crossSectionDialog.onResetCuttingPlanes = function() {
						self.cuttingPlaneControl.resetCuttingPlanes();
					};

					this.crossSectionInitHandlers(crossSectionDialog);
				}

				self.viewer.getCuttingManager().activateCuttingSections();
			},

			resetView: function() {
				if (this.resetViewInProgress) {
					return;
				}
				this.resetViewInProgress = true;

				var self = this;

				this.resetViewBase(VC.Utils.Enums.Dialogs.DynamicModelBrowser);

				this.viewer.reset()
					.then(function() {
						self.recreateTriad();
						self.resetViewInProgress = false;
						//set "IsoView" direction as a workaround of known issue in HOOPS Communicator 2019
						self.setViewDirection('IsoView', self.directionDuration);
					});

				// Switch to the initial view of component geometry from activated CAD View
				var handleOperator = this.viewer.getOperatorManager().getOperator(Communicator.OperatorId.Handle);
				handleOperator.removeHandles();
			},

			requestNodes: function(nodeIds) {
				this.viewer.getModel().requestNodes(nodeIds);
			},

			onViewerLoaded: function() {
				this.inherited(arguments);
				var viewer = this.viewer.getView();
				viewer.setBackgroundColor(this.scsFileBackgroundColor, this.scsFileBackgroundColor);
				viewer.getHiddenLineSettings().setObscuredLineTransparency(0);

				this.DialogManager.openDialog(VC.Utils.Enums.Dialogs.DynamicModelBrowser);
			},

			onSelectItemOnModel: function(pathsStr) {
			},

			onSelectRowOnTgvTreeGrid: function(pathsStr) {
				if (this.isModelReadyForSelection) {
					var nodeIdsToSelect = this.pathNodeDictionary[pathsStr];
					if (nodeIdsToSelect) {
						this.isSelectionFromTree = true;
						nodeIdsToSelect = nodeIdsToSelect.map(Number);
						var selectionManager = this.viewer.selectionManager;
						selectionManager.clear();

						var selectionMode = nodeIdsToSelect.length > 0 ? Communicator.SelectionMode.Add : Communicator.SelectionMode.Set;
						nodeIdsToSelect.forEach(function(nodeId) { selectionManager.selectNode(nodeId, selectionMode); });
						this.isSelectionFromTree = false;
					}
				}
			},

			loadViewerToolbar: function() {
				var self = this;
				const showModelBrowserFunction = lang.partial(lang.hitch(self, self.showModelBrowser), VC.Utils.Enums.Dialogs.DynamicModelBrowser);

				this.toolbarContainer.createViewToolbar(VC.Utils.Enums.TNames.DynamicHoopsViewerToolbar);
				this.loadViewerToolbarBase();

				this.toolbarContainer.viewToolbar.bindButtonBehaviour(VC.Utils.Enums.TButtonEvents.btnModelBrowserClick, showModelBrowserFunction);
				this.toolbarContainer.viewToolbar.bindButtonBehaviour(VC.Utils.Enums.TButtonEvents.btnResetViewClick, lang.hitch(self, self.resetView));

				this.bindDialogClick(this.toolbarContainer.viewToolbar.btnModelBrowser, VC.Utils.Enums.Dialogs.DynamicModelBrowser);
				// "Exploded View" button should be initially disabled because there is no loaded model
				this.setBtnExplodedViewAccessibility();
			},

			loadContainer: function(args) {
				VC.Utils.Page.LoadModules(['Widgets/HoopsViewerContainer']);

				const tgvdId = args.tgvdId ? args.tgvdId : '3A6AC1EA2B2C41E98670E837E8858A0B';
				var htmlContainer = new VC.Widgets.ViewerContainer();
				htmlContainer.placeAt(document.body);

				var modelBrowserPanelArgs = {
					tgvdId: tgvdId,
					rootItemId: args.markupHolderId
				};
				this.DialogManager.viewerType = this.type;
				var modelBrowserPanel = this.DialogManager.getExistingOrNewDialog(VC.Utils.Enums.Dialogs.DynamicModelBrowser, modelBrowserPanelArgs);

				this.initViewerContent(modelBrowserPanel, htmlContainer);

				return htmlContainer;
			},

			assignModelBrowserHandlers: function(modelBrowser) {
				modelBrowser.onAfterShow = this.onAfterShowModelBrowserHandler.bind(this, VC.Utils.Enums.Dialogs.DynamicModelBrowser);

				modelBrowser.onAfterHide = this.onAfterHideModelBrowserHandler.bind(this, VC.Utils.Enums.Dialogs.DynamicModelBrowser);

				modelBrowser.viewsTab.onGetCustomViews = this.getCustomViews.bind(this);

				modelBrowser.viewsTab.onSelectStandardView = this.onSelectStandardViewHandler.bind(this);

				modelBrowser.viewsTab.onSelectCustomView = this.onSelectCustomViewHandler.bind(this);
			},

			onSelectStandardViewHandler: function(viewDirectionType) {
				this.setViewDirection(viewDirectionType, this.directionDuration);
			},

			onSelectCustomViewHandler: function(cadViewId) {
				this.applyCustomView(cadViewId);
			},

			initITContainer: function() {
				var self = this;

				this.loadViewerToolbar();
				this.loadMarkupToolbar();

				this.toolbarContainer.onBtnViewClick = function() {
					// Implement in the task about view/markup mode
				};

				this.toolbarContainer.onBtnMarkupClick = function() {
					// Implement in the task about view/markup mode
				};

				this.assignBasicToolbarSwitchClickHandler();

				// T-000932 Temporary disable markupMode in Dynamic
				this.toolbarContainer.btnMarkup.enable(false);
			},

			refresh3DView: function() {
				this.isFirstOpen = false;
				var crossSectionDialog = this.DialogManager.getExistingDialog(VC.Utils.Enums.Dialogs.CrossSection);
				if (crossSectionDialog) {
					crossSectionDialog.resetDialogContentState();
				}
				this.DialogManager.closeAllDialogsButThis(VC.Utils.Enums.Dialogs.DynamicModelBrowser);
				this.recreateScene(this.loadModel.bind(this), this.recreateTriad.bind(this));
			},

			loadModel: function() {
				this.loadShatteredModel();
				this.viewer.reset();
				this.measurementMarkup = null;
			},

			fillPathNodeDictionary: function() {
				this.pathNodeDictionary = {};
				var modelXmlDocument = VC.Utils.createXMLDocument();
				modelXmlDocument.loadXML(this.modelXml);
				this.fillNodePath('0', '', modelXmlDocument);
			},

			fillNodePath: function(nodeId, pathToParent, modelXmlDocument) {
				var self = this;
				var queryItemRefIdPropertyName = 'QUERY ITEM REF ID';
				var itemIdPropertyName = 'ITEM ID';
				var xPathNodeTemplate = './/ProductOccurence[@Id=\'{0}\']';
				var xPathPropertyTemplate = './Attributes/Attr[@Name=\'{0}\']/@Value';

				var modelNode = modelXmlDocument.selectSingleNode(xPathNodeTemplate.Format(nodeId));
				var queryItemId = modelNode.selectSingleNode(xPathPropertyTemplate.Format(queryItemRefIdPropertyName)).value;
				var itemId = modelNode.selectSingleNode(xPathPropertyTemplate.Format(itemIdPropertyName)).value;
				var nodePath = pathToParent + queryItemId + ':' + itemId;
				if (this.pathNodeDictionary[nodePath]) {
					this.pathNodeDictionary[nodePath].push(nodeId);
				} else {
					this.pathNodeDictionary[nodePath] = [nodeId];
				}

				var nodeChildren = modelNode.getAttributeNode('Children');
				if (nodeChildren && nodeChildren.value) {
					nodePath += '/';
					var nodeIds = nodeChildren.value.split(' ');
					nodeIds.forEach(function(nodeId) { self.fillNodePath(nodeId, nodePath, modelXmlDocument); });
				}
			},

			// clears model in hoops view viewer
			// we should clear viewers model each time,
			// except situation when modelXml is empty
			recreateScene: function(createSceneCallback, afterClearSceneCallback) {
				var self = this;
				if (this.modelXml === '') {
					if (createSceneCallback) {
						createSceneCallback();
					}
				} else {
					self.viewer.getModel().clear().then(function() {
						if (createSceneCallback) {
							createSceneCallback();
						}

						if (afterClearSceneCallback) {
							afterClearSceneCallback();
						}
					}).catch(function() {
						//call recreateScene second time as a workaround of known issue(ID: 34345) in HOOPS Communicator 2019
						self.recreateScene(createSceneCallback, afterClearSceneCallback);
					});
				}
			}
		});
	})());
});
