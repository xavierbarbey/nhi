﻿define([
	'dojo/_base/declare',
	'TechDoc/Aras/Client/Controls/TechDoc/Action/ActionBase',
	'TechDoc/Aras/Client/Controls/TechDoc/ViewModel/DocumentationEnums'
],
function(declare, ActionBase, Enums) {
	return declare('Aras.Client.Controls.TechDoc.Action.ChangeConditionAction', ActionBase, {
		constructor: function(args) {
		},

		Execute: function(/*Object*/context) {
			var optionalContent = this._viewmodel.OptionalContent();
			var targetElement = context.selectedItem;
			var isDisabled = !this._viewmodel.IsEqualEditableLevel(Enums.EditLevels.FullAllow);
			var isExternalBlock = targetElement.is('ArasBlockXmlSchemaElement') && targetElement.isBlockExternal();
			var param = {
				isDisabled: isDisabled,
				title: this.aras.getResource('../Modules/aras.innovator.TDF', isDisabled ? 'action.viewelementcondition' : 'action.changeelementcondition'),
				formId: '68962F55F96E4583AD53676C3BEF91EC', // tp_ChangeBlockCondition Form
				aras: this.aras,
				isEditMode: true,
				condition: JSON.parse(targetElement.Condition()),
				externalcondition: isExternalBlock ? optionalContent.GetElementCondition(context.selectedItem, Enums.ByReferenceType.External) : {},
				optionFamilies: optionalContent.OptionFamilies(),
				dialogWidth: 322,
				dialogHeight: 375,
				content: 'ShowFormAsADialog.html'
			};

			this.actionsHelper.topWindow.ArasModules.Dialog.show('iframe', param).promise.then(
				function(result) {
					if (result) {
						targetElement.Condition(result);
					}
				}
			);
		}
	});
});
