﻿define([
	'dojo/_base/declare',
	'TechDoc/Aras/Client/Controls/TechDoc/Action/ActionBase'
],
function(declare, ActionBase) {
	return declare('Aras.Client.Controls.TechDoc.Action.CopyElementAction', ActionBase, {
		constructor: function(args) {
		},

		Execute: function(/*Object*/args) {
			var selectedItems = args.selectedItems;
			var clipboard = args.clipboard;
			var data = [];
			var allReferences = {};
			var externalProvider = this._viewmodel.OriginExternalProvider();
			var itemRefences;
			var selectedItem;
			var clonedObject;
			var clonedXmlNode;
			var referenceId;
			var i;

			for (i = 0; i < selectedItems.length; i++) {
				selectedItem = selectedItems[i];

				clonedObject = selectedItem.Clone();
				clonedXmlNode = clonedObject.Origin();
				data.push(clonedXmlNode);

				itemRefences = this.findNodeReferences(clonedXmlNode, externalProvider);
				for (referenceId in itemRefences) {
					if (!allReferences[referenceId]) {
						allReferences[referenceId] = itemRefences[referenceId].cloneNode(true);
					}
				}
			}

			clipboard.setData('StructureXml', {content: data, references: allReferences});
		},

		findNodeReferences: function(targetNode, externalProvider, excludeSelfNode) {
			var resultHash = {};

			if (targetNode) {
				var nodesWithReference = targetNode.selectNodes(excludeSelfNode ? './/*[@ref-id]' : 'descendant-or-self::node()[@ref-id]');
				var currentNode;
				var referenceId;
				var innerReferences;
				var referenceNode;
				var i;

				for (i = 0; i < nodesWithReference.length; i++) {
					currentNode = nodesWithReference[i];
					referenceId = currentNode.getAttribute('ref-id');

					referenceNode = externalProvider.GetExternalNodeByRefId(referenceId);
					resultHash[referenceId] = referenceNode;

					innerReferences = this.findNodeReferences(referenceNode, externalProvider, true);
					for (referenceId in innerReferences) {
						if (!resultHash[referenceId]) {
							resultHash[referenceId] = innerReferences[referenceId].cloneNode(true);
						}
					}
				}
			}

			return resultHash;
		}
	});
});
