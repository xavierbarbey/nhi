﻿import Dialog from '../core/Dialog';
import { createButton } from './dialogCommonApi';
import SvgManager from '../core/SvgManager';

function confirmModule(message = '', options = {}) {
	const {
		additionalButton,
		cancelButtonModifier,
		okButtonModifier,
		okButtonText,
		title
	} = options;
	const cssClassName = 'aras-dialog-confirm';
	const confirmDialog = new Dialog('html', {
		classList: cssClassName,
		title
	});

	const image = SvgManager.createHyperHTMLNode(
		aras.getBaseURL('/images/Warning.svg'),
		{ class: `${cssClassName}__img` }
	);
	const okBtn = createButton(
		okButtonText || aras.getResource('', 'common.ok'),
		`${okButtonModifier || 'aras-button_primary'}`,
		confirmDialog.close.bind(confirmDialog, 'ok'),
		true
	);
	const cancelBtn = createButton(
		aras.getResource('', 'common.cancel'),
		`${cancelButtonModifier || 'aras-button_secondary-light'}`,
		confirmDialog.close.bind(confirmDialog, 'cancel')
	);
	let additionalBtn;
	if (additionalButton) {
		const { actionName, buttonModifier, text } = additionalButton;
		additionalBtn = createButton(
			text,
			`${buttonModifier || 'aras-button_secondary'}`,
			confirmDialog.close.bind(confirmDialog, actionName)
		);
	}

	HyperHTMLElement.hyper(confirmDialog.contentNode)`
		<div
			class="${cssClassName + '__container'}"
		>
			${image}
			<span class="${cssClassName + '__text'}">${message}</span>
		</div>
		<div
			class="${cssClassName + '__container'}"
		>
			${okBtn}
			${additionalBtn}
			${cancelBtn}
		</div>
	`;

	confirmDialog.show();
	return confirmDialog.promise;
}

export default confirmModule;
