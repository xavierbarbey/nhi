﻿// @flow
const sidebarCssClass = 'aras-sidebar';
const rightSidebarCssClass = sidebarCssClass + '_right';
const hiddenCssClass = sidebarCssClass + '_hidden';
const pinnedCssClass = sidebarCssClass + '_pinned';

export default class Sidebar extends HyperHTMLElement {
	_focusOutSetTimeout = null;
	toggleVisibility(visible: ?boolean) {
		const isVisible = this.isVisible;
		const args = [hiddenCssClass];
		if (visible !== undefined) {
			args.push(!visible);
		}
		this.classList.toggle(...args);
		if (isVisible && !visible) {
			this.dispatchEvent(new CustomEvent('sidebarClose', { bubbles: true }));
		} else if (!isVisible && (visible || visible === undefined)) {
			const onTransitionEnd = () => {
				this._setFocus();
				this.removeEventListener('transitionend', onTransitionEnd);
			};
			this.addEventListener('transitionend', onTransitionEnd);
			this.dispatchEvent(new CustomEvent('sidebarShow', { bubbles: true }));
		}
	}

	get isPinned() {
		return this.classList.contains(pinnedCssClass);
	}

	get isVisible() {
		return !this.classList.contains(hiddenCssClass);
	}

	togglePin(pinned: boolean) {
		const args = [pinnedCssClass];
		if (pinned !== undefined) {
			args.push(pinned);
		}
		this.classList.toggle(...args);
		if (this.isPinned) {
			this.removeEventListener('focusout', this._focusOutHandler);
		} else {
			this.addEventListener('focusout', this._focusOutHandler);
		}
	}

	connectedCallback() {
		this.classList.add(sidebarCssClass, hiddenCssClass);
		if (this.hasAttribute('right')) {
			this.classList.add(rightSidebarCssClass);
		}
		this.setAttribute('tabindex', '0');
		this.addEventListener('focusout', this._focusOutHandler);
	}

	disconnectedCallback() {
		if (!this._focusOutSetTimeout) {
			return;
		}

		clearTimeout(this._focusOutSetTimeout);
		this._focusOutSetTimeout = null;
	}

	_setFocus() {
		if (this.isVisible && document.activeElement !== this) {
			this.focus();
		}
	}

	_focusOutHandler() {
		// setTimeout was added so that activeElement equal to the element on which the click was called
		// 100ms delay was added because this will be enough to check it's necessary to close the sidebar
		this._focusOutSetTimeout = setTimeout(() => {
			const activeElement = document.activeElement;

			if (!this._isElementInSidebar(activeElement)) {
				this.toggleVisibility(false);
			}
		}, 100);
	}

	_isElementInSidebar(element: ?Element) {
		return element && this.contains(element);
	}
}
