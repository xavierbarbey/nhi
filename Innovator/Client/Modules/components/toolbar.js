﻿import { defaultTemplates, toolbarFormatters } from './toolbarTemplates';
import HTMLCustomElement from './htmlCustomElement';
import trimSeparators from './trimSeparators';

const keyCodes = {
	37: 'ArrowLeft',
	39: 'ArrowRight'
};

export default class Toolbar extends HTMLCustomElement {
	_leftContainerItems = [];
	_rightContainerItems = [];
	_renderingPromise = null;
	datastore = new Map();
	templates = defaultTemplates;
	focusedItemId = null;
	static get observedAttributes() {
		return ['role'];
	}

	_resizeHandler = () => {
		// for correct display of images after resize in IE
		this._updateImages();
		this.render();
	};

	static extendFormatters(formatter) {
		Object.assign(toolbarFormatters, formatter);
	}

	connectedCallback() {
		if (!this.hasAttribute('role')) {
			this.setAttribute('role', 'toolbar');
		}

		this._initKeyboardHandler();
	}

	disconnectedCallback() {
		window.removeEventListener('resize', this._resizeHandler);
	}

	attributeChangedCallback() {
		this._initKeyboardHandler();
		this.render();
	}

	init() {
		window.addEventListener('resize', this._resizeHandler);
		this.on('click', (parentItemId, event) =>
			this._checkedItemClickHandler(parentItemId, event)
		);
	}

	_initKeyboardHandler() {
		if (this._isToolbarRole()) {
			this.addEventListener('keydown', this._keyboardEventHandler);
		} else {
			this.removeEventListener('keydown', this._keyboardEventHandler);
		}
	}

	async _keyboardEventHandler(e) {
		const key = e.code || keyCodes[e.keyCode];
		let nextItemId;
		switch (key) {
			case 'ArrowLeft':
				nextItemId = this._getPrevFocusable();
				break;
			case 'ArrowRight':
				nextItemId = this._getNextFocusable();
				break;
		}

		if (nextItemId) {
			e.preventDefault();
			await this._focusItem(nextItemId);

			// preventDefault isn't working for 'select' in FF: https://bugzilla.mozilla.org/show_bug.cgi?id=1019630
			const targetNode = e.target;
			if (
				targetNode.nodeName === 'SELECT' &&
				/Firefox\/(\S+)/.test(window.navigator.userAgent)
			) {
				const originalDisplay = targetNode.style.display;
				targetNode.style.display = 'none';
				targetNode.clientHeight; // execute re-rendering of HTML engine
				targetNode.style.display = originalDisplay;
				// don't call anything below what will re-render HTML
			}
		}
	}

	_getFocusableItems() {
		return [...this._leftContainerItems, ...this._rightContainerItems].filter(
			itemId => this._isItemFocusable(itemId)
		);
	}

	_getNextFocusable() {
		const itemsId = this._getFocusableItems();
		if (!itemsId.length) {
			return null;
		}

		return itemsId[itemsId.indexOf(this.focusedItemId) + 1] || itemsId[0];
	}

	_getPrevFocusable() {
		const itemsId = this._getFocusableItems();
		if (!itemsId.length) {
			return null;
		}

		return (
			itemsId[itemsId.indexOf(this.focusedItemId) - 1] ||
			itemsId[itemsId.length - 1]
		);
	}

	async _focusItem(itemId) {
		this.focusedItemId = itemId;
		await this.render();
		const dataIdSelector = `[data-id="${itemId}"]`;
		const focusNode = this.querySelector(
			`${dataIdSelector}[tabindex="0"], ${dataIdSelector} [tabindex="0"]`
		);
		if (focusNode) {
			focusNode.focus();
		}
	}

	_isItemFocusable(itemId) {
		const item = this.datastore.get(itemId);

		return (
			item &&
			!item.hidden &&
			!(item.attributes && item.attributes['aria-hidden']) &&
			item.type !== 'separator' &&
			(!item.disabled || ['button', 'dropdownMenu'].includes(item.type))
		);
	}

	_checkedItemClickHandler(parentItemId, event) {
		const node = event.target;
		const targetNode = node.closest('li[data-index]');

		if (!targetNode) {
			return Promise.resolve();
		}

		const parentItem = this.data.get(parentItemId);
		const dataMap = parentItem.data;
		const targetNodeIndex = targetNode.dataset.index;
		const targetItem = dataMap.get(targetNodeIndex);

		if (
			!targetItem.disabled &&
			(targetItem.checked !== undefined || targetItem.type === 'checkbox')
		) {
			const groupId = targetItem.group_id;
			if (groupId) {
				dataMap.forEach(item => {
					if (item.group_id !== groupId) {
						return;
					}

					item.checked = false;
					dataMap.set(item.id, Object.assign({}, item));
				});
				targetItem.checked = true;
			} else {
				targetItem.checked = !targetItem.checked;
			}

			dataMap.set(targetItem.id, Object.assign({}, targetItem));
			this.data.set(parentItemId, Object.assign({}, parentItem));
			return this.render();
		}

		return Promise.resolve();
	}

	_render() {
		if (
			this._isToolbarRole() &&
			(!this.focusedItemId || !this._isItemFocusable(this.focusedItemId))
		) {
			this.focusedItemId = this._getNextFocusable();
		}

		const root = this.templates.root(this._getVnodeProperties());
		Inferno.render(root, this);
	}

	render() {
		if (this._renderingPromise) {
			return this._renderingPromise;
		}
		this._renderingPromise = Promise.resolve().then(() => {
			this._render();
			this._renderingPromise = null;
		});
		return this._renderingPromise;
	}

	set data(dataMap) {
		const self = this;
		this.datastore = dataMap;
		this._rightContainerItems = [];
		this._leftContainerItems = [];
		this.datastore.forEach(function(value, key) {
			if (value.right) {
				self._rightContainerItems.push(key);
			} else {
				self._leftContainerItems.push(key);
			}
		});
		this.render();
	}

	get data() {
		return this.datastore;
	}

	set container(newContainerItems) {
		this._leftContainerItems = newContainerItems;
		this.render();
	}

	get container() {
		return this._leftContainerItems;
	}

	set rightContainer(newContainerItems) {
		this._rightContainerItems = newContainerItems;
		this.render();
	}

	get rightContainer() {
		return this._rightContainerItems;
	}

	on(eventType, callback) {
		const self = this;
		const handler = function(event) {
			const targetItemId = self._getIdByDomElement(event.target);
			const item = self.data.get(targetItemId);
			if (!item || item.disabled) {
				return;
			}
			if (targetItemId) {
				callback(targetItemId, event);
			}
		};
		this.addEventListener(eventType, handler);
		return function() {
			self.removeEventListener(eventType, handler);
		};
	}

	_getIdByDomElement(element) {
		const targetNodeSelector = '[data-id]';
		const targetNode = element.closest(targetNodeSelector);
		return targetNode ? targetNode.dataset.id : null;
	}

	_getVnodeProperties() {
		const leftContainerItems = trimSeparators(
			this.data,
			this._leftContainerItems
		);
		const rightContainerItems = trimSeparators(
			this.data,
			this._rightContainerItems
		);
		return {
			leftContainerItems,
			rightContainerItems,
			toolbar: this,
			options: {
				isToolbarRole: this._isToolbarRole()
			}
		};
	}

	_updateImages() {
		const images = this.querySelectorAll('.aras-toolbar__image img');
		for (let i = 0; i < images.length; i++) {
			images[i].src = images[i].getAttribute('src');
		}
	}

	_isToolbarRole() {
		return this.getAttribute('role') === 'toolbar';
	}
}
