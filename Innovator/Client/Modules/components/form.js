﻿const showItemInFrame = frame => {
	const formMode = window.isNew ? 'add' : window.isEditMode ? 'edit' : 'view';
	aras.uiShowItemInFrameEx(frame.contentWindow, window.item, formMode, 0);
};
export default class Form extends HyperHTMLElement {
	connectedCallback() {
		this.style.height = window.formHeight + 'px';
		this.html`
			<iframe
				id="instance" frameborder="0" width="100%" height="100%"
				onconnected="${e => {
					showItemInFrame(e.target);
				}}"
				onload="${e => {
					window.returnBlockerHelper.attachBlocker(e.target.contentWindow);
				}}"
			></iframe>
		`;
	}

	render() {
		if (this.firstElementChild) {
			showItemInFrame(this.firstElementChild);
		}
	}
}
