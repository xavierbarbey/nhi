﻿import gridEditors from './editors';
import intl from '../../core/intl';

const getValidationMethod = (validatorType, headId, grid, metadata) => {
	const dataType = grid.head.get(headId, 'dataType');
	return value => {
		return {
			valid: validators[validatorType](value, metadata),
			validationMessage: aras.getResource(
				'',
				'cui_grid.value_property_invalid',
				dataType
			)
		};
	};
};

const validators = {
	ubigint(value) {
		const uBigIntMaxValue = '18446744073709551615';
		const ubiValue = !isNaN(intl.number.parseInt(value)) && bigInt(value);
		const isValid =
			ubiValue && !ubiValue.isNegative() && !ubiValue.greater(uBigIntMaxValue);
		return !value || isValid;
	},
	integer(value) {
		return !value || !isNaN(intl.number.parseInt(value));
	},
	float(value, metadata = {}) {
		const scale = metadata.scale;
		const precision = metadata.precision;
		const float = intl.number.parseFloat(
			value,
			scale && precision ? precision - scale : undefined
		);
		return !value || !isNaN(float);
	},
	date(value, metadata) {
		return (
			!value ||
			aras
				.getIomSessionContext()
				.convertToNeutral(value, 'date', metadata.format)
		);
	}
};

const dataTypeEditors = {
	string: gridEditors.text,
	date(dom, headId, rowId, value, grid, metadata) {
		metadata.checkValidity = getValidationMethod(
			'date',
			headId,
			grid,
			metadata
		);
		return gridEditors.calendar(dom, headId, rowId, value, grid, metadata);
	},
	list: gridEditors.select,
	'filter list'(dom, headId, rowId, value, grid, metadata) {
		const clonedMetadata = { ...metadata };
		const propertyName = grid.head.get(headId, 'pattern');
		const columnName = grid.settings.indexHead.find(columnName =>
			columnName.startsWith(propertyName)
		);
		const filterValue = grid.rows.get(rowId, propertyName || columnName);

		clonedMetadata.list = clonedMetadata.list.filter(
			option => option.filter === '' || option.filter === filterValue
		);
		return gridEditors.select(dom, headId, rowId, value, grid, clonedMetadata);
	},
	'color list': gridEditors.select,
	decimal(dom, headId, rowId, value, grid, metadata) {
		metadata.checkValidity = getValidationMethod(
			'float',
			headId,
			grid,
			metadata
		);
		return gridEditors.text(dom, headId, rowId, value, grid, metadata);
	},
	float(dom, headId, rowId, value, grid, metadata) {
		metadata.checkValidity = getValidationMethod('float', headId, grid);
		return gridEditors.text(dom, headId, rowId, value, grid, metadata);
	},
	integer(dom, headId, rowId, value, grid, metadata) {
		metadata.checkValidity = getValidationMethod('integer', headId, grid);
		return gridEditors.text(dom, headId, rowId, value, grid, metadata);
	},
	ubigint(dom, headId, rowId, value, grid, metadata) {
		metadata.checkValidity = getValidationMethod('ubigint', headId, grid);
		return gridEditors.text(dom, headId, rowId, value, grid, metadata);
	},
	md5: gridEditors.text,
	federated: gridEditors.text,
	ml_string: gridEditors.text,
	item: gridEditors.link
};

export default dataTypeEditors;
