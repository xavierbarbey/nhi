﻿import intl from '../../core/intl';
import SvgManager from '../../core/SvgManager';

const gridFormatters = {
	boolean: function(headId, rowId, value, grid) {
		const applyEdit = function(e) {
			const isDisabled =
				!grid.view.defaultSettings.editable ||
				!grid.checkEditAvailability(headId, rowId, grid);
			const target = e.target;
			const newValue = target.checked;
			if (!isDisabled) {
				grid.dom.dispatchEvent(
					new CustomEvent('applyEdit', {
						detail: {
							headId: headId,
							rowId: rowId,
							value: newValue
						}
					})
				);
			} else {
				e.preventDefault();
			}
		};
		return {
			className: 'aras-grid-row-cell__boolean',
			children: [
				{
					tag: 'label',
					className: 'aras-checkbox',
					children: [
						{
							tag: 'input',
							className: 'aras-checkbox__input',
							attrs: {
								type: 'checkbox',
								defaultChecked: value,
								onClick: applyEdit
							}
						},
						{
							tag: 'span',
							className: 'aras-checkbox__check-button',
							style: 'margin-right:0;'
						}
					]
				}
			]
		};
	},
	img: function(headId, rowId, value) {
		const svgVNode = SvgManager.createInfernoVNode(value);
		if (svgVNode) {
			const rowClassName = 'aras-grid-row-cell__img';
			return {
				className: rowClassName,
				children: [svgVNode]
			};
		}

		return {
			children: []
		};
	},
	link: function(headId, rowId, value, grid) {
		return {
			children: [
				{
					tag: 'span',
					className: 'aras-grid-link',
					children: [value]
				}
			]
		};
	},
	calendar: function(headId, rowId, value, grid, metadata) {
		const date = intl.date.parse(value);
		const formattedDate = date
			? intl.date.format(date, metadata ? metadata.format : undefined)
			: '';
		return {
			className: 'aras-grid-row-cell__calendar',
			children: [
				{
					tag: 'span',
					children: [formattedDate]
				}
			]
		};
	},
	select: function(headId, rowId, value, grid, metadata) {
		const options = metadata.options || metadata.list || [];
		const result = options.find(function(option) {
			return option.value.toString() === value;
		});
		const label = result ? result.label || result.value : value;
		return {
			className: 'aras-grid-row-cell__select',
			children: [
				{
					tag: 'span',
					children: [label]
				}
			]
		};
	}
};

export default gridFormatters;
