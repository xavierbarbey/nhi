﻿import gridFormatters from './formatters';
import intl from '../../core/intl';

let RESTRICTED_RESOURCE;

const dataTypeFormatters = {
	decimal(headId, rowId, value, grid, metadata) {
		return {
			children: [
				value
					? intl.number.format(value, {
							minimumFractionDigits: metadata.scale
					  })
					: ''
			]
		};
	},
	float(headId, rowId, value, grid) {
		return {
			children: [value ? intl.number.format(value) : '']
		};
	},
	file(headId, rowId, value, grid) {
		if (!value) {
			return {};
		}

		const propertyName = grid.head.get(headId, 'name') || headId;
		const fileName =
			grid.rows.get(rowId, `${propertyName}@aras.keyed_name`) || value;

		if (grid.rows.get(rowId, `${propertyName}@aras.discover_only`) === '1') {
			return {
				children: [fileName]
			};
		}

		const isTemp =
			grid.rows.get(rowId, `${propertyName}@aras.action`) === 'add';
		const svgLink = `../images/${
			isTemp ? 'FilePropertyUnsaved.svg' : 'FileProperty.svg'
		}`;

		const svgNode = ArasModules.SvgManager.createInfernoVNode(svgLink);
		svgNode.className = 'aras-grid-file-icon';

		const linkTemplate = isTemp
			? {
					children: [<span>{fileName}</span>]
			  }
			: gridFormatters.link(headId, rowId, fileName, grid);

		linkTemplate.children.unshift(svgNode);
		return linkTemplate;
	},
	mv_list(headId, rowId, value, grid, metadata) {
		const resultString = value
			.split(',')
			.map(value => {
				const foundElement = metadata.list.find(
					listElement => listElement.value === value
				);

				return foundElement ? foundElement.label : value;
			})
			.join(', ');

		return {
			children: [resultString]
		};
	},
	bool(headId, rowId, value, grid) {
		value = Boolean(Number(value));
		return gridFormatters.boolean(headId, rowId, value, grid);
	},
	color(headId, rowId, value, grid) {
		return {
			style: {
				'background-color': value
			},
			children: null
		};
	},
	'color list'(headId, rowId, value, grid, metadata) {
		const list = gridFormatters.select(headId, rowId, value, grid, metadata);
		const color = gridFormatters.color(headId, rowId, value, grid);

		return {
			...color,
			...list
		};
	},
	date(headId, rowId, value, grid, metadata) {
		return {
			children: [
				aras.convertFromNeutral(
					value,
					'date',
					aras.getDateFormatByPattern(
						grid.head.get(headId, 'pattern') || 'short_date'
					)
				)
			]
		};
	},
	item(headId, rowId, value, grid) {
		if (!value) {
			return {};
		}

		const propertyName = grid.head.get(headId, 'name') || headId;
		const itemName =
			grid.rows.get(rowId, `${propertyName}@aras.keyed_name`) || value;

		if (grid.rows.get(rowId, `${propertyName}@aras.discover_only`) === '1') {
			return {
				children: [itemName]
			};
		}

		return gridFormatters.link(headId, rowId, itemName, grid);
	},
	image(headId, rowId, value, grid) {
		value = grid.rows.get(rowId, `${headId}@aras.keyed_name`) || value;
		return gridFormatters.img(headId, rowId, value, grid);
	},
	restricted(headId, rowId, value, grid) {
		if (!RESTRICTED_RESOURCE) {
			RESTRICTED_RESOURCE = aras.getResource(
				'',
				'common.restricted_property_warning'
			);
		}
		return {
			children: [RESTRICTED_RESOURCE],
			style: {
				color: '#ff0000'
			}
		};
	},
	md5(headId, rowId, value, grid) {
		return {
			children: ['***']
		};
	},
	'claim by'(headId, rowId, value, grid, metadata) {
		const rowInfo = grid.rows._store.get(rowId) || {};
		const isDiscoverOnly = rowInfo['@aras.discover_only'] === '1';
		const isTemp = rowInfo['@aras.isTemp'] === '1';
		const isDirty = rowInfo['@aras.isDirty'] === '1';
		const isEditState = rowInfo['@aras.isEditState'] === '1';
		const lockedById = value && typeof value === 'object' ? value.id : value;

		let icon = '';
		if (!grid.rows.has(rowId)) {
			icon = '../images/NullRelated.svg';
		} else if (isDiscoverOnly) {
			icon = '../images/Blocked.svg';
		} else if (isTemp) {
			icon = '../images/New.svg';
		} else if (lockedById) {
			icon =
				lockedById === metadata.currentUserId
					? `../images/${isDirty || isEditState ? 'Edit' : 'ClaimOn'}.svg`
					: '../images/ClaimOther.svg';
		}

		return gridFormatters.img(headId, rowId, icon, grid);
	},
	list: gridFormatters.select,
	'filter list': gridFormatters.select
};

export default dataTypeFormatters;
