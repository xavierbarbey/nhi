﻿import Grid from '../grid/grid';
import TreeGridView from './treeGridView';
import TreeGridActions from './treeGridActions';
import Keyboard from '../grid/keyboard';

/**
 * TreeGrid Commonent
 *
 * @class
 * @name TreeGrid
 * @extends Grid
 * @param {object}   dom Dom Element used as container
 * @param {object} options
 * @param {boolean} [options.multiSelect=true] options.multiSelect - enable/disable multi select
 * @param {boolean} [options.resizable=true] options.resizable - enable/disable resize of columns
 * @param {boolean} [options.search=true] options.search - show/hide simple search
 * @param {boolean} [options.editable=false] options.editable - enable/disable edit cell
 * @param {boolean} [options.sortable=true] options.sortable - enable/disable sort columns
 *
 * @property {Map} head - Data of heads grid which store in Grid. Returns wrapper on Map on get method for calls render grid after any changes.
 * @property {Map} rows - Data of rows grid which store in Grid. Returns wrapper on Map on get method for calls render grid after any changes.
 * @property {Set} roots - Roots ids
 *
 * @property {object} settings - grid state
 * @property {string[]} settings.selectedRows - ids of selected rows
 * @property {{headId: string, rowId: string, editing: boolean}} settings.focusedCell - focus cell object
 * @property {{headId: string, desc: boolean}[]} settings.orderBy - sortable heads array
 */
class TreeGrid extends Grid {
	metadata = {};
	constructor(dom, options) {
		super(dom, options);
		this.settings.indexTreeRows = {};
		this.settings.expanded = new Set();
		const self = this;
		Object.defineProperty(this.settings, 'treeHeadView', {
			get: function() {
				return this._treeHeadView;
			},
			set: function(treeHeadView) {
				this._treeHeadView = treeHeadView;
				self.render();
			}
		});
		Object.freeze(this.settings.expanded);
	}
	/**
	 * The first initialization of Grid. Create view, action, keyboard classes and set options
	 *
	 * @memberof TreeGrid.prototype
	 * @override
	 */
	initialization(options) {
		this.view = new TreeGridView(this.dom, options);
		this.actions = new TreeGridActions(this);
		this.keyboard = new Keyboard(this);
	}
	get roots() {
		return this.settings.indexTreeRows.roots || [];
	}
	set roots(value) {
		this.settings.indexTreeRows = { roots: value };
		const indexTreeRows = this.settings.indexTreeRows;
		if (this.rows) {
			this.rows._store.forEach(function(child, key) {
				if (child.children && child.children !== true) {
					indexTreeRows[key] = child.children.slice();
				}
			});

			if (this.settings.orderBy.length) {
				this.sort();
			} else {
				this.metadata = this.actions._calcRowsMetadata(indexTreeRows);
			}
		}
		this.settings.indexRows = value.slice();
		this.render();
	}
	/**
	 * Implementation of lazy loading branches
	 *
	 * @private
	 * @param   {string} rowId - row id
	 * @returns {Promise.<boolean>}  - end of operation. Promise(true) means that row has no children.
	 */
	_buildBranchData(rowId) {
		return this.loadBranchDataHandler(rowId).then(
			function(loadedItems) {
				if (loadedItems) {
					const parentRow = this.rows._store.get(rowId);
					parentRow.children = [];
					loadedItems.forEach(function(item, key) {
						parentRow.children.push(key);
						this.rows._store.set(key, item);
					}, this);
					this.settings.indexTreeRows[rowId] = parentRow.children.slice();
					if (this.settings.orderBy.length) {
						this.settings.indexTreeRows[rowId].sort(
							this.actions._sortFn.bind(this)
						);
					}
					const branchMetadata = this.actions._calcRowsMetadata(
						this.settings.indexTreeRows,
						rowId
					);

					Object.assign(this.metadata, branchMetadata);
					return false;
				}
				const row = this.rows.get(rowId);
				delete row.children;
				this.rows.set(rowId, row);
				delete this.settings.indexTreeRows[rowId];
				return true;
			}.bind(this)
		);
	}
	/**
	 * Handler which implement logic of lazy loading branches
	 *
	 * @memberof TreeGrid.prototype
	 * @returns {Promise.<Map>} - end of operation with data.
	 */
	loadBranchDataHandler() {
		return Promise.resolve();
	}
	/**
	 * Expand branch
	 *
	 * @memberof TreeGrid.prototype
	 * @param   {string} rowId - row id
	 * @returns {Promise}  - end of operation
	 */
	expand(rowId) {
		if (rowId !== undefined && !this.settings.expanded.has(rowId)) {
			return this.actions._toggleExpanded(rowId);
		}
		return Promise.resolve();
	}
	/**
	 * Collapse branch
	 *
	 * @memberof TreeGrid.prototype
	 * @param   {string} rowId - row id
	 * @returns {Promise} - end of operation
	 */
	collapse(rowId) {
		if (rowId !== undefined && this.settings.expanded.has(rowId)) {
			return this.actions._toggleExpanded(rowId);
		}
		return Promise.resolve();
	}
	/**
	 * Standard logic of sorting
	 *
	 * @private
	 * @returns {Promise} - the end of operation
	 */
	_sort() {
		const indexTreeRows = this.settings.indexTreeRows;
		const indexTreeRowsIds = new Set(Object.keys(indexTreeRows));
		indexTreeRowsIds.forEach(row =>
			indexTreeRows[row].sort(this.actions._sortFn.bind(this))
		);

		let i = 0;
		const indexRows = this.settings.indexRows;
		const expanded = this.settings.expanded;
		const updateIndexRows = function(key) {
			indexTreeRows[key].forEach(rowId => {
				indexRows[i++] = rowId;
				if (indexTreeRowsIds.has(rowId) && expanded.has(rowId)) {
					updateIndexRows(rowId);
				}
			});
		};

		updateIndexRows('roots');
		this.metadata = this.actions._calcRowsMetadata(indexTreeRows);
		return Promise.resolve();
	}
}

export default TreeGrid;
