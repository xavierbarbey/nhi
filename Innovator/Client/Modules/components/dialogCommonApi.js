﻿export function appendButton(
	btnContainer,
	btnText,
	btnModifier,
	clickHandler,
	autoFocus
) {
	const dialogButton = createButton(
		btnText,
		btnModifier,
		clickHandler,
		autoFocus
	);

	btnContainer.appendChild(dialogButton);

	return dialogButton;
}

export function createButton(btnText, btnModifier, clickHandler, autoFocus) {
	return HyperHTMLElement.hyper`
		<button
			autofocus="${autoFocus}"
			class="${`aras-button ${btnModifier}`}"
			onclick=${clickHandler}
		>
			<span
				class="aras-button__text"
			>
				${btnText}
			</span>
		</button>
	`;
}
