﻿define([
	'dojo/_base/declare',
	'dojo/_base/connect'
], function(declare, connect) {
	const ATTACHABLE_GRID_EVENT_TYPES = ['onSelectRow'];

	return declare([], {
		constructor: function(mainPage) {
			Object.defineProperty(this, 'visibleChildrenMaxCount', {
				set: function(visibleChildrenMaxCount) {
					mainPage.visibleChildrenMaxCount = visibleChildrenMaxCount;
				},
				get: function() {
					return mainPage.visibleChildrenMaxCount;
				},
				enumerable: true,
				configurable: true
			});
			Object.defineProperty(this, 'parametersProvider', {
				enumerable: true,
				get: function() {
					return {
						getParameters: function() {
							return mainPage._parametersProvider.getParameters();
						},
						setParameter: function(name, value) {
							mainPage._parametersProvider.setParameter(name, value);
						}
					};
				}
			});
			this.toolbar = {
				refresh: function() {
					mainPage._updateToolbarItems();
				}
			};
			this.reload = function() {
				mainPage.reload();
			};
			this.modifyParameters = function() {
				mainPage._modifyParameters();
			};
			this.getViewDefinitionId = function() {
				return mainPage._treeGridViewDefinitionNode.getAttribute('id');
			};
			this.grid = {
				addEventListener: function(type, listener) {
					if (ATTACHABLE_GRID_EVENT_TYPES.indexOf(type) === -1) {
						return;
					}

					const handleToLinkedListener = connect.connect(mainPage._grid, type, listener);
					mainPage._eventHandlers.push(handleToLinkedListener);

					return handleToLinkedListener;
				},
				removeEventListener: function(listenerHandle) {
					const handlesToLinkedListeners = mainPage._eventHandlers;

					const indexOfHandleToLinkedListener = handlesToLinkedListeners.indexOf(listenerHandle);
					if (indexOfHandleToLinkedListener !== -1) {
						handlesToLinkedListeners.splice(indexOfHandleToLinkedListener, 1);
						listenerHandle.remove();
					}
				},
				collapseLevel: function() {
					var grid = mainPage._grid;

					grid.collapseLevel(grid.getSelectedItemIDs_Experimental());
				},
				expandAll: function() {
					return mainPage.expandAll();
				}
			};
			this.gridToXml = function() {
				return mainPage._tgvTreeGridToXml.toXml(mainPage._grid);
			};
			this.getGridData = function() {
				var grid = mainPage._grid;
				if (!grid) {
					return {};
				}
				var selectedColIndex;
				var selectedRowId;
				//we need to call getPrevFocusedCell because in time of showing ContextMenu the menu is focused and grid.getFocusedCell() returns null
				var gridFocusedCell = grid.getFocusedCell() || grid.getPrevFocusedCell();
				if (gridFocusedCell && gridFocusedCell.headId) {
					selectedColIndex = grid.getColumnIndex(gridFocusedCell.headId);
					selectedRowId = gridFocusedCell.rowId;
				}

				var selectedRows = [];
				var focusInfo = [];
				var gridRow = selectedRowId && grid.getRow(selectedRowId);
				var cells = [];
				var focusedCell;
				var colCount = grid.getColumnCount();
				if (gridRow) {
					for (var j = 0; j < colCount; j++) {
						var cell = {
							data: gridRow.cells[j].data,
							text: grid.getCellValue(selectedRowId, j)
						};
						cells.push(cell);
						if (j === selectedColIndex) {
							focusedCell = cell;
						}
					}
				}
				var row = {
					cells: cells
				};
				selectedRows.push(row);
				if (focusedCell) {
					focusInfo.push({
						//we haven't multiselect now, but, we need this only for a case if several items was selected.
						//cellColumnIndex: selectedColIndex,
						cell: focusedCell,
						row: row
					});
				}

				return {
					selection: {
						rows: selectedRows
					},
					focus: focusInfo
				};
			};
		}
	});
});
