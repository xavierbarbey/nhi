﻿import Sidebar from '../components/sidebar';
import ContextMenu from '../components/contextMenu';
import utils from '../core/utils';
import { contentTabId } from './sidebarDataConverter';
import secondaryMenu from './secondaryMenu';

const pinUrl = 'svg-pinnedoff';
const unPinUrl = 'svg-pinnedon';
const disabledRowHighlightingClass =
	'aras-navigation-panel_row-highlighting-disabled';

const dashboardProperties = ['startPage', 'on_click_handler', 'formId'];
const isDashboard = item =>
	dashboardProperties.some(property => item[property]) || !item.itemTypeId;

function navFormatter(nav, item) {
	const itemValue = item.value;
	if (itemValue.children) {
		return null;
	}

	const isDashboardItem = isDashboard(itemValue);
	if (itemValue.itemTypeId || isDashboardItem) {
		const leafTemplate = nav.templates.getDefaultTemplate(item);
		const nodeClassList = isDashboardItem
			? 'aras-nav-leaf-ico'
			: 'aras-button aras-button_c aras-nav-leaf-ico';
		const nodeIconUrl = isDashboardItem
			? '../images/OpenInTab.svg'
			: '../images/ExecuteSearch.svg';

		leafTemplate.push(
			Inferno.createVNode(
				Inferno.getFlagsForElementVnode('span'),
				'span',
				nodeClassList,
				ArasModules.SvgManager.createInfernoVNode(nodeIconUrl),
				utils.infernoFlags.unknownChildren
			)
		);

		return leafTemplate;
	}

	return null;
}

export default class NavigationPanel extends Sidebar {
	nav = document.createElement('aras-nav');
	tabs = document.createElement('aras-navigation-panel-tabs');

	togglePin(pinned) {
		super.togglePin(pinned);
		this.render();
	}

	toggleVisibility(visible) {
		super.toggleVisibility(visible);
		const splitter = this.nextElementSibling;
		splitter.classList.toggle('aras-hide', !this.isVisible);
		const navButtonID =
			'com.aras.innovator.cui_default.mwh_header_navigation_button';
		const toolbar = document.querySelector('aras-toolbar');
		const navButton = toolbar.data.get(navButtonID);
		if (this.isVisible !== navButton.state) {
			toolbar.data.set(
				navButtonID,
				Object.assign({}, navButton, { state: this.isVisible })
			);
			toolbar.render();
		}
	}

	render() {
		const secondaryMenuData = this._getSecondaryMenuData();
		const baseUrl = window.location.href.replace(window.location.hash, '');
		const pinIconUrl = baseUrl + '#' + (this.isPinned ? unPinUrl : pinUrl);

		this.html`
		<div class="aras-navigation-panel__header">
			<button
				class="${'aras-button aras-button_c ' + (secondaryMenuData ? '' : 'aras-hide')}"
				onclick="${() => this._hideSecondaryMenu()}"
			>
				${ArasModules.SvgManager.createHyperHTMLNode('../images/BackFull.svg', {
					class: 'aras-button__icon'
				})}
			</button>
			${ArasModules.SvgManager.createHyperHTMLNode(
				secondaryMenuData && secondaryMenuData.icon,
				{ class: 'aras-navigation-panel__header-icon' }
			)}
			<span class="aras-navigation-panel__header-title">
				${
					secondaryMenuData
						? secondaryMenuData.pluralLabel
						: aras.getResource('', 'common.contents')
				}
			</span>
			<span class="aras-navigation-panel__pin-icon aras-button aras-button_c" onclick="${() =>
				this.togglePin()}">
				<svg class="aras-button__icon"><use href="${pinIconUrl}"></use></svg>
			</span>
		</div>
		${this.nav}
		${secondaryMenu(this, secondaryMenuData)}
		${this.tabs}`;
	}

	connectedCallback() {
		super.connectedCallback();
		this.classList.add('aras-navigation-panel', disabledRowHighlightingClass);
		this.setAttribute('role', 'navigation');
		this.popupMenu = new ContextMenu();
		this._initContextMenuHandlers();
		this._initContextMenuListeners();
		const observer = new MutationObserver(mutations => {
			const navPanel = mutations[0].target;
			const panelWidth = navPanel.style.width;
			navPanel.nextElementSibling.style.left = panelWidth;
		});

		observer.observe(this, {
			attributes: true,
			attributeFilter: ['style']
		});
		this.render();
	}

	created() {
		this._initTOC();
		this._initTabs();
	}

	_initContextMenuHandlers() {
		const sidebarTabs = this.tabs;
		const contextMenuMethods = {
			pinItemType: (id, tabData) => sidebarTabs.addTab(id, tabData),
			unpinItemType: id => {
				const tabsData = sidebarTabs.data;
				const removedTab = tabsData.get(id);
				sidebarTabs.removeTab(id);
				return removedTab;
			},
			getItemTypeByItemTypeId: itemTypeId => {
				let tabId;
				const tabsData = sidebarTabs.data;
				tabsData.forEach((data, id) => {
					if (data.itemTypeId === itemTypeId) {
						tabId = id;
					}
				});
				return tabsData.get(tabId);
			},
			isDashboard,
			openSearch: (...args) => window.arasTabs.openSearch(...args)
		};
		const dispatchContextMenuEvent = (location, event, options) => {
			event.preventDefault();
			const detail = {
				location,
				x: event.clientX,
				y: event.clientY,
				...options
			};
			const cuiEvent = new CustomEvent('navigationPanelContextMenu', {
				detail
			});
			this.dispatchEvent(cuiEvent);
		};
		sidebarTabs.on('contextmenu', (itemKey, event) => {
			const currentTarget = sidebarTabs.data.get(itemKey);
			dispatchContextMenuEvent('PopupMenuMainWindowTOC', event, {
				currentTarget,
				...contextMenuMethods
			});
		});
		const nav = this.nav;
		nav.on('contextmenu', (itemKey, event) => {
			nav.select(itemKey);
			const currentTarget = nav.data.get(itemKey);
			dispatchContextMenuEvent('PopupMenuMainWindowTOC', event, {
				currentTarget,
				...contextMenuMethods
			});
		});
		this._favoritesContextMenuHandler = event => {
			const favoriteItemRow = event.target.closest('.aras-nav__child');
			if (!favoriteItemRow) {
				return;
			}

			favoriteItemRow.classList.add('aras-nav__child_selected');
			const unselectHighlightedItem = () => {
				favoriteItemRow.classList.remove('aras-nav__child_selected');
				this.popupMenu.dom.removeEventListener(
					'contextMenuClose',
					unselectHighlightedItem
				);
			};
			this.popupMenu.dom.addEventListener(
				'contextMenuClose',
				unselectHighlightedItem
			);

			const favoriteId = favoriteItemRow.dataset.id;
			const currentTarget = window.favorites.get(favoriteId);
			dispatchContextMenuEvent('PopupMenuSecondaryMenu', event, {
				currentTarget
			});
		};
	}

	_initContextMenuListeners() {
		const contextMenuDom = this.popupMenu.dom;
		contextMenuDom.addEventListener('contextMenuShow', () => {
			this.classList.remove(disabledRowHighlightingClass);
		});
		contextMenuDom.addEventListener('contextMenuClose', () => {
			this.classList.add(disabledRowHighlightingClass);
			if (!this.isPinned) {
				this._focusOutHandler();
			}
		});
		contextMenuDom.addEventListener('click', () => {
			this._hideIfNotPinned();
		});
	}

	_initTOC() {
		const selectTab = dataItem => {
			const tabs = this.tabs;
			tabs.data.forEach((tabItem, id) => {
				if (tabItem.itemTypeId === dataItem.itemTypeId) {
					tabs.selectTab(id);
				}
			});
		};
		this.nav.classList.add('aras-nav-toc');
		this.nav.formatter = navFormatter.bind(null, this.nav);
		const handler = (itemKey, event) => {
			if (
				event.type === 'keydown' &&
				![13, 32, 'Enter', 'Space', 'NumpadEnter'].includes(
					event.code || event.keyCode
				)
			) {
				return;
			}
			const dataItem = this.nav.data.get(itemKey);
			const isNewTabOpened =
				dataItem.on_click_handler ||
				dataItem.formId ||
				dataItem.startPage ||
				event.target.closest('.aras-button.aras-nav-leaf-ico');
			if (isNewTabOpened) {
				this._hideIfNotPinned();
			} else if (dataItem.itemTypeId) {
				this._showSecondaryMenu(dataItem.itemTypeId);
				selectTab(dataItem);
			}
		};
		this.nav.on('click', handler);
		this.nav.on('keydown', handler);
	}

	_initTabs() {
		this.tabs.on('select', id => {
			if (id === contentTabId) {
				this._hideSecondaryMenu();
				return;
			}

			const item = this.tabs.data.get(id);
			this._showSecondaryMenu(item.itemTypeId);
		});
		this.tabs.on('click', id => {
			if (id === contentTabId) {
				this._hideSecondaryMenu();
			}
		});
	}

	_isElementInSidebar(element) {
		return (
			super._isElementInSidebar(element) ||
			(element && this.popupMenu.dom.contains(element))
		);
	}

	_hideIfNotPinned() {
		if (!this.isPinned) {
			this.toggleVisibility(false);
			document.documentElement.focus(); // for correct work of sidebar in IE and FF
		}
	}

	_getSecondaryMenuData() {
		const itemTypeId = this._secondaryMenuItemTypeId;
		if (!itemTypeId) {
			return null;
		}

		const itemType = aras.getItemTypeDictionary(itemTypeId, 'id');
		if (itemType.isError()) {
			return null;
		}
		const itemTypeName = itemType.getProperty('name');
		const singularLabel = itemType.getProperty('label') || itemTypeName;
		const pluralLabel = itemType.getProperty('label_plural') || singularLabel;
		const canAdd = aras.getPermissions('can_add', itemTypeId);
		const icon =
			itemType.getProperty('open_icon') || '../images/DefaultItemType.svg';
		const favoritesFilterCriteria = {
			quickAccessFlag: '1',
			contextType: itemTypeName
		};
		const favoriteSearches = window.favorites.getDataMap(
			'Search',
			favoritesFilterCriteria
		);
		const favoriteItems = window.favorites.getDataMap(
			'Item',
			favoritesFilterCriteria
		);
		const favoriteCategories = [];
		if (favoriteSearches.size) {
			favoriteCategories.push({
				icon,
				overlayIcons: ['../images/SavedSearchOverlay.svg'],
				unpinIcon: '../images/Close.svg',
				label: aras.getResource(
					'',
					'navigation_panel.secondary_menu.favorite_searches'
				),
				items: Array.from(favoriteSearches.values())
			});
		}
		if (favoriteItems.size) {
			favoriteCategories.push({
				icon,
				label: pluralLabel,
				items: Array.from(favoriteItems.values())
			});
		}
		const loggedUserIdentity = aras.getIsAliasIdentityIDForLoggedUser();

		return {
			icon,
			singularLabel,
			pluralLabel,
			itemTypeId,
			canAdd,
			loggedUserIdentity,
			favoriteCategories
		};
	}

	transitionHandler = () => {
		this.querySelector('.aras-secondary-menu').focus();
		this.nav.style.visibility = 'hidden';
		this.removeEventListener('transitionend', this.transitionHandler);
	};

	async _showSecondaryMenu(itemTypeId) {
		this._secondaryMenuItemTypeId = itemTypeId;
		await this.render();
		this.addEventListener('transitionend', this.transitionHandler);
	}

	_hideSecondaryMenu() {
		this._secondaryMenuItemTypeId = null;
		this.tabs.selectTab(contentTabId);
		this.render();
		this.nav.style.visibility = 'visible';
		this.nav.focus();
	}

	_favoritesClick(event) {
		const favoriteItemRow = event.target.closest('.aras-nav__child');
		if (!favoriteItemRow) {
			return;
		}

		const favoriteId = favoriteItemRow.dataset.id;
		const favoriteItem = window.favorites.get(favoriteId);
		const unpinIconNode = event.target.closest(
			'.aras-button.aras-nav-leaf-ico'
		);
		if (unpinIconNode) {
			window.favorites.removeFromQuickAccess(favoriteId);
		} else if (favoriteItem.category === 'Item') {
			aras.uiShowItemLastVersion(
				favoriteItem.contextType,
				favoriteItem.additional_data.id
			);
			this._hideIfNotPinned();
		} else if (favoriteItem.category === 'Search') {
			const secondaryMenuData = this._getSecondaryMenuData();
			this._searchItemType(secondaryMenuData.itemTypeId, favoriteId);
		}
	}

	_createNewItem(itemTypeId) {
		return aras.uiNewItemExAsync(itemTypeId).then(() => {
			this._hideIfNotPinned();
		});
	}

	_searchItemType(itemTypeId, favoriteSearchId) {
		return Promise.resolve(
			window.arasTabs.openSearch(itemTypeId, favoriteSearchId)
		).then(() => {
			this._hideIfNotPinned();
		});
	}
}

NavigationPanel.define('aras-navigation-panel');
