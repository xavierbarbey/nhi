﻿import { adaptGridHeader, adaptRelationshipGridHeader } from './cuiGridData';

const cachedRowStyles = new Map();

async function cuiGrid(control, options = {}) {
	let gridHeaderInfo;
	const gridWrapper = options.gridWrapper;
	if ('relatedItemTypeId' in options) {
		gridHeaderInfo = await adaptRelationshipGridHeader(options.itemTypeId, options.userPreferences, options.relatedItemTypeId);
	} else {
		gridHeaderInfo = await adaptGridHeader(options.itemTypeId, options.userPreferences);
		gridWrapper._itemType = await aras.getItemTypeDictionaryJson(options.itemTypeId, 'id');
	}

	gridWrapper.grid_Experimental.order = gridHeaderInfo.columnsOrder;

	initGridHeader(control, gridHeaderInfo, gridWrapper);
}

function initGridHeader(grid, headerInfo = {headMap: new Map(), indexHead: [], metadata: {headLists: []}}, gridWrapper = {}) {
	grid.head = headerInfo.headMap;
	grid.settings.indexHead = headerInfo.indexHead;
	grid.rows = new Map();
	grid.settings.indexRows = [];
	grid.settings.selectedRows = [];
	grid.getCellStyles = getCellStyles;
	grid.getCellType = getCellType;
	grid.getCellMetadata = getCellMetadataHandler(headerInfo.metadata, gridWrapper);
}

function getCellStyles(headId, rowId) {
	const styleString = this.rows.get(rowId, 'css') || '';
	const rowStyles = cachedRowStyles.get(styleString) || parseCss(styleString);
	cachedRowStyles.set(styleString, rowStyles);

	const propertyName = this.head.get(headId, 'name') || headId;
	return rowStyles.get(propertyName) || {};
}

function getCellType(headId, rowId, value, type) {
	const propertyName = this.head.get(headId, 'name') || headId;
	const isRestricted = this.rows.get(rowId, `${propertyName}@aras.restricted`);
	if (isRestricted) {
		return 'restricted';
	}

	return this.head.get(headId, 'dataType');
}

const getCellMetadataHandler = (metadata = {}, gridWrapper) => {
	const currentUserId = aras.getCurrentUserID();
	return function(headId, rowId, type) {
		const headInfo = this.head._store.get(headId);
		let list = metadata.headLists[headInfo.dataSource] || [];
		if (type === 'classification') {
			const linkProperty = headInfo.linkProperty;
			list = metadata.classStructure.get(
				linkProperty ? metadata.linkItemTypeName[linkProperty] : metadata.itemTypeName
			);
		}

		return {
			list,
			currentUserId,
			format: rowId === 'searchRow' ? 'short_date' : headInfo.pattern,
			itemType: headInfo.dataSourceName,
			scale: headInfo.scale,
			precision: headInfo.precision,
			editorClickHandler: () => {
				gridWrapper.turnEditOff();
				gridWrapper.onStartEdit_Experimental(rowId, headId);
				gridWrapper.onInputHelperShow_Experimental(rowId, headInfo.layoutIndex);
			},
			handler: () => {
				gridWrapper.onInputHelperShow_Experimental(rowId, headInfo.layoutIndex);
			}
		};
	};
};

const normalizeCSS = (css = '') => {
	css = css.trim();
	if (!css) {
		return '';
	}

	const trimKeys = '{}:;';
	css = trimKeys.split('').reduce((css, key) => {
		return css.replace(new RegExp('[\\s]*\\' + key +  '[\\s]*', 'g'), key);
	}, css);
	return css;
};

const parseCss = cssString => {
	cssString = normalizeCSS(cssString);

	const cssBlocks = cssString.match(/\..*?\}/g) || [];
	const stylesMap = cssBlocks.reduce((acc, cssBlock) => {
		const propertyName = cssBlock.match(/\..*(?=\{)/)[0].slice(1);
		const rules = cssBlock.match(/\{.*(?=\})/)[0].slice(1);
		const resultObject = {};
		rules.split(';').forEach(rule => {
			if (!rule) {
				return;
			}

			rule = rule.split(':');
			resultObject[rule[0]] = rule[1];
		});

		acc.set(propertyName, resultObject);
		return acc;
	}, new Map());

	return stylesMap;
};

export default cuiGrid;
