﻿import cuiMethods from './cuiMethods';
import cuiToolbar from './cuiToolbar';

const defaultControlType = 'default';
const accordionComponentName = 'aras-accordion';

const defaultConstructor = (control, childNodes, metadata) => {
	const { additional_data: additionalData = {}, name, aria_label: ariaLabel } = control;
	const { cssClass = '', cssStyle = '', attributes = {} } = additionalData;
	const { webComponentName } = metadata;

	return HyperHTMLElement.wire(null, `:${name}`)(
		[
			`<${webComponentName} class="`,
			`" style="`,
			`" aria-label="`,
			...Object.keys(attributes).map(attribute => `" ${attribute}="`),
			`" >`,
			`</${webComponentName}>`
		],
		cssClass,
		cssStyle,
		ariaLabel,
		...Object.values(attributes),
		childNodes
	);
};

const tabContainerControlConstructor = (control, childNodes, metadata) => {
	const tabContainerNode = defaultConstructor(control, [], metadata);
	const switcher = HyperHTMLElement.wire()`
		<aras-switcher>${childNodes}</aras-switcher>
	`;

	tabContainerNode.switcher = switcher;
	return [ tabContainerNode, switcher ];
};

const tabElementControlConstructor = (control, childNodes, metadata) => {
	const { additional_data: additionalData = {}, label, name } = control;
	control.additional_data = {
		...additionalData,
		attributes: {
			'data-tab-id': name,
			'data-tab-label': label || name,
			'switcher-pane-id': name,
			...additionalData.attributes
		}
	};

	return defaultConstructor(control, childNodes, metadata);
};

const initTabContainerControl = control => {
	const switcher = control.switcher;
	const tabs = Array
		.from(switcher.children)
		.filter(child => child.matches('[data-tab-id]'));
	if (!tabs.length) {
		return;
	}

	const accordion = control.closest(accordionComponentName);
	if (accordion) {
		control.on('click', () => {
			if (accordion.collapsed) {
				accordion.expand();
			}
		});
		const keyCodes = {
			13: 'Enter',
			32: 'Space',
		};
		control.on('keydown', (id, e) => {
			const key = e.code || keyCodes[e.keyCode];
			if (accordion.collapsed && Object.values(keyCodes).includes(key)) {
				accordion.expand();
			}
		});
	}

	const [ firstTab ] = tabs;
	tabs.forEach(tab => {
		control.addTab(
			tab.dataset.tabId,
			{
				label: tab.dataset.tabLabel
			}
		);
	});
	control.selectTab(firstTab.dataset.tabId);
};

const cuiControls = {
	[defaultControlType]: {
		constructor: defaultConstructor,
		webComponentName: 'div'
	},
	AccordionElementControl: {
		webComponentName: accordionComponentName
	},
	FormControl: {
		webComponentName: 'aras-form'
	},
	TabContainerControl: {
		constructor: tabContainerControlConstructor,
		initControl: initTabContainerControl,
		webComponentName: 'aras-tabs'
	},
	TabElementControl: {
		constructor: tabElementControlConstructor
	},
	ToolbarControl: {
		eventHandler: cuiMethods.reinitControlItems,
		initControl: cuiToolbar,
		webComponentName: 'aras-toolbar'
	}
};

const getControlMetadata = type => ({...cuiControls[defaultControlType], ...cuiControls[type]});

export default getControlMetadata;
