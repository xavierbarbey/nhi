﻿import CuiLayout from './CuiLayout';

export default class RelationshipsGridCuiLayout extends CuiLayout {
	_getCuiControls() {
		return Promise.resolve([
			{
				control_type: 'ToolbarControl',
				id: 'relationshipsTitleBar',
				additional_data: {
					cssClass: 'aras-titlebar aras-titlebar_c',
					attributes: {
						role: 'heading',
						'aria-level': '2'
					}
				},
				name: 'ItemView.RelationshipsTitleBar',
				'location@keyed_name': 'ItemView.RelationshipsTitleBar'
			},
			{
				control_type: 'ToolbarControl',
				additional_data: {
					cssClass: 'aras-commandbar'
				},
				id: 'relationshipsCommandBar',
				name: 'ItemView.RelationshipsCommandBar',
				aria_label: aras.getResource('', 'aria_label.ItemView.RelationshipsCommandBar'),
				'location@keyed_name': 'ItemView.RelationshipsCommandBar'
			}
		]);
	}
}
