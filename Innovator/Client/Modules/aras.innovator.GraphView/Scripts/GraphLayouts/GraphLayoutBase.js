﻿define([
	'dojo/_base/declare'
],
function(declare) {
	return declare('GraphNavigation.Layouts.GraphLayoutBase', null, {
		d3: null,
		ownerControl: null,

		constructor: function(initialArguments) {
			this.d3 = initialArguments.d3Framework;
		},

		setOwner: function(newOwner) {
			if (newOwner) {
				this.ownerControl = newOwner;
			}
		},

		_getConnectorInterpolationPoints: function(connectorData) {
			const points = [];

			if (connectorData && connectorData.source && connectorData.target) {
				const sourceConnectorMargin = Math.max(50, (this.isVertical ? connectorData.source.height : connectorData.source.width) / 2);
				const targetConnectorMargin = Math.max(50, (this.isVertical ? connectorData.target.height : connectorData.target.width) / 2);

				const startX = connectorData.source.x + (this.isVertical ? 0 : connectorData.source.width / 2);
				const startY = connectorData.source.y + (this.isVertical ? connectorData.source.height / 2 : 0);
				const endX = connectorData.target.x - (this.isVertical ? 0 : connectorData.target.width / 2);
				const endY = connectorData.target.y - (this.isVertical ? connectorData.target.height / 2 : 0);

				points.push([startX, startY]);
				points.push([startX + (this.isVertical ? 0 : sourceConnectorMargin), startY + (this.isVertical ? sourceConnectorMargin : 0)]);

				if (this._isLabelShiftRequired(connectorData)) {
					points.push([this.isVertical ? connectorData.x : startX + sourceConnectorMargin, this.isVertical ? startY + sourceConnectorMargin : connectorData.y]);
					points.push([connectorData.x, connectorData.y]);
					points.push([this.isVertical ? connectorData.x : endX - targetConnectorMargin, this.isVertical ? endY - targetConnectorMargin : connectorData.y]);
				}

				points.push([endX - (this.isVertical ? 0 : targetConnectorMargin), endY - (this.isVertical ? targetConnectorMargin : 0)]);
				if (connectorData.arrowhead) {
					points.push([endX - (this.isVertical ? 0 : connectorData.arrowhead.height), endY - (this.isVertical ? connectorData.arrowhead.height : 0)]);
				}
				points.push([endX, endY]);
			}

			return points;
		},

		_computeConnectorLabelDefaultPosition: function(connectorData) {
			connectorData.x = (connectorData.source.x + connectorData.target.x) * 0.5;
			connectorData.y = (connectorData.source.y + connectorData.target.y) * 0.5;

			if (this._isLabelShiftRequired(connectorData)) {
				if (this.isVertical) {
					const horizontalShift = connectorData.source.x - connectorData.target.x < 0 ? -connectorData.source.width : connectorData.source.width;
					connectorData.x = connectorData.source.x + horizontalShift;
				} else {
					const verticalShift = connectorData.source.y - connectorData.target.y < 0 ? -connectorData.source.height : connectorData.source.height;
					connectorData.y = connectorData.source.y + verticalShift;
				}
			}
		},

		_isLabelShiftRequired: function(connectorData) {
			if (this.isVertical) {
				return Math.abs(connectorData.source.x - connectorData.target.x) < 2 * connectorData.target.width && connectorData.source.y >= connectorData.target.y;
			} else {
				return Math.abs(connectorData.source.y - connectorData.target.y) < 2 * connectorData.target.height && connectorData.source.x >= connectorData.target.x;
			}
		},

		_getInputExpandButtonCoordinates: function(dataItem) {
			if (dataItem) {
				return this.isVertical ? {x: dataItem.width / 2, y: 0} : {x: 0, y: dataItem.height / 2};
			}
		},

		_getOutputExpandButtonCoordinates: function(dataItem) {
			if (dataItem) {
				return this.isVertical ? {x: dataItem.width / 2, y: dataItem.height} : {x: dataItem.width, y: dataItem.height / 2};
			}
		}
	});
});
