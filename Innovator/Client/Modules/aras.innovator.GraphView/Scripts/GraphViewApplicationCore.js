﻿define([
	'dojo/_base/declare',
	'dojo/aspect',
	'GraphView/Scripts/GraphDataLoader',
	'GraphView/Scripts/GraphControl',
	'GraphView/Scripts/GraphLayouts/GraphLayoutsEnum'
],
function(declare, aspect, GraphDataLoader, GraphControl, GraphLayoutsEnum) {
	return declare('GraphNavigation.GraphViewApplicationCore', null, {
		arasObject: null,
		toolbarControl: null,
		graphControl: null,
		containerNode: null,
		panCoordinates: null,
		mouseButtonClickCount: 0,
		contextItem: null,
		graphViewDefinitionId: null,
		gvdToolbarItemPrefix: 'toolbar_',

		constructor: function(initialArguments) {
			if (!initialArguments || !initialArguments.arasObject) {
				throw new Error('"arasObject" parameter should be specified in constructor arguments for GraphViewApplicationCore');
			}

			const initialLayout = initialArguments.graphLayout || GraphLayoutsEnum.LayoutType.HorizontalTree;
			this.arasObject = initialArguments.arasObject;
			this.contextItem = initialArguments.contextItem;
			this.contextItemType = initialArguments.contextItem.getAttribute('type');
			this.dataLoader = new GraphDataLoader({
				arasObject: this.arasObject
			});
			this.graphViewDefinitionId = initialArguments.gvdId;
			if (!this.graphViewDefinitionId) {
				const gvdList = this.dataLoader.getGVDList(this.contextItemType);
				this.graphViewDefinitionId = gvdList.length ? gvdList[0].id : '';
			}
			this.dataLoader.setGvdId(this.graphViewDefinitionId);
			this.graphControl = new GraphControl({
				connectId: 'graphView',
				graphLayout: initialLayout,
				arasObject: this.arasObject
			});

			this.containerNode = document.querySelector('#graphView');
			this.svgNode = this.containerNode.firstChild;

			clientControlsFactory.createControl('Aras.Client.Controls.Public.ToolBar', {id: 'graphToolbar', connectId: 'graphToolbar'},	function(control) {
				control.loadXml(this.arasObject.getI18NXMLResource('graph_toolbar.xml'));

				clientControlsFactory.on(control, {
					'onClick': this.onToolbarItemClick.bind(this),
					'onChange': this.onToolbarLayoutChange.bind(this),
					'onDropDownItemClick': this.onGVDDropdownClick.bind(this)
				});

				control.show();
				this.toolbarControl = control;
				this.graphLayoutChoise = control.getItem('graphLayout');
				this.graphGVDChoise = control.getItem('openGVD');
				this.initLayoutDropdown(initialLayout);
				this.initGVDListDropdown(this.graphViewDefinitionId);
			}.bind(this));
		},

		init: function() {
			aspect.after(this.graphControl, 'onMenuInit', this.onPopupMenuInit.bind(this), true);
			aspect.after(this.graphControl, 'onMenuSetup', this.onPopupMenuSetup.bind(this), true);
			aspect.after(this.graphControl, 'onMenuItemClick', this.onPopupMenuItemClick.bind(this), true);
			aspect.after(this.graphControl, 'onItemExtend', this.onItemExtend.bind(this), true);

			window.addEventListener('resize', this.onWindowResize.bind(this));
			window.addEventListener('beforeunload', this.onBeforeUnload.bind(this));
			this.svgNode.addEventListener('selectstart', this.onViewerSelectStart.bind(this));
			this.svgNode.addEventListener('wheel', this.onWheelEventHandler.bind(this));
			this.svgNode.addEventListener('mousedown', this.onViewerStartPan.bind(this));
			this.svgNode.addEventListener('mouseup', this.onDoubleClickHandler.bind(this));
			document.addEventListener('mousemove', this.onViewerPan.bind(this));
			document.addEventListener('mouseleave', this.onViewerEndPan.bind(this));
			document.addEventListener('mouseup', this.onViewerEndPan.bind(this));
		},

		initLayoutDropdown: function(initialValue) {
			if (this.graphLayoutChoise) {
				this.graphLayoutChoise.removeAll();

				let layoutList = GraphLayoutsEnum.getLayoutItemsList();
				Array.prototype.forEach.call(layoutList, function(layoutItem) {
					this.graphLayoutChoise.Add(layoutItem.id, layoutItem.label);
				}.bind(this));

				if (initialValue) {
					this.graphLayoutChoise.setSelected(GraphLayoutsEnum.getLayoutIdByType(initialValue));
				}
			}
		},

		initGVDListDropdown: function(initialGvdId) {
			if (this.graphGVDChoise) {
				this.graphGVDChoise.removeAll();

				const widgetItem = this.graphGVDChoise._item_Experimental;
				const contextItemType = this.contextItem.getAttribute('type');
				const gvdList = this.dataLoader.getGVDList(contextItemType);
				Array.prototype.forEach.call(gvdList, function(gvd) {
					this.graphGVDChoise.Add(this.gvdToolbarItemPrefix + gvd.id, gvd.name);
					if (initialGvdId === gvd.id) {
						widgetItem.setLabel(widgetItem.label + ' ' + gvd.name);
						this.graphGVDChoise.setSelected(this.gvdToolbarItemPrefix + gvd.id);
					}
				}.bind(this));
			}
		},

		loadView: function(contextItemId, contextItemType) {
			let graphData = this.dataLoader.loadData(contextItemId, contextItemType, this.graphViewDefinitionId);

			if (graphData && graphData.nodes && graphData.nodes.length) {
				this.graphControl.setContextItemId(graphData.nodes[0].id);
				this.graphControl.loadGraph(graphData);
			}
		},

		_isConnectorItem: function(itemData) {
			return itemData && itemData.source && itemData.target;
		},

		getGraphPopupItems: function() {
			return [
				{id: 'center_context_item', name: 'Center on Context Item'},
				{id: 'fit_all', name: 'Fit All'},
				{id: 'graph_separator', separator: true},
				{id: 'layout', name: 'Layout', subMenu: [
					{id: 'vertical_tree_layout', name: 'Vertical Tree Layout'},
					{id: 'horizontal_tree_layout', name: 'Horizontal Tree Layout'},
					{id: 'force_layout', name: 'Force Layout'}
				]}
			];
		},

		getGraphItemPopupItems: function(itemData) {
			const isPopupItemAvailable = function(item) {
				return item.itemId && item.type;
			};

			return [
				{id: 'expand', name: 'Expand', isAvailable: function() { return !this._isConnectorItem(itemData); }.bind(this)},
				{id: 'collapse', name: 'Collapse'},
				{id: 'center_node', name: 'Center in Window'},
				{id: 'node_separator', separator: true, isAvailable: isPopupItemAvailable},
				{id: 'open_graph_view', name: 'Open Graph View', subMenu: this.getPopupGvdItems(itemData && itemData.type), isAvailable: isPopupItemAvailable},
				{id: 'open_item', name: 'Open Item', isAvailable: isPopupItemAvailable}
			];
		},

		getPopupGvdItems: function(itemType) {
			if (itemType) {
				return this.dataLoader.getGVDList(itemType).map(function(gvd) {
					return {
						id: String.prototype.concat(gvd.id, '_', gvd.name),
						name: gvd.name
					};
				});
			}
		},

		getVisibleMenuItemsHash: function(itemData) {
			let visibleMenuItemsHash = {};
			const visibleMenuItems = itemData ? this.getGraphItemPopupItems(itemData) : this.getGraphPopupItems();
			const setMenuItemVisible = function(menuItem) {
				visibleMenuItemsHash[menuItem.id] = menuItem.isAvailable ? menuItem.isAvailable(itemData) : true;
			};

			Array.prototype.forEach.call(visibleMenuItems, function(menuItem) {
				setMenuItemVisible(menuItem);

				if (menuItem.subMenu) {
					Array.prototype.forEach.call(menuItem.subMenu, setMenuItemVisible);
				}
			});

			return visibleMenuItemsHash;
		},

		onPopupMenuInit: function(itemData) {
			return this.getGraphPopupItems().concat(this.getGraphItemPopupItems(itemData));
		},

		onPopupMenuSetup: function(menuControl) {
			const visibleMenuItemsHash = this.getVisibleMenuItemsHash(menuControl.rowId.data);
			let newGVDs = this.getPopupGvdItems(menuControl.rowId.data && menuControl.rowId.data.type);

			Array.prototype.forEach.call(newGVDs || [], function(gvd) {
				if (!menuControl.collectionMenu[gvd.id]) {
					menuControl.addRange([gvd], 'open_graph_view');
				}
			});

			for (let menuItemId in menuControl.collectionMenu) {
				menuControl.setHide(menuItemId, !visibleMenuItemsHash[menuItemId]);
			}
			for (let menuSeparatorId in menuControl.collectionSeparator) {
				menuControl.setHideSeparator(menuSeparatorId, !visibleMenuItemsHash[menuSeparatorId]);
			}
		},

		onPopupMenuItemClick: function(commandId, itemData) {
			switch (commandId) {
				case 'center_context_item':
					this.graphControl.centerCINode();
					break;
				case 'fit_all':
					this.graphControl.centerView();
					break;
				case 'expand':
					this.graphControl.expandNode(itemData);
					break;
				case 'collapse':
					if (this._isConnectorItem(itemData)) {
						this.graphControl.collapseConnector(itemData);
					} else {
						this.graphControl.collapseNode(itemData);
					}
					break;
				case 'center_node':
					this.graphControl.centerNode(this._isConnectorItem(itemData) ? itemData.connectorDomNode : itemData.domNode);
					break;
				case 'open_item':
					this.arasObject.uiShowItem(itemData.type, itemData.itemId);
					break;
				default:
					let isLayoutItemId = GraphLayoutsEnum.getLayoutItemsList().filter(function(item) {return item.id === commandId;}).length === 1;
					if (isLayoutItemId) {
						this.graphLayoutChoise.setSelected(commandId);
					} else {
						this.arasObject.evalItemMethod(
							'gn_ShowGraph',
							this.arasObject.getItemById(itemData.type, itemData.itemId),
							{gvdId: commandId.substr(0, commandId.indexOf('_'))}
						);
					}
					break;
			}
		},

		onToolbarItemClick: function(targetButton) {
			let actionId = targetButton.getId();

			switch (actionId) {
				case 'centerCI':
					this.graphControl.centerCINode();
					break;
				case 'fitGraph':
					this.graphControl.centerView();
					break;
				case 'openGVD':
					const selectedWidget = this.graphGVDChoise.getItem(this.gvdToolbarItemPrefix + this.graphViewDefinitionId)._item_Experimental;
					selectedWidget.domNode.classList.add('dijitSelectSelectedOption');
					selectedWidget.domNode.classList.add('dijitMenuItemSelected');
					break;
			}
		},

		onToolbarLayoutChange: function(targetDropdown) {
			let selectedItemId = targetDropdown.getSelectedItem();
			let selectedLayout = GraphLayoutsEnum.getLayoutTypeById(selectedItemId);

			if (selectedLayout) {
				this.graphControl.setLayoutType(selectedLayout);
			}
		},

		onGVDDropdownClick: function(gvdId) {
			if (gvdId) {
				gvdId = gvdId.replace(this.gvdToolbarItemPrefix, '');
				this.arasObject.evalItemMethod('gn_ShowGraph', this.contextItem, {gvdId: gvdId});
			}
		},

		onItemExtend: function(nodeData) {
			if (nodeData) {
				const nodeId = JSON.stringify(nodeData.node_key);
				return this.dataLoader.extendData(
					nodeId,
					nodeData.type,
					this.graphViewDefinitionId,
					nodeData.activeOutgoingConnectors
				);
			}
		},

		onWindowResize: function() {
			this.graphControl.adjustViewBounds();
		},

		onViewerSelectStart: function(targetEvent) {
			this._stopEvent(targetEvent);
		},

		_stopEvent: function(targetEvent) {
			targetEvent.stopPropagation();
			targetEvent.preventDefault();
		},

		setGraphScale: function(newScale) {
			const currentCenterX = (this.containerNode.scrollLeft + this.containerNode.clientWidth / 2) / this.containerNode.scrollWidth;
			const currentCenterY = (this.containerNode.scrollTop + this.containerNode.clientHeight / 2) / this.containerNode.scrollHeight;

			this.graphControl.setGraphScale(newScale);

			this.containerNode.scrollLeft = currentCenterX * this.containerNode.scrollWidth - this.containerNode.clientWidth / 2;
			this.containerNode.scrollTop = currentCenterY * this.containerNode.scrollHeight - this.containerNode.clientHeight / 2;
		},

		onViewerStartPan: function(mouseEvent) {
			this.panCoordinates = {x: mouseEvent.clientX, y: mouseEvent.clientY};

			if (mouseEvent.button === 1) {
				// prevent default behavior for middle mouse button
				this._stopEvent(mouseEvent);
			}
		},

		onViewerPan: function(mouseEvent) {
			if (this.panCoordinates) {
				let containerNode = this.containerNode;
				let deltaX = mouseEvent.clientX - this.panCoordinates.x;
				let deltaY = mouseEvent.clientY - this.panCoordinates.y;

				if (deltaX) {
					containerNode.scrollLeft -= deltaX * 2;
				}
				if (deltaY) {
					containerNode.scrollTop -= deltaY * 2;
				}

				this.panCoordinates.x = mouseEvent.clientX;
				this.panCoordinates.y = mouseEvent.clientY;
			}
		},

		onViewerEndPan: function() {
			this.panCoordinates = null;
		},

		onDoubleClickHandler: function(mouseEvent) {
			if (mouseEvent.button < 2) {
				this.mouseButtonClickCount++;

				if (!this.doubleClickTimeout) {
					this.doubleClickTimeout = setTimeout(function() {
						clearTimeout(this.doubleClickTimeout);
						this.doubleClickTimeout = null;

						if (this.mouseButtonClickCount >= 2) {
							this.graphControl.centerView();
						}
						this.mouseButtonClickCount = 0;
					}.bind(this), 250);
				}
			} else {
				this.mouseButtonClickCount = 0;
			}
		},

		onWheelEventHandler: function(wheelEvent) {
			let scaleStep = wheelEvent.deltaY > 0 ? -0.1 : 0.1;
			let actualGraphScale = this.graphControl.getGraphScale();

			this.setGraphScale(actualGraphScale + scaleStep);
			this._stopEvent(wheelEvent);
		},

		onBeforeUnload: function() {
			window.removeEventListener('resize', this.onWindowResize);
			this.svgNode.removeEventListener('selectstart', this.onViewerSelectStart);
			this.svgNode.removeEventListener('wheel', this.onWheelEventHandler);
			this.svgNode.removeEventListener('mousedown', this.onViewerStartPan);
			this.svgNode.removeEventListener('mousedown', this.onDoubleClickHandler);
			document.removeEventListener('mousemove', this.onViewerPan);
			document.removeEventListener('mouseleave', this.onViewerEndPan);
			document.removeEventListener('mouseup', this.onViewerEndPan);
		}
	});
});
