﻿define([
	'dojo/_base/declare'
],
function(declare) {
	return declare('GraphNavigation.GraphDataLoader', null, {
		arasObject: null,
		gvdId: null,
		templatesHash: null,
		gvdListHash: null,
		defaultTemplate: {
			container: {
				type: 'grid',
				style: {
					width: 200,
					height: 90,
					color: '#F7F7F7',
					padding: {
						left: 12,
						right: 12,
						top: 12,
						bottom: 12
					},
					rows: [{height: 20}, {height: 16}, {height: 16}, {height: 16}],
					cols: [{width: 22}, {width: 120}, {width: 35}],
					border: {
						width: 1,
						color: '#555555',
						cornerRadius: 2,
						shadow: {
							horizontal: 1,
							vertical: 2,
							color: '#000000',
							opacity: 0.24
						}
					},
					cells: [
						{
							content: 'image',
							content_binding: 'icon',
							col: 0,
							row: 0
						},
						{
							content: 'text',
							content_binding: 'keyed_name',
							col: 1,
							row: 0
						},
						{
							content: 'text',
							content_binding: 'generation',
							col: 2,
							row: 0,
							style: {
								'vertical-alignment': 'top',
								'horizontal-alignment': 'end',
								font: {
									size: 10,
									family: 'Tahoma',
									weight: 'normal',
									color: '#777777'
								}
							}
						},
						{
							content: 'text',
							content_binding: 'name',
							col: 1,
							row: 1,
							style: {
								font: {
									size: 12,
									family: 'Tahoma',
									weight: 'normal',
									color: '#333333'
								}
							}
						},
						{
							content: 'text',
							content_binding: 'classification',
							col: 1,
							row: 2,
							style: {
								font: {
									size: 10,
									family: 'Tahoma',
									weight: 'normal',
									color: '#333333'
								}
							}
						},
						{
							content: 'text',
							content_binding: 'state',
							col: 1,
							row: 3,
							style: {
								font: {
									size: 10,
									family: 'Tahoma',
									weight: 'normal',
									color: '#777777'
								}
							}
						}
					]
				}
			},
			style: {
				arrowhead: {
					height: 17,
					width: 7
				}
			}
		},

		constructor: function(initialArguments) {
			this.arasObject = initialArguments.arasObject;
			this.templatesHash = {};
			this.gvdListHash = {};
		},

		setGvdId: function(newGvdId) {
			if (newGvdId) {
				this.gvdId = newGvdId;
			}
		},

		loadData: function(id, itemTypeName, gvdId) {
			if (id && itemTypeName) {
				return this._requestGraphData({
					id: id,
					type: itemTypeName,
					action: 'GetGraph',
					definition: gvdId || this.gvdId
				});
			}
		},

		extendData: function(id, itemTypeName, gvdId, outgoingConnectorKeys) {
			if (id && itemTypeName) {
				return this._requestGraphData({
					id: id,
					type: itemTypeName,
					action: 'GetNeighbors',
					definition: gvdId || this.gvdId,
					outgoingConnectorKeys: outgoingConnectorKeys
				});
			}
		},

		getGVDList: function(itemTypeName) {
			if (itemTypeName) {
				if (this.gvdListHash[itemTypeName]) {
					return this.gvdListHash[itemTypeName];
				}

				let requestItem = this.arasObject.newIOMItem('Method', 'gn_GetGVDList');
				requestItem.setPropertyItem('item', this.arasObject.getItemTypeDictionary(itemTypeName));
				requestItem = requestItem.apply();
				if (requestItem.isError()) {
					this.arasObject.AlertError(requestItem);
					return;
				}

				const resultGVDList = [{
					id: '',
					name: 'Default',
					description: 'Default GVD with empty id that is used for Raw Ad-hoc graph navigation'
				}];
				const viewDefinitionsCount = requestItem.getItemCount();
				for (let i = 0; i < viewDefinitionsCount; i++) {
					let viewDefinition = requestItem.getItemByIndex(i);

					resultGVDList.push({
						id: viewDefinition.getProperty('id'),
						name: viewDefinition.getProperty('name'),
						description: viewDefinition.getProperty('description')
					});
				}

				return (this.gvdListHash[itemTypeName] = resultGVDList);
			}
		},

		_requestGraphData: function(requestData) {
			if (requestData) {
				let requestItem = this.arasObject.newIOMItem('Method', 'gn_GraphNavigation');
				requestItem.setProperty('item_type', requestData.type);
				requestItem.setProperty('item_ids', requestData.id);
				requestItem.setProperty('case', requestData.action);
				requestItem.setProperty('definition', requestData.definition);

				if (requestData.outgoingConnectorKeys && requestData.outgoingConnectorKeys.length) {
					Array.prototype.forEach.call(requestData.outgoingConnectorKeys, function(connectorKey) {
						let outgoingConnector = this.arasObject.newIOMItem('outgoing_connector');
						outgoingConnector.setProperty('key', JSON.stringify(connectorKey));
						requestItem.addRelationship(outgoingConnector);
					}.bind(this));
				}

				requestItem = requestItem.apply();
				if (requestItem.isError()) {
					this.arasObject.AlertError(requestItem);
					return;
				}

				let jsonData = requestItem.getResult();
				try {
					let graphJson = JSON.parse(jsonData);
					return this.prepareGraphData(graphJson);
				} catch (exception) {
					return this.arasObject.AlertError('Server returned invalid JSON structure.');
				}
			}
		},

		_getNodeKey: function(nodeCompositeKey) {
			if (nodeCompositeKey && typeof nodeCompositeKey === 'object') {
				let objectKeys = Object.keys(nodeCompositeKey.nid).sort();
				let compositeKey = nodeCompositeKey.gvnid + '_';

				for (let i = 0, length = objectKeys.length; i < length; i++) {
					let key = objectKeys[i];
					compositeKey += String.prototype.concat(key, ':', nodeCompositeKey.nid[key]);
				}
				return compositeKey;
			}
		},

		prepareGraphData: function(graphData) {
			if (graphData) {
				Array.prototype.forEach.call(graphData.nodes, function(node) {
					node.id = this._getNodeKey(node.node_key);
					node.itemId = node.node_key.tiid;
					node.type = this.arasObject.getItemTypeName(node.node_key.titid);
					node.typeId = node.node_key.titid;
					node.graphViewNodeId = node.node_key.gvnid;
					if (node.outgoing_connectors && node.outgoing_connectors.connector_keys && node.outgoing_connectors.connector_keys.length) {
						node.activeOutgoingConnectors = node.outgoing_connectors.connector_keys;
						node.hiddenOutgoingConnectors = [];
					}

					try {
						this._applyNodeTemplate(node);
					} catch (e) {
						this._applyNodeTemplate(node, this.defaultTemplate);
					}
				}.bind(this));

				Array.prototype.forEach.call(graphData.connectors, function(connector) {
					connector.source = this._getNodeKey(connector.source_node_key);
					connector.target = this._getNodeKey(connector.target_node_key);

					const connectorType = Object.keys(connector.connector_key.cid)[0];
					const connectorId = connector.connector_key.cid[connectorType];
					if (connectorType && connector.target_node_key.tiid !== connectorId) {
						connector.type = connectorType;
						connector.itemId = connectorId;
					}
					try {
						this._applyConnectorTemplate(connector);
					} catch (e) {
						this._applyConnectorTemplate(connector, this.defaultTemplate);
					}
				}.bind(this));

				return graphData;
			}
		},

		_resolveViewCardTemplate: function(templateId, itemTypeName) {
			if (templateId) {
				if (this.templatesHash[templateId]) {
					return this.templatesHash[templateId];
				}

				let viewCardItem = this.arasObject.newIOMItem('gn_ViewCard', 'get');
				viewCardItem.setAttribute('select', 'template_definition');
				viewCardItem.setID(templateId);
				viewCardItem = viewCardItem.apply();
				if (!viewCardItem.isError()) {
					let jsonTemplateDefinition = viewCardItem.getProperty('template_definition');
					try {
						return (this.templatesHash[templateId] = JSON.parse(jsonTemplateDefinition));
					} catch (e) {
						console.log('Invalid template JSON for View Card with id: ' + templateId);
					}
				}
			}

			if (itemTypeName) {
				const itemTypeId = this.arasObject.getItemTypeDictionary(itemTypeName).getID();

				if (this.templatesHash[itemTypeId + this.gvdId]) {
					return this.templatesHash[itemTypeId + this.gvdId];
				}

				let viewCardItem = this.arasObject.newIOMItem('Method', 'gn_GetViewCard');
				viewCardItem.setProperty('gvd_id', this.gvdId);
				viewCardItem.setProperty('item_type_id', itemTypeId);
				viewCardItem = viewCardItem.apply();
				if (!viewCardItem.isError()) {
					let jsonTemplateDefinition = viewCardItem.getProperty('template_definition');
					try {
						return (this.templatesHash[itemTypeId + this.gvdId] = JSON.parse(jsonTemplateDefinition));
					} catch (e) {
						console.log('Invalid template JSON for View Card with id: ' + viewCardItem.getID());
					}
				}
			}

			return this.defaultTemplate;
		},

		_applyNodeTemplate: function(nodeData, template) {
			let nodeTemplate = template || this._resolveViewCardTemplate(nodeData.template_id, nodeData.type);
			let containerStyle = nodeTemplate.container.style;

			nodeData.width = containerStyle.width;
			nodeData.height = containerStyle.height;
			nodeData.fill = containerStyle.color;
			if (containerStyle.border) {
				nodeData.rx = containerStyle.border.cornerRadius;
				nodeData.stroke = containerStyle.border.color;
				nodeData.strokeWidth = containerStyle.border.width;
				nodeData.shadow = containerStyle.border.shadow;
			}

			this._applyNodeContentTemplate(nodeData, nodeTemplate);
		},

		_applyNodeContentTemplate: function(nodeData, nodeTemplate) {
			this.prebuildNodeStyles(nodeData, nodeTemplate);

			let computedCellPositions;
			switch (nodeTemplate.container.type) {
				case 'grid':
					computedCellPositions = this._applyGridCellPositions(nodeData, nodeTemplate);
					break;
				default:
					computedCellPositions = this._applyGridCellPositions(nodeData, nodeTemplate);
					break;
			}

			this._applyCellStyles(nodeData, nodeTemplate, computedCellPositions);
		},

		prebuildNodeStyles: function(nodeData, nodeTemplate) {
			const containerStyle = nodeTemplate.container.style;
			const padding = containerStyle.padding || 0;

			containerStyle.padding = {
				left: padding.left || padding,
				right: padding.right || padding,
				top: padding.top || padding,
				bottom: padding.bottom || padding
			};
		},

		_applyGridCellPositions: function(nodeData, nodeTemplate) {
			const containerStyle = nodeTemplate.container.style;
			const nodePadding = containerStyle.padding;
			const computedCellPositions = [];
			let xPos = nodePadding.left;
			let yPos = nodePadding.top;

			const nonEmptyRows = {};
			const nonEmptyCols = {};
			Array.prototype.forEach.call(containerStyle.cells, function(cell) {
				if (nodeData.data_bindings[cell.content_binding]) {
					nonEmptyRows[cell.row] = true;
					nonEmptyCols[cell.col] = true;
				}
			});
			Array.prototype.forEach.call(containerStyle.rows, function(row, rowIndex) {
				computedCellPositions.push([]);

				Array.prototype.forEach.call(containerStyle.cols, function(col, colIndex) {
					computedCellPositions[rowIndex].push({
						x: xPos,
						y: yPos,
						width: col.width,
						height: row.height
					});

					if (nonEmptyCols[colIndex]) {
						xPos += col.width;
					} else {
						nodeData.width -= col.width;
						nonEmptyCols[colIndex] = true;
					}
				});

				xPos = nodePadding.left;
				if (nonEmptyRows[rowIndex]) {
					yPos += row.height;
				} else {
					nodeData.height -= row.height;
				}
			});

			return computedCellPositions;
		},

		_applyCellStyles: function(nodeData, nodeTemplate, computedCellStyles) {
			nodeData.cells = [];

			Array.prototype.forEach.call(nodeTemplate.container.style.cells, function(cell) {
				let cellData = computedCellStyles[cell.row][cell.col];

				cellData.type = cell.content;
				cellData.value = nodeData.data_bindings[cell.content_binding];
				cellData.connections = cell.connections;

				cell.style = cell.style || {};
				cellData.font = cell.style.font || {};
				cellData.backgroundColor = cell.style.backgroundColor;
				cellData.horizontalAlignment = cell.style.horizontalAlignment;
				cellData.verticalAlignment = cell.style.verticalAlignment;
				cellData.textDecoration = cell.style.textDecoration || {};
				cellData.contentWidth = cell.style.width;
				cellData.contentHeight = cell.style.height;

				if (cell.style.border) {
					cellData.rx = cell.style.border.cornerRadius;
					cellData.stroke = cell.style.border.color;
					cellData.strokeWidth = cell.style.border.width;
				}

				nodeData.cells.push(cellData);
			});
		},

		_applyConnectorTemplate: function(connectorData) {
			let connectorTemplate = this._resolveViewCardTemplate(connectorData.template_id);
			let connectorStyle = connectorTemplate.style;

			connectorData.connectorColor = connectorStyle.color;
			connectorData.connectorWeight = connectorStyle.weight;
			connectorData.connectorShadow = connectorStyle.shadow;
			connectorData.arrowhead = connectorStyle.arrowhead;

			if (connectorData.data_bindings) {
				this._applyNodeTemplate(connectorData, connectorTemplate);
			}
		}
	});
});
