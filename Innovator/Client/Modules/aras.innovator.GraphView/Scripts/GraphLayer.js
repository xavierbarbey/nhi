﻿define([
	'dojo/_base/declare',
	'Controls/VariantsTree/Layers/VisualizationLayer'
],
function(declare, VisualizationBase) {
	return declare('GraphNavigation.Layouts.GraphLayer', VisualizationBase, {
		activeGraphLayout: null,
		_uniqueDefinitionIdCounter: null,
		_definitionHash: null,
		_nodesHash: null,
		_connectorsHash: null,
		_vaultIconsHash: null,
		defaultSettings: {
			borderWidth: 3,
			node: {
				rx: 2,
				width: 45,
				height: 45,
				color: '#F7F7F7',
				borderColor: '#555555',
			},
			connector: {
				color: '#555555',
				width: 1
			},
			shadow: {
				dx: 4,
				dy: 4,
				color: '#555555',
				opacity: 0.5
			},
			image: {
				width: 16,
				height: 16
			},
			text: {
				size: 14,
				weight: 'bold',
				family: 'Tahoma',
				color: '#333333',
				horizontalAlignment: 'start',
				verticalAlignment: 'middle',
			},
			expandButton: {
				radius: 8,
				states: {
					nothingToExpand: 0,
					allCollapsed: 1,
					allExpanded: 2,
					someExpanded: 3,
					neverExtended: 9
				}
			},
			_dragMinDistance: 4,
		},

		constructor: function() {
			this._nodesHash = {};
			this._connectorsHash = {};
			this._vaultIconsHash = {};
		},

		getActualLayerBounds: function() {
			let dataItems = this._getExpandedNodes();

			if (!dataItems || !dataItems.length) {
				return {x: 0, right: 0, y: 0, bottom: 0, width: 0, height: 0};
			}

			let layerBounds = {
				x: dataItems[0].x - dataItems[0].width / 2,
				right: dataItems[0].x + dataItems[0].width / 2,
				y: dataItems[0].y - dataItems[0].height / 2,
				bottom: dataItems[0].y + dataItems[0].height / 2
			};

			for (let i = 0; i < dataItems.length; i++) {
				let xCoor = dataItems[i].x - dataItems[i].width / 2;
				let yCoor = dataItems[i].y - dataItems[i].height / 2;

				if (xCoor < layerBounds.x) {
					layerBounds.x = xCoor;
				} else {
					layerBounds.right = Math.max(layerBounds.right, xCoor + dataItems[i].width);
				}

				if (yCoor < layerBounds.y) {
					layerBounds.y = yCoor;
				} else {
					layerBounds.bottom = Math.max(layerBounds.bottom, yCoor + dataItems[i].height);
				}
			}

			layerBounds.width = layerBounds.right - layerBounds.x;
			layerBounds.height = layerBounds.bottom - layerBounds.y;

			return layerBounds;
		},

		getRootNode: function(graphData) {
			if (graphData && graphData.nodes) {
				let filteredItems = graphData.nodes.filter(function(node) {
					return node.id === this.ownerControl.contextItemId;
				}.bind(this));

				return filteredItems && filteredItems[0];
			} else {
				return this._nodesHash[this.ownerControl.contextItemId];
			}
		},

		setGraphLayout: function(newLayout) {
			if (newLayout) {
				this.activeGraphLayout = newLayout;
				this.activeGraphLayout.setOwner(this);

				let allNodes = (this.layerData && this.layerData.nodes) || [];
				Array.prototype.forEach.call(allNodes, function(nodeData) {
					nodeData.inputExpandPosition = newLayout._getInputExpandButtonCoordinates(nodeData);
					nodeData.outputExpandPosition = newLayout._getOutputExpandButtonCoordinates(nodeData);
				});
			}
		},

		_initializeProperties: function(initialArguments) {
			this.inherited(arguments);

			this.d3 = initialArguments.d3Framework;
			this._uniqueDefinitionIdCounter = 0;
			this._definitionHash = {};
			this._connectorGenerator = this.d3.line()
				.curve(this.d3.curveBasis);

			this.setGraphLayout(initialArguments.graphLayout);
		},

		_appendIconDefinition: function(iconData) {
			if (iconData && iconData.value) {
				let defKey = this._getDefinitionKey(iconData);
				let storedDefinition = this._definitionHash[defKey];
				if (storedDefinition) {
					return storedDefinition;
				}

				this.ownerControl.svgDefs.append('image').attr('id', ++this._uniqueDefinitionIdCounter)
					.attr('href', this._getImageUrl(iconData.value))
					.attr('width', iconData.contentWidth || iconData.width || this.defaultSettings.image.width)
					.attr('height', iconData.contentHeight || iconData.height || this.defaultSettings.image.height);

				return (this._definitionHash[defKey] = window.location.href + '#' + this._uniqueDefinitionIdCounter);
			}
			return '';
		},

		_getImageUrl: function(imageData) {
			if (imageData && imageData.toLowerCase().indexOf('vault:\/\/\/\?fileid=') != -1) { // jshint ignore:line
				const fileId = imageData.substr(imageData.length - 32);
				return this._vaultIconsHash[fileId];
			}
			return imageData;
		},

		_getExpandButtonClass: function(expandButtonState) {
			if (isFinite(expandButtonState)) {
				const buttonStates = this.defaultSettings.expandButton.states;

				switch (expandButtonState) {
					case buttonStates.allExpanded:
						return 'allExpanded';
					case buttonStates.someExpanded:
						return 'someExpanded';
					case buttonStates.allCollapsed:
					case buttonStates.neverExtended:
						return 'nothingExpanded';
					default:
						return 'emptyButton';
				}
			}
			return 'emptyButton';
		},

		_calculateOutgoingExpandButtonClass: function(nodeData) {
			if (nodeData) {
				let calculatedExpandButtonState = this._getNodeExpandState(nodeData);

				return 'outgoingCollapseButton ' + this._getExpandButtonClass(calculatedExpandButtonState);
			}
		},

		_calculateIncomingExpandButtonClass: function(nodeData) {
			if (nodeData) {
				let calculatedExpandButtonState = this._getNodeExpandState(nodeData, true);

				return 'incomingCollapseButton ' + this._getExpandButtonClass(calculatedExpandButtonState);
			}
		},

		_getNodeExpandState: function(nodeData, calculateParentsState) {
			const expandButtonStates = this.defaultSettings.expandButton.states;

			if (nodeData && nodeData.children && nodeData.parents) {
				const hasUncollapsedItems = calculateParentsState ? nodeData.parents.length > 0 : nodeData.children.length > 0;
				const hasCollapsedItems = calculateParentsState ? nodeData.collapsedParents.length > 0 : nodeData.collapsedChildren.length > 0;
				const wasAlreadyExpanded = calculateParentsState ? nodeData.wasParentsExtended : nodeData.wasChildrenExtended;

				if (hasUncollapsedItems) {
					if (hasCollapsedItems) {
						return expandButtonStates.someExpanded;
					} else {
						return wasAlreadyExpanded ? expandButtonStates.allExpanded : expandButtonStates.someExpanded;
					}
				} else {
					if (hasCollapsedItems) {
						return wasAlreadyExpanded ? expandButtonStates.allCollapsed : expandButtonStates.neverExtended;
					} else {
						return wasAlreadyExpanded ? expandButtonStates.nothingToExpand : expandButtonStates.neverExtended;
					}
				}
			} else {
				return expandButtonStates.nothingToExpand;
			}
		},

		_appendArrowHeadDefinition: function(arrowSettings) {
			if (arrowSettings) {
				let defKey = 'arrowhead_' + this._getDefinitionKey(arrowSettings);
				let storedDefinition = this._definitionHash[defKey];
				if (storedDefinition) {
					return storedDefinition;
				}

				arrowSettings.width = arrowSettings.width || 12;
				arrowSettings.height = arrowSettings.height || 24;

				this.ownerControl.svgDefs.append('marker')
					.attr('id', ++this._uniqueDefinitionIdCounter)
					.attr('markerUnits', 'userSpaceOnUse')
					.attr('viewBox', '-0 -' + (arrowSettings.width / 2) + ' ' + arrowSettings.height + ' ' + arrowSettings.height)
					.attr('refX', arrowSettings.height + (this.defaultSettings.expandButton.radius / 2))
					.attr('refY', 0)
					.attr('orient', 'auto')
					.attr('markerWidth', Math.max(arrowSettings.width, arrowSettings.height))
					.attr('markerHeight', Math.max(arrowSettings.width, arrowSettings.height))
					.append('svg:path')
					.attr('d', this._getArrowHeadPath(arrowSettings))
					.attr('fill', arrowSettings.color || this.defaultSettings.connector.color);

				return (this._definitionHash[defKey] = 'url(' + window.location.href + '#' + this._uniqueDefinitionIdCounter + ')');
			}
			return '';
		},

		_getArrowHeadPath: function(arrowSettings) {
			let arrowHeadPath = '';

			if (arrowSettings) {
				arrowHeadPath = 'M 0,-' + (arrowSettings.width / 2) +
					' L ' + arrowSettings.height + ' ,0' +
					'L 0,' + (arrowSettings.width / 2);
			}

			return arrowHeadPath;
		},

		_appendShadowDefinition: function(shadowSettings) {
			if (shadowSettings) {
				let defKey = this._getDefinitionKey(shadowSettings);
				let storedDefinition = this._definitionHash[defKey];
				if (storedDefinition) {
					return storedDefinition;
				}

				let filter = this.ownerControl.svgDefs.append('filter')
					.attr('id', ++this._uniqueDefinitionIdCounter)
					.attr('height', '120%')
					.attr('width', '120%');
				filter.append('feGaussianBlur')
					.attr('in', 'SourceAlpha')
					.attr('stdDeviation', 1.8)
					.attr('result', 'blur');
				filter.append('feOffset')
					.attr('in', 'blur')
					.attr('dx', shadowSettings.horizontal || this.defaultSettings.shadow.dx)
					.attr('dy', shadowSettings.vertical || this.defaultSettings.shadow.dy)
					.attr('result', 'offsetBlur');
				filter.append('feFlood')
					.attr('flood-color', shadowSettings.color || this.defaultSettings.shadow.color)
					.attr('flood-opacity', shadowSettings.opacity || this.defaultSettings.shadow.opacity)
					.attr('result', 'offsetColor');
				filter.append('feComposite')
					.attr('in', 'offsetColor')
					.attr('in2', 'offsetBlur')
					.attr('operator', 'in')
					.attr('result', 'offsetBlur');
				let feMerge = filter.append('feMerge');
				feMerge.append('feMergeNode')
					.attr('in', 'offsetBlur');
				feMerge.append('feMergeNode')
					.attr('in', 'SourceGraphic');

				return (this._definitionHash[defKey] = 'url(' + window.location.href + '#' + this._uniqueDefinitionIdCounter + ')');
			}
			return '';
		},

		_getDefinitionKey: function(dataSettings) {
			if (dataSettings && typeof dataSettings === 'object') {
				let objectKeys = Object.keys(dataSettings).sort();
				let compositeKey = '';

				for (let i = 0, length = objectKeys.length; i < length; i++) {
					let key = objectKeys[i];
					compositeKey += String.prototype.concat(key, ':', dataSettings[key]);
				}
				return compositeKey;
			}
		},

		_getConnectorHashKey: function(connectorData) {
			if (connectorData && connectorData.source && connectorData.target) {
				let sourceId = connectorData.source.id || connectorData.source;
				let targetId = connectorData.target.id || connectorData.target;

				return connectorData.id || String.prototype.concat(sourceId, targetId);
			}
		},

		_getVaultSecurityTokens: function(fileIDs) {
			let body = '{"parameters":[';
			for (let i = 0; i < fileIDs.length; i++) {
				body += '{"__type":"FileDownloadParameters","fileId":"' + fileIDs[i] + '", "dbName": "' + this.ownerControl.arasObject.getDatabase() + '"},';
			}
			body = body.slice(0, body.length - 1) + ']}';
			const url = this.ownerControl.arasObject.getServerBaseURL() + 'AuthenticationBroker.asmx/GetFilesDownloadTokens?rnd=' + Math.random();
			const xmlHttpRequest = new XMLHttpRequest();
			xmlHttpRequest.open('POST', url, false);
			const headers = this.ownerControl.arasObject.getHttpHeadersForSoapMessage('GetFilesDownloadTokens');
			for (let hName in headers) {
				xmlHttpRequest.setRequestHeader(hName, headers[hName]);
			}
			xmlHttpRequest.setRequestHeader('content-type', 'application/json');
			xmlHttpRequest.send(body);
			return JSON.parse(xmlHttpRequest.responseText).d;
		},

		_preprocessData: function(layerData) {
			if (!layerData) {
				return {nodes: [], connectors: []};
			}

			const foundVaultIconFileIds = [];
			for (let i = 0, length = layerData.nodes.length; i < length; i++) {
				let node = layerData.nodes[i];
				this._nodesHash[node.id] = node;
				node.children = [];
				node.collapsedChildren = [];
				node.parents = [];
				node.collapsedParents = [];
				node.level = Infinity;

				this._applyInitialNodeState(node);
				this._lookupVaultIconsFiles(node, foundVaultIconFileIds);
			}
			for (let i = 0, length = layerData.connectors.length; i < length; i++) {
				let connector = layerData.connectors[i];
				let sourceId = connector.source.id || (connector.source = this._nodesHash[connector.source]).id;
				let targetId = connector.target.id || (connector.target = this._nodesHash[connector.target]).id;

				this._nodesHash[sourceId].children.push(this._nodesHash[targetId]);
				this._nodesHash[targetId].parents.push(this._nodesHash[sourceId]);
				this._connectorsHash[this._getConnectorHashKey(connector)] = connector;
				this._lookupVaultIconsFiles(connector, foundVaultIconFileIds);
			}

			if (foundVaultIconFileIds.length > 0) {
				const vaultIconUrls = this.ownerControl.arasObject.IomInnovator.getFileUrls(foundVaultIconFileIds)[0];
				const securityTokens = this._getVaultSecurityTokens(foundVaultIconFileIds);

				for (let i = 0; i < foundVaultIconFileIds.length; i++) {
					this._vaultIconsHash[foundVaultIconFileIds[i]] = vaultIconUrls[i] + '&token=' + securityTokens[i];
				}
			}

			return layerData;
		},

		_lookupVaultIconsFiles: function(itemData, vaultFileIds) {
			if (!itemData || !itemData.cells) {
				return;
			}

			Array.prototype.forEach.call(itemData.cells, function(itemCell) {
				if (itemCell.value && itemCell.type === 'image' && itemCell.value.toLowerCase().indexOf('vault:\/\/\/\?fileid=') != -1) { // jshint ignore:line
					vaultFileIds.push(itemCell.value.substr(itemCell.value.length - 32));
				}
			});
		},

		_applyInitialNodeState: function(nodeData) {
			if (nodeData) {
				nodeData.x = nodeData.x || 0;
				nodeData.y = nodeData.y || 0;
				nodeData.width = nodeData.width ? Math.max(nodeData.width, this.defaultSettings.node.width) : this.defaultSettings.node.width;
				nodeData.height = nodeData.height ? Math.max(nodeData.height, this.defaultSettings.node.height) : this.defaultSettings.node.height;
				nodeData.inputExpandPosition = this.activeGraphLayout._getInputExpandButtonCoordinates(nodeData);
				nodeData.outputExpandPosition = this.activeGraphLayout._getOutputExpandButtonCoordinates(nodeData);
			}
		},

		_getNormalizedText: function(text, textData) {
			if (text && textData) {
				let requiredSpace = this._getTextLength(text, textData.font && textData.font.size);
				let availableSpace = textData.width - 12;

				return this.normalizeText(text, Math.floor(availableSpace / (requiredSpace / text.length)));
			}
		},

		_getTextLength: function(text, fontSize) {
			let canvas = this._getTextLength.canvas || (this._getTextLength.canvas = document.createElement('canvas'));
			let context = canvas.getContext('2d');
			context.font = (fontSize || this.defaultSettings.text.size) + 'px sans-serif';
			return context.measureText(text).width;
		},

		_getNodeTransform: function(nodeData) {
			return 'translate(' + (nodeData.x - nodeData.width / 2) + ',' + (nodeData.y - nodeData.height / 2) + ')';
		},

		_getNodeImageTransform: function(imageData) {
			return 'translate(' + (imageData.x + ',' + imageData.y) + ')';
		},

		_getNodeTextTransform: function(textData) {
			let xPos = textData.x + (textData.horizontalAlignment === 'middle' ? textData.width / 2 : (textData.horizontalAlignment === 'end' ? textData.width : 0));
			let yPos = textData.y + textData.height / 2;

			return 'translate(' + xPos + ',' + yPos + ')';
		},

		_getConnectorPath: function(connectorData) {
			this.activeGraphLayout._computeConnectorLabelDefaultPosition(connectorData);
			const interpolationPoints = this.activeGraphLayout._getConnectorInterpolationPoints(connectorData) || [];

			return this._connectorGenerator(interpolationPoints);
		},

		_getConnectorLabelTransform: function(connectorData) {
			this.activeGraphLayout._computeConnectorLabelDefaultPosition(connectorData);

			return this._getNodeTransform(connectorData);
		},

		getFirstParentWithData: function(domNode) {
			while (domNode && domNode !== this.containerNode) {
				if (this.d3.select(domNode).datum()) {
					return domNode;
				}
				domNode = domNode.parentNode;
			}
		},

		_buildExpandButton: function() {
			const outerCircleHtml = this.wrapInTag('', 'circle', {
				r: this.defaultSettings.expandButton.radius,
				class: 'outerExpandButtonCircle'
			});
			const innerCircleHtml = this.wrapInTag('', 'circle', {
				r: this.defaultSettings.expandButton.radius / 2,
				class: 'innerExpandButtonCircle'
			});
			return outerCircleHtml + innerCircleHtml;
		},

		_buildNode: function(dataItem) {
			let resultNode = this._buildNodeContent(dataItem);

			return this.wrapInTag(resultNode, 'g', {
				transform: this._getNodeTransform(dataItem)
			});
		},

		_buildNodeContent: function(dataItem) {
			let resultHTML = '';

			if (dataItem) {
				let defaultSettings = this.defaultSettings;

				resultHTML += this.wrapInTag('', 'rect', {
					width: dataItem.width,
					height: dataItem.height,
					rx: dataItem.rx || defaultSettings.node.rx,
					fill: dataItem.fill || defaultSettings.node.color,
					stroke: dataItem.stroke || defaultSettings.node.borderColor,
					'stroke-width': dataItem.strokeWidth || defaultSettings.borderWidth,
					filter: this._appendShadowDefinition(dataItem.shadow)
				});

				if (dataItem.cells) {
					for (let i = 0, length = dataItem.cells.length; i < length; i++) {
						let cellData = dataItem.cells[i];
						let cellContent = this.wrapInTag('', 'rect', {
							x: cellData.x,
							y: cellData.y,
							width: cellData.width,
							height: cellData.height,
							fill: cellData.backgroundColor || 'none',
							rx: cellData.rx,
							stroke: cellData.stroke,
							'stroke-width': cellData.strokeWidth || 0
						});
						if (cellData.type === 'image') {
							cellContent += this.wrapInTag('', 'use', {
								href: this._appendIconDefinition(cellData),
								transform: this._getNodeImageTransform(cellData)
							});
						} else if (cellData.type === 'text') {
							cellContent += this.wrapInTag(this._getNormalizedText(cellData.value, cellData) || '', 'text', {
								'text-decoration': (cellData.textDecoration && cellData.textDecoration.line),
								'text-anchor': cellData.horizontalAlignment || defaultSettings.text.horizontalAlignment,
								'alignment-baseline': cellData.verticalAlignment || defaultSettings.text.verticalAlignment,
								'font-size': (cellData.font && cellData.font.size) || defaultSettings.text.size,
								'font-family': (cellData.font && cellData.font.family) || defaultSettings.text.family,
								'font-weight': (cellData.font && cellData.font.weight) || defaultSettings.text.weight,
								fill: (cellData.font && cellData.font.color) || defaultSettings.text.color,
								filter: this._appendShadowDefinition(cellData.textDecoration && cellData.textDecoration.shadow),
								transform: this._getNodeTextTransform(cellData)
							});
						}

						resultHTML += this.wrapInTag(cellContent, 'g', {});
					}
				}

				const incomingButtonState = (dataItem.connectors && dataItem.connectors.incoming) || this._getNodeExpandState(dataItem, true);
				const incomePosition = this.activeGraphLayout._getInputExpandButtonCoordinates(dataItem);
				resultHTML += this.wrapInTag(this._buildExpandButton(), 'g', {
					transform: 'translate(' + [incomePosition.x, incomePosition.y] + ')',
					class: 'incomingCollapseButton ' + this._getExpandButtonClass(incomingButtonState.status || incomingButtonState)
				});

				if (dataItem.activeOutgoingConnectors && dataItem.hiddenOutgoingConnectors) {
					const outgoingButtonState = (dataItem.connectors && dataItem.connectors.outgoing) || this._getNodeExpandState(dataItem);
					const outgoingPosition = this.activeGraphLayout._getOutputExpandButtonCoordinates(dataItem);
					resultHTML += this.wrapInTag(this._buildExpandButton(), 'g', {
						transform: 'translate(' + [outgoingPosition.x, outgoingPosition.y] + ')',
						class: 'outgoingCollapseButton ' + this._getExpandButtonClass(outgoingButtonState.status || outgoingButtonState)
					});
				}
			}

			return resultHTML;
		},

		_buildConnectorLabel: function(dataItem) {
			let resultHtml = '';

			if (dataItem) {
				resultHtml += this.wrapInTag(this._buildNodeContent(dataItem), 'g', {
					transform: this._getConnectorLabelTransform(dataItem)
				});
			}

			return resultHtml;
		},

		_buildConnectorPath: function(dataItem) {
			let resultHTML = '';

			if (dataItem) {
				resultHTML += this.wrapInTag('', 'path', {
					d: this._getConnectorPath(dataItem),
					stroke: dataItem.connectorColor,
					'stroke-width': dataItem.connectorWeight,
					'marker-end': this._appendArrowHeadDefinition(dataItem.arrowhead)
				});
			}

			return resultHTML;
		},

		showTooltip: function(dataItem) {
			if (dataItem && dataItem.data_bindings && Object.keys(dataItem.data_bindings).length) {
				let propsArray = [];

				for (let key in dataItem.data_bindings) {
					propsArray.push({
						value: dataItem.data_bindings[key]
					});
				}

				this.ownerControl.showTooltip(dataItem, propsArray);
			}

			this.d3.event.stopPropagation();
			this.d3.event.preventDefault();
		},

		_renderItems: function(graphData) {
			if (!graphData || !graphData.nodes || !graphData.connectors || (graphData.nodes.length + graphData.connectors.length === 0)) {
				return;
			}

			let tempSvgContainer = this.contentNode.ownerDocument.createElement('div');
			let nodesContent = '';
			let connectorsContent = '';
			let labelsContent = '';
			let i;

			for (i = 0; i < graphData.nodes.length; i++) {
				nodesContent += this._buildNode.call(this, graphData.nodes[i]);
			}
			for (i = 0; i < graphData.connectors.length; i++) {
				if ((graphData.connectors[i].data_bindings && Object.keys(graphData.connectors[i].data_bindings).length)) {
					labelsContent += this._buildConnectorLabel.call(this, graphData.connectors[i]);
				}
				connectorsContent += this._buildConnectorPath.call(this, graphData.connectors[i]);
			}

			tempSvgContainer.innerHTML = '<svg>' +
				'<g id="connectors">' + connectorsContent + '</g>' +
				'<g id="labels">' + labelsContent + '</g>' +
				'<g id="nodes">' + nodesContent + '</g></svg>';

			let connectorContainer = tempSvgContainer.firstChild.childNodes[0];
			let labelContainer = tempSvgContainer.firstChild.childNodes[1];
			let nodeContainer = tempSvgContainer.firstChild.childNodes[2];

			this.connectors = this.d3.selectAll(connectorContainer.childNodes)
				.data(graphData.connectors)
				.each(function(d, i, domNodes) {
					d.connectorDomNode = domNodes[i];
				})
				.on('mousemove', this.showTooltip.bind(this));
			this._appendNodeContent(connectorContainer);

			if (labelContainer.childNodes.length) {
				const graphConnectorsWithData = labelContainer.childNodes.length === graphData.connectors.length ?
					graphData.connectors : graphData.connectors.filter(function(connector) {
						return connector.data_bindings;
					});

				this.labels = this.d3.selectAll(labelContainer.childNodes)
					.data(graphConnectorsWithData)
					.each(function(d, i, domNodes) {
						d.labelDomNode = domNodes[i];
					});
				this._appendNodeContent(labelContainer);
			}

			this.nodes = this.d3.selectAll(nodeContainer.childNodes)
				.data(graphData.nodes)
				.on('click', this._nodeClick.bind(this))
				.on('mousemove', this.showTooltip.bind(this))
				.each(function(nodeData, i, domNodes) {
					nodeData.domNode = domNodes[i];
				}).call(this.d3.drag()
					.subject(function(d) { return {x: d[0], y: d[1]}; })
					.on('start', this._nodeDragStart.bind(this))
					.on('drag', this._nodeDrag.bind(this))
					.on('end', this._nodeDragEnd.bind(this))
				);

			this.d3.selectAll(nodeContainer.childNodes).selectAll('.incomingCollapseButton')
				.data(function(d) { return [d]; });

			this.d3.selectAll(nodeContainer.childNodes).selectAll('.outgoingCollapseButton')
				.data(function(d) { return [d]; })
				.on('contextmenu', this.onShowExpandSettingsMenu.bind(this));

			this._appendNodeContent(nodeContainer);

			return graphData;
		},

		renderHandler: function() {
			this.activeGraphLayout.updateGraphLayout(this.layerData);

			this._renderItems(this.layerData);
		},

		_appendNodeContent: function(nodeToAppend) {
			if (nodeToAppend) {
				let existingNode = this.contentNode.querySelector('#' + nodeToAppend.getAttribute('id'));
				if (existingNode) {
					for (let i = 0, length = nodeToAppend.childElementCount; i < length; i++) {
						existingNode.appendChild(nodeToAppend.childNodes[0]);
					}
				} else {
					this.contentNode.appendChild(nodeToAppend);
				}
			}
		},

		_nodeClick: function(nodeData) {
			if (nodeData) {
				this.ownerControl.centerNode(nodeData.domNode);
			}
		},

		_nodeDragStart: function(nodeData) {
			this.ownerControl.hideTooltip();
			this.ownerControl.hideExpandSettingsMenu();

			const svgWidth = this.ownerControl.svgNode.attr('width');
			const svgHeight = this.ownerControl.svgNode.attr('height');
			const offsetX = this.ownerControl.currentTransform.x;
			const offsetY = this.ownerControl.currentTransform.y;
			this._dragBounds = {
				startX: nodeData.x,
				startY: nodeData.y,
				x: -offsetX / this.ownerControl.currentTransform.k + nodeData.width,
				y: -offsetY / this.ownerControl.currentTransform.k + nodeData.height,
				right: (svgWidth - offsetX) / this.ownerControl.currentTransform.k - nodeData.width,
				bottom: (svgHeight - offsetY) / this.ownerControl.currentTransform.k - nodeData.height
			};
		},

		_nodeDrag: function(nodeData) {
			if (nodeData) {
				const newX = nodeData.x + this.d3.event.dx;
				const newY = nodeData.y + this.d3.event.dy;
				if (this._dragBounds.x < newX && this._dragBounds.right > newX) {
					nodeData.fx = (nodeData.x = newX);
				}
				if (this._dragBounds.y < newY && this._dragBounds.bottom > newY) {
					nodeData.fy = (nodeData.y = newY);
				}

				nodeData.domNode.setAttribute('transform', this._getNodeTransform(nodeData));

				Array.prototype.forEach.call(this.layerData.connectors, function(connectorData) {
					if (connectorData.target.id === nodeData.id || connectorData.source.id === nodeData.id) {
						let pathLength = connectorData.connectorDomNode.getTotalLength();
						let centerPoint = connectorData.connectorDomNode.getPointAtLength(pathLength / 2);

						connectorData.x = centerPoint.x;
						connectorData.y = centerPoint.y;
						if (connectorData.labelDomNode) {
							connectorData.labelDomNode.setAttribute('transform', this._getConnectorLabelTransform(connectorData));
						}
						connectorData.connectorDomNode.setAttribute('d', this._getConnectorPath(connectorData));
						if (this.ownerControl.arasObject.Browser.isIe()) {
							// There is an issue with IE browser,  when svg:path is not updated if it has "marker-start" or/and "marker-end" attributes.
							// Applied workaround described here https://stackoverflow.com/questions/15693178/svg-line-markers-not-updating-when-line-moves-in-ie10
							connectorData.connectorDomNode.parentNode.insertBefore(connectorData.connectorDomNode, connectorData.connectorDomNode);
						}
					}
				}.bind(this));
			}
		},

		_nodeDragEnd: function(nodeData) {
			const dx = nodeData.x - this._dragBounds.startX;
			const dy = nodeData.y - this._dragBounds.startY;
			if (dx * dx + dy * dy < this.defaultSettings._dragMinDistance * this.defaultSettings._dragMinDistance) {
				const dataNode = this.getFirstParentWithData(this.d3.event.sourceEvent.target);
				if (dataNode === nodeData.domNode) {
					this._nodeClick(nodeData);
				} else if (dataNode.getAttribute('class').indexOf('outgoingCollapseButton') > -1) {
					this.onToggleChildren(nodeData);
				} else {
					this.onToggleParents(nodeData);
				}
			}
		},

		wrapInTag: function(sourceString, tagName, tagAttributes) {
			if (tagName) {
				let attributeString = '';

				if (tagAttributes) {
					for (let attributeName in tagAttributes) {
						if (tagAttributes[attributeName] !== '' && tagAttributes[attributeName] !== undefined) {
							attributeString += ' ' + attributeName + '="' + tagAttributes[attributeName] + '"';
						}
					}
				}

				return '<' + tagName + attributeString + '>' + sourceString + '</' + tagName + '>';
			} else {
				return sourceString || '';
			}
		},

		_setNodesLevel: function(graphData) {
			let root = this.getRootNode(graphData);
			root.level = 0;

			this._bypassStructure(root, function(dataItem) {
				let relatedItems = dataItem.children.concat(dataItem.parents);
				Array.prototype.forEach.call(relatedItems, function(item) {
					dataItem.level = Math.min(dataItem.level, item.level + 1);
					item.level = Math.min(item.level, dataItem.level + 1);
				});
			});
		},

		_bypassStructure: function(dataItem, bypassMethod, visitedNodes) {
			visitedNodes = visitedNodes || {};
			if (dataItem && bypassMethod) {
				let relatedItems = dataItem.children.concat(dataItem.parents);

				for (let i = 0; i < relatedItems.length; i++) {
					let currentItem = relatedItems[i];

					if (!visitedNodes[currentItem.id]) {
						visitedNodes[currentItem.id] = true;

						bypassMethod(currentItem);
						this._bypassStructure(currentItem, bypassMethod, visitedNodes);
					}
				}
			}
		},

		_getConnectedSubGraph: function(dataItem, visitedNodes) {
			visitedNodes = visitedNodes || [];

			if (dataItem && visitedNodes.indexOf(dataItem) === -1) {
				visitedNodes.push(dataItem);

				let relatedItems = dataItem.children.concat(dataItem.parents);
				for (let i = 0, length = relatedItems.length; i < length; i++) {
					let currentItem = relatedItems[i];

					this._getConnectedSubGraph(currentItem, visitedNodes);
				}
			}

			return visitedNodes;
		},

		_getExpandedNodes: function() {
			let root = this.getRootNode();
			return this._getConnectedSubGraph(root);
		},

		_getExpandedConnectors: function(expandedNodes) {
			expandedNodes = expandedNodes || this._getExpandedNodes();

			return this.layerData.connectors.filter(function(d) {
				return expandedNodes.indexOf(d.source) !== -1 && expandedNodes.indexOf(d.target) !== -1 &&
					d.source.children.indexOf(d.target) !== -1 && d.target.parents.indexOf(d.source) !== -1;
			});
		},

		_dataItemComparer: function(dataItem) {
			return (dataItem && dataItem.id) || (dataItem.source.id + dataItem.target.id);
		},

		invalidateLayer: function(nodeData) {
			let d3ContenNode = this.d3.select(this.contentNode);

			let allNodes = d3ContenNode.selectAll('#nodes>g');
			let allConnectors = d3ContenNode.selectAll('#connectors>path');
			let allLabels = d3ContenNode.selectAll('#labels>g');

			let visibleNodes = this._getExpandedNodes();
			let visibleConnectors = this._getExpandedConnectors(visibleNodes);

			this.activeGraphLayout.updateGraphLayout({
				nodes: visibleNodes,
				connectors: visibleConnectors
			});

			let visibleDomNodes = allNodes.data(visibleNodes, this._dataItemComparer);
			let visibleDomLabels = allLabels.data(visibleConnectors, this._dataItemComparer);
			let visibleDomConnectors = allConnectors.data(visibleConnectors, this._dataItemComparer);

			// hiding and deleting collapsed nodes
			visibleDomNodes.exit()
				.remove();
			visibleDomLabels.exit()
				.remove();
			visibleDomConnectors.exit()
				.remove();

			// positioning visible nodes
			visibleDomNodes
				.attr('transform', this._getNodeTransform);
			visibleDomNodes.select('g.outgoingCollapseButton')
				.attr('class', this._calculateOutgoingExpandButtonClass.bind(this))
				.attr('transform', function(nodeData) {
					return 'translate(' + [nodeData.outputExpandPosition && nodeData.outputExpandPosition.x,
							nodeData.outputExpandPosition && nodeData.outputExpandPosition.y] + ')';
				});
			visibleDomNodes.select('g.incomingCollapseButton')
				.attr('class', this._calculateIncomingExpandButtonClass.bind(this))
				.attr('transform', function(nodeData) {
					return 'translate(' + [nodeData.inputExpandPosition && nodeData.inputExpandPosition.x,
							nodeData.inputExpandPosition && nodeData.inputExpandPosition.y] + ')';
				});
			visibleDomLabels
				.attr('transform', this._getConnectorLabelTransform.bind(this));
			visibleDomConnectors
				.attr('d', this._getConnectorPath.bind(this));

			if (this.ownerControl.arasObject.Browser.isIe()) {
				// There is an issue with IE browser,  when svg:path is not updated if it has "marker-start" or/and "marker-end" attributes.
				// Applied workaround described here https://stackoverflow.com/questions/15693178/svg-line-markers-not-updating-when-line-moves-in-ie10
				visibleDomConnectors.each(function(connector) {
					connector.connectorDomNode.parentNode.insertBefore(connector.connectorDomNode, connector.connectorDomNode);
				}.bind(this));
			}

			// creating expanded nodes
			let nodes = visibleDomNodes.enter().data();
			let connectors = visibleDomConnectors.enter().data();
			if (nodes.length || connectors.length) {
				this._renderItems({
					nodes: nodes,
					connectors: connectors
				});
			}

			this.ownerControl.adjustViewBounds();
			if (nodeData) {
				this.ownerControl.centerNode(nodeData.domNode);
			}
		},

		onShowExpandSettingsMenu: function(nodeData) {
			this.ownerControl.hideTooltip();
			this.d3.event.stopPropagation();
			this.d3.event.preventDefault();

			if (nodeData.activeOutgoingConnectors && nodeData.hiddenOutgoingConnectors &&
				nodeData.activeOutgoingConnectors.length + nodeData.hiddenOutgoingConnectors.length > 0) {
				const positionX = this.d3.event.pageX - this.ownerControl.containerNode.offsetLeft + this.ownerControl.containerNode.scrollLeft;
				const positionY = this.d3.event.pageY - this.ownerControl.containerNode.offsetTop + this.ownerControl.containerNode.scrollTop;
				this.ownerControl.expandSettingsMenu.showPopupWindow(nodeData, positionX, positionY);
			}
		},

		onToggleChildren: function(nodeData, optionalParameters) {
			optionalParameters = optionalParameters || {};

			if (nodeData.isChildrenExpanded === false) {
				this.expandChildNodes(nodeData);
			} else if (nodeData.isChildrenExpanded === true) {
				this.collapseChildNodes(nodeData);
			} else {
				this.extendChildNodes(nodeData);
				nodeData.isChildrenExpanded = true;
			}

			if (!optionalParameters.skipInvalidation) {
				this.invalidateLayer(nodeData);
			}
		},

		collapseChildNodes: function(nodeData) {
			this._setNodesLevel(this.layerData);
			const hasParentRelationships = nodeData.parents.length > 1 || (nodeData.parents.length === 1 && nodeData.parents[0] !== nodeData);
			const childrenToCollapse = hasParentRelationships ? nodeData.children : nodeData.children.filter(function(item) {
				return nodeData.level - 1 !== item.level;
			});

			this._swapItemsBetweenArrays(nodeData, childrenToCollapse, 'parents', 'collapsedParents');

			nodeData.collapsedChildren = nodeData.collapsedChildren.concat(childrenToCollapse);
			nodeData.children = hasParentRelationships ? [] : nodeData.children.filter(function(item) {
				return nodeData.level - 1 === item.level;
			});
			nodeData.isChildrenExpanded = false;
		},

		expandChildNodes: function(nodeData) {
			if (!nodeData.activeOutgoingConnectors) {
				const childrenToExpand = nodeData.collapsedChildren;

				this._swapItemsBetweenArrays(nodeData, childrenToExpand, 'collapsedParents', 'parents');

				nodeData.children = nodeData.children.concat(childrenToExpand);
				nodeData.collapsedChildren = [];
			} else {
				const allRelatedItems = nodeData.collapsedChildren.concat(nodeData.children);
				nodeData.children = [];
				nodeData.collapsedChildren = [];
				const visibleItemIds = nodeData.activeOutgoingConnectors.map(function(item) {
					return item.gvcid || this._getDefinitionKey(item);
				}.bind(this));

				Array.prototype.forEach.call(allRelatedItems, function(relatedItem) {
					const connectorBetweenNodes = this._connectorsHash[this._getConnectorHashKey({
						source: nodeData,
						target: relatedItem
					})].connector_key;
					if (visibleItemIds.indexOf(connectorBetweenNodes.gvcid || this._getDefinitionKey(connectorBetweenNodes)) >= 0) {
						nodeData.children.push(relatedItem);

						const parentIndex = relatedItem.collapsedParents.indexOf(nodeData);
						if (parentIndex >= 0) {
							relatedItem.collapsedParents.splice(parentIndex, 1);
							relatedItem.parents.push(nodeData);
						}
					} else {
						nodeData.collapsedChildren.push(relatedItem);

						const parentIndex = relatedItem.parents.indexOf(nodeData);
						if (parentIndex >= 0) {
							relatedItem.parents.splice(parentIndex, 1);
							relatedItem.collapsedParents.push(nodeData);
						}
					}
				}.bind(this));
			}
			nodeData.isChildrenExpanded = true;
		},

		extendChildNodes: function(nodeData) {
			let extendedGraph = this.ownerControl.onItemExtend(nodeData);

			if (extendedGraph) {
				let graphNodeSet = [];
				let graphConnectorSet = [];

				for (let i = 0; i < extendedGraph.nodes.length; i++) {
					let extendedNode = extendedGraph.nodes[i];

					if (!this._nodesHash[extendedNode.id]) {
						graphNodeSet.push(extendedNode);
					}
				}

				for (let i = 0; i < extendedGraph.connectors.length; i++) {
					let connector = extendedGraph.connectors[i];
					let connectorHashId = this._getConnectorHashKey(connector);

					if (!this._connectorsHash[connectorHashId]) {
						graphConnectorSet.push(connector);
						this._connectorsHash[connectorHashId] = connector;
					}
				}

				this._preprocessData({
					nodes: graphNodeSet,
					connectors: graphConnectorSet
				});
				this.layerData.nodes = this.layerData.nodes.concat(graphNodeSet);
				this.layerData.connectors = this.layerData.connectors.concat(graphConnectorSet);
			}

			if (!nodeData.hiddenOutgoingConnectors || nodeData.hiddenOutgoingConnectors.length === 0) {
				nodeData.wasChildrenExtended = true;
			}
		},

		onToggleParents: function(nodeData, optionalParameters) {
			optionalParameters = optionalParameters || {};

			if (nodeData.isParentsExpanded === false) {
				this.expandParentNodes(nodeData);
			} else if (nodeData.isParentsExpanded === true) {
				this.collapseParentNodes(nodeData);
			} else {
				this.extendParentNodes(nodeData);
				nodeData.isParentsExpanded = true;
			}

			if (!optionalParameters.skipInvalidation) {
				this.invalidateLayer(nodeData);
			}
		},

		collapseParentNodes: function(nodeData) {
			this._setNodesLevel(this.layerData);

			const parentsToCollapse = nodeData.parents.filter(function(item) {
				return nodeData.level - 1 !== item.level;
			});

			this._swapItemsBetweenArrays(nodeData, parentsToCollapse, 'children', 'collapsedChildren');

			nodeData.collapsedParents = nodeData.collapsedParents.concat(parentsToCollapse);
			nodeData.parents = nodeData.parents.filter(function(item) {
				return nodeData.level - 1 === item.level;
			});

			nodeData.isParentsExpanded = false;
		},

		expandParentNodes: function(nodeData) {
			const parentsToExpand = nodeData.collapsedParents;

			this._swapItemsBetweenArrays(nodeData, parentsToExpand, 'collapsedChildren', 'children');

			nodeData.parents = nodeData.parents.concat(parentsToExpand);
			nodeData.collapsedParents = [];

			nodeData.isParentsExpanded = true;
		},

		extendParentNodes: function(nodeData) {
			this.expandParentNodes(nodeData);
			nodeData.wasParentsExtended = true;
		},

		collapseConnector: function(connectorData) {
			this._swapItemsBetweenArrays(connectorData.source, [connectorData.target], 'parents', 'collapsedParents');
			this._swapItemsBetweenArrays(connectorData.target, [connectorData.source], 'children', 'collapsedChildren');
		},

		_swapItemsBetweenArrays: function(rootNode, itemsToProcess, sourceArrayName, targetArrayName) {
			Array.prototype.forEach.call(itemsToProcess, function(itemToProcess) {
				const itemIndex = itemToProcess[sourceArrayName].indexOf(rootNode);

				if (itemIndex >= 0) {
					itemToProcess[sourceArrayName].splice(itemIndex, 1);
					itemToProcess[targetArrayName].push(rootNode);
				}
			});
		}
	});
});
