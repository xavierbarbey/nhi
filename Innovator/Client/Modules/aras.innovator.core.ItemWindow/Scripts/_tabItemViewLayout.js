﻿var LocationSearches = {};

var item = window.opener[paramObjectName].item;
var itemID = window.opener[paramObjectName].itemID;
var itemTypeName = window.opener[paramObjectName].itemTypeName;
var itemType = window.opener[paramObjectName].itemType;
var viewMode = window.opener[paramObjectName].viewMode;
var isEditMode = window.opener[paramObjectName].isEditMode;
var isTearOff = window.opener[paramObjectName].isTearOff;
var isNew = window.opener[paramObjectName].isNew;
var itemTypeLabel = window.opener[paramObjectName].itemTypeLabel;
var databaseName = window.opener[paramObjectName].databaseName;
var isSSVCEnabled = window.opener[paramObjectName].isSSVCEnabled;
var reserveSpaceForSidebar = window.opener[paramObjectName].reserveSpaceForSidebar;
var formHeight = window.opener[paramObjectName].formHeight;
var formWidth = window.opener[paramObjectName].formWidth;
window.opener[paramObjectName] = undefined;
var viewManager = null;
var topWindow = aras.getMostTopWindowWithAras(window);

const isCuiLayout = !!window.CuiLayout;
if (!isCuiLayout && (aras.isTempEx(item) || aras.isLockedByUser(item))) {
	isEditMode = true;
}

function getViewersTabs() {
	return dijit.byId('viewers');
}

function getDiscussionPanel() {
	return dijit.byId('discussion');
}

function getSidebar() {
	return dijit.byId('sidebar');
}

function getRightPlaceHolder() {
	return document.getElementById('rightSidebarContentPane');
}

function getViewManager() {
	return viewManager;
}

var windowStateObject = {
	isVisible: true,
	state: 'tabs on',

	resizeContainer: function() {
		var rels = dijit.byId('centerMiddle');
		if (!document.getElementById('formContentPane').offsetHeight || rels.domNode.offsetHeight < 30 || this.state === 'tabs on') {
			return;
		}
		this.state = 'tabs on';
		rels.domNode.classList.remove('centerMiddle__hidden');
		var tabs = document.getElementById('tabs-container');
		tabs.classList.remove('tabs__hidden');

		this.updateButtons();
		rels._layout();
	},

	setExpand: function() {
		this.state = 'tabs max';

		var rels = dijit.byId('centerMiddle');
		const isCuiLayout = !rels || rels.domNode.closest('.aras-item-view__relationship-accordion');

		rels.domNode.classList.remove('centerMiddle__hidden');
		var tabs = document.getElementById('tabs-container');
		tabs.classList.remove('tabs__hidden');
		document.getElementById('formContentPane').style.height = isCuiLayout ? '' : '0';

		if (!rels) {
			return;
		}

		const relationshipIframe = document.getElementById(relationships.currTabID);
		const relationshipsGrid = relationshipIframe.contentWindow.grid;

		rels.resize();

		if (relationshipsGrid) {
			relationshipsGrid.grid_Experimental.update();
		}
	},

	setCollapse: function() {
		var that = this;
		require(['dojo/dom-geometry'], function(domGeom) {
			if (that.isVisible) {
				that.state = 'tabs min';
			}
			var bc = document.getElementById('CenterBorderContainer');
			var lc = document.getElementById('formContentPane');
			var rels = dijit.byId('centerMiddle');
			var relsTabBar = dijit.byId('centerMiddle_relTabbar');
			var tabs = document.getElementById('tabs-container');
			const isCuiLayout = that.isVisible && (!rels || rels.domNode.closest('.aras-item-view__relationship-accordion'));

			if (rels) {
				rels.domNode.classList.toggle('centerMiddle__hidden', !isCuiLayout);
				tabs.classList.toggle('tabs__hidden', !isCuiLayout);
			}

			var btns = document.getElementById('relationshipExpandButtons');

			var boxBc = domGeom.getContentBox(bc);
			const boxBtns = btns && domGeom.getContentBox(btns);
			const h = boxBc.h - ((that.isVisible && boxBtns && boxBtns.h + 8) || 0);
			lc.style.height = isCuiLayout ? '' : h + 'px';

			if (!relsTabBar) {
				return;
			}

			const relationshipIframe = document.getElementById(relationships.currTabID);
			const relationshipsGrid = relationshipIframe.contentWindow.grid;

			relsTabBar.resize();

			if (relationshipsGrid) {
				relationshipsGrid.grid_Experimental.update();
			}
		});
	},

	setHidden: function() {
		// should be called only from SimpleTabContainer.js when we switched between tabs (in a left sideBar) to remember current state of tabs in a form
		this.isVisible = false;
		const formSplitter = document.getElementById('form-splitter');

		if (formSplitter) {
			formSplitter.classList.add('aras-hide');
		}

		this.setCollapse();
	},

	setVisible: function() { // should be called only from SimpleTabContainer.js when we switched between tabs to restore state of tabs in a form
		this.isVisible = true;
		const formSplitter = document.getElementById('form-splitter');

		if (formSplitter) {
			formSplitter.classList.remove('aras-hide');
		}

		switch (this.state) {
			case 'tabs min':
				this.setCollapse();
				break;
			case 'tabs max':
				this.setExpand();
				break;
			case 'tabs on':
				this.setNormal();
				break;
			default:
				throw new Error('Cannot make tabs visible when the state is \'tabs off\'');
		}

		this.updateButtons();
	},

	setNormal: function() {
		this.state = 'tabs on';

		var rels = dijit.byId('centerMiddle');
		rels.domNode.classList.remove('centerMiddle__hidden');
		var tabs = document.getElementById('tabs-container');
		tabs.classList.remove('tabs__hidden');

		var lc = document.getElementById('formContentPane');
		var form = dijit.byId('form');
		if (!form) {
			lc.style.height = '';
			if (rels) {
				const relationshipIframe = document.getElementById(relationships.currTabID);
				const relationshipGrid = relationshipIframe.contentWindow.grid;
				rels.resize();

				if (relationshipGrid) {
					relationshipGrid.grid_Experimental.update();
				}
			}
			return;
		}
		var formSize = form.getFormSize();
		lc.style.height = formSize.h + 'px';
		rels.resize();
	},

	setOff: function() {
		this.state = 'tabs off';
	},

	resizeByState: function() {
		if (this.state === 'tabs max') {
			this.setExpand();
		} else if (this.state === 'tabs min' || this.isVisible === false) {
			this.setCollapse();
		}
	},

	updateButtons: function() {
		var that = this;
		require(['dojo/dom-class'], function(domClass) {
			var expandBtn = dijit.byId('expandBtn');
			var collapseBtn = dijit.byId('collapseBtn');
			var normalBtn = dijit.byId('normalBtn');

			if (!expandBtn || !collapseBtn || !normalBtn) {
				return;
			}
			switch (that.state) {
				case 'tabs on':
					domClass.remove(expandBtn.domNode, 'dijitHidden');
					domClass.remove(collapseBtn.domNode, 'dijitHidden');
					domClass.add(normalBtn.domNode, 'dijitHidden');
					break;
				case 'tabs max':
					domClass.remove(collapseBtn.domNode, 'dijitHidden');
					domClass.remove(normalBtn.domNode, 'dijitHidden');
					domClass.add(expandBtn.domNode, 'dijitHidden');
					break;
				case 'tabs min':
					domClass.remove(expandBtn.domNode, 'dijitHidden');
					domClass.remove(normalBtn.domNode, 'dijitHidden');
					domClass.add(collapseBtn.domNode, 'dijitHidden');
					break;
			}
		});
	}
};

window.addEventListener('load', function() {
	require(['dijit/Tooltip'], function() {
		new dijit.Tooltip({
			connectId: ['expandBtn', 'collapseBtn', 'normalBtn'],
			getContent: function(node) {
				var text = aras.getResource('', 'Tearoff.showTab');
				if (node.id === 'collapseBtn') {
					text = aras.getResource('', 'Tearoff.minimizeTabs');
				} else if (node.id === 'expandBtn') {
					text = aras.getResource('', 'Tearoff.maximizeTabs');
				}
				return '<span style="white-space: nowrap;">' + text + '</span>';
			}
		});
	});
});

var relationshipsGridInfoProvider = {
	getInstance: function(relshipsGridId) { return this; },
	getMenu: function() { return window.tearOffMenuController; },
	waitForMenu: true
};

function getItem() {
	return item;
}

function setItem(value) {
	item = value;
}

function getIOMItem() {
	var iomItem = aras.newIOMItem();
	var itemNode = getItem();
	iomItem.dom = itemNode.ownerDocument;
	iomItem.node = itemNode;

	return iomItem;
}

function updateRootItem(itemNd, reloadRelationshipQueryString) {
	if (!itemNd) {
		aras.AlertError('Failed to get the ' + window.itemTypeName, '', '');
		return false;
	}

	if (itemNd.getAttribute('type') != window.itemTypeName) {
		aras.AlertError('Invalid ItemType specified: (' + itemNd.getAttribute('type') + ').');
		return false;
	}

	itemID = itemNd.getAttribute('id');
	var wasLocked = (aras.isLockedByUser(item) || aras.isTempEx(item));
	item = itemNd;

	if (isEditMode) {
		isEditMode = wasLocked;
		setEditMode(reloadRelationshipQueryString);
	} else {
		isEditMode = wasLocked;
		setViewMode();
	}

	if (window.frameElement) {
		const currentTabID = this.frameElement.id;
		const newTabID = aras.mainWindowName + '_' + itemID;

		if (currentTabID !== newTabID) {
			arasTabsobj.updateTabInformation(currentTabID, newTabID);
		}
	}

	if (isSSVCEnabled) {
		require([
			'SSVC/Scripts/Classes/SSVCViewManager'],
			function(SSVCViewManager) {
				var viewers = dijit.byId('viewers');
				var leftPlaceHolder = getSidebar();

				var viewManager = new SSVCViewManager({});
				viewManager.clearContainers({
					tabContainer: viewers,
					sidebarContainer: leftPlaceHolder
				});
				loadSidebar();
			});
	}
}

function setEditMode(reloadRelationshipQueryString) {
	var prevMode = isEditMode;
	isEditMode = true;
	var formNd = aras.uiGetForm4ItemEx(item, 'edit');
	let fieldNds = [];
	if (formNd) {
		var bodyNd = formNd.selectSingleNode('Relationships/Item[@type="Body"]');
		fieldNds = bodyNd.selectNodes('Relationships/Item[@type="Field" and (not(@action) or (@action!="delete" and @action!="purge"))]');
	}

	var fieldsHash = [];
	var formFrame = document.getElementById('instance');

	if (formFrame) {
		var fieldNd;
		var fieldId;
		var setExpHandler;
		var i;

		for (i = 0; i < fieldNds.length; i++) {
			fieldNd = fieldNds[i];
			fieldId = fieldNd.getAttribute('id');
			setExpHandler = formFrame.contentWindow['expression_' + fieldId + '_setExpression'];

			if (setExpHandler) {
				setExpHandler(isEditMode);
			}
		}
	}
	var titleNode = document.getElementsByTagName('title');
	if (titleNode && titleNode[0]) {
		titleNode[0].innerHTML = aras.getResource('', 'ui_methods_ex.itemtype_label_item_keyed_name', itemTypeLabel, aras.getKeyedNameEx(item));
	}

	if (isTearOff) {
		if (document.getElementById('tearoff_menu') && document.getElementById('tearoff_menu').contentWindow.setEditMode) {
			document.getElementById('tearoff_menu').contentWindow.setEditMode();
		}
	}

	if (formFrame) {
		var addFormID = aras.uiGetFormID4ItemEx(item, 'add');
		var editFormID = aras.uiGetFormID4ItemEx(item, 'edit');
		var viewFormID = aras.uiGetFormID4ItemEx(item, 'view');
		var prevFormID = formFrame.contentWindow.document.formID;

		if (item.getAttribute('action') == 'add') {
			if (prevFormID != addFormID) {
				aras.uiShowItemInFrameEx(formFrame.contentWindow, item, 'add', 0);
			} else if (formFrame.contentWindow.document.forms.MainDataForm) {
				aras.uiPopulateFormWithItemEx(formFrame.contentWindow.document.forms.MainDataForm, item, itemType, true);
			}
		} else if ((prevMode != isEditMode && editFormID != viewFormID) || editFormID != prevFormID || !editFormID) {
			aras.uiShowItemInFrameEx(formFrame.contentWindow, item, 'edit', 0);
		} else if (formFrame.contentWindow.document.getElementById('MainDataForm')) {
			aras.uiPopulateFormWithItemEx(formFrame.contentWindow.document.forms.MainDataForm, item, itemType, true);
		}
	}

	if (window.relationships) {
		if ((window.relationships.contentWindow.setEditMode && !reloadRelationshipQueryString) || window.relationships.relTabbar.isRelationshipTab()) {
			window.relationships.contentWindow.setEditMode();
		} else {
			if (reloadRelationshipQueryString) {
				window.relationships.reload(reloadRelationshipQueryString);
			}
		}
	}
}

function setViewMode() {
	var prevMode = isEditMode;
	isEditMode = false;
	var formNd = aras.uiGetForm4ItemEx(item, 'edit');
	let fieldNds = [];
	if (formNd) {
		var bodyNd = formNd.selectSingleNode('Relationships/Item[@type="Body"]');
		fieldNds = bodyNd.selectNodes('Relationships/Item[@type="Field" and (not(@action) or (@action!="delete" and @action!="purge"))]');
	}

	var fieldsHash = [];
	var formFrame = document.getElementById('instance');

	if (formFrame) {
		var fieldNd;
		var fieldId;
		var setExpHandler;
		var i;

		for (i = 0; i < fieldNds.length; i++) {
			fieldNd = fieldNds[i];
			fieldId = fieldNd.getAttribute('id');
			setExpHandler = formFrame.contentWindow['expression_' + fieldId + '_setExpression'];

			if (setExpHandler) {
				setExpHandler(isEditMode);
			}
		}
	}

	document.title = aras.getResource('', 'ui_methods_ex.itemtype_label_item_keyed_name_readonly', itemTypeLabel, aras.getKeyedNameEx(item));

	if (isTearOff) {
		if (document.getElementById('tearoff_menu') && document.getElementById('tearoff_menu').contentWindow.setViewMode) {
			document.getElementById('tearoff_menu').contentWindow.setViewMode();
		}
	}

	if (formFrame) {
		var editFormID = aras.uiGetFormID4ItemEx(item, 'edit');
		var viewFormID = aras.uiGetFormID4ItemEx(item, 'view');
		var prevFormID = formFrame.contentWindow.document.formID;

		if ((prevMode != isEditMode && editFormID != viewFormID) || viewFormID != prevFormID || !viewFormID) {
			aras.uiShowItemInFrameEx(formFrame.contentWindow, item, 'view', 0);
		} else if (formFrame.contentWindow.document.forms.MainDataForm) {
			aras.uiPopulateFormWithItemEx(formFrame.contentWindow.document.forms.MainDataForm, item, itemType, false);
		}
	}

	if (window.relationships && window.relationships.setViewMode) {
		window.relationships.setViewMode();
	}
}

function turnWindowReadyOn() {
	//that script will set windowReady = true and cause menuUpdate
	window.windowReady = true;
	window.updateMenuState();
}

var statusbar;
var relationships;
var relationshipsControl;

function updateKeyedNameOnSidebar() {
	var keyedName = '';
	var keyedNameNode = item.selectSingleNode('keyed_name');

	if (keyedNameNode) {
		keyedName = keyedNameNode.text;
	}

	var divKeyedName = document.getElementById('keyedName');
	if (divKeyedName) {
		divKeyedName.textContent = keyedName;
	}
}

//args = { style: ... }
function createSidebar() {
	let sidebar = getSidebar();
	if (sidebar) {
		return sidebar;
	}
	const itemTypeColor = aras.getItemTypeColor(window.itemTypeName, 'name');
	const style = 'width:30px;height:100%;position:relative;background-color:' + itemTypeColor;
	sidebar = new Sidebar({
		id: 'sidebar',
		style: style,
		baseClass: 'sidebar'
	});
	document.getElementById('leftSidebarContentPane').appendChild(sidebar.domNode);
	return sidebar;
}

function createSplitters() {
	var ssvcSplitter = document.getElementById('ssvc-splitter');
	var formSplitter = document.getElementById('form-splitter');
	window.ArasModules.splitter(formSplitter);

	document.addEventListener('mouseup', function(e) {
		var ssvcSplitterClasses = ssvcSplitter.firstElementChild && ssvcSplitter.firstElementChild.classList;
		const formSplitterClasses = formSplitter && formSplitter.firstElementChild && formSplitter.firstElementChild.classList;
		var ssvcDrag = ssvcSplitterClasses && ssvcSplitterClasses.contains('aras-splitter-ghost_draggable');
		var formDrag = formSplitterClasses && formSplitterClasses.contains('aras-splitter-ghost_draggable');
		if (ssvcDrag || formDrag) {
			setTimeout(function() {
				var relationshipContainer = dijit.byId('centerMiddle');
				if (relationshipContainer) {
					relationshipContainer.resize();
					var evt = new CustomEvent('resize');
					window.dispatchEvent(evt);
				}
				windowStateObject.resizeContainer.call(windowStateObject);
			}, 0);
		}
	});
}

(function() {
	var loadWidgetsListener = function() {
		var tabContainer = dijit.byId('viewers');
		var centerMiddle = dijit.byId('centerMiddle');

		if (tabContainer) {
			tabContainer.startup();
		}

		createSplitters();

		if (centerMiddle) {
			windowStateObject.resizeByState();
			window.onresize = function() {
				windowStateObject.resizeByState();
			};
			centerMiddle.resize();
		}

		turnWindowReadyOn();

		loadSidebar();
		aras.browserHelper.toggleSpinner(document, false);
	};

	document.addEventListener('loadWidgets', loadWidgetsListener);

	window.getUnsavedFiles = function() {
		var unsavedFiles = window.item.selectNodes('descendant::Item[@type="File" and @isTemp="1"]');
		//if a file is not in associatedFileList then it has already been saved (save unlock and close button)
		return Array.prototype.filter.call(unsavedFiles, function(node) {
			return aras.vault.vault.associatedFileList[node.getAttribute('id')];
		});
	};
	// We need to remove listener before the window will be closed
	// because used 'dojo.emit' which use own inner events without
	// handlers and IE will try to use handler from another registered event
	window.addEventListener('beforeunload', function() {
		document.removeEventListener('loadWidgets', loadWidgetsListener, false);
		//If browser is IE, have unsaved files and item hasn't been deleted then show dialog
		if (aras.Browser.isIe() && getUnsavedFiles().length > 0 && item.getAttribute('action') !== 'skip') {
			return aras.getResource('', 'file_management.files_will_be_lost');
		}
	}, false);

	window.removeAllUnsavedFiles = function() {
		var unsavedFiles = getUnsavedFiles();
		if (unsavedFiles.length === 0) {
			return;
		}
		for (var i = 0; i < unsavedFiles.length; i++) {
			var node = unsavedFiles[i];
			node.parentNode.parentNode.removeChild(node.parentNode);
			var itemId = node.getAttribute('id');
			aras.vault.vault.removeFileFromList(itemId);
			aras.deleteItem('File', itemId, true);
		}
		delete aras.tabsTitles.File;
		//Check attribute 'action' if item has been deleted
		if (item.getAttribute('action') !== 'skip') {
			updateItemsGrid(window.item);
		}
	};

	//all unsaved files must be removed from closed tearoff in IE
	//because there is no access to files created in one window from another window
	if (aras.Browser.isIe()) {
		window.addEventListener('unload', removeAllUnsavedFiles);
	}
})();

// event is used in ui_methodsEx.Aras.prototype.RefillWindow and aras.innovator.SSVC\Scripts\Feed.js
function dispatchSidebarLoadedEvent() {
	var event = document.createEvent('Event');
	event.initEvent('loadSideBar', true, false);
	document.dispatchEvent(event);
}

document.addEventListener('DOMContentLoaded', function() {
	if (window.reserveSpaceForSidebar) {
		document.getElementById('leftSidebarContentPane').style.width = '30px';
	}
});

function loadSidebar() {
	var errorHandler = function(error) {
		if (error instanceof Error) {
			aras.AlertError(error.message, error, error.stack);
			return;
		}

		if (error instanceof XMLHttpRequest) {
			var xml = ArasModules.xml.parseString(error.responseText);

			// Show alert even if `faultcode: 0`. Errors must be show.
			if (aras.hasFault(xml, false)) {
				aras.AlertError(aras.getFaultString(xml));
				return;
			}
		}

		var stack = null;
		var message = error || aras.getResource('', 'common.client_side_err');
		try {
			// HACK: Error.stack assigned by IE11 only after throw Error
			throw new Error(message);
		} catch (errorWithStack) {
			stack = errorWithStack.stack;
		}

		aras.AlertError(message, error, stack);
	};

	//require of module (SSVCViewManager) are needed for synchronous execution dojo.require in the method VC_GetSidebarCUI
	require([
		'Controls/Sidebar',
		'SSVC/Scripts/Classes/SSVCDataManager',
		'SSVC/Scripts/Classes/SSVCViewManager'
	], function(sidebar, dataManager) {
		Promise.resolve()
			.then(function() {
				if (isSSVCEnabled) {
					return dataManager
						.updateDataForItem(window.item)
						.catch(errorHandler);
				}
			})
			.then(function() {
				var contextParams = {locationName: 'ItemWindowSidebar'};
				return topWindow.cui.loadSidebarAsync(contextParams).then(function(items) {
					if (items.length > 0) {
						var sidebar = createSidebar();
						topWindow.cui.initSidebar(sidebar, items);
						topWindow.cui.dispatchCommandBarLoadedEvent(contextParams.locationName);
					}
				});
			})
			.then(function() {
				checkReservedSpaceForSidebar();
			})
			.then(function() {
				if (isSSVCEnabled) {
					return asyncLoadingSSVC();
				}
			})
			.then(function() {
				dispatchSidebarLoadedEvent();
			})
			.catch(errorHandler);
	});
}

function checkReservedSpaceForSidebar() {
	var sidebar = getSidebar();
	// check that space for sidebar was reserved correctly, otherwise do resize form
	if (!sidebar && window.reserveSpaceForSidebar || sidebar && !window.reserveSpaceForSidebar) {
		if (window.reserveSpaceForSidebar) {
			document.getElementById('leftSidebarContentPane').style.width = '';
		}
	}
}

function asyncLoadingSSVC() {
	return promisifiedRequire(['SSVC/Scripts/Classes/SSVCViewManager', 'SSVC/Scripts/Classes/SSVCDataManager'])
		.then(function(modules) {
			var SSVCViewManager = modules[0];
			var dataManager = modules[1];
			var rightPlaceHolder = getRightPlaceHolder();

			viewManager = new SSVCViewManager({
				aras: aras,
				dojo: dojo,
				dojoRequire: require
			});
			viewManager.fillContainers({
				dataManager: dataManager,
				discussionContainer: rightPlaceHolder,
				isSSVCEnabled: !!aras.getItemProperty(item, 'config_id', ''),
				aras: aras
			});

			updateKeyedNameOnSidebar();
		});
}

/**
 * Adapter require() to Promise.
 *
 * @param {string[]} requiredModules - Module names to require
 * @returns {Promise<Object[]>}
 */
function promisifiedRequire(requiredModules) {
	return new Promise(function(resolve, reject) {
		require(
			requiredModules,
			function() {
				// Resolve the arguments as an array.
				resolve(Array.prototype.slice.call(arguments, 0));
			},
			reject);
	});
}
