﻿(function(externalParent) {
	const normalizeFloatDecimal = function(value) {
		const numberToString = window.ArasModules.intl.number.toString;
		const parseFloat = window.ArasModules.intl.number.parseFloat;

		const numberValue = parseFloat(value);
		const stringValue = isNaN(numberValue)
			? value
			: numberToString(numberValue);
		return stringValue;
	};

	const generateJsonNode = function(criteria, condition, type) {
		let value = criteria;
		if (type === 'decimal' || type === 'float') {
			value = normalizeFloatDecimal(criteria);
		}
		return {
			'@attrs': {
				condition: condition
			},
			'@value': value
		};
	};

	const parseNumberQuery = function(criterias, propName, type) {
		return criterias.reduce(function(acc, criteria) {
			acc[propName] = acc[propName] || [];
			const criteriaParts = criteria.split(/(\.{3}|>=|<=|>|<)/);
			if (criteriaParts.length !== 3) {
				acc[propName].push(generateJsonNode(criteria, 'eq', type));
				return acc;
			}
			if (criteriaParts[0].length === 0) {
				switch (criteriaParts[1]) {
					case '>=':
						acc[propName].push(generateJsonNode(criteriaParts[2], 'ge', type));
						break;
					case '<=':
						acc[propName].push(generateJsonNode(criteriaParts[2], 'le', type));
						break;
					case '>':
						acc[propName].push(generateJsonNode(criteriaParts[2], 'gt', type));
						break;
					case '<':
						acc[propName].push(generateJsonNode(criteriaParts[2], 'lt', type));
						break;
				}
			} else if (criteriaParts[1] === '...') {
				acc.AND = acc.AND || [];
				const rangeCriteria = {};
				rangeCriteria[propName] = [
					generateJsonNode(criteriaParts[0], 'ge', type),
					generateJsonNode(criteriaParts[2], 'le', type)
				];
				acc.AND.push(rangeCriteria);
			}
			return acc;
		}, {});
	};

	const simpleToAml = function(criteria, propName, type) {
		const criterias = criteria.split('|');

		let jsonForParse;
		if (
			type === 'float' ||
			type === 'decimal' ||
			type === 'integer' ||
			type === 'ubigint' ||
			type === 'global_version'
		) {
			jsonForParse = parseNumberQuery(criterias, propName, type);
		} else {
			jsonForParse = criterias.reduce(function(acc, criteria) {
				acc[propName].push(generateJsonNode(criteria, 'eq', type));
				return acc;
			}, {});
		}

		if (criterias.length > 1) {
			jsonForParse = {
				OR: jsonForParse
			};
		}

		return window.ArasModules.jsonToXml(jsonForParse);
	};

	const searchConverter = {
		simpleToAml: simpleToAml
	};
	externalParent = Object.assign(externalParent, {
		searchConverter: searchConverter
	});
	window.ArasCore = window.ArasCore || externalParent;
})(window.ArasCore || {});
