﻿/* eslint-disable no-unused-vars */
import NavigationPanelTabs from '../../NavigationPanel/NavigationPanelTabs';
import NavigationPanel from '../../NavigationPanel/NavigationPanel';
import UserNotificationsContainer from './userNotifications';
import HeaderTabs from './headerTabs';
import MainWindowCuiLayout from './MainWindowCuiLayout';
import FavoritesManager from '../FavoritesManager';
import mainPageEventHandlers from './mainPageEventHandlers';

window.mainPageEventHandlers = mainPageEventHandlers;
window.UserNotificationContainer = UserNotificationsContainer;
window.MainWindowCuiLayout = MainWindowCuiLayout;
window.FavoritesManager = FavoritesManager;
HeaderTabs.define('aras-header-tabs');
