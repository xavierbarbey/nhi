﻿import validateBrowserCertified from './validateBrowserCertified';
import disabledCookiesDialog from '../dialogs/disabledCookies';
import getResourceManager from './getResourceManager';

function initLoginFrame() {
	const tzLabel = aras.browserHelper.tzInfo.getTimeZoneLabel();
	const winTzName = aras.browserHelper.tzInfo.getTimeZoneNameFromLocalStorage(
		tzLabel
	);

	if (winTzName) {
		aras.setCommonPropertyValue('systemInfo_CurrentTimeZoneName', winTzName);
		return window.login();
	}

	const winTzNames = aras.browserHelper.tzInfo.getWindowsTimeZoneNames(tzLabel);

	if (winTzNames.length === 0) {
		return aras.AlertError(aras.getResource('', 'tz.get_currentzone_fail'));
	} else if (winTzNames.length === 1) {
		aras.setCommonPropertyValue(
			'systemInfo_CurrentTimeZoneName',
			winTzNames[0]
		);
		aras.browserHelper.tzInfo.setTimeZoneNameInLocalStorage(
			tzLabel,
			winTzNames[0]
		);
		return window.login();
	}

	return window.showTimeZoneSelectFrame(tzLabel, winTzNames, function(tzName) {
		aras.setCommonPropertyValue('systemInfo_CurrentTimeZoneName', tzName);
		aras.browserHelper.tzInfo.setTimeZoneNameInLocalStorage(tzLabel, tzName);
		window.hideTimeZoneSelectFrame();
		return window.login();
	});
}
function validateCookiesEnabled() {
	return navigator.cookieEnabled ? Promise.resolve() : disabledCookiesDialog();
}
function onBeforeUnloadHandler() {
	if (aras.getCommonPropertyValue('exitInProgress') === true) {
		return;
	}

	let resourceManager;
	if (aras.isDirtyItems()) {
		window.onLogoutCommand();
		resourceManager = getResourceManager(
			'core',
			'ui_resources.xml',
			aras.getSessionContextLanguageCode()
		);
		return resourceManager.getString('setup.beforeunload_warning');
	} else if (
		window.arasTabs &&
		!!window.arasTabs.tabs.length &&
		aras.getCommonPropertyValue('exitWithoutSavingInProgress') !== true
	) {
		resourceManager = getResourceManager(
			'core',
			'ui_resources.xml',
			aras.getSessionContextLanguageCode()
		);
		return resourceManager.getString('setup.tab_close_warning');
	}
}
function onLoadHandler() {
	aras.browserHelper.toggleSpinner(document, false, 'dimmer_spinner');

	validateCookiesEnabled()
		.then(validateBrowserCertified)
		.then(initLoginFrame)
		.then(() => {
			aras.setWindowLangAttribute(window);
		});
}
function onUnloadHandler() {
	if (!window.aras) {
		return;
	}
	aras.setCommonPropertyValue('exitInProgress', true);
	aras.getOpenedWindowsCount(true);
	aras.unlockEditStateItems();
	aras.logout();
}

export default { onBeforeUnloadHandler, onLoadHandler, onUnloadHandler };
