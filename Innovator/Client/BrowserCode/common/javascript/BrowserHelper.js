﻿function BrowserHelper(aras, aWindow) {
	'use strict';
	this.window = aWindow;
	this.aras = aras;
}

BrowserHelper.prototype.initComponents = function() {
	var timeZoneInfo = null;

	Object.defineProperty(this, 'tzInfo', {
		get: function() {
			if (!timeZoneInfo) {
				timeZoneInfo = new this.window.TimeZonesInformation();
			}
			return timeZoneInfo;
		}
	});
};

BrowserHelper.prototype.adjustHeightTexAreaWithNullableRows = function(textArea) {
	return this.aras.Browser.isIe() ?
		textArea.offsetHeight :
		((textArea.offsetHeight - 2) / 3 + 2); // There is 3 lines in case of rows == 0
};

BrowserHelper.prototype.getNodeTranslationElement = function(srcNode, nodeName, translationXMLNsURI, lang) {
	if (this.aras.Browser.isIe()) {
		return srcNode.selectSingleNode('*[local-name()=\'' + nodeName + '\' and namespace-uri()=\'' + translationXMLNsURI + '\' and @xml:lang=\'' + lang + '\']');
	}
	var nds = srcNode.getElementsByTagNameNS(translationXMLNsURI, nodeName);
	var i;
	var resNd;
	for (i = 0; i < nds.length; i++) {
		if (nds[i].parentNode == srcNode && nds[i].getAttribute('xml:lang') === lang) {
			resNd = nds[i];
			break;
		}
	}
	return resNd;
};

BrowserHelper.prototype.createTranslationNode = function(srcNode, nodeName, translationXMLNsURI, translationXMLNdPrefix) {
	if (srcNode.ownerDocument.createElementNS) {
		return srcNode.ownerDocument.createElementNS(translationXMLNsURI, translationXMLNdPrefix + ':' + nodeName);
	}
	var resNd = srcNode.ownerDocument.createNode(1, translationXMLNdPrefix + ':' + nodeName, translationXMLNsURI);
	resNd.setAttribute('xmlns:' + translationXMLNdPrefix, translationXMLNsURI);
	return resNd;
};

BrowserHelper.prototype.getTextContentPropertyName = function() {
	return 'textContent';
};

BrowserHelper.prototype.getHeightDiffBetweenTearOffAndMainWindow = function() {
	//Height of Title Bar in Main window is less than in Tear-Off window
	return this.aras.Browser.isIe() ? 0 : 8;
};

BrowserHelper.prototype.isWindowClosed = function(window) {
	var res = false;
	try {
		res = window.closed;
	}
	catch (e) {
		//"Permission denied."
		// "-2146823281" - "Object expected" - this error sometimes generates by MS edge, when window is closed and reference is corrupted
		if (e.number != -2146828218 && e.number != -2146823281) {
			throw e;
		} else {
			res = true;
		}
	}
	return res;
};

/***
 * Move window to specified coordinates
 * @param aWindow target window to move
 * @param x
 * @param y
 */
BrowserHelper.prototype.moveWindowTo = function(aWindow, x, y) {
	if (!this.aras.Browser.isEdge()) {
		aWindow.moveTo(x, y);
	}
};

/***
 * Resize window to specified measurements
 * @param aWindow window to resize
 * @param width
 * @param height
 */
BrowserHelper.prototype.resizeWindowTo = function(aWindow, width, height) {
	if (this.aras.Browser.isIe() && aWindow.dialogArguments) {
		//some dialog shown with a call to showModalDialog or showModelessDialog method
		//window.resizeTo still doesn't work for dialogs in IE. Tested in IE 9 - IE 11.
		var frameWidth = aWindow.outerWidth - aWindow.innerWidth;
		var frameHeight = aWindow.outerHeight - aWindow.innerHeight;
		//according to MSDN dialogHeight property returns the height of the content area and doesn't include the height of the frame.
		//http://msdn.microsoft.com/en-us/library/ie/ms533724%28v=vs.85%29.aspx
		aWindow.dialogHeight = (height - frameHeight) + 'px';
		aWindow.dialogWidth = (width - frameWidth) + 'px';
	} else if (!this.aras.Browser.isEdge()) {
		//regular window
		aWindow.resizeTo(width, height);
	}
};

/***
 * Set focus to specified window
 * @param aWindow Window to focus
 */
BrowserHelper.prototype.setFocus = function(aWindow) {
	// window.focus is not working in chrome for parent window.
	// We need use window.open with empty url as 1-st argument and name of exits window as 2-nd argument.
	if (aras.Browser.isCh() && aWindow.opener && !aWindow.frameElement) {
		aWindow.opener.open('', aWindow.name);
	}
	//setTimeout to fix window.focus in Mozilla Firefox
	setTimeout(function() { aWindow.focus(); }, 0);
};

BrowserHelper.prototype.lockNewWindowOpen = function(aras, lockFlag) {
	if (this.aras.Browser.isIe()) {
		aras.commonProperties.lockNewWindowOpen = lockFlag;
	}
};

BrowserHelper.prototype.newWindowCanBeOpened = function(aras) {
	return !aras.commonProperties.lockNewWindowOpen;
};

BrowserHelper.prototype.fixSizeOnModalDialogResize = function(modalDialogWin, idsToResizeWidth, idsToResizeHeight, fixBody, cb) {
	if (this.aras.Browser.isIe()) {
		modalDialogWin.addEventListener('resize', function() {
			var i;
			var doc = modalDialogWin.document;
			var w = doc.documentElement.clientWidth;
			var h = doc.documentElement.clientHeight;
			var oldw = modalDialogWin['BrowserHelper_oldClientWidth'];
			var oldh = modalDialogWin['BrowserHelper_oldClientHeight'];

			if (w != oldw) {
				if (idsToResizeWidth) {
					idsToResizeWidth = typeof idsToResizeWidth === 'string' ? [idsToResizeWidth] : idsToResizeWidth;
					for (i = 0; i < idsToResizeWidth.length; i++) {
						doc.getElementById(idsToResizeWidth[i]).style.width = w + 'px';
					}
				}

				if (fixBody) {
					doc.body.width = w + 'px';
				}

				modalDialogWin['BrowserHelper_oldClientWidth'] = w;
			}

			if (idsToResizeHeight && h != oldh) {
				if (idsToResizeHeight) {
					idsToResizeHeight = typeof idsToResizeHeight === 'string' ? [idsToResizeHeight] : idsToResizeHeight;
					for (i = 0; i < idsToResizeHeight.length; i++) {
						doc.getElementById(idsToResizeHeight[i]).style.height = h + 'px';
					}
				}

				if (fixBody) {
					doc.body.height = h + 'px';
				}

				modalDialogWin['BrowserHelper_oldClientHeight'] = h;
			}

			if (cb) {
				cb({'old': {w: oldw, h: oldh}, 'new': {w: w, h: h}});
			}
		});
	}
};

BrowserHelper.prototype.adjustGridSize = function(aWindow, handler, asFunc) {
	if (this.aras.Browser.isIe()) {
		if (asFunc) {
			aWindow.onresize = handler;
		} else {
			aWindow.addEventListener('resize', handler, false);
		}
		handler();
	}
};

/**
 * Turning on/off class in "spinner" element (classList is required)
 *
 * @param {HTMLElement|Document} context - current container object
 * @param {string} state - turn on/off spinner
 * @param {string} [spinnerId] - id attribute of spinner element
 * @return {boolean} spinner found or no
 */
BrowserHelper.prototype.toggleSpinner = function(context, state, spinnerId) {
	spinnerId = spinnerId || 'dimmer_spinner';

	var spinnerEl = (context.ownerDocument || context).getElementById(spinnerId);

	if (!spinnerEl) {
		return false;
	}

	spinnerEl.classList.toggle('aras-hide', !state);
	return true;
};

// +++++++ Methods for generating XSLT stylesheets for Grids
BrowserHelper.prototype.addXSLTCssFunctions = function(container) {
	container.push('<xsl:template name="initItemCSS">\n');
	container.push('	<xsl:param name="css"/>\n');
	container.push('	<xsl:param name="fed_css"/>\n');
	container.push('	<xsl:value-of select="translate(concat($css, $fed_css), \'&#x20;&#x9;&#xD;&#xA;\', \'\')"/>\n');
	container.push('</xsl:template>\n');

	container.push('<xsl:template name="getPropertyStyleEx">\n');
	container.push('	<xsl:param name="css"/>\n');
	container.push('	<xsl:param name="propName"/>\n');
	container.push('	<xsl:text>;</xsl:text>\n');
	container.push('	<xsl:variable name="delimiter" select="concat(\'.\', $propName, \'{\')"/>\n');
	container.push('	<xsl:if test="string-length(substring-after($css, $delimiter))">\n');
	container.push('		<xsl:variable name="resCss" select="substring-before(substring-after($css, $delimiter), \'}\')"/>\n');
	container.push('		<xsl:value-of select="$resCss"/>\n');
	container.push('		<xsl:if test="substring($resCss, string-length($resCss)) != \';\'">\n');
	container.push('			<xsl:text>;</xsl:text>\n');
	container.push('		</xsl:if>\n');
	container.push('		<xsl:call-template name="getPropertyStyleEx">\n');
	container.push('			<xsl:with-param name="css" select="substring-after($css, $delimiter)"/>\n');
	container.push('			<xsl:with-param name="propName" select="$propName"/>\n');
	container.push('		</xsl:call-template>\n');
	container.push('	</xsl:if>\n');
	container.push('</xsl:template>\n');
};

BrowserHelper.prototype.addXSLTSpecialNamespaces = function(container) {};
BrowserHelper.prototype.addXSLTInitItemCSSCall = function(container, cssValue, fedCssValue) {
	container.push('<xsl:call-template name="initItemCSS">\n');
	container.push('	<xsl:with-param name="css" select="' + cssValue + '"/>\n');
	container.push('	<xsl:with-param name="fed_css" select="' + fedCssValue + '"/>\n');
	container.push('</xsl:call-template>\n');
};

BrowserHelper.prototype.addXSLTGetPropertyStyleExCall = function(container, nameCSS, cellCSSVariableName1, name) {
	container.push('<xsl:variable name="' + nameCSS + '" >\n');
	container.push('	<xsl:call-template name="getPropertyStyleEx">\n');
	container.push('		<xsl:with-param name="css" select="string($' + cellCSSVariableName1 + ')" />\n');
	container.push('		<xsl:with-param name="propName" >' + name + '</xsl:with-param>\n');
	container.push('	</xsl:call-template>\n');
	container.push('</xsl:variable>\n');
};
// ------- Methods for generating XSLT stylesheets for Grids

BrowserHelper.prototype.escapeUrl = function(url) {
	//after xslt transformation of the data for grid in FF need to escape the url on the pictures src
	if (this.aras.Browser.isFf()) {
		return url.replace(/&/g, '&amp;');
	} else {
		return url;
	}
};

//IE does not properly set the focus in input fields in the new iframes
//We programmatically set the focus on the input field, and then remove the focus
//then IE will be able to operate normally with all input fields.
BrowserHelper.prototype.restoreFocusForIE = function(window) {
	if (this.aras.Browser.isIe()) {
		var document = window.document;
		var inputTMP = document.createElement('INPUT');
		inputTMP.setAttribute('type', 'text');
		inputTMP.style.position = 'absolute';
		inputTMP.style.top = '-1000px';
		document.body.appendChild(inputTMP);
		inputTMP.focus();
		document.body.focus();
		document.body.removeChild(inputTMP);
	}
};
