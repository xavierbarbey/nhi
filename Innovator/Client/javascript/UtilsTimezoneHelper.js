﻿function UtilsTimezoneHelper() {
	var currentTimezoneOffset = new Date(2014, 0, 1).getTimezoneOffset();

	this.getTimezoneNameFromJs = function UtilsTimezoneHelperGetTimezoneNameFromJs() {
		//some value coming from LOCAL_MACHINE\Software\Microsoft\Windows NT\CurrentVersion\Time Zones
		var defaultMapping = {
			'-13:00': 'Dateline Standard Time',
			'-12:00': 'Dateline Standard Time',
			'-11:00': 'Samoa Standard Time',
			'-10:00': 'Hawaiian Standard Time',
			'-09:00': 'Alaskan Standard Time',
			'-08:00': 'Pacific Standard Time',
			'-07:00': 'US Mountain Standard Time',
			'-06:00': 'Canada Central Standard Time',
			'-05:00': 'Eastern Standard Time',
			'-04:30': 'Venezuela Standard Time',
			'-04:00': 'Atlantic Standard Time',
			'-03:30': 'Newfoundland Standard Time',
			'-03:00': 'SA Eastern Standard Time',
			'-02:00': 'Mid-Atlantic Standard Time',
			'-01:00': 'Azores Standard Time',
			'+00:00': 'GMT Standard Time',
			'+01:00': 'W. Europe Standard Time',
			'+02:00': 'E. Europe Standard Time',
			'+03:00': 'Russian Standard Time',
			'+03:30': 'Iran Standard Time',
			'+04:00': 'Arabian Standard Time',
			'+04:30': 'Afghanistan Standard Time',
			'+05:00': 'West Asia Standard Time',
			'+05:30': 'India Standard Time',
			'+05:45': 'Nepal Standard Time',
			'+06:00': 'Central Asia Standard Time',
			'+06:30': 'Myanmar Standard Time',
			'+07:00': 'North Asia Standard Time',
			'+08:00': 'China Standard Time',
			'+09:00': 'Tokyo Standard Time',
			'+09:30': 'Cen. Australia Standard Time',
			'+10:00': 'E. Australia Standard Time',
			'+11:00': 'Central Pacific Standard Time',
			'+12:00': 'New Zealand Standard Time',
			'+13:00': 'Tonga Standard Time',
			//IR-025787. It is necessary as there is a difference between Standard Time and Daylight Saving Time
			'+14:00': 'Tonga Standard Time'
		};

		var offset = currentTimezoneOffset;
		var h = (Math.abs(offset) - Math.abs(offset) % 60) / 60;
		var m = Math.abs(offset) - h * 60;
		var key = (offset <= 0 ? '+' : '-') +
					(h < 10 ? '0' : '') + h + ':' +
						(m < 10 ? '0' : '') + m;
		key = defaultMapping[key];
		return key || 'UTC';
	};

	/*	this.test = function() {
			var fireTest = function (offset, expectedRes) {
				currentTimezoneOffset = offset;
				var res = this.getTimezoneNameFromJs();
				if (res !== expectedRes) {
					alert(res + " !== " + expectedRes);
				}
			}.bind(this);
			fireTest(12 * 60, "Dateline Standard Time");
			fireTest(12 * 60 - 20, "UTC");
			fireTest(5 * 60, "Eastern Standard Time");
			fireTest(15, "UTC");
			fireTest(0, "UTC");
			fireTest(-15, "UTC");
			fireTest(-3 * 60, "Russian Standard Time");
			fireTest(-5 * 60 - 30, "India Standard Time");
			fireTest(-5 * 60 - 45.5, "UTC");
			alert("passed");
		}; */
}
