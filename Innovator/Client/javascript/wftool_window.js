﻿// (c) Copyright by Aras Corporation, 2004-2007.

function isTearOffMode() {
	if (window.isTearOff) {
		return window.isTearOff;
	}
	var isTearOff = true;
	var editor = (isTearOff ? window : work);
	if (editor.currWFNode === undefined) {
		editor = (!isTearOff ? window : work);
		if (editor.currWFNode !== undefined) {
			return !isTearOff;
		}
	}
	return isTearOff;
}

var menuFrame;

function getMenuFrame() {
	if (!menuFrame) {
		menuFrame = (isTearOffMode() ? tearOffMenuController : menu);
	}
	return menuFrame;
}

function getFrameWindow(frameId) {
	return document.getElementById(frameId).contentWindow;
}

function updateProvidedItemsGrid(itemsGrid, updatedItem) {
	var isTearOff = isTearOffMode();
	if (!updatedItem || !isTearOff) {
		return false;
	}
	var updatedID = updatedItem.getAttribute('id');

	if (itemsGrid.isItemsGrid) {
		if (itemsGrid.itemTypeName == itemTypeName) {
			var grid = itemsGrid.grid;
			var currentId = (grid.getRowIndex(itemID) > -1) ? itemID : updatedID;
			if (grid.getRowIndex(currentId) > -1) {
				var wasSelected = (grid.getSelectedItemIds().indexOf(currentId) > -1);

				if (updatedID != currentId) {
					itemsGrid.deleteRow(item);
				}
				itemsGrid.updateRow(updatedItem);

				if (wasSelected) {
					if (updatedID == currentId) {
						itemsGrid.onSelectItem(currentId);
					} else {
						var currSel = grid.getSelectedId();
						//if (currSel)
						itemsGrid.onSelectItem(currSel);
					}
				} //if (wasSelected)
			}
		}
	} //if (itemsGrid.isItemsGrid)
}

function deleteProvidedItemsGrid(itemsGrid) {
	if (itemsGrid.isItemsGrid && itemsGrid.itemTypeName === itemTypeName) {
		const grid = itemsGrid.grid;
		if (grid.getRowIndex(itemID) > -1) {
			grid.deleteRow(itemID);
		}
	}
}

window.aras = window.aras || window.parent.aras;
var updateItemsGrid = window.aras.decorateForMultipleGrids(updateProvidedItemsGrid);
var deleteRowFunc = window.aras.decorateForMultipleGrids(deleteProvidedItemsGrid);

function updateMenuState() {
	var isTearOff = isTearOffMode();
	if (isTearOff && window && window.refreshMenuState) {
		window.refreshMenuState();
	}
}

function onSaveAndCloseCommand() {
	return onSaveCommand();
}

function onSaveUnlockAndExitCommand() {
	var isTearOff = isTearOffMode();
	var editor = (isTearOff ? window : work);

	var currWFNode = editor.currWFNode;
	update2Edit(currWFNode);

	var res = null;
	currWFNode = editor.currWFNode;
	//replace all action="update" to "edit"
	var items = currWFNode.selectNodes('//Item[@action=\'update\']');
	for (var i = 0; i < items.length; i++) {
		if (items(i).getAttribute('type') != 'Workflow Map') {
			items(i).setAttribute('action', 'edit');
		}
	}

	var statusId;
	if (isTearOff) {
		statusId = aras.showStatusMessage('status', aras.getResource('', 'common.saving'), '../images/Progress.gif');
	}
	res = aras.saveItemEx(editor.currWFNode, false);
	if (isTearOff && statusId) {
		aras.clearStatusMessage(statusId);
	}
	if (!res) {
		return true;
	}

	if (isTearOff) {
		statusId = aras.showStatusMessage('status', aras.getResource('', 'common.unlocking'), '../images/Progress.gif');
	}
	res = aras.unlockItemEx(res);
	if (isTearOff && statusId) {
		aras.clearStatusMessage(statusId);
	}
	if (!res) {
		return true;
	}
	if (isTearOff) {
		updateItemsGrid(res);
		editor.currWFNode = null;
		window.close();
	}

	return true;
}

/* replace all action="update" to "edit" */
function update2Edit(currWFNode) {
	var items = currWFNode.selectNodes('//Item[@action=\'update\']');
	for (var i = 0; i < items.length; i++) {
		if (items[i].getAttribute('type') != 'Workflow Map') {
			items[i].setAttribute('action', 'edit');
		}
	}
}

function onSaveCommand() {
	var isTearOff = isTearOffMode();
	var editor = (isTearOff ? window : work);

	var currWFNode = editor.currWFNode;
	update2Edit(currWFNode);

	var statusId = aras.showStatusMessage('status', aras.getResource('', 'common.saving'), '../images/Progress.gif');
	var res = aras.saveItemEx(currWFNode);
	aras.clearStatusMessage(statusId);
	if (!res) {
		return true;
	}
	if (isTearOff) {
		updateItemsGrid(res);
	}
	unlockChildRelatedItems(res);

	window['system_res'] = res;
	window.setTimeout(function() {
		aras.uiReShowItemEx((isTearOff ? window : work).currWFNode.getAttribute('id'), window['system_res'], '');
	}, 0);

	return true;
}

function unlockChildRelatedItems(currWorkflow) {
	var workflowId = currWorkflow.getAttribute('id');
	var b = isEditMode;
	var q = new Item();
	q.loadAML(
		'<Item type=\'Workflow Map Activity\' related_expand=\'0\' select=\'related_id\' action=\'get\'>' +
		'<source_id>' + workflowId + '</source_id>' +
		'<related_id>' +
			'<Item type=\'Activity Template\'>' +
			'<locked_by_id condition=\'is not null\'/>' +
			'</Item>' +
		'</related_id>' +
		'</Item>');
	var r = q.apply();
	if (!r.isError()) {
		var n = r.getItemCount();
		for (var i = 0; i < n; i++) {
			var t = r.getItemByIndex(i);
			q.loadAML(
				'<Item type=\'Activity Template\' id=\'' + t.getProperty('related_id') + '\' ' +
				'action=\'unlock\' />');
			q.apply();
		}
	}
}

function updateRootItem(itemNd) {
	var isTearOff = isTearOffMode();
	var editor = (isTearOff ? window : work);
	editor.currWFID = itemNd.getAttribute('id');
	editor.setEditMode();
}

function updateRootItem2(itemNd) {
	var isTearOff = isTearOffMode();
	var editor = (isTearOff ? window : work);
	editor.currWFNode = itemNd;
}

function onUnlockCommand(silentMode) {
	var isTearOff = isTearOffMode();
	var editor = (isTearOff ? window : work);

	var currWFNode = editor.currWFNode;
	update2Edit(currWFNode);

	var res = aras.unlockItemEx(editor.currWFNode, silentMode);
	if (res) {
		var newID = res.getAttribute('id');
		editor.setViewMode(newID);
		if (isTearOff) {
			updateItemsGrid(res);
		}
	}

}

function onUndoCommand() {
	var editor = isTearOffMode() ? getFrameWindow('editor') : work;
	if (!aras.isDirtyEx(editor.currWFNode)) {
		aras.AlertError(aras.getResource('', 'common.nothing_undo'));
		return true;
	}

	if (!aras.confirm(aras.getResource('', 'common.undo_discard_changes'))) {
		return true;
	}
	unlockChildRelatedItems(editor.currWFNode);
	aras.removeFromCache(currWFID);
	editor.setEditMode();
	return true;
}

function onLockCommand() {
	var isTearOff = isTearOffMode();
	var editor = (isTearOff ? window : work);
	var res = aras.lockItemEx(editor.currWFNode);
	if (res) {
		editor.setEditMode();
		if (isTearOff) {
			updateItemsGrid(res);
		}
	}
}

function onEditCommand() {
	if (aras.isTempEx(item)) {
		return true;
	}

	var isLockedByUser = aras.isLockedByUser(item);

	if (!isLockedByUser && this.onLockCommand) {
		return this.onLockCommand();
	}

	return true;
}

function onPurgeCommand() {
	return onPurgeDeleteCommand('purge');
}

function onDeleteCommand(silentMode) {
	return onPurgeDeleteCommand('delete', silentMode);
}

function onPurgeDeleteCommand(command, silentMode) {
	var isTearOff = isTearOffMode();
	var editor = (isTearOff ? window : work);
	var commandResult = false;

	if (command == 'purge') {
		commandResult = aras.purgeItem('Workflow Map', editor.currWFID, false);
	} else {
		commandResult = aras.deleteItem('Workflow Map', editor.currWFID, silentMode);
	}

	if (commandResult) {
		if (window.isTearOff) {
			deleteRowFunc();
			editor.currWFNode = null;
			window.close();
		} else {
			var wfIT = aras.getItemFromServerByName('ItemType', 'Workflow Map', 'id');
			if (wfIT) {
				work.location.replace('itemsGrid.html?itemtypeID=' + wfIT.getID());
			} else {
				work.location.replace('../scripts/blank.html');
			}
		}
	}
	return {result: commandResult ? 'Deleted' : 'Canceled'};
}

function onPrintCommand() {
	//no any code since pdf printing approach implementation
	return true;
}

function onExport2OfficeCommand(targetAppType) {
	var editor = (isTearOff ? window : work);
	var itm = editor.currWFNode;
	if (itm) {
		aras.export2Office(function() { return ''; }, targetAppType, itm);
	}
}

function onShowAccess() {
}

function onShowWhereUsed() {
	var params = {
		aras: aras,
		dialogWidth: 600,
		dialogHeight: 400,
		resizable: true,
		content: 'whereUsed.html?id=' + itemID + '&type_name=' + itemTypeName + '&curLy=' +
			aras.getPreferenceItemProperty('Core_GlobalLayout', null, 'core_structure_layout') + '&toolbar=1&where=dialog'
	};
	var win = aras.getMostTopWindowWithAras(window);
	win.ArasModules.Dialog.show('iframe', params);
}

function onShowWorkflow() {
	var editmode = isEditMode ? 1 : 0;
	var relTypeID = aras.getItemFromServerByName('RelationshipType', 'Workflow', 'id').getID();
	var params = {
		LocationSearch: '?db=' + aras.getDatabase() + '&ITName=' + itemTypeName + '&itemID=' + itemID + '&relTypeID=' + relTypeID +
			'&editMode=' + editmode + '&tabbar=0&toolbar=1&where=dialog',
		aras: aras,
		item: item,
		dialogWidth: 750,
		dialogHeight: 500,
		resizable: true,
		content: 'relationships.html'
	};
	var win = aras.getMostTopWindowWithAras(window);
	win.ArasModules.Dialog.show('iframe', params);
}

function onShowLifeCycle() {
	var params = {
		aras: aras,
		title: aras.getResource('', 'lifecycledialog.lc', aras.getKeyedNameEx(item)),
		item: item,
		dialogWidth: 600,
		dialogHeight: 400,
		resizable: true,
		content: 'LifeCycleDialog.html'
	};

	var win = aras.getMostTopWindowWithAras(window);
	win.ArasModules.Dialog.show('iframe', params).promise.then(
		function(res) {
			if (typeof (res) == 'string' && res == 'null') {
				deleteRowFromItemsGrid(itemID);
				window.close();
				return;
			}
			if (!res) {
				return;
			}

			if (isTearOff) {
				updateItemsGrid(res);
			}

			isEditMode = false;
			aras.uiReShowItemEx(itemID, res, '');
		}
	);
}

var windowReady = true;
