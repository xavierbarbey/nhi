﻿/*
Used by MainGridFactory.js
*/

function BaseItemTypeGrid() {
	var topWindow = aras.getMostTopWindowWithAras(window);
	this.cui = topWindow.cui;
}

BaseItemTypeGrid.prototype.onInitialize = function BaseItemTypeGrid_onInitialize() {
	if (!this.cui) {
		var topWindow = aras.getMostTopWindowWithAras(window);
		this.cui = topWindow.cui;
	}

	currItemType = aras.getItemTypeDictionary(aras.getItemTypeName(itemTypeID));
	if (!currItemType || currItemType.isError()) {
		currItemType = null;
		return false;
	}

	currItemType = currItemType.node;
	varName_queryDate = 'IT_' + itemTypeID + '_queryDate';
	visiblePropNds = [];

	xml_ready_flag = false;

	if(aras.getLanguageDirection() === 'rtl') {
		document.documentElement.dir = 'rtl';
	}

	itemTypeName = aras.getItemProperty(currItemType, 'name');
	itemTypeLabel = aras.getItemProperty(currItemType, 'label');
	if (!itemTypeLabel) {
		itemTypeLabel = itemTypeName;
	}

	isVersionableIT = (aras.getItemProperty(currItemType, 'is_versionable') == '1');
	showReleaseEffectiveDateRows(isVersionableIT);
	use_src_accessIT = (aras.getItemProperty(currItemType, 'use_src_access') == '1');
	isManualyVersionableIT = (isVersionableIT && aras.getItemProperty(currItemType, 'manual_versioning') == '1');
	isRelationshipIT = (aras.getItemProperty(currItemType, 'is_relationship') == '1');
	can_addFlg = aras.getPermissions('can_add', itemTypeID);

	visiblePropNds = aras.getvisiblePropsForItemType(currItemType);
	aras.uiInitItemsGridSetups(currItemType, visiblePropNds);

	var gridSetups = aras.sGridsSetups[itemTypeName];
	if (!(gridSetups)) {
		return false;
	}

	gridSetups.query = aras.newQryItem(itemTypeName);
	currQryItem = gridSetups.query;
	currQryItem.setPage(1);
	currQryItem.removeAllCriterias();
	this.initMenuVariables();

	return true;
};

BaseItemTypeGrid.prototype.initMenuVariables = function BaseItemTypeGrid_initMenuVariables() {
	popupMenuState = {};
	this.popupMenuStateChanged = false;
};

BaseItemTypeGrid.prototype.onLockCommand = function BaseItemTypeGrid_onLockCommand(ignorePolymophicWarning, itemIds) {
	itemIds = itemIds || grid.getSelectedItemIds();

	if (!itemIds.length) {
		aras.AlertError(aras.getResource('', 'itemsgrid.select_item_type_first', itemTypeLabel));
		return false;
	}

	var realItemTypeNames = getRealItemTypeNames(itemIds),
		realItemTypeName, itemId, i;

	for (i = 0; i < itemIds.length; i++) {
		itemId = itemIds[i];

		if (!execInTearOffWin(itemId, 'lock')) {
			realItemTypeName = realItemTypeNames[itemId];

			if (realItemTypeName) {
				focus();

				if (aras.lockItem(itemId, realItemTypeName)) {
					var itemNode = aras.getItemById('', itemId, 0);

					if (itemNode) {
						if (updateRowSearchGrids(itemNode) === false) {
							return;
						}
					}
				}
			}
		}
	}

	onSelectItem(itemIds[0]);
};

BaseItemTypeGrid.prototype.updateItem = function BaseItemTypeGrid_updateItem(oldItem, newItem) {
	var qry = currQryItem.getResult();
	if (qry) {
		var type = oldItem.getAttribute('type');
		var typeId = oldItem.getAttribute('typeId');
		var id = oldItem.getAttribute('id');
		var typeCondition = '@type=\'' + type + '\'', i;

		var polyItems = aras.getPolymorphicsWhereUsedAsPolySource(typeId);
		if (polyItems.length > 0) {
			for (i = 0; i < polyItems.length; i++) {
				var polyItem = polyItems[i];
				typeCondition += ' or @type=\'' + polyItem.name + '\'';
			}
		}
		var nodes = qry.selectNodes('./Item/*[(' + typeCondition + ') and text()=\'' + id + '\']');

		for (i = 0; i < nodes.length; i++) {
			var node = nodes[i];
			var idNode = node.parentNode.getAttribute('id');
			var indexColumn = grid.getColumnIndex(node.nodeName + '_D');
			if (idNode && indexColumn > -1) {
				var cell = grid.cells(idNode, indexColumn);
				if (aras.getItemProperty(newItem, 'keyed_name') !== aras.getItemProperty(oldItem, 'keyed_name')) {
					node.setAttribute('keyed_name', aras.getItemProperty(newItem, 'keyed_name'));
					cell.setValue(aras.getItemProperty(newItem, 'keyed_name'));
				}

				if (oldItem.getAttribute('id') !== newItem.getAttribute('id')) {
					node.text = newItem.getAttribute('id');
					cell.setValue(aras.getItemProperty(newItem, 'keyed_name'));
					cell.setLink('\'' + type + '\',\'' + newItem.getAttribute('id') + '\'');
				}
			}
		}
	}
};

BaseItemTypeGrid.prototype.onUnlockCommand = function BaseItemTypeGrid_onUnlockCommand(ignorePolymophicWarning, itemIds) {
	itemIds = itemIds || grid.getSelectedItemIds();

	if (!itemIds.length) {
		aras.AlertError(aras.getResource('', 'itemsgrid.select_item_type_first', itemTypeLabel));
		return false;
	}

	var realItemTypeNames = getRealItemTypeNames(itemIds),
		unlockedItem, itemNode,
		realItemTypeName, itemId, i;
	var dlgPromise = Promise.resolve();
	var topWin = aras.getMostTopWindowWithAras(window);
	var dialogParams = {
		additionalButton: {
			text: aras.getResource('', 'common.discard'),
			actionName: 'discard'
		},
		okButtonText: aras.getResource('', 'common.save'),
		title: aras.getResource('', 'item_methods_ex.unsaved_changes')
	};
	var unlockItem = function(itemNode, saveChanges) {
		unlockedItem = aras.unlockItemEx(itemNode, saveChanges);

		if (!unlockedItem) {
			return;
		}
		if (itemNode && unlockedItem.getAttribute('id') != itemNode.getAttribute('id')) {
			deleteRowSearchGrids(itemNode);
		}
		itemNode = unlockedItem;
		if (itemNode) {
			updateRowSearchGrids(itemNode);
		}
	};
	var processItem = function(itemNode, realItemTypeName) {
		var isDirty = aras.isDirtyEx(itemNode);

		if (isDirty) {
			const dialogMessage = aras.getResource('', 'item_methods_ex.changes_not_saved');
			return topWin.ArasModules.Dialog.confirm(dialogMessage, dialogParams).then(function(res) {
				if (!res || res === 'cancel') {
					return;
				}
				unlockItem(itemNode, res === 'ok');
			});
		} else {
			unlockItem(itemNode, false);
		}
	};

	for (i = 0; i < itemIds.length; i++) {
		itemId = itemIds[i];

		if (!execInTearOffWin(itemId, 'unlock')) {
			realItemTypeName = realItemTypeNames[itemId];

			if (realItemTypeName) {
				itemNode = aras.getFromCache(itemId);
				focus();

				if (!itemNode) {
					itemNode = aras.unlockItem(itemId, realItemTypeName);
					if (itemNode && updateRowSearchGrids(itemNode) === false) {
						return;
					}
				} else {
					dlgPromise = dlgPromise.then(processItem.bind(this, itemNode, realItemTypeName));
				}
			}
		}
	}

	dlgPromise.then(function() {
		onSelectItem(itemIds[0]);
	});
};

BaseItemTypeGrid.prototype.isUnusedAction = function BaseItemTypeGrid_isUnusedAction(action) {
	return false;
};

BaseItemTypeGrid.prototype.showError = function BaseItemTypeGrid_showError(error) {
	if (error) {
		aras.AlertError(error);
	}
	return !!error;
};

BaseItemTypeGrid.prototype.onLink = function BaseItemTypeGrid_onLink(typeName, id, altMode) {
	var itemType = aras.getItemTypeDictionary(typeName, 'name');
	if (itemType && !itemType.isError() && aras.isPolymorphic(itemType.node)) {
		var item = aras.getItemById(typeName, id, 0);
		if (item.getAttribute('type') === typeName) {
			var polyTypeId = aras.getItemProperty(item, 'itemtype');
			typeName = aras.getItemTypeName(polyTypeId);
			aras.removeFromCache(item);
		}
	}
	aras.uiShowItem(typeName, id, null, altMode);
};

BaseItemTypeGrid.prototype.initCuiContextMenu = function() {
	const control = new ArasModules.ContextMenu(document.body);

	return window.cuiContextMenu(
		control,
		'PopupMenuItemGrid',
		{
			itemTypeId: itemTypeID,
			itemTypeName: itemTypeName,
			item_classification: '',
			eventState: {}
		})
		.then(function(contextMenu) {
			grid.contexMenu_Experimental = contextMenu;
			grid.contextMenuData = control.data;
		});
};

BaseItemTypeGrid.prototype.setContextMenuState = function() {
	grid.contextMenuData.forEach(function(item, key) {
		if (key in popupMenuState) {
			item.hidden = !popupMenuState[key];
		}
	});
};

BaseItemTypeGrid.prototype.onHeaderMenuClicked = function(commandId, rowsId, col) {
	switch (commandId) {
		case "hideCol":
			hideColumn(col);
			var columnSelectionBlock = document.getElementById('column_select_block');
			if (!columnSelectionBlock.classList.contains('hidden')) {
				var column = columnSelectionControl.columns[col];
				if (!column.hidden) {
					columnSelectionControl.toggleRowSelection(column.propertyId, true);
				}
			}
			break;
		case "insertCol":
			showColumn(col);
			break;
	}
};

// ================= Handlers for Actions AddItemsForChange (In Documents & Parts) =============================
function handlerForAddItemsForChange(cmdID) {
	var arasObj = aras;
	var itemIDs = grid.getSelectedItemIDs();

	var arr = cmdID.split(':');
	var act = arasObj.getItemFromServer('Action', arr[1], 'name,method(name,method_type,method_code),type,target,location,body,on_complete(name,method_type,method_code),item_query');
	if (!act) {
		return;
	}


	var countStartDefaultAction = 0;
	function startDefaultAction() {
		countStartDefaultAction++;
		if (countStartDefaultAction == itemIDs.length) {
			//Start Action for last select Document(or Part). And Action show form for all selected Documents(or Parts)
			arasObj.invokeAction(act.node, arr[2], itemIDs[itemIDs.length - 1]);
		}
	}

	for (var i = 0; i < itemIDs.length; i++) {
		var itemID = itemIDs[i];
		var tempItem =aras.itemsCache.getItemByXPath('//Item[@id=\'' + itemID + '\' and (@isDirty=\'1\' or @isTemp=\'1\')]');

		if (tempItem) {
			if (arasObj.confirm(arasObj.getResource('plm', 'changeitem.saveit'))) {
				arasObj.saveItemExAsync(tempItem).then(startDefaultAction);
			}
		} else {
			startDefaultAction();
		}
	}
}

//For Documents
window['onAction:83FB72FC3E4D42B8B51BCD7F4194E527:B88C14B99EF449828C5D926E39EE8B89Command'] = handlerForAddItemsForChange;
//For Parts
window['onAction:83FB72FC3E4D42B8B51BCD7F4194E527:4F1AC04A2B484F3ABA4E20DB63808A88Command'] = handlerForAddItemsForChange;
//For Cad
window['onAction:83FB72FC3E4D42B8B51BCD7F4194E527:CCF205347C814DD1AF056875E0A880ACCommand'] = handlerForAddItemsForChange;
// ================= Handlers for AddItemsForChange (In Documents & Parts) =============================
