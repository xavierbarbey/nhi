﻿/*
Used by MainGridFactory.js
*/

function InBasketTaskGrid() {
	InBasketTaskGrid.superclass.constructor();
	var properties = ['created_by_id', 'created_on', 'modified_by_id', 'modified_on', 'locked_by_id',
		'major_rev', 'release_date', 'effective_date', 'generation', 'state'];
	this.propertiesHelper = {
		properties: properties.map(function(propertyId) {
			return {
				id: propertyId,
				label: aras.getResource('', 'item_info_table.' + (propertyId !== 'locked_by_id' ? propertyId : 'claimed_by_id') + '_txt')
			};
		}),
		showHeader: true,
		showThumbnails: true
	};
}

inherit(InBasketTaskGrid, BaseItemTypeGrid);

InBasketTaskGrid.prototype.setMenuState = function InBasketTaskGridSetMenuState(rowId, col) {
	if (!currQryItem) {
		return;
	}
	var res = currQryItem.getResult();
	var itemNd = res.selectSingleNode('Item[@id="' + rowId + '"]');
	if (!itemNd) {
		itemNd = arasObj.getFromCache(rowId);
	}

	var brokenFlg = !Boolean(itemNd);
	itemTypeName = aras.getItemTypeName(aras.getItemProperty(itemNd, 'itemtype'));

	var prefix = 'com.aras.innovator.cui_default.pmig_';
	var keys = Object.keys(popupMenuState);
	var i;
	var itemIDs;
	var discoverOnlyFlg;
	var lockFlg;
	var unlockFlg;

	if (keys.length === 0) {
		popupMenuState[prefix + 'separator1'] = true;
		popupMenuState[prefix + 'ClaimTask'] = false;
		popupMenuState[prefix + 'UnclaimTask'] = false;
		popupMenuState[prefix + 'claim_separator'] = true;
		popupMenuState[prefix + 'separator3'] = true;

		// disabled actions
		popupMenuState[prefix + 'Save As'] = false;
		popupMenuState[prefix + 'View'] = false;
		popupMenuState[prefix + 'Purge'] = false;
		popupMenuState[prefix + 'Delete'] = false;
		popupMenuState[prefix + 'Version'] = false;
		popupMenuState[prefix + 'Revisions'] = false;
		popupMenuState[prefix + 'Promote'] = false;
		popupMenuState[prefix + 'Where Used'] = false;
		popupMenuState[prefix + 'Structure Browser'] = false;
		popupMenuState[prefix + 'Properties'] = false;
		popupMenuState[prefix + 'Add to Desktop'] = false;
	}

	if (!brokenFlg && currItemType) {
		itemIDs = grid.getSelectedItemIds();
		var lockedBy = arasObj.getItemProperty(itemNd, 'locked_by_id');
		var isTemp = arasObj.isTempEx(itemNd);

		discoverOnlyFlg = (itemNd && itemNd.getAttribute('discover_only') == '1');
		var editFlg = ((isTemp || lockedBy == userID) && !discoverOnlyFlg);
		lockFlg = arasObj.uiItemCanBeLockedByUser(itemNd, isRelationshipIT, window['use_src_accessIT']) &&
			(arasObj.getItemProperty(itemNd, 'my_assignment') === '1');
		if (itemTypeName === 'Workflow Task') {
			lockFlg = lockFlg && (arasObj.getItemProperty(itemNd, 'status').toLowerCase() === 'active');
		}
		unlockFlg = (lockedBy == userID || (!isTemp && lockedBy !== '' && arasObj.isAdminUser()));

		if (itemIDs.length > 1) {
			var idsArray = itemIDs.map(function(value) {
				return '@id=\'' + value + '\'';
			});

			var itemNds = res.selectNodes('Item[' + idsArray.join(' or ') + ']');
			var currItemTypeName;
			for (i = 0; i < itemNds.length; i++) {
				itemNd = itemNds[i];
				itemID = arasObj.getItemProperty(itemNd, 'id');
				if (!itemNd) {
					brokenFlg = true;
					break;
				}

				currItemTypeName = itemTypeName;
				itemTypeName = aras.getItemTypeName(aras.getItemProperty(itemNd, 'itemtype'));

				ItemIsLocked = arasObj.isLocked(itemNd);

				lockedBy = arasObj.getItemProperty(itemNd, 'locked_by_id');
				isTemp = arasObj.isTempEx(itemNd);
				isDirty = arasObj.isDirtyEx(itemNd);

				editFlg = editFlg && (isTemp || (lockedBy == userID));
				lockFlg = lockFlg && arasObj.uiItemCanBeLockedByUser(itemNd, isRelationshipIT, window['use_src_accessIT']) &&
					(arasObj.getItemProperty(itemNd, 'my_assignment') === '1') && !isFunctionDisabled(itemTypeName, 'Lock');
				if (itemTypeName === 'Workflow Task') {
					lockFlg = lockFlg && (arasObj.getItemProperty(itemNd, 'status').toLowerCase() === 'active');
				}

				unlockFlg = unlockFlg && (lockedBy == userID || (!isTemp && lockedBy !== '' && arasObj.isAdminUser())) && !isFunctionDisabled(itemTypeName, 'Unlock');
			}
			itemTypeName = currItemTypeName;
		}
	}

	if (!brokenFlg && currQryItem) {
		var newPopupMenuState = [];
		newPopupMenuState[prefix + 'ClaimTask'] = lockFlg && !isFunctionDisabled(itemTypeName, 'Lock');
		newPopupMenuState[prefix + 'UnclaimTask'] = unlockFlg && !isFunctionDisabled(itemTypeName, 'Unlock');
		newPopupMenuState[prefix + 'claim_separator'] = popupMenuState[prefix + 'ClaimTask'] || popupMenuState[prefix + 'UnclaimTask'];
		newPopupMenuState[prefix + 'Version'] = false;
		newPopupMenuState[prefix + 'Revisions'] = false;
		newPopupMenuState[prefix + 'Where Used'] = false;
		newPopupMenuState[prefix + 'Structure Browser'] = false;
		newPopupMenuState[prefix + 'Properties'] = false;
		newPopupMenuState[prefix + 'Add to Desktop'] = false;

		keys = Object.keys(newPopupMenuState);
		for (i = 0; i < keys.length; i++) {
			var key = keys[i];
			var value = newPopupMenuState[key];
			if (popupMenuState[key] !== value) {
				popupMenuStateChanged = true;
				popupMenuState[key] = value;
			}
		}
	}

	itemTypeName = 'InBasket Task';
};

InBasketTaskGrid.prototype.openCompletionDialog = function InBasketTaskGridOpenCompletionDialog(itemId, itemTypeName) {
	//purpose: to delay modal dialog opening when the dialog has controls.
	var showModalDialogWithDelay = function(url, paramsObj, windowOptions, doRepopulateAfterDialog) {
		setTimeout(function() {
			windowOptions.resizable = true;
			windowOptions.scroll = true;
			windowOptions.status = false;
			windowOptions.help = false;
			paramsObj.content = url;
			paramsObj.isPopup = true;
			var dialog = window.parent.ArasModules.Dialog.show('iframe', Object.assign({}, paramsObj, windowOptions));
			dialog.promise.then(function(data) {
				if (data && data.updated) {
					const itemsGridArray = topWnd.arasTabs.getSearchGridTabs(itemTypeID);
					itemsGridArray.forEach(function(itemsGrid) {
						itemsGrid.doSearch();
					});
				}
			});
		}, 10);
	};

	function getItemFromServer(amlQuery) {
		var tmpRes = arasObj.soapSend('ApplyItem', amlQuery);
		if (tmpRes.getFaultCode() !== 0) {
			arasObj.AlertError(tmpRes);
			return null;
		}
		return tmpRes.getResult().selectSingleNode('Item');
	}

	var item = new Item(itemTypeName, 'get');
	item.setID(itemId);
	item = item.apply();
	var itemNd = item.node;
	var lockedById = arasObj.getItemProperty(itemNd, 'locked_by_id');
	var error;
	var wndOptions;
	var params;

	if (lockedById !== '' && lockedById !== userID) {
		error = 'itemsgrid.unable_complete_task_claimed_by_someone_else';
	} else {
		if (itemNd.getAttribute('type') === 'Workflow Task') {
			params = {
				aras: arasObj,
				activity: getItemFromServer(
					'<Item type=\'Activity\' action=\'get\'>' +
						'<Relationships>' +
							'<Item type=\'Activity Assignment\' action=\'get\'><id>' + itemNd.getAttribute('id') + '</id></Item>' +
							'<Item type=\'Activity Task\' action=\'get\'/>' +
							'<Item type=\'Activity Variable\' action=\'get\'/>' +
							'<Item type=\'Workflow Process Path\' action=\'get\'/>' +
						'</Relationships>' +
					'</Item>'),
				wflName: arasObj.getItemPropertyAttribute(itemNd, 'container', 'keyed_name'),
				wflId: arasObj.getItemProperty(itemNd, 'container'),
				assignmentId: itemNd.getAttribute('id'),
				itemId: arasObj.getItemProperty(itemNd, 'item')
			};
			wndOptions = {dialogHeight:  550 , dialogWidth: 700};

			showModalDialogWithDelay('InBasket/InBasket-VoteDialog.aspx', params, wndOptions);
		} else if (itemNd.getAttribute('type') === 'Project Task') {
			var acwFormNd = arasObj.getFormForDisplay('Activity Completion Worksheet', 'by-name', false);
			var formWidth;
			var formHeight;
			if (acwFormNd && acwFormNd.node) {
				formWidth = parseInt(aras.getItemProperty(acwFormNd.node, 'width'));
				formHeight = parseInt(aras.getItemProperty(acwFormNd.node, 'height'));
			}
			var tmpRes = getItemFromServer(
				'<Item type=\'Activity2\' action=\'get\' select=\'id\'>' +
					'<OR>' +
						'<id>' + itemNd.getAttribute('id') + '</id>' +
						'<id condition=\'in\'>SELECT source_id FROM Activity2_Assignment WHERE id=\'' + itemNd.getAttribute('id') + '\'</id>' +
					'</OR>' +
				'</Item>');
			if (tmpRes) {
				wndOptions = {dialogWidth: formWidth || 700, dialogHeight:  800};

				window.focus();
				showModalDialogWithDelay('../Solutions/Project/scripts/ActivityCompletionWorksheet/ACWDialog.html', [window, tmpRes.getAttribute('id')], wndOptions);
			}

		} else if (itemNd.getAttribute('type') === 'FMEA Task') {
			var fmeaAction = getItemFromServer('<Item type=\'FMEA Action\' action=\'get\' select=\'milestone_comment,milestone_name,' +
				'milestone_due_date\' id=\'' + itemNd.getAttribute('id') + '\'></Item>');
			params = {
				aras: arasObj,
				itemId: itemNd.getAttribute('id'),
				itemName: arasObj.getItemProperty(fmeaAction, 'milestone_name'),
				itemComments: arasObj.getItemProperty(fmeaAction, 'milestone_comment'),
				itemParent: arasObj.getItemPropertyAttribute(itemNd, 'container', 'keyed_name'),
				itemParentId: arasObj.getItemProperty(itemNd, 'container'),
				itemParentType: arasObj.getItemPropertyAttribute(itemNd, 'container', 'type'),
				itemDueDate: arasObj.getItemProperty(fmeaAction, 'milestone_due_date'),
				'part_number': arasObj.getItemPropertyAttribute(itemNd, 'item', 'keyed_name'),
				lockedById: arasObj.getItemProperty(itemNd, 'locked_by_id')
			};
			wndOptions = (params.itemParentType === 'Process Planner') ? {dialogHeight: 695, dialogWidth: 700} : {dialogHeight: 510, dialogWidth: 700};
			showModalDialogWithDelay(arasObj.getScriptsURL() + '../Solutions/QP/scripts/MyTasksCompletionDialog.html', params, wndOptions, true);
		} else {
			var formTypeCompleteValue = 'complete';
			var formId = arasObj.uiGetFormID4ItemEx(itemNd, formTypeCompleteValue);

			if (!formId) {
				arasObj.AlertError(arasObj.getResource('', 'ui_methods_ex.form_not_specified_for_you', formTypeCompleteValue));
				return null;
			}

			var formNd = arasObj.getFormForDisplay(formId);

			formNd = formNd ? formNd.node : null;

			var param = {
				aras: arasObj,
				title: arasObj.getResource('', 'itemsgrid.completion_dialog'),
				item: item,
				formId: formId
			};

			var width = aras.getItemProperty(formNd, 'width') || 400;
			var height = aras.getItemProperty(formNd, 'height') || 300;
			showModalDialogWithDelay('ShowFormAsADialog.html', param, {dialogHeight: height, dialogWidth: width});
		}
	}
	return error ? arasObj.getResource(null, error) : null;
};

InBasketTaskGrid.prototype.onEditCommand = function InBasketTaskGridOnEditCommand(itemId) {
	return !this.showError(this.openCompletionDialog(itemId, itemTypeName));
};

InBasketTaskGrid.prototype.onDoubleClick = function InBasketTaskGridOnDoubleClick(itemId) {
	return !this.showError(this.openCompletionDialog(itemId, itemTypeName));
};

InBasketTaskGrid.prototype.onLink = function InBasketTaskGridOnLink(typeName, id) {
	switch (typeName) {
		case 'Workflow Process':
			var item = arasObj.getItemById(typeName, id, 0);
			var params = {};
			params.aras = arasObj;
			params.processID = id;
			params.processName = arasObj.getItemProperty(item, 'name');
			params.dialogWidth = 850;
			params.dialogHeight = 470;
			params.content = 'WorkflowProcess/WflProcessViewer.aspx';

			var win = arasObj.getMostTopWindowWithAras(window);
			(win.main || win).ArasModules.Dialog.show('iframe', params);
			break;
		case 'InBasket Task':
			this.showError(this.openComplitionDialog(id, typeName));
			break;
		default:
			InBasketTaskGrid.superclass.onLink(typeName, id);
	}
};

InBasketTaskGrid.prototype.onMenuClicked = function InBasketTaskGridOnMenuClicked(m, rowId, col) {
	switch (m) {
		case 'locked_criteria:clear':
			grid.setCellValue('input_row', 0, '<img src=\'\'>');
			break;
		case 'locked_criteria:by_me':
			grid.setCellValue('input_row', 0, '<img src=\'../images/ClaimedByMe.svg\'>');
			break;
		case 'locked_criteria:by_others':
			grid.setCellValue('input_row', 0, '<img src=\'../images/ClaimedByOthers.svg\'>');
			break;
		case 'locked_criteria:by_anyone':
			grid.setCellValue('input_row', 0, '<img src=\'../images/ClaimedByAnyone.svg\'>');
			break;
		default:
			break;
	}

	return m;
};

window['onAction:1020CBF7E25E4479A260FA5AF4E4A378:BC7977377FFF40D59FF14205914E9C71Command'] = function() {
	var itemId = grid.getSelectedId();
	var item = new Item(itemTypeName, 'get');
	item.setAttribute('select', 'config_id, container');
	item.setID(itemId);
	item = item.apply();

	var idActivity = item.getProperty('config_id');
	var projectId = item.getProperty('container');

	var projectNumber;
	var wbsId;
	var wbs;

	var project = aras.getItem('Project', '@id=\'' + projectId + '\'', '<id>' + projectId + '</id>');
	if (project) {
		wbsId = aras.getItemProperty(project, 'wbs_id');
		projectNumber = aras.getItemProperty(project, 'project_number');
	}

	eval(aras.getFileText(aras.getBaseURL() + '/Solutions/Project/javascript/gantt_methods.js'));
	eval(aras.getFileText(aras.getBaseURL() + '/Solutions/Project/javascript/scheduling_methods.js'));

	function showGanttChart(wbsId, projectNumber) {
		wbs = getWBS(wbsId);
		initActNums();
		sortItems(wbs.documentElement);
		showGanttInternal(wbs, projectNumber);
	}

	function getWBS(wbsId) {
		var xml = aras.createXMLDocument();
		var query = aras.createXMLDocument();

		query.load(aras.getI18NXMLResource('query.xml', aras.getScriptsURL() + '../Solutions/Project/'));
		query.selectSingleNode('//*[@repeatTimes]').setAttribute('repeatTimes', -1);
		query.documentElement.setAttribute('id', wbsId);
		var result = aras.applyItem(query.documentElement.xml);

		xml.loadXML(result);
		return xml;
	}

	showGanttChart(wbsId, projectNumber);
};
