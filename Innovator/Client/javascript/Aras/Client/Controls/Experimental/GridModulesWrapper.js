﻿/*jslint nomen: true */
/*global define*/
define([
	'dojo/_base/config',
	'dojo/date/stamp',
	'dojo/store/Memory',
	'dojo/dom-class',
	'dojo/_base/event',
	'dojo/_base/connect',
	'dojo/aspect',
	'./ContextMenu',
	'dijit/popup'
],
function(config, DateIso, Memory, domClass, event, connect, aspect, ContextMenu, popup) {
	'use strict';
	return {
		uniqueIdGenerator: {_uniqueIDNo: 0, get: function() { return (++this._uniqueIDNo).toString(); }},

		initTextDirection: function(gridDomNode, direction) {
			gridDomNode.setAttribute('class', 'languageDirection_' + direction + ' ' + gridDomNode.getAttribute('class'));
		},

		getXML: function(grid, useValues, withSubRows) {
			var getGridAttributesXML = function() {
				var xml = '';
				if (grid.isEditable()) {
					xml += 'editable=\'true\' ';
				}
				return xml;
			};

			var getHeaderRowAttributesXML = function(style) {
				var xml = '';
				for (var i = 0; i < style.length; i++) {
					var st = style[i].split(':');
					//TODO: it seems that such style of seting fonts should be replaced setting only one field font - but perhaps our, e.g., xsl uses them
					if (st[0] == 'font-family') {
						xml += ' font-family=\'' + st[1] + '\'';
					}
					if (style['font-size']) {
						xml += ' font-size=\'' + st[1] + '\'';
					}
					if (st[0] == 'font-style') {
						xml += ' font-style=\'' + st[1] + '\'';
					}
					if (st[0] == 'font-weight') {
						xml += ' font-weight=\'' + st[1] + '\'';
					}
					if (st[0] == 'background-color') {
						xml += ' bgColor=\'' + st[1] + '\'';
					}
					if (st[0] == 'color') {
						xml += ' textColor=\'' + st[1] + '\'';
					}
					if (st[0] == 'text-align') {
						xml += ' align=\'' + st[1].substr(0, 1) + '\'';
					}
				}
				return xml;
			};

			var getColumnAttributesXML = function(column) {
				let xml = '';
				xml += ' width=\'' + parseInt(column.width) + '\'';
				//TODO: here mistake: edit can be like FilterComboBox, null, but should be EDIT, COMBO:0...
				xml += ' edit=\'' + column.editableType + '\'';
				if (!column.sort) {
					xml += ' sort=\'NOSORT\'';
				} else if ('NUMERIC' === column.sort) {
					xml += ' sort=\'NUMERIC\'';
				} else if ('DATE' === column.sort) {
					xml += ' sort=\'DATE\'';
				}
				if (column.locale) {
					xml += ' locale=\'' + column.locale + '\'';
				}
				const columnOrder = grid._grid.settings.indexHead.indexOf(column.field);
				xml += ' order=\'' + (columnOrder < 0 ? column.layoutIndex : columnOrder) + '\'';
				if (column.bginvert) {
					xml += ' bgInvert=\'' + column.bginvert + '\'';
				}
				if (column.hidden) {
					xml += ' resizable="' + !column.hidden + '"';
				}
				if (column.columnCssStyles) {
					const textAlign = column.columnCssStyles['text-align'];
					//do not use st[1].substr(0, 1) instead of st[1], because, e.g., exportToExcel require full names
					xml += ' align=\'' + textAlign + '\'';
				}

				return xml;
			};

			var getGridHeadXML = function() {
				let headXml = '<thead>';
				let columnXml = '<columns>';
				const headMap = grid._grid.head;

				grid._grid.settings.indexHead.forEach(function(columnName) {
					const cell = headMap.get(columnName);
					headXml += '<th align=\'c\'>';
					headXml += (' ' === cell.label) ? '' : cell.label;
					headXml += '</th>';

					columnXml += '<column' + getColumnAttributesXML(cell) + ' />';
				});
				headXml += '</thead>';
				columnXml += '</columns>';
				return headXml + columnXml;
			};

			var getRowAttributesXML = function(rowId) {
				var xml = '';
				xml += ' id=\'' + rowId + '\'';
				if (rowId) {
					xml += ' action=\'' + rowId + '\'';
				}
				if (grid._grid.settings.selectedRows.includes(rowId)) {
					xml += ' selected=\'true\'';
				}
				if (grid.rowHeight) {
					xml += ' height=\'' + grid.rowHeight + '\'';
				}
				return xml;
			};

			var getCellAttributesXML = function(cell) {
				var xml = '',
					font = cell.getFont(),
					bgColor = cell.getBgColor(),
					textColor = cell.getTextColor();

				if (font) {
					xml += ' font="' + font + '"';
				}
				if (bgColor) {
					xml += ' bgColor="' + bgColor.replace(/!important/, '') + '"';
				}
				if (textColor) {
					xml += ' textColor="' + textColor + '"';
				}

				return xml;
			};

			var encodeHTML = function(html) {
				if (typeof (html) == 'string') {
					return html.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;').replace(/'/g, '&apos;');
				}
				return html;
			};

			var getCellXML = function(cell) {
				//TODO: implement useValues - e.g., it can be usefull to select choose values or labels from MVLisp, but perhaps to remove, complecated logic for each type of field, for simple text example it gives the same results
				var xml = '<td' + getCellAttributesXML(cell) + '>' + encodeHTML(cell.getValue()) + '</td>';
				return xml;
			};

			var getRowXml = function(rowId) {
				var xml = '<tr' + getRowAttributesXML(rowId) + '>',
					i, cell,
					colsCount = grid.GetColumnCount();

				for (i = 0; i < colsCount; i++) {
					cell = grid.cells_Experimental(rowId, i, true);
					xml += getCellXML(cell);
				}

				//TODO: add logic of userdata

				//TODO: add logic of withSubRows, blocked by implementation of getChildItemsId
				//if (withSubRows && this.getChildItemsId) {
				//}

				xml += '</tr>';
				return xml;
			};

			var gridXml = '<table ',
				rows = [],
				i;

			//TODO: code below is partial copy of getVisibleItemIDs() to fill rows array, but avoid excessive join-split logic.
			//Should be reviewed after introducing the new grid intead of dojo grid.
			for (i = 0; i < grid.getRowCount() ; i++) {
				rows.push(grid.getRowId(i));
			}

			var rowsCount = rows.length;

			gridXml += getGridAttributesXML() + '>';
			gridXml += getGridHeadXML();
			for (i = 0; i < rowsCount; i++) {
				gridXml += getRowXml(rows[i]);
			}
			gridXml += '</table>';
			return gridXml;
		},

		inputRow: function(grid) {
			var setters = {
				value: function(index, value) {
					var field = ('string' === typeof index) ? index : this.grid.grid_Experimental.order[index];
					if (grid._grid.head.get(field, 'searchType') === 'multiValueList') {
						value = (value.length > 0) ? value.split(',') : [];
					}
					this.grid._grid.head.set(field, value, 'searchValue');
				},
				disabled: function(index, value) {
					const field = ('string' === typeof index) ? index : this.grid.grid_Experimental.order[index];
					const fieldObject = this.grid._grid.head.get(field);
					const defaultSearchType = fieldObject._defaultSearchType;
					let searchType = fieldObject.searchType;
					if (value) {
						if (!defaultSearchType) {
							this.grid._grid.head.set(field, fieldObject.searchType, '_defaultSearchType');
						}
						searchType = 'disabled';
					} else if (defaultSearchType) {
						searchType = defaultSearchType;
					}
					this.grid._grid.head.set(field, searchType, 'searchType');
				},
				comboList: function(index, values, labels) {
					var field = ('string' === typeof index) ? index : this.grid.nameColumns[index],
						obj = this.grid.inputRowCollections[field],
						data = [],
						i;

					if ('dijit.form.FilteringSelect' === obj.declaredClass) {
						labels = labels || values;
						for (i = 0; i < values.length; i += 1) {
							data.push({name: values[i], id: labels[i]});
						}
						obj.set('store', new Memory({data: data}));
					}
				},
				visible: function(value) {
					this.grid.showInputRow_Experimental(value);
				}
			},
				getters = {
					value: function(index) {
						var field = ('string' === typeof index) ? index : this.grid.grid_Experimental.order[index];
						var value = grid._grid.head.get(field, 'searchValue');
						if (Array.isArray(value)) {
							value = value.join(',');
						}
						return value;
					},
					disabled: function(index) {
						var field = ('string' === typeof index) ? index : this.grid.grid_Experimental.order[index];
						return (this.grid._grid.head.get(field, 'disabled') === true);
					},
					visible: function() {
						return this.grid.isInputRowVisible_Experimental();
					}
				};

			return {
				grid: grid,

				set: function(index, key, value) {
					if (2 === arguments.length && getters[index]) {
						setters[index].call(this, key);
						return;
					}
					var field = ('string' === typeof index) ? index : this.grid.grid_Experimental.order[index],
						args;

					if (setters[key] && field) {
						args = Array.prototype.slice.call(arguments, 2);
						args.splice(0, 0, index);
						setters[key].apply(this, args);
					} else {
						throw 'Grid message: can\'t call setter ' + key + ' in inputRow with value ' + value + ', column ' + index;
					}
				},

				get: function(index, key) {
					if (1 === arguments.length && getters[index]) {
						return getters[index].call(this);
					}

					var field = ('string' === typeof index) ? index : this.grid.grid_Experimental.order[index];

					if (getters[key] && field) {
						return getters[key].call(this, field);
					}
					throw 'Grid message: can\'t call getter ' + key + ' in inputRow, column ' + index;
				}
			};
		},

		items: function(grid) {// grid = gridPublicContainer or TreeGridPublicContainer
			var store = function() {
				return grid._store || grid.grid_Experimental.store;
			},
				setters = {
					value: function(item, field, value) {
						store().setValue(item, field, value);
					}
				},
				getters = {
					value: function(item, field) {
						return store().getValue(item, field);
					},
					id: function(item) {
						return store().getIdentity(item);
					}
				};

			return {
				grid: grid.grid_Experimental,

				onNew: function(id) {
					//this is event call when add new row in grid
				},

				isDirty: function(id) {
					var item = store()._getItemByIdentity(id);
					if (item > -1) {
						return store().isDirty(item);
					}
				},

				add: function(item) {
					if (this.grid.edit.isEditing()) {
						this.grid.edit.apply();
					}
					this.grid.beginUpdate();
					store().newItem(item);
					store().save();
					this.grid.endUpdate();
				},

				remove: function(id) {
					var item = store()._getItemByIdentity(id);
					if (item) {
						store().deleteItem(item);
					}
				},

				getAllId: function() {
					var result = [],
						onComplete = function(items) {
							var i, length = items.length;
							for (i = 0; i < length; i += 1) {
								result.push(store().getIdentity(items[i]));
							}
						};
					onComplete.bind(this);
					store().fetch({onComplete: onComplete});
					return result;
				},

				is: function(id) {
					return this.grid.parentContainer._grid.rows.has(id);
				},

				set: function(id, key, value) {
					var item = store()._getItemByIdentity(id),
						args;
					if (setters[key] && item) {
						args = Array.prototype.slice.call(arguments, 2);
						args.splice(0, 0, item);
						setters[key].apply(this, args);
					}
				},

				get: function(id, key, field) {
					var item = null;
					if ('id' === key) {
						item = this.grid.getItem(id);
					} else {
						item = store()._getItemByIdentity(id);
					}
					if (getters[key] && item) {
						return getters[key].call(this, item, field);
					}
				}
			};
		},

		columns: function(grid) { // grid = arasDataGrid or arasTreeGrid
			var setters = {
				editable: function(index, value) {
					this.grid.getCell(index).editable = value;
				},
				editType: function(index, type) {
					this.grid.getCell(index).editableType = type;
				},
				comboList: function(index, list, lables) {
					this.grid.getCell(index).options = list;
					this.grid.getCell(index).optionsLables = lables;
				},
				formatter: function(index, func) {
					this.grid.getCell(index).formatter = func;
				}
			},
				getters = {
					editable: function(index) {
						return this.grid.getCell(index).editable;
					},
					index: function(index) {
						return index;
					},
					name: function(index) {
						return this.grid.nameColumns[index];
					},
					count: function() {
						return this.grid.order.length;
					},
					width: function(index) {
						var cell = this.grid.getCell(index);
						return cell.unitWidth || cell.width;
					}
				};

			return {
				grid: grid,

				set: function(field, key, value) {
					var index = ('string' === typeof field) ? this.grid.nameColumns.indexOf(field) : this.grid.order[field],
						args;
					if (setters[key] && index > -1) {
						args = Array.prototype.slice.call(arguments, 2);
						args.splice(0, 0, index);
						setters[key].apply(this, args);
					}
				},

				get: function(field, key) {
					if (1 === arguments.length && getters[field]) {
						return getters[field].call(this);
					}

					const index = ('string' === typeof field) ? this.grid.order.indexOf(field) : field;
					if (getters[key] && index > -1) {
						return getters[key].call(this, index);
					}
				}
			};
		},

		edit: function(grid) { // grid = arasDataGrid or arasTreeGrid

			return {
				grid: grid,

				setErrorMessage: function(message) {
					if (this.grid.edit.isEditing()) {
						var editorWidget = this.grid.edit.info.cell.widget;

						if (editorWidget) {
							editorWidget.invalidMessage = message;
							editorWidget.focus();
							return true;
						}
					}
					return false;
				},

				set: function(id, field) {
					var columnIndex = ('string' === typeof field) ? this.grid.nameColumns.indexOf(field) : this.grid.order[field],
						cell = this.grid.getCell(columnIndex),
						item = this.grid.store._getItemByIdentity(id),
						index = item ? this.grid.getItemIndex(item) : -1;
					this.grid.focus.setFocusCell(cell, index);
					this.grid.edit.setEditCell(cell, index);
				}
			};
		},

		focus: function(grid) { // grid = arasDataGrid or arasTreeGrid

			return {
				grid: grid,

				isLastCell: function() {
					return this.grid.focus.isLastFocusCell();
				},

				isFirstCell: function() {
					return this.grid.focus.isFirstFocusCell();
				}
			};
		},

		selection: function(grid) { // grid = arasDataGrid or arasTreeGrid

			var setters = {
				multi: function(value) {
					this.grid._grid.view.defaultSettings.multiSelect = value;
				},
				none: function(value) {
					this.grid._grid.view.defaultSettings.multiSelect = false;
				}
			},
				getters = {
					id: function() {
						return this.grid._grid.settings.selectedRows[0] || '';
					},
					index: function() {
						return this.grid._grid.settings.indexRows.indexOf(this._grid.settings.selectedRows[0] || '');
					},
					ids: function() {
						return this.grid._grid.settings.selectedRows.slice();
					}
				};

			return {
				grid: grid,

				clear: function() {
					this.grid._grid.settings.selectedRows = [];
				},

				set: function(key, value) {
					if (setters[key]) {
						var args = Array.prototype.slice.call(arguments, 1);
						setters[key].apply(this, args);
					} else {
						throw 'Grid message: can\'t call getter ' + key + ' in Selection';
					}
				},

				get: function(key) {
					if (getters[key]) {
						return getters[key].call(this);
					}
					throw 'Grid message: can\'t call getter ' + key + ' in Selection';
				}
			};
		},

		events: {
			onFocusSearchCell: function(e) {
				if (this._oldSearchHeadId) {
					var field = this._oldSearchHeadId;
					this._oldSearchHeadId = null;
					this.onApplyEdit_Experimental('input_row', field);
				}
				if (e.detail && e.detail.indexRow === 'searchRow') {
					this._oldSearchHeadId = this._grid.settings.focusedCell.headId;
				}
			},
			onFocusBodyCell: function(e) {
				const rowId = e.detail && this._grid.settings.focusedCell.rowId;
				if (!rowId || rowId === 'searchRow') {
					return;
				}
				const cell = this.cells(rowId, e.detail.indexHead);
				this.gridSelectCell(cell);	
			},
			onRowDblClick: function(headId, rowId, e) {
				this.gridDoubleClick(rowId, this._grid.settings.indexHead.indexOf(headId), e.ctrlKey || e.metaKey);
			},

			onSelected: function(event) {
				var rowIndex = event.detail.index;
				var rowId = this._grid.settings.indexRows[rowIndex];
				this.gridRowSelect(rowId, this._grid.view.defaultSettings.multiSelect);
			},

			onMoveColumn: function() {
				var cells = this.grid_Experimental.layout.cells,
					order = this.grid_Experimental.order,
					nameColumns = this.grid_Experimental.nameColumns,
					inputs = this.grid_Experimental.inputRowCollections,
					cell,
					i;
				for (i = 0; i < cells.length; i++) {
					cell = cells[i];
					order[cell.layoutIndex] = i;
					nameColumns[i] = cell.field;
					inputs[cell.field].index = cells[cell.layoutIndex].layoutIndex;
				}
				this.grid_Experimental.resize();
			},

			onRowContextMenu: function(headId, rowId, e) {
				if (this.grid_Experimental.edit.isEditing()) {
					this.grid_Experimental.edit.apply();
				}
				var columnIndex = e.target.cellIndex;
				this.contexMenu_Experimental.rowId = rowId;
				this.contexMenu_Experimental.columnIndex = columnIndex;
				this.gridMenuInit(rowId, columnIndex);
				const itemTypeNode = aras.getItemTypeForClient(itemTypeName).node;
				this.getMenu().show({
					x: e.clientX,
					y: e.clientY
				}, {
					rowId: rowId,
					selectedRowsIds: this.getSelectedItemIds(),
					favorites: aras.getMainWindow().favorites,
					isPolymorphic: aras.isPolymorphic(itemTypeNode)
				});
				e.preventDefault();
			},

			onGrigAreaContextMenu: function(event) {
				const targetElement = event.target;
				if (targetElement.cellIndex !== undefined || targetElement.closest('.aras-grid-row-cell') ||
					targetElement.closest('.aras-grid-head-cell') || targetElement.closest('.aras-grid-search-row-cell')) {
					return;
				}
				window.popupMenuState = {};
				window.setMenuState && window.setMenuState();
				if (this._grid.settings.selectedRows.length) {
					this._grid.settings.selectedRows = [];
					this._grid.render();
				}
				this.gridMenuInit();
				const itemTypeNode = aras.getItemTypeForClient(window.itemTypeName).node;
				this.getMenu().show({
					x: event.clientX,
					y: event.clientY
				}, {
					rowId: null,
					selectedRowsIds: [],
					favorites: aras.getMainWindow().favorites,
					isPolymorphic: aras.isPolymorphic(itemTypeNode)
				});
				event.preventDefault();
			},

			onHeaderEvent: function(cellName, e) {
				if (e.type == 'contextmenu') {
					this.grid_Experimental.headerMenu = this.headerContexMenu_Experimental.menu;

					var rowId = 'header_row';
					e.rowIndex = '-1';
					this.headerContexMenu_Experimental.rowId = rowId;
					var columnIndex = this.grid_Experimental.order.indexOf(cellName);

					this.grid_Experimental.onHeaderCellContextMenu(e);
					this.headerContexMenu_Experimental.columnIndex = columnIndex;

					this.headerContexMenu_Experimental.show({
						x: e.clientX,
						y: e.clientY
					});
					e.preventDefault();
				}
			},

			onRowClick: function(headId, rowId, event) {
				// Ctrl+click/shift remove row from selection and toolbar didn't updated
				if (this._grid.settings.selectedRows.indexOf(rowId) > -1) {
					this.gridClick(rowId, this.GetColumnIndex(headId));
				}
			},

			gridLinkClick: function(headId, rowId, event) {
				let rowInfo = this._grid.rows._store.get(rowId);
				const headInfo = this._grid.head._store.get(headId);
				if (headInfo.linkProperty) {
					rowInfo = this._grid.rows._store.get(rowInfo[headInfo.linkProperty]) || {};
				}
				const propertyName = headInfo.name || headId;
				let action = rowInfo.id || rowInfo.uniqueId;
				let link = '';
				const currentProperty = rowInfo[propertyName];
				if (currentProperty && rowInfo[propertyName + '@aras.discover_only'] !== '1') {
					const dataSourceName = headInfo.dataSourceName;
					const isFile = dataSourceName === 'File';

					if (['list', 'filter list', 'color list'].includes(headInfo.dataType)) {
						link = '\'List\',';
					} else if (headInfo.dataType === 'sequence') {
						link = '\'Sequence\',';
					} else {
						link = '\'' + (dataSourceName || rowInfo[propertyName + '@aras.type']) + '\',';
					}

					action = isFile ? rowInfo[propertyName + '@aras.action'] : action;
					link += '\'' + currentProperty + '\'';
				}

				if (event.target.classList && event.target.classList.contains('aras-grid-link') && action !== 'add') {
					this.gridLinkClick(link, event.ctrlKey || event.metaKey);
				}
				if (event.target.closest('.aras-grid-file-icon')) {
					aras.getMainWindow().ArasModules.Dialog.show('iframe', {
						aras: aras,
						fileId: link.replace(/'/g, '').split(',')[1],
						editable: false,
						type: 'ManageFileProperty',
						title: 'Manage File'
					});
				}
			},

			onStartEdit: function(e) {
				const focusedCell = this._grid.settings.focusedCell;
				if (!focusedCell || !focusedCell.editing) {
					return;
				}
				this.onStartEdit_Experimental(focusedCell.rowId, focusedCell.headId);
			},

			onApplyCellEdit: function(e) {
				const value = e.detail.value;
				const rowId = e.detail.rowId;
				const field = e.detail.headId;
				this.onApplyEdit_Experimental(rowId, field, value);
			},

			onCancelEdit: function() {
				const rowId = this._grid.settings.focusedCell.rowId;
				this.onCancelEdit_Experimental(rowId);
			},

			dokeydown: function(ev) {
				this.gridKeyPress(ev);

				//Ctrl+A selection handler
				var isMacOS = /mac/i.test(navigator.platform);
				var isCtrlPressed = isMacOS ? ev.metaKey : ev.ctrlKey;
				if (isCtrlPressed && 'a' === ev.key) {
					event.stop(ev);
					this.selectAll();
				}
			},

			onFocusInputRow: function(rowId, field) {
				this.onStartEdit_Experimental(rowId, field);
				var cell = this.cells(rowId, this.getColumnIndex(field));
				this.gridSelectCell(cell);
			},

			onChangeInputRow: function(rowId, field) {
				this.onApplyEdit_Experimental(rowId, field);
			},

			onStartSearch: function() {
				this.onStartSearch_Experimental();
			},

			// this - dojo DataGrid or dojo TreeGrid
			//TODO: applied only for TreeGrid - to move method
			onStyleRow: function(inRow) {
				var gridItem = this.getItem(inRow.index);

				if (gridItem) {
					var id = this.store.getIdentity(gridItem),
						styleRow;

					if (this.styleGrid[id] && this.styleGrid[id].styleRow) {
						styleRow = this.styleGrid[id].styleRow;
						for (var styleItem in styleRow) {
							if (styleRow.hasOwnProperty(styleItem)) {
								inRow.customStyles += styleItem + ':' + styleRow[styleItem] + ';';
							}
						}
					}
				}
			},

			onBlur: function() {
				this.edit._applyStarted = true;
				this.edit.apply();
			}
		},

		// grid DnD functionality
		dnd: function(gridContainer) {
			var dndModule = {
				gridContainer: gridContainer,
				gridWidget: gridContainer.grid_Experimental,
				gridView: null,
				dragContainer: null,
				dragSourceRowNode: null,
				dragTargetRowId: null,
				dragStarted: false,
				isDragPending: false,
				startPoint: null,
				dropPoint: null,
				dragDelay: 10,
				visibleColumnsCount: 4,
				ctrlKeyState: false,
				_isMacOs: /mac/i.test(navigator.platform),
				styling: {
					containerNode: 'dndDragNodeContainer',
					avatarNode: 'dndDragAvatar',
					sourceNode: 'dndDragSource',
					placeholderNode: 'dndDragPlaceholder'
				},
				onMouseDownRowIndex: null,

				init: function() {
					aspect.after(this.gridWidget, 'buildViews', function() {
						this.gridView = this.gridWidget.views.views[0];
						this.attachDnDEventListeners();
					}.bind(this));
				},

				// method used to attach mouse event listeners to current gridView
				attachDnDEventListeners: function() {
					connect.connect(this.gridView.contentNode, 'mousedown', this, this.processDnDEvent);
					connect.connect(this.gridView.scrollboxNode, 'mouseup', this, this.processDnDEvent);
					connect.connect(this.gridView.scrollboxNode, 'mouseenter', this, this.processDnDEvent);
					connect.connect(this.gridView.scrollboxNode, 'mousemove', this, this.processDnDEvent);
					connect.connect(this.gridView.scrollboxNode, 'mouseleave', this, this.processDnDEvent);
					connect.connect(this.gridView.scrollboxNode, 'keydown', this, this.processDnDEvent);
					connect.connect(this.gridView.scrollboxNode, 'keyup', this, this.processDnDEvent);
				},

				createDragContainer: function() {
					this.dragContainer = {
						containerNode: document.createElement('div'),
						avatarNode: document.createElement('div'),
						sourceNode: document.createElement('div'),
						placeholderNode: document.createElement('div')
					};

					this.dragContainer.containerNode.appendChild(this.dragContainer.avatarNode);
					this.dragContainer.containerNode.appendChild(this.dragContainer.sourceNode);
					document.body.appendChild(this.dragContainer.containerNode);

					// apply node styles
					this.dragContainer.sourceNode.innerHTML = this.cleanupCssStyles(this.dragSourceRowNode.outerHTML);

					this.dragContainer.containerNode.className = this.styling.containerNode;
					this.dragContainer.avatarNode.className = this.styling.avatarNode;
					this.dragContainer.sourceNode.className = this.styling.sourceNode;
					this.dragContainer.placeholderNode.className = this.styling.placeholderNode;
					this.dragContainer.placeholderNode.style.width = this.getRowContentWidth(this.dragSourceRowNode) + 'px';

					var sourceWidth = 0, columnIndex = 0,
						columnsToAdd = this.visibleColumnsCount,
						layoutCell;

					while (columnsToAdd && columnIndex < this.gridWidget.layout.cellCount) {
						layoutCell = this.gridWidget.layout.cells[columnIndex];

						if (!layoutCell.hidden) {
							sourceWidth += parseInt(layoutCell.width);
							columnsToAdd--;
						}

						columnIndex++;
					}

					this.dragContainer.sourceNode.style.width = sourceWidth + 'px';
					this.drawAvatar();
				},

				drawAvatar: function() {
					if (this.dragContainer) {
						this.dragContainer.avatarNode.innerHTML = this.ctrlKeyState ? 'Copying' : 'Moving';
					}
				},

				getRowContentWidth: function(rowNode) {
					var contentWidth = 0,
						i;

					for (i = 0; i < rowNode.childNodes.length; i++) {
						contentWidth += rowNode.childNodes[i].offsetWidth;
					}

					return contentWidth;
				},

				repositionDragContainer: function(mouseX, mouseY) {
					if (this.dragContainer) {
						var containerNode = this.dragContainer.containerNode;

						containerNode.style.left = (mouseX + 40) + 'px';
						containerNode.style.top = parseInt((mouseY) - containerNode.offsetHeight / 2) + 'px';
					}
				},

				cleanupCssStyles: function(nodeHtml) {
					return nodeHtml.replace(/dojoxGridRowSelected|dojoxGridRowOver|dojoxGridCellFocus/g, '');
				},

				destroyDragContainer: function() {
					if (this.dragContainer) {
						var placeholderParent = this.dragContainer.placeholderNode.parentNode;

						if (placeholderParent) {
							this.gridView.contentNode.style.height = (this.gridView.contentNode.offsetHeight - this.dragContainer.placeholderNode.offsetHeight) + 'px';
							placeholderParent.removeChild(this.dragContainer.placeholderNode);
						}

						document.body.removeChild(this.dragContainer.containerNode);
						this.dragContainer = null;
					}
				},

				searchDropTarget: function(e) {
					if (e.rowIndex >= 0) {
						var targetRowIndex = e.rowIndex,
							targetRowNode = this.gridView.rowNodes[targetRowIndex],
							parentNode = targetRowNode.parentNode,
							targetRowRectangle = targetRowNode.getBoundingClientRect(),
							middleY = parseInt(targetRowRectangle.top + targetRowRectangle.height / 2),
							placeholderAttached = this.dragContainer.placeholderNode.parentNode,
							targetRowId;

						if (e.pageY < middleY) {
							if (e.rowIndex > 0) {
								targetRowIndex = targetRowIndex - 1;
							} else {
								targetRowIndex = -1;
							}
						}

						targetRowId = targetRowIndex === -1 ? '' : this.gridContainer.getRowId(targetRowIndex);

						if (this.dragTargetRowId === null || this.dragTargetRowId !== targetRowId) {
							if (targetRowIndex == -1) {
								this.dragTargetRowId = '';
							} else {
								this.dragTargetRowId = targetRowId;
							}

							if (targetRowIndex + 1 < this.gridWidget.rowCount) {
								parentNode.insertBefore(this.dragContainer.placeholderNode, this.gridView.rowNodes[targetRowIndex + 1]);
							} else {
								parentNode.appendChild(this.dragContainer.placeholderNode);
							}
						}

						// if placeholder was just added to contentNode, then adjust contentNode height
						if (!placeholderAttached) {
							this.gridView.contentNode.style.height = this.gridView.contentNode.scrollHeight + 'px';
						}

						//this.dragContainer.avatarNode.style.backgroundColor = "#1f6d42";
						return true;
					} else {
						//this.dragContainer.avatarNode.style.backgroundColor = "#b83b1d";
						return false;
					}
				},

				processDnDEvent: function(e) {
					switch (e.type) {
						case 'mouseup':
							this.gridView.content.decorateEvent(e);
							this.onMouseUp(e);
							break;
						case 'mousedown':
							this.gridView.content.decorateEvent(e);
							this.onMouseDown(e);
							break;
						case 'mousemove':
							this.gridView.content.decorateEvent(e);
							this.onMouseMove(e);
							break;
							//mouseout
						case 'mouseout':
						case 'mouseleave':
							this.onMouseLeave(e);
							break;
						case 'keydown':
							this.onKeyDown(e);
							break;
						case 'keyup':
							this.onKeyUp(e);
							break;
					}
				},

				canStartDrag: function(e) {
					if (this.isDragPending) {
						if (Math.abs(e.pageX - this.startPoint.x) > this.dragDelay || Math.abs(e.pageY - this.startPoint.y) > this.dragDelay) {
							this.gridContainer.setSelectedRow(this.gridContainer.getRowId(this.onMouseDownRowIndex), true, false);
							return this.gridContainer.canDragDrop_Experimental(e);
						}
					}

					return false;
				},

				beginDrag: function() {
					this.createDragContainer();
					this.isDragPending = false;
					this.dragStarted = true;

					// execute onDragStart_Experimental event handler
					if (this.gridContainer.onDragStart_Experimental) {
						this.gridContainer.onDragStart_Experimental();
					}
				},

				endDrag: function(dropAllowed, e) {
					if (this.dragStarted) {
						this.destroyDragContainer();
						this.dragStarted = false;

						if (dropAllowed) {
							// execute onDragDrop_Experimental event handler
							if (this.gridContainer.onDragDrop_Experimental) {
								this.gridContainer.onDragDrop_Experimental(this.dragTargetRowId, this.ctrlKeyState);
							}
						}
					}

					this.isDragPending = false;
				},

				onMouseDown: function(e) {
					if (e.button === 0 && e.rowIndex >= 0) {
						if (this.gridContainer.isEditable() && !this.gridWidget.edit.isEditing()) {
							this.dragSourceRowNode = e.rowNode;
							this.startPoint = {x: e.pageX, y: e.pageY};
							this.onMouseDownRowIndex = e.rowIndex;
							this.isDragPending = true;
						}
					}
				},

				onMouseUp: function(e) {
					this.dropPoint = {x: e.pageX, y: e.pageY};
					this.endDrag(true, e);
				},

				onMouseMove: function(e) {
					if (this.dragStarted) {
						this.repositionDragContainer(e.pageX, e.pageY);
						this.searchDropTarget(e);
					} else if (this.canStartDrag(e)) {
						this.beginDrag();

						this.refreshCtrlState(e);
						this.repositionDragContainer(e.pageX, e.pageY);
					}
				},

				onMouseLeave: function(e) {
					this.endDrag(false);
				},

				onKeyDown: function(e) {
					if (this.dragStarted) {
						switch (e.keyCode) {
							case 27:
								// if escape pressed, then stop dragging
								this.endDrag(false);
								break;
							case 17:
								this.refreshCtrlState(e);
								break;
						}
					}
				},

				onKeyUp: function(e) {
					if (this.dragStarted) {
						switch (e.keyCode) {
							case 17:
								this.refreshCtrlState(e);
								break;
						}
					}
				},

				refreshCtrlState: function(e) {
					var isCtrlPressed = this._isMacOs ? e.metaKey : e.ctrlKey;

					if (isCtrlPressed !== this.ctrlKeyState) {
						this.ctrlKeyState = isCtrlPressed;

						if (this.dragStarted) {
							this.drawAvatar();
						}
					}
				}
			};

			dndModule.init();
			return dndModule;
		},

		redline: function(grid) {
			return {
				gridContainer: grid,
				dataGrid: grid.grid_Experimental,
				isInitialized: false,
				compareColumnList: [],
				baselineRows: [],
				baselineTableRows: [],
				redLineDiff: new Map(),
				isRedlineActive: false,
				defaultRowStatus: {rowStatus: 'initial', baselineIndex: undefined},
				gridRowIdPrefix: 'rlrow',
				gridRowIdIndex: 0,
				_rowStyleHandler: null,
				styling: {
					deletedRow: 'redlineDeletedRow',
					newRow: 'redlineNewRow',
					baseValue: 'redlineValue',
					baseValueContainer: 'redlineValueContainer',
					divLineThrough: 'redlineDivLine'
				},

				addGridColumnsToCompareList: function() {
					this.compareColumnList = grid.grid_Experimental.order.slice();
				},

				addColumnToCompareList: function(columnName) {
					if (this.compareColumnList.indexOf(columnName) == -1) {
						this.compareColumnList.push(columnName);
					}
				},

				attachRowStyleHandler: function() {
					this._rowStyleHandler = connect.connect(this.dataGrid, 'onStyleRow', this, function(row) {
						var storeItem = this.dataGrid.getItem(row.index);
						if (storeItem && storeItem.redlineStatus) {
							var redlineRowStatus = this.dataGrid.store.getValue(storeItem, 'redlineStatus');

							switch (redlineRowStatus.rowStatus) {
								case 'deleted':
									row.customClasses += ' ' + this.styling.deletedRow;
									break;
								case 'new':
									row.customClasses += ' ' + this.styling.newRow;
									break;
							}
						}
					});
				},

				detachRowStyleHandler: function() {
					if (this._rowStyleHandler) {
						this._rowStyleHandler.remove();
					}
				},

				clearCompareList: function() {
					this.compareColumnList.length = 0;
				},

				enableRedlineMode: function(doEnabled) {
					if (this.isRedlineActive != doEnabled) {
						this.isRedlineActive = doEnabled;

						if (this.isRedlineActive) {
							// some actions during redline activation
							this.attachRowStyleHandler();
						} else {
							this.dataGrid.beginUpdate();
							this.clearGridStore();
							this.dataGrid.endUpdate();

							this.detachRowStyleHandler();
						}
					}
				},

				formatBaseValue: function(structureCell, baseValue, cellInfo) {
					var formattedValue,
						formatType = 'simple',
						i;

					if ('boolean' === typeof (baseValue)) {
						formatType = 'boolean';
					} else if (/^(&lt;|<)img[\w\W]*>$/.test(baseValue)) {
						var cellTopIndent = 4,
							cellBottomIndent = 4,
							rowHeight = this.gridContainer.rowHeight,
							maxSize = rowHeight - (cellTopIndent + cellBottomIndent),
							styleImg = 'margin: 0px; max-height: ' + maxSize + 'px; max-width: ' + maxSize + 'px;';

						if (/src=['"]{2}/.test(baseValue)) {
							baseValue = '';
						} else {
							formatType = 'image';
							baseValue = baseValue.replace(/(&lt;|<)img/g, '<img style="' + styleImg + '" ').replace(/(src=['"](?!http[s]*:\/\/))/g, '$1' + config.baseUrl + '../../cbin/');
						}
					} else {
						var listId = cellInfo && cellInfo.listId,
							list;
						//listId can be 0 and it's truly value, so don't use "if (listId)..."
						if (listId !== undefined && listId !== '') {
							list = structureCell.cellLayoutLists[listId];
						}

						var editableType = cellInfo && cellInfo.editableType || structureCell.editableType,
							optionsLables = list ? list.labels : structureCell.optionsLables,
							options = list ? list.values : structureCell.options;

						var converter = dojoConfig.arasContext.converter;
						if (optionsLables && optionsLables.length) {
							if ('CheckedMultiSelect' === editableType) {
								if (baseValue && baseValue !== '') {
									var valueArray = baseValue.split(',');

									baseValue = optionsLables[options.indexOf(valueArray[0])];
									for (i = 1; i < valueArray.length; i++) {
										baseValue += ', ' + optionsLables[options.indexOf(valueArray[i])];
									}
								} else {
									baseValue = '';
								}
							} else {
								baseValue = optionsLables[options.indexOf(baseValue)] || baseValue;
							}
						} else if ('NUMERIC' === structureCell.sort) {
							if (structureCell.inputformat) {
								baseValue = converter.convertFromNeutral(baseValue, 'decimal', structureCell.inputformat);
							} else {
								baseValue = converter.convertFromNeutral(baseValue, 'float');
							}
						} else if ('DATE' === structureCell.sort && structureCell.inputformat) {
							baseValue = converter.convertFromNeutral(baseValue, 'date', structureCell.inputformat);
						}
					}

					switch (formatType) {
						case 'simple':
							baseValue = this._escapeHtml(baseValue);
							formattedValue = '<div class=\'' + this.styling.baseValue + ' ' + this.styling.baseValueContainer + '\'>';
							formattedValue += baseValue;
							formattedValue += '</div>';
							break;
						case 'boolean':
							formattedValue = '<div class=\'' + this.styling.baseValue + ' ' + this.styling.baseValueContainer + '\'>';
							formattedValue += '<input class=\'arasCheckboxOrRadio\' type=\'checkbox\' onclick=\'return false\'';
							formattedValue += baseValue ? 'checked' : '';
							formattedValue += ' /><label></label>';
							formattedValue += '<div class=\'' + this.styling.divLineThrough + '\'></div>';
							formattedValue += '</div>';
							break;
						case 'image':
							formattedValue = '<div class=\'' + this.styling.baseValue + ' ' + this.styling.baseValueContainer + '\'>';
							formattedValue += baseValue;
							formattedValue += '<div class=\'' + this.styling.divLineThrough + '\'></div>';
							formattedValue += '</div>';
							break;
					}

					return formattedValue;
				},

				initialize: function() {
					if (!this.isInitialized) {
						// some initialization, before using redline functionality
						var self = this;
						Object.defineProperty(this.gridContainer, 'EnableDiffMode', {
							get: function() {
								return self.isRedlineActive;
							},
							set: function(doEnabled) {
								self.enableRedlineMode(doEnabled);
							}
						});

						this.isInitialized = true;
					}
				},

				isColumnComparable: function(columnName) {
					return (this.compareColumnList.indexOf(columnName) > -1);
				},

				isRedlineId: function(rowId) {
					return (rowId && rowId.indexOf(this.gridRowIdPrefix) === 0);
				},

				removeColumnFromCompareList: function(columnName) {
					var columnIndex = this.compareColumnList.indexOf(columnName);

					if (columnIndex != -1) {
						this.compareColumnList.splice(columnIndex, 1);
					}
				},

				_escapeHtml: function(htmlString) {
					return htmlString
						.replace(/&/g, '&amp;')
						.replace(/</g, '&lt;')
						.replace(/>/g, '&gt;')
						.replace(/"/g, '&quot;')
						.replace(/'/g, '&#039;');
				},

				_getStoreItems: function() {
					var fetchedItems;

					this.dataGrid.store.fetch({
						onComplete: function(items) {
							fetchedItems = items;
						}
					});
					return fetchedItems;
				},

				clearGridStore: function() {
					var store = this.dataGrid.store,
						storeItems = this._getStoreItems(),
						redlineStatus,
						item, i,
						rowsAffected = 0;

					this.dataGrid.beginUpdate();
					for (i = 0; i < storeItems.length; i++) {
						item = storeItems[i];
						redlineStatus = store.getValue(item, 'redlineStatus');

						if (redlineStatus) {
							switch (redlineStatus.rowStatus) {
								case 'deleted':
									store.deleteItem(item);
									break;
								default:
									store.setValue(item, 'redlineStatus', this.defaultRowStatus);
									break;
							}
							rowsAffected++;
						}
					}

					if (rowsAffected) {
						store.save();
					}
					this.dataGrid.endUpdate();
				},

				loadBaselineXML: function(xml) {
					this.baselineRows.length = 0;
					this.baselineTableRows = null;
					this.clearCompareList();

					if (xml) {
						var dom = new XmlDocument(),
							rowData,
							i;

						dom.loadXML(xml);
						this.baselineTableRows = dom.selectNodes('./table/tr');
						for (i = 0; i < this.baselineTableRows.length; i++) {
							rowData = this._prepareBaselineRow(this.baselineTableRows[i]);
							this.baselineRows.push(rowData);
						}
					}
				},

				_generateRedlineDiff: function() {
					this.redLineDiff.forEach(function(value, key) {
						if (value.rowStatus === 'edited') {
							this._compareValuesInLine(key);
						}
					}, this);
				},

				_compareValuesInLine: function(key) {
					const store = grid._grid.rows._store;
					const currentItem = store.get(key);
					const redLineItemDiff = this.redLineDiff.get(key);
					const baseLineItem = this.baselineRows[redLineItemDiff.baselineIndex];
					const compareColumnList = this.compareColumnList;
					compareColumnList.forEach(function(propName) {
						if (currentItem[propName] !== baseLineItem[propName]) {
							this._addPropertyToDiff(key, propName, baseLineItem[propName]);
						}
					}, this);
				},

				_addPropertyToDiff: function(key, prop, value) {
					const redLineItemDiff = Object.assign({}, this.redLineDiff.get(key));
					if (!redLineItemDiff.diff) {
						redLineItemDiff.diff = {};
					}
					redLineItemDiff.diff[prop] = value;
					this.redLineDiff.set(key, redLineItemDiff);
				},

				refreshRedlineView: function() {
					this.clearGridStore();
					if (this.baselineRows.length) {
						const storeItems = grid._grid.rows._store;
						const storeItemsSize = storeItems.size;
						const matchesArray = [];
						const currentMatchIndexes = new Array(storeItemsSize);
						const baselineMatchIndexes = new Array(this.baselineRows.length);
						let mismatchCount = parseInt(this.compareColumnList.length / 2 + 1);
						let minMatchCount;
						let bestMatchIndex;
						let bestMatchValue;
						let newRowId;

						mismatchCount = mismatchCount > 5 ? 5 : mismatchCount;
						minMatchCount = this.compareColumnList.length - mismatchCount;
						for (let i = 0; i < this.baselineRows.length; i++) {
							matchesArray.push(this._compareBaselineRow(storeItems, this.baselineRows[i]));
						}

						for (let i = 0 ; i < storeItemsSize; i++) {
							bestMatchValue = 0;
							for (let j = 0 ; j < this.baselineRows.length; j++) {
								if (matchesArray[j][i] > bestMatchValue) {
									bestMatchIndex = j;
									bestMatchValue = matchesArray[j][i];
								}
							}

							if (bestMatchValue > minMatchCount) {
								for (let j = 0 ; j < storeItemsSize; j++) {
									matchesArray[bestMatchIndex][j] = 0;
								}
								currentMatchIndexes[i] = bestMatchIndex;
								baselineMatchIndexes[bestMatchIndex] = i;
							}
						}

						let i = 0;
						storeItems.forEach(function(value, key) {
							if (typeof currentMatchIndexes[i] === 'undefined') {
								this.redLineDiff.set(key, {rowStatus: 'new'});
							} else {
								this.redLineDiff.set(key, {
									rowStatus: 'edited',
									baselineIndex: currentMatchIndexes[i]
								});
							}
							i++;
						}, this);

						for (let i = 0 ; i < this.baselineRows.length; i++) {
							if (typeof baselineMatchIndexes[i] === 'undefined') {
								const currentRow = this.baselineTableRows[i];
								const rowObj = {
									getUserData: function() {
										return currentRow.selectNodes('./userdata');
									},
									getFields: function() {
										return currentRow.selectNodes('./td');
									},
									getFieldText: function(fieldItem) {
										return fieldItem.text;
									},
									getFieldAttribute: function(fieldItem, attr) {
										return fieldItem.getAttribute(attr) || currentRow.getAttribute(attr);
									}
								};

								newRowId = this._newRedlineRowId();
								if (this.gridContainer._addRowImplementation_Experimental(newRowId, rowObj)) {
									this.redLineDiff.set(newRowId, {rowStatus: 'deleted'});
								}
							}
						}
					}
					this._generateRedlineDiff();
				},

				_newRedlineRowId: function() {
					return this.gridRowIdPrefix + this.gridRowIdIndex++;
				},

				_prepareBaselineRow: function(row) {
					const reCheck = /<checkbox.*>/;
					const reCheckState = /.*(state|value)=["'](1|true)["'].*/;
					const fields = row.selectNodes('./td');
					const newRow = {fields: {}, dataType: {}};
					const store = grid._grid._head._store;
					const columnCount = grid.GetColumnCount();
					const order = grid.grid_Experimental.order;
					let layoutCell;

					for (let i = columnCount - fields.length; i > 0; i--) {
						fields.push('');
					}

					for (let i = 0; i < fields.length; i++) {
						layoutCell = store.get(order[i]);

						if (layoutCell) {
							const fieldName = layoutCell.field;
							const sort = layoutCell.sort;
							const fieldText = fields[i].text;
							const fieldDataType = fields[i].getAttribute('fdt') || row.getAttribute('fdt') || '';
							let fieldValue = fieldText;

							if (reCheck.test(fieldText)) {
								fieldValue = reCheckState.test(fieldText);
							} else if ('NUMERIC' === sort) {
								fieldValue = fieldText ? parseFloat(fieldText) : '';
							} else if ('DATE' === sort) {
								fieldValue = fieldText;
							} else {
								fieldValue = fieldText;
								const link = fields[i].getAttribute('link') || row.getAttribute('link');
								if (link) {
									newRow[fieldName + 'link'] = link;
								}
							}

							newRow[fieldName] = fieldValue;
							newRow.fields[fieldName] = fieldName;
							newRow.dataType[fieldName] = fieldDataType;
						} else {
							return;
						}
					}

					return newRow;
				},

				_compareBaselineRow: function(storeItems, baseRow) {
					const compareMap = [];
					const compareColumnList = this.compareColumnList;
					const indexRows = grid._grid.settings.indexRows;
					const head = grid._grid.head._store;

					indexRows.forEach(function(id) {
						const matchesCount = compareColumnList.reduce(function(res, columnName) {
							let currentItemColumnName = columnName;
							let item = storeItems.get(id);
							const columnData = head.get(columnName);
							const linkProperty = columnData.linkProperty;
							if (linkProperty && item[linkProperty]) {
								const relatedId = item[linkProperty]
								item = storeItems.get(relatedId);
								currentItemColumnName = columnData.name;
							}
							if (item[currentItemColumnName] === baseRow[columnName]) {
								res++;
							}
							return res;
						}, 0);
						compareMap.push(matchesCount);
					});
					return compareMap;
				},

			};
		},

		initClearData: function(grid) {//todo: remove this override after fix in dojo;
			var that = grid;
			grid._clearData = function() {
				that.height = 0;
				that._by_idty = {};
				that._by_idx = [];
				that._pages = [];
				that._bop = that._eop = -1;
				that._isLoaded = false;
				that._isLoading = false;
			};
		},

		_parseCSS: function(css) {
			function normalizeCSS(cssString) {
				if (!css) {
					return '';
				}
				css = css.trim();
				css = css.replace(/[ \t]*\{[ \t]*/g, '{');
				css = css.replace(/[ \t]*\}[ \t]*/g, '}');
				css = css.replace(/[ \t]*:[ \t]*/g, ':');
				css = css.replace(/[ \t]*;[ \t]*/g, ';');
				css = css.replace(/[ \t]*-[ \t]*/g, '-');
				return css;
			}
			css = normalizeCSS(css);
			var rulesArr = css.split(';');
			var result = {};
			for (var i = 0; i < rulesArr.length; i++) {
				var rule = rulesArr[i].split(':');
				if (rule && rule[0] !== '' && !result[rule[0]]) {
					result[rule[0]] = rule[1];
				}
			}
			return result;
		},

		GetColumnIndex: function(grid, columnName) {
			var index = grid.grid_Experimental.order.indexOf(columnName);
			return index === -1 ? undefined : index;
		},

		formatter: function(grid) {  //  grid = Public GridContainer or Public TreeGridContainer
			var cellBorderWidth = 1,
				cellLeftRightPadding = 3,
				cellTopBottomPadding = 3;

			return {
				grid: grid,
				cellTopIndent: cellBorderWidth + cellTopBottomPadding,
				cellBottomIndent: cellBorderWidth + cellTopBottomPadding,

				img: function(value) {
					if (!value) {
						return '';
					}
					//var maxSizeHeight = this.grid.rowHeight - 6;
					//var maxSizeWidth = this.getHeaderNode().clientWidth - 10;
					var maxSize = this.grid.rowHeight - 7;
					var style = 'style="max-height: ' + maxSize + 'px; max-width: ' + maxSize + 'px;"';
					return '<img ' + style + ' src="' + config.baseUrl + '../../cbin/' + value + '" />';
				},

				html: function(value, rowIndex) {
					var item, id, frameWidget;
					if (!value) {
						return value;
					}
					if (!/^<html>.*^<\/html>$/.test(value)) {
						value = '<html><body style=\'background-color: transparent;margin:0;\'>' + value + '</body></html>';
					}
					item = this.grid.getItem(rowIndex);
					id = item.uniqueId[0];
					frameWidget = new safeIFrame({
						id: id,
						index: rowIndex,
						headerCell: this,
						htmlString: value.replace(/&amp;/g, '&').replace(/&lt;/g, '<'),
						grid: this.grid,
						width: this.unitWidth || this.width
					});
					frameWidget._destroyOnRemove = true;
					return frameWidget;
				},

				formatHandler: function(layoutCell, storeValue, index) {
					var item = this.grid.grid_Experimental.getItem(index), store = this.grid.grid_Experimental.store,
						attrs = store.getValue(item, 'attrs') ? store.getValue(item, 'attrs')[layoutCell.field] : {},
						attrItem, valueArray, i,
						redlineModule = this.grid.redline_Experimental,
						redlineRowStatus = store.getValue(item, 'redlineStatus'),
						cellContent;
					var customStyle = '', styleItem, style = store.getValue(item, 'style') ? store.getValue(item, 'style')[layoutCell.field] : {};
					var restrictedWarning = aras.getResource('', 'common.restricted_property_warning');

					var markup = layoutCell.markup[2],
						cellInfo = item === undefined ? undefined : store.getValue(item, layoutCell.field + '$cellInfo'),
						listId = cellInfo && cellInfo.listId,
						list;

					//listId can be 0 and it's truly value, so don't use "if (listId)..."
					if (listId !== undefined && listId !== '') {
						list = layoutCell.cellLayoutLists[listId];
					}
					var editableType = cellInfo && cellInfo.editableType || layoutCell.editableType,
						optionsLables = list ? list.labels : layoutCell.optionsLables,
						options = list ? list.values : layoutCell.options;

					for (attrItem in attrs) {
						if (attrs.hasOwnProperty(attrItem) && markup.indexOf(attrItem) < 0) {
							markup = '" ' + attrItem + '="' + attrs[attrItem] + markup;
						}
					}
					layoutCell.markup[2] = markup;

					var isFieldLink = store.getValue(item, layoutCell.field + 'link');
					for (styleItem in style) {
						if (style.hasOwnProperty(styleItem)) {
							customStyle += styleItem + ':' + style[styleItem] + ';';
						}
					}

					layoutCell.customStyles.push(customStyle);

					var cellViewTypeSettings;
					if (this.grid.cellViewTypeHelper_Experimental) {
						var cellViewType = store.getValue(item, layoutCell.field + '$cellViewType');
						cellViewTypeSettings = this.grid.cellViewTypeHelper_Experimental.getCellViewTypeSettings(cellViewType);
					}
					var sort;
					var inputformat;
					if (cellViewTypeSettings) {
						sort = cellViewTypeSettings.sort;
						inputformat = cellViewTypeSettings.inputformat;
					} else {
						sort = layoutCell.sort;
						inputformat = layoutCell.inputformat;
					}

					var converter = dojoConfig.arasContext.converter;
					if (layoutCell.externalWidget && layoutCell.externalWidget.functionalFlags.render) {
						cellContent = layoutCell.externalWidget.generateCellHTML(layoutCell, storeValue, index);
					} else if ('boolean' === typeof (storeValue)) {
						cellContent = '<div class=\'checkBoxContainer\'>';
						cellContent += '<input class=\'aras_grid_input_checkbox arasCheckboxOrRadio\' type=\'checkbox\' onclick=\'( function(e){ e.target.isCheckBox = true; e.preventDefault(); return false;} )(event)\' ';
						cellContent += storeValue ? 'checked' : '';
						cellContent += ' /><div class=\'aras_grid_checkbox ' + (storeValue ? 'checked' : 'unchecked') + '\'></div>';
						cellContent += '</div>';
					} else if (/^&lt;img[\w\W]*>$/.test(storeValue)) {
						if (/src=['"]{2}/.test(storeValue)) {
							cellContent = '';
						} else {
							var maxSize = this.grid.rowHeight - (this.cellTopIndent + this.cellBottomIndent),
								styleImg = 'margin: 0px; max-height: ' + maxSize + 'px; max-width: ' + maxSize + 'px;';

							storeValue = storeValue.replace(/&lt;img/g, '<img');
							cellContent = storeValue.replace(/<img/g, '<img style="' + styleImg + '" ').replace(/(src=['"](?!http[s]*:\/\/))/g, '$1' + config.baseUrl + '../../cbin/');
						}
					} else if (isFieldLink) {
						cellContent = '<a href="#" class="gridLink" ' + (customStyle ? 'style="' + customStyle + '"' : '') + '>' + storeValue + '</a>';
					} else if (optionsLables && optionsLables.length) {
						if ('CheckedMultiSelect' === editableType) {
							if (storeValue && storeValue !== '') {
								if (storeValue === restrictedWarning) {
									cellContent = storeValue;
								} else {
									valueArray = storeValue.split(',');

									cellContent = optionsLables[options.indexOf(valueArray[0])];
									for (i = 1; i < valueArray.length; i++) {
										cellContent += ', ' + optionsLables[options.indexOf(valueArray[i])];
									}
								}
							} else {
								cellContent = '';
							}
						} else {
							cellContent = optionsLables[options.indexOf(storeValue)] || storeValue;
						}
					} else if ('NUMERIC' === sort) {
						if (inputformat) {
							cellContent = converter.convertFromNeutral(storeValue, 'decimal', inputformat);
						} else {
							cellContent = converter.convertFromNeutral(storeValue, 'float');
						}
					} else if ('DATE' === sort && inputformat) {
						cellContent = converter.convertFromNeutral(storeValue, 'date', inputformat);
					} else {
						cellContent = storeValue;
					}

					if (redlineRowStatus && redlineRowStatus.rowStatus == 'edited') {
						if (redlineModule.isColumnComparable(layoutCell.field)) {
							var baselineRow = redlineModule.baselineRows[redlineRowStatus.baselineIndex],
								baseValue = baselineRow[layoutCell.field],
								isBaseValueExists = baseValue || typeof baseValue == 'boolean';

							if (isBaseValueExists && baseValue != storeValue) {
								cellContent += redlineModule.formatBaseValue(layoutCell, baseValue, cellInfo);
							}
						}
					}

					return cellContent;
				}
			};
		},

		getUserData_Gm: function(store, rowId, keyOptional) {
			if (rowId === null || rowId === undefined) {
				throw new Error('The \'rowId\' value cannot be null or undefined.');
			}
			var item = store._getItemByIdentity(rowId);
			if (!item) {
				return;
			}

			var key = keyOptional !== undefined && keyOptional !== null ? keyOptional : 'key7aa8b83dArasAnyConst';
			return item.userData$Gm && item.userData$Gm[key];
		},

		setUserData_Gm: function(store, rowId, keyOrValue, value) {
			if (rowId === null || rowId === undefined) {
				throw new Error('The \'rowId\' value cannot be null or undefined.');
			}
			var item = store._getItemByIdentity(rowId);
			if (!item) {
				return;
			}
			if (!item.userData$Gm) {
				item.userData$Gm = [];
			}

			if (value !== undefined) {
				item.userData$Gm[keyOrValue] = value;
			} else {
				var key = 'key7aa8b83dArasAnyConst';
				item.userData$Gm[key] = keyOrValue;
			}
		},

		moveRowUpDownForPublicGrid: function(grid, rowId, isUp) {
			var index = grid.getRowIndex(rowId),
				arrayOfAllItems = grid.grid_Experimental.store._arrayOfAllItems,
				arrayOfTopLevelItems = grid.grid_Experimental.store._arrayOfTopLevelItems,
				increment = isUp ? -1 : 1,
				rowAbove = arrayOfAllItems[index + increment],
				row = arrayOfAllItems[index];

			if (!row || !rowAbove) {
				return;
			}

			var itemNumPropName = grid.grid_Experimental.store._itemNumPropName,
				rowAboveNumProp = rowAbove[itemNumPropName];

			rowAbove[itemNumPropName] = row[itemNumPropName];
			row[itemNumPropName] = rowAboveNumProp;

			arrayOfTopLevelItems[index + increment] = arrayOfAllItems[index + increment] = row;
			arrayOfTopLevelItems[index] = arrayOfAllItems[index] = rowAbove;
			grid.grid_Experimental._refresh();
		},

		setFont_Gm: function(fontString, fieldStyle) {
			if (!fontString) {
				return;
			}

			delete fieldStyle.font;

			var fontParts = fontString.split('-');
			fieldStyle['font-family'] = fontParts[0];

			if (fontParts.length > 1) {
				var fontSize = fontParts[fontParts.length - 1];
				if (!isNaN(parseFloat(fontSize))) {
					fieldStyle['font-size'] = fontSize + 'pt';
				}

				var fontStyle = fontParts[1];
				if (fontStyle.indexOf('italic') >= 0) {
					fieldStyle['font-style'] = fieldStyle['font-style'];
				}

				if (fontStyle.indexOf('bold') >= 0) {
					fieldStyle['font-weight'] = 'bold';
				}
			}
		},

		getAlign_Gm: function(align) {
			var alignLetter = align && align[0].toLowerCase();
			if ('c' === alignLetter) {
				return 'text-align:center;';
			} else if ('r' === alignLetter) {
				return 'text-align:right;';
			} else {
				return 'text-align:left;';
			}
		},
	};
});
