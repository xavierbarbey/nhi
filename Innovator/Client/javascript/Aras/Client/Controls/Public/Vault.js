﻿function Vault(vaultInstance, fakeMethodHandler) {
	/// <summary>
	/// "aras.vault" instance of the class can be used in custom JavaScript code.
	/// Vault provides batch file uploading/downloading capabilities together with related
	/// file manipulation routines.
	/// </summary>
	/// <remarks>
	/// It has user friendly interface, displays a progress bar during time consuming upload/download
	/// process and has warning/error messages system to inform the user about any collisions.<br/>
	/// User has a possibility to add/remove files to the batch list, rename files and folders,
	/// cancel current job.<br/>
	/// User may select files from both local and network-mapped folders, optionally including all
	/// sub-folders.
	/// Additionally, he can enter any valid network path and browse it in the file selection dialog.
	/// <p>
	/// File transfer works over http, SSL (https) with or without proxy. This is achieved
	/// by using browser's native connection classes.<br/>
	/// And the most attractive feature is the possibility to upload huge files (unlimited file size)
	/// with no timeouts or memory leacks (known java bug).<br/>
	/// We use all available network traffic, so the transfer will go as fast as your LAN/WAN allows.<br/>
	/// You can submit your form data together with the file. It is usually required to send the state
	/// information back to the server.
	/// </p>
	/// <p>
	/// What else can I do with Vault applet that I can't with a usual FILE form input field?
	/// - Well, you can control, filter and preprocess the file list that user has selected.
	/// You can enable or disable to transfer some file types basing on your application logic,
	/// and you can collect additional information related to those files.
	/// And finally, you don't need to reload your page while you transferring the files.
	/// </p>
	/// <p>
	/// We use the standard "multipart/form-data" content encoding, so the applet is compatible
	/// with any server-side uploading component.
	/// The quality of the server-side component as well as the hard-drive performance
	/// will also affect the resulting transfer rate.
	/// </p>
	/// </remarks>

	this.vault = vaultInstance;

	this.init = function vault_init() {
		var doFakeVault = false, fakeVaultMethod;
		if (!this.vault) {
			doFakeVault = true;
			this.vault = {};
			fakeVaultMethod = function() {
				if (fakeMethodHandler) {
					fakeMethodHandler();
				}
			};
		}

		for (var funcName in this) {
			var obj = this[funcName];
			var firstChar = funcName.charAt(0);

			if (typeof(obj) == 'function' && !this.hasOwnProperty(funcName) && firstChar.toLowerCase() == firstChar) {
				var newFuncName = firstChar.toUpperCase() + funcName.slice(1);
				this[newFuncName] = obj;
				if (doFakeVault) {
					this.vault[newFuncName] = this.vault[funcName] = fakeVaultMethod;
				}
			}
		}
	};
}

Vault.prototype.getOS = function() {
	/// <summary>
	/// Returns the string defining the operating system the applet is running on.
	/// Is not supported. Use navigator.userAgent to detect Windows platform.
	/// </summary>
	throw new Error('The method "getOS" from Vault API is not supported');
};

Vault.prototype.mkDir = function() {
	/// <summary>
	/// Creates a directory structure on the local file system.<br/>
	/// Mode: download.
	/// Is not supported because it works directly with files system
	/// </summary>
	throw new Error('The method "mkDir" from Vault API is not supported');
};

Vault.prototype.fileCreateWithSaveAsDialog = function() {
	/// <summary>
	/// Creates or overwrites the specified file and show SaveAsDialog.
	/// Is not supported because it works directly with files system
	/// </summary>
	throw new Error('The method "fileCreateWithSaveAsDialog" from Vault API is not supported');
};

Vault.prototype.fileExists = function() {
	/// <summary>
	/// Gets a value indicating whether a file exists.
	/// Is not supported because it works directly with files system
	/// </summary>
	throw new Error('The method "fileExists" from Vault API is not supported');
};

Vault.prototype.directoryExists = function() {
	/// <summary>
	/// Gets a value indicating whether the directory exists.
	/// Is not supported because it works directly with files system
	/// </summary>
	throw new Error('The method "directoryExists" from Vault API is not supported');
};

Vault.prototype.getParentDir = function() {
	/// <summary>
	/// Gets parent directory.
	/// Is not supported because it works directly with files system
	/// </summary>
	throw new Error('The method "getParentDir" from Vault API is not supported');
};

Vault.prototype.setWorkingDir = function() {
	/// <summary>
	/// Sets the local working directory for the applet (where files are to be downloaded into).<br/>
	/// Initially working directory can be initialized from {@link #pWORKINGDIR pWORKINGDIR} parameter.<br/>
	/// Mode: download.
	/// Is not supported because it works directly with files system
	/// </summary>
	throw new Error('The method "setWorkingDir" from Vault API is not supported');
};

Vault.prototype.setFileName = function() {
	/// <summary>
	/// Sets the Filename property. This can be used to set the file name as an alternative to the SelectFile() method.
	/// Is not supported because it works directly with files system
	/// </summary>
	throw new Error('The method "setFileName" from Vault API is not supported');
};

Vault.prototype.setClientData = function(name, value_Renamed) {
	/// <summary>
	/// Set userdata (form fields) for uploading.<br/>
	/// Mode: upload.
	/// </summary>
	/// <param name="name" type="string"></param>
	/// <param name="value_Renamed" type="string"></param>
	return this.vault.setClientData(name, value_Renamed);
};

Vault.prototype.getClientData = function(name) {
	/// <summary>
	/// Gets userdata (form fields) for uploading.
	/// </summary>
	/// <param name="name" type="string"></param>
	/// <returns>string</returns>
	return this.vault.getClientData(name);
};

Vault.prototype.selectFolder = function() {
	/// <summary>
	/// Displays a folder selection dialog box that allows the user to browse the local file system and select a folder.
	/// Filename property is set to null
	/// Is not supported because it works directly with files system
	/// </summary>
	throw new Error('The method "selectFolder" from Vault API is not supported');
};

Vault.prototype.selectSavePath = function() {
	/// <summary>
	/// Displays a SaveFile dialog box, that allows to browse local file system and select a file path for "save" operation.
	/// initialPath parameter used to setup initial directory and filename.
	/// dialogTitle string will be displayed in title bar.
	/// Is not supported because it works directly with files system
	/// </summary>
	throw new Error('The method "selectSavePath" from Vault API is not supported');
};

Vault.prototype.selectFile = function() {
	/// <summary>
	/// Displays a file selection dialog box that allows the user to browse the local file system and select a file.
	/// The WorkingDir property for the applet is also set if the user browses to a directory.
	/// The working directory for the dialog box is initialized to the working directory for the applet.
	/// </summary>
	/// <returns>
	/// Promise. Returns the Promise object which will be resolved with selected file if user selects file.
	/// </returns>
	return this.vault.selectFile();
};

Vault.prototype.getFileChecksum = function(fileName) {
	/// <summary>
	/// Gets checksum of the current file.
	/// </summary>
	/// <param name="fileName" type="string"></param>
	/// <returns>string</returns>
	return this.vault.getFileChecksum(fileName);
};

Vault.prototype.addFileToDownloadList = function() {
	/// <summary>
	/// Add the specified file URL to the download list.
	/// Is not supported because multiple downloading cannot be performed without extensions
	/// </summary>
	throw new Error('The method "addFileToDownloadList" from Vault API is not supported');
};

Vault.prototype.addFileToList = function(fileID, filename) {
	/// <summary>
	/// Add the specified file URL to the fileList.
	/// </summary>
	/// <param name="fileID" type="string"></param>
	/// <param name="filename" type="string"></param>
	/// <returns>bool</returns>
	return this.vault.addFileToList(fileID, filename);
};

Vault.prototype.sendFile = function() {
	/// <summary>
	/// Send file from clientData to specified url from working directory.
	/// Is not supported because it works with files system directly.
	/// There is no access to files system without extensions anymore.
	/// </summary>
	throw new Error('The method "sendFile" from Vault API is not supported');
};

Vault.prototype.sendFiles = function(serverUrl) {
	/// <summary>
	/// Send files from clientData to specified url.
	/// </summary>
	/// <param name="serverUrl" type="string"></param>
	/// <returns>bool</returns>
	return this.vault.sendFiles(serverUrl);
};

Vault.prototype.sendFilesAsync = function(serverUrl) {
	/// <summary>
	/// Send files from clientData to specified url in asynchronous mode
	/// </summary>
	/// <param name="serverUrl" type="string"></param>
	/// <returns>Promise object</returns>
	return this.vault.sendFilesAsync(serverUrl);
};

Vault.prototype.getResponse = function() {
	/// <summary>
	/// Returns the Response property, which is set with the server response data from the upload() method call.
	/// </summary>
	/// <returns>string</returns>
	return this.vault.getResponse();
};

Vault.prototype.getWorkingDir = function() {
	/// <summary>
	/// Get applet's current working directory (where files are to be downloaded into).
	/// Is not supported because it works with files system directly.
	/// </summary>
	throw new Error('The method "getWorkingDir" from Vault API is not supported');
};

Vault.prototype.getLastError = function() {
	/// <summary>
	/// Get the error message from the last operation.
	/// </summary>
	/// <returns>string</returns>
	return this.vault.getLastError();
};

Vault.prototype.clearClientData = function() {
	/// <summary>
	/// Clear all userdata values.
	/// </summary>
	return this.vault.clearClientData();
};

Vault.prototype.clearDownloadList = function() {
	/// <summary>
	/// Clears download file list.
	/// Is not supported.
	/// </summary>
	throw new Error('The method "clearDownloadList" from Vault API is not supported');
};

Vault.prototype.clearFileList = function() {
	/// <summary>
	/// Clears fileList.
	/// </summary>
	return this.vault.clearFileList();
};

Vault.prototype.setLocalFileName = function(filename) {
	/// <summary>
	/// Sets local file name.
	/// </summary>
	/// <param name="filename" type="string"></param>
	return this.vault.setLocalFileName(filename);
};

Vault.prototype.deleteFile = function() {
	/// <summary>
	/// Delete the file from the local file system specified by the argument,
	/// which is a fully qualified path to the file.
	/// Is not supported because it works with files system directly.
	/// </summary>
	throw new Error('The method "deleteFile" from Vault API is not supported');
};

Vault.prototype.downloadFileAndExecute = function() {
	/// <summary>
	/// Method that downloads file from web url, save it in users working directory and if
	/// user want opens it with appropriate program. If the same file already exists at the
	/// working directory, file not downloading anew.
	/// Is not supported because it works with files system directly.
	/// </summary>
	throw new Error('The method "downloadFileAndExecute" from Vault API is not supported');
};

Vault.prototype.downloadFile = function(strUrl) {
	/// <summary>
	/// Download file from fileUrl to working directory with specified credentials and post data.
	/// To use this method call <see cref="M:SetLocalFileName"/>.
	/// </summary>
	/// <param name="strUrl" type="string">Url to download file from.</param>
	/// <returns>bool, true if file downloaded successfully, false otherwise.</returns>
	return this.vault.downloadFile(strUrl);
};

Vault.prototype.downloadFiles = function() {
	/// <summary>
	/// Download files from downloadFileList to working directory with specified credentials and post data.
	/// To use this method call <see cref="M:addFileToDownloadList"/>.
	/// Multiple files downloading is not supported. Use <see cref="M:downloadFile"/> instead.
	/// </summary>
	throw new Error('The method "downloadFiles" from Vault API is not supported');
};

Vault.prototype.fileCreate = function() {
	/// <summary>
	///  Creates or overwrites the specified file.
	/// Is not supported because it works directly with files system
	/// </summary>
	throw new Error('The method "fileCreate" from Vault API is not supported');
};

Vault.prototype.fileOpenAppend = function() {
	/// <summary>
	/// Opens file with access to append material to a file.
	/// Is not supported because it works directly with files system
	/// </summary>
	throw new Error('The method "fileOpenAppend" from Vault API is not supported');
};

Vault.prototype.fileOpenWrite = function() {
	/// <summary>
	/// Opens file with access to write material to a file.
	/// Is not supported because it works directly with files system
	/// </summary>
	throw new Error('The method "fileOpenWrite" from Vault API is not supported');
};

Vault.prototype.fileWriteLine = function() {
	/// <summary>
	/// Writes a string followed by a line terminator to the text stream.
	/// Is not supported because it works directly with files system
	/// </summary>
	throw new Error('The method "fileWriteLine" from Vault API is not supported');
};

Vault.prototype.fileClose = function() {
	/// <summary>
	/// Closes the readers and the writers and releases any system resources associated with the them.
	/// Is not supported because it works directly with files system
	/// </summary>
	throw new Error('The method "fileClose" from Vault API is not supported');
};

Vault.prototype.getFileSize = function(fileName) {
	/// <summary>
	///  Gets the size of the current file.
	/// </summary>
	/// <param name="fileName" type="string"></param>
	/// <returns></returns>
	return this.vault.getFileSize(fileName);
};

Vault.prototype.writeText = function() {
	/// <summary>
	/// If file exists, writes a string to the stream. Else creates files and writes text.
	/// Is not supported because it works directly with files system
	/// </summary>
	throw new Error('The method "writeText" from Vault API is not supported');
};

Vault.prototype.readText = function(path, encoding) {
	/// <summary>
	/// Reads the stream from the current position to the end of the stream.
	/// </summary>
	/// <param name="fname" type="string"></param>
	/// <param name="encoding" type="string">Parameter for System.Text.Encoding.GetEncoding method. "UTF-8" is default.</param>
	/// <returns>
	/// string. The rest of the stream as a string, from the current position to the end.
	/// If the current position is at the end of the stream, returns the empty string("").
	/// </returns>
	return this.vault.readText(path, encoding);
};

Vault.prototype.readBase64 = function(path, offset, count) {
	/// <summary>
	/// Reads the string of base64 encoding bytes from the offset position to the offset + count of the stream.
	/// </summary>
	/// <param name="offset" type="int">start position</param>
	/// <param name="count" type="int">count of bytes</param>
	/// <param name="fname" type="string">path</param>
	/// <returns>
	/// string. Specified count of base64 encoded bytes from specified offset
	/// </returns>
	return this.vault.readBase64(path, offset, count);
};

Vault.prototype.setFileFieldName = function() {
	/// <summary>
	/// Sets field name for file (send() method). Default value is <b>vault_file</b>
	/// Is not supported. Was used to set ID of a file that is sent through sendFile method
	/// </summary>
	throw new Error('The method "setFileFieldName" from Vault API is not supported');
};

Vault.prototype.getFileFieldName = function() {
	/// <summary>
	/// Gets current field name for file (send() method). Default value is <b>vault_file</b>
	/// Is not supported. Was used to get ID of a file that is sent through sendFile method
	/// </summary>
	throw new Error('The method "getFileFieldName" from Vault API is not supported');
};

Vault.prototype.urlEncode = function() {
	/// <summary>
	/// Encodes a URL string.
	/// Is not supported. Use native encodeURIComponent
	/// </summary>
	throw new Error('The method "urlEncode" from Vault API is not supported');
};

Vault.prototype.urlDecode = function() {
	/// <summary>
	/// Converts a string that has been encoded for transmission in a URL into a decoded string.
	/// Is not supported. Use native decodeURIComponent
	/// </summary>
	throw new Error('The method "urlDecode" from Vault API is not supported');
};

/*@cc_on
@if (@register_classes == 1)
Type.registerNamespace("Aras.Client.Controls.Public");
Aras.Client.Controls.Public.Vault = Vault;
Vault.registerClass("Aras.Client.Controls.Public.Vault");
@end
@*/
