﻿function ClientUrlsUtility(baseUrl) {
	if (!baseUrl) {
		throw new Error('Parameter baseUrl must be defined and be not empty.');
	}

	//(?:pattern) is a non-capturing match
	//the task is to capture path to Client folder and also capture "salt=..." if it is present
	var rex = new RegExp('(.*/Client)(?:/X-(salt=[^\\/]*)-X)?(?:\/|$)', 'i');
	var reResults = rex.exec(baseUrl);
	this._baseUrlWithoutSalt = reResults[1];
	this._salt = '';
	if (reResults[2]) {
		this._salt = reResults[2];
	}
}

ClientUrlsUtility.prototype.getBaseUrlWithoutSalt = function ClientUrlsUtilityGetBaseUrlWithoutSalt() {
	return this._baseUrlWithoutSalt;
};

ClientUrlsUtility.prototype.getSalt = function ClientUrlsUtilityGetSalt() {
	return this._salt;
};
