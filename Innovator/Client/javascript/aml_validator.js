﻿function AmlValidator(aras) {
	this.aras = aras;

	this.types = {};

	// XmlDocument that contains xslt transformation applied for each aml before validation.
	this.xslt = null;

	this.schemaGlobalObjects = [];

	this.xslt = this.aras.createXMLDocument();
	this.xslt.loadXML(
		'<?xml version="1.0" encoding="UTF-8"?>' +
		'<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">' +
		'  <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>' +
		'  <xsl:template match="/ | @* | node()">' +
		'    <xsl:choose>' +
		'      <xsl:when test="local-name() = \'OR\'">' +
		'        <xsl:call-template name="OR"/>' +
		'      </xsl:when>' +
		'      <xsl:when test="local-name() = \'NOT\'">' +
		'        <xsl:call-template name="NOT"/>' +
		'      </xsl:when>' +
		'      <xsl:when test="local-name() = \'Relationships\'">' +
		'        <xsl:call-template name="TransformRelationships"/>' +
		'      </xsl:when>' +
		'      <xsl:otherwise>' +
		'        <xsl:call-template name="doCopy"/>' +
		'      </xsl:otherwise>' +
		'    </xsl:choose>' +
		'  </xsl:template>' +
		'  <xsl:template name="NOT">' +
		'    <xsl:element name="NOT">' +
		'      <xsl:for-each select="*">' +
		'        <xsl:choose>' +
		'          <xsl:when test="local-name() = \'OR\'">' +
		'            <xsl:call-template name="OR"/>' +
		'          </xsl:when>' +
		'          <xsl:otherwise>' +
		'            <xsl:call-template name="doCopy"/>' +
		'          </xsl:otherwise>' +
		'        </xsl:choose>' +
		'      </xsl:for-each>' +
		'    </xsl:element>' +
		'  </xsl:template>' +
		'  <xsl:template name="OR">' +
		'    <xsl:variable name="firstNotORNode" select="*[local-name() != \'OR\']"/>' +
		'    <xsl:choose>' +
		'      <!-- ' +
		'      If OR contains not OR nodes. ' +
		'      1) Find first not OR node.' +
		'      2) Rename current OR node to OR-firstNotORNodeName.' +
		'      3) Rename all nested OR nodes to OR-firstNotORNodeName.' +
		'      -->' +
		'      <xsl:when test="count($firstNotORNode) > 0">' +
		'        <xsl:call-template name="createNewOR">' +
		'          <xsl:with-param name="newORName" select="local-name($firstNotORNode[1])"/>' +
		'        </xsl:call-template>' +
		'      </xsl:when>' +
		'      <!-- ' +
		'      Otherwise:' +
		'      1) If OR has OR child nodes, find the deepest OR node having not OR node element.' +
		'      2) If found such OR, rename it and all it"s parent OR nodes.' +
		'      3) If such OR not found, just copy structure.' +
		'      -->' +
		'      <xsl:when test="count(*[local-name() != \'OR\']) > 0">' +
		'        <xsl:call-template name="OR"/>' +
		'      </xsl:when>' +
		'      <xsl:otherwise>' +
		'        <xsl:call-template name="doCopy"/>' +
		'      </xsl:otherwise>' +
		'    </xsl:choose>' +
		'  </xsl:template>' +
		'  <xsl:template name="TransformRelationships">' +
		'    <xsl:element name="Relationships">' +
		'      <xsl:for-each select="*">' +
		'        <xsl:choose>' +
		'          <xsl:when test="local-name()=\'Item\'">' +
		'            <xsl:call-template name="createNewItem">' +
		'              <xsl:with-param name="newItemName" select="translate(@type, \' \', \'-\')"/>' +
		'            </xsl:call-template>' +
		'          </xsl:when>' +
		'          <xsl:otherwise>' +
		'            <xsl:call-template name="doCopy"/>' +
		'          </xsl:otherwise>' +
		'        </xsl:choose>' +
		'      </xsl:for-each>' +
		'    </xsl:element>' +
		'  </xsl:template>' +
		'  <xsl:template name="createNewItem">' +
		'    <xsl:param name="newItemName"/>' +
		'    <xsl:element name="Item-type-{$newItemName}-">' +
		'      <xsl:for-each select="@* | child::node()">' +
		'        <xsl:call-template name="doCopy"/>' +
		'      </xsl:for-each>' +
		'    </xsl:element>' +
		'  </xsl:template>' +
		'  <xsl:template name="createNewOR">' +
		'    <xsl:param name="newORName"/>' +
		'    <xsl:element name="OR-{$newORName}">' +
		'      <xsl:for-each select="child::node()">' +
		'       <xsl:choose>' +
		'          <xsl:when test="local-name(.) = \'OR\'">' +
		'            <xsl:call-template name="createNewOR">' +
		'              <xsl:with-param name="newORName" select="$newORName"/>' +
		'            </xsl:call-template>' +
		'          </xsl:when>' +
		'          <xsl:otherwise>' +
		'            <xsl:call-template name="doCopy"/>' +
		'          </xsl:otherwise>' +
		'        </xsl:choose>' +
		'      </xsl:for-each>' +
		'    </xsl:element>' +
		'  </xsl:template>' +
		'  <xsl:template name="doCopy">' +
		'    <xsl:copy>' +
		'      <xsl:apply-templates select="@* | node()"/>' +
		'    </xsl:copy>' +
		'  </xsl:template>' +
		'</xsl:stylesheet>');

	this._transformAmlBeforeValidation = function(amlToValidate) {
		var xd = this.aras.createXMLDocument();
		xd.validateOnParse = true;

		xd.loadXML(amlToValidate);
		xd.loadXML(xd.transformNode(this.xslt));
		return xd.xml;
	};

	this._transformXPath = function(XPath) {
		return XPath.replace(/\W/g, '-').replace(/[-]{2,}/g, '-');
	};

	this._getTypeOfLastItemInXPath = function(XPath) {
		/// <summary>
		/// Gets type of last Item in the XPath.
		/// </summary>
		/// <param name="XPath" type="string">Any XPath. Expected xpath like Item[@type="A"]/prop_item/Item[@type="B"]</param>
		/// <returns type="string">Type of last Item in the XPath or null.</returns>
		var res = /[@type=[''|""]([\w| ]*)[''|""]]$/.exec(XPath);
		if (res !== null) {
			res = res[1];
		}

		return res;
	};

	this._generatePropertyOR = function(orName, propName, propDT) {
		var res = '';
		res += '  <xs:complexType name="' + orName + '">';
		res += '    <xs:choice maxOccurs="unbounded">';
		res += this._generatePropertyDefinition(propName, propDT);
		res += '      <xs:element name="OR-' + propName + '" type="' + orName + '"/>';
		res += '    </xs:choice>';
		res += '  </xs:complexType>';
		return res;
	};

	this._generatePropertyDefinition = function(propName, propDT) {
		return '<xs:element name="' + propName + '" type="CONST-' + propDT.replace(' ', '-') + '"/>';
	};
}

AmlValidator.prototype.getRootItemXPath = function AmlValidatorGetRootItemXPath() {
	var rootItemXPath = '';
	for (var XPath in this.validationInfoObject) {
		rootItemXPath = XPath.split('/')[0];
		break;
	}
	return rootItemXPath;
};

AmlValidator.prototype.validate = function AmlValidatorValidate(amlToValidate, xmlSchemaCache) {
	/// <summary>
	/// Performs run-time validation on the aml  using the XMLSchemaCache object.
	/// </summary>
	/// <param name="amlToValidate" type="string">Any XPath. Expected xpath like Item[@type="A"]/prop_item/Item[@type="B"]</param>
	/// <param name="xmlSchemaCache" type="Object">XMLSchemaCache object containing validation schema.</param>
	/// <returns type="string">Returns empty string if validation passed successfully. Otherwise returns error message string.</returns>
	if (!amlToValidate || !xmlSchemaCache) {
		return '';
	}
	var schemas = [];
	schemas.push({namespace: '', xml: xmlSchemaCache});
	var res = this.aras.ValidateXml(schemas, this._transformAmlBeforeValidation(amlToValidate));
	var isValid = res.selectSingleNode('Result/isvalid').text == 'true';
	if (!isValid) {
		//validation failed
		return false;
	} else {
		//validation passed
		return true;
	}
};

AmlValidator.prototype.generateItemDefinition = function AmlValidatorGenerateItemDefinition(parentItemXPath, isRoot, withKeyedName) {
	if (!parentItemXPath) {
		return '';
	}

	var typeOfItem = this._getTypeOfLastItemInXPath(parentItemXPath);

	var newItemTagName = 'Item';
	var parentItemXPathSplitted = parentItemXPath.split('/');
	if (parentItemXPathSplitted.length > 2 && 'Relationships' == parentItemXPathSplitted[parentItemXPathSplitted.length - 2]) {
		newItemTagName = this._transformXPath(parentItemXPathSplitted[parentItemXPathSplitted.length - 1]);
	}

	var itemDefinition = [];
	itemDefinition.push('  <xs:element name="' + newItemTagName + '">');
	itemDefinition.push('    <xs:complexType>');

	this.generateInnerItemDefinition(parentItemXPath, itemDefinition, withKeyedName);
	this.generateItemAttributesDefinition(typeOfItem, itemDefinition, isRoot);

	itemDefinition.push('    </xs:complexType>');
	itemDefinition.push('  </xs:element>');

	return itemDefinition.join('');
};

AmlValidator.prototype.generateDefinitionForNotGroup = function AmlValidatorGenerateDefinitionForNotGroup(parentItemXPath, parentItemDef, withKeyedName) {
	var transformedParentItemXPath = this._transformXPath(parentItemXPath);

	var orGroupName = 'OR-Group-' + transformedParentItemXPath;
	var nameOfNotDefinition = 'NOT-' + transformedParentItemXPath;

	var groupDefinition = [];
	groupDefinition.push('<xs:complexType name="' + nameOfNotDefinition + '">');
	groupDefinition.push('  <xs:choice maxOccurs="unbounded">');
	groupDefinition.push('    <xs:group ref="' + orGroupName + '" maxOccurs="unbounded"/>');

	if (withKeyedName) {
		groupDefinition.push('    <xs:element name="keyed_name" type="CONST-string"/>');
	}

	var parentItemXPathDeep = parentItemXPath.split('/').length;

	for (var currentXPath in this.validationInfoObject) {
		if (currentXPath.indexOf(parentItemXPath) == -1) {
			continue;
		}

		var currentXPathArray = currentXPath.split('/');
		var currentXPathDeep = currentXPathArray.length;
		if (currentXPathDeep == parentItemXPathDeep + 1) {
			var propDef = this.validationInfoObject[currentXPath];
			var realPropDef = this.aras.getRealPropertyForForeignProperty(propDef);
			var propDataType = this.aras.getItemProperty(realPropDef, 'data_type');
			var propName = this.aras.getItemProperty(propDef, 'name');

			if ('item' != propDataType) {
				groupDefinition.push(this._generatePropertyDefinition(propName, propDataType));
			}
		}
	}

	groupDefinition.push('  </xs:choice>');
	groupDefinition.push('</xs:complexType>');

	this.schemaGlobalObjects.push(groupDefinition.join(''));

	return nameOfNotDefinition;
};

AmlValidator.prototype.generateItemPropertyDefinition = function AmlValidatorGenerateItemPropertyDefinition(propertyXPath, propName) {

	var itemPropertyArray = [];
	itemPropertyArray.push('<xs:element name="' + propName + '">');
	itemPropertyArray.push('  <xs:complexType>');
	itemPropertyArray.push('    <xs:choice minOccurs="0" maxOccurs="unbounded">');
	for (var currentXPath in this.validationInfoObject) {
		if (currentXPath.indexOf(propertyXPath) == -1) {
			continue;
		}

		var newItemXPath = '';
		var withKeyedName = false;

		if (currentXPath == propertyXPath) {
			var propDef = this.validationInfoObject[currentXPath];
			var realPropertyDef = this.aras.getRealPropertyForForeignProperty(propDef);
			var propertyDataSource = this.aras.getItemPropertyAttribute(realPropertyDef, 'data_source', 'name');
			var typeCriterion = '';
			if (propertyDataSource) {
				typeCriterion = '[@type=\'' + propertyDataSource + '\']';
			}

			newItemXPath = currentXPath + '/Item' + typeCriterion;
			if (!this.validationInfoObject[newItemXPath + '/keyed_name']) {
				withKeyedName = true;
			}
		} else {
			var currentXPathArray = currentXPath.split('/');
			var propertyXPathDeep = propertyXPath.split('/').length;

			var newItemXPathArray = [];
			for (var j = 0; j < propertyXPathDeep; j++) {
				newItemXPathArray.push(currentXPathArray[j]);
			}

			newItemXPath = newItemXPathArray.join('/');
			// Exclude situation when
			// currentXPath = "Item[@type="InBasket Task"]/container_type_id" and
			// propertyXPath = "Item[@type="InBasket Task"]/container"
			if (newItemXPath.length > 0 && newItemXPath != propertyXPath) {
				continue;
			}

			newItemXPath += '/' + currentXPathArray[propertyXPathDeep];

			withKeyedName = false;
			if (this.validationInfoObject[newItemXPath] && !this.validationInfoObject[newItemXPath + '/keyed_name']) {
				withKeyedName = true;
				this.aras.deletePropertyFromObject(this.validationInfoObject, newItemXPath);
			}
		}

		itemPropertyArray.push(this.generateItemDefinition(newItemXPath, false, withKeyedName));
	}
	itemPropertyArray.push('    </xs:choice>');
	itemPropertyArray.push('    <xs:attribute name="condition" type="' + this.types.item + '" use="optional"/>');
	itemPropertyArray.push('  </xs:complexType>');
	itemPropertyArray.push('</xs:element>');
	return itemPropertyArray.join('');
};

AmlValidator.prototype.generateInnerItemDefinition = function AmlValidatorGenerateInnerItemDefinition(parentItemXPath, parentItemDef, withKeyedName) {
	var transformedParentItemXPath = this._transformXPath(parentItemXPath);

	var orName = 'OR-' + transformedParentItemXPath;
	var orGroupName = 'OR-Group-' + transformedParentItemXPath;

	var notName = this.generateDefinitionForNotGroup(parentItemXPath, parentItemDef, withKeyedName);

	this.schemaGlobalObjects.push('<xs:complexType name="' + orName + '">');
	this.schemaGlobalObjects.push('  <xs:choice maxOccurs="unbounded">');
	this.schemaGlobalObjects.push('    <xs:group ref="' + orGroupName + '" maxOccurs="unbounded"/>');
	this.schemaGlobalObjects.push('  </xs:choice>');
	this.schemaGlobalObjects.push('</xs:complexType>');

	var orGroupDefinition = [];
	orGroupDefinition.push('<xs:group name="' + orGroupName + '">');
	orGroupDefinition.push('  <xs:choice>');
	orGroupDefinition.push('    <xs:element name="OR" type="' + orName + '"/>');
	orGroupDefinition.push('    <xs:element name="NOT" type="' + notName + '"/>');

	var parentItemXPathDeep = parentItemXPath.split('/').length;

	parentItemDef.push('      <xs:choice minOccurs="0" maxOccurs="unbounded">');

	if (withKeyedName) {
		parentItemDef.push('      <xs:element name="keyed_name" type="CONST-string"/>');
		orGroupDefinition.push('    <xs:element name="OR-keyed_name" type="CONST-OR-keyed_name"/>');
	}

	for (var currentXPath in this.validationInfoObject) {
		if (currentXPath.indexOf(parentItemXPath) == -1) {
			continue;
		}

		var propName;
		var currentXPathArray = currentXPath.split('/');
		var currentXPathDeep = currentXPathArray.length;
		if (currentXPathDeep == parentItemXPathDeep + 1) {
			var propDef = this.validationInfoObject[currentXPath];
			var realPropDef = this.aras.getRealPropertyForForeignProperty(propDef);
			var propDT = this.aras.getItemProperty(realPropDef, 'data_type');
			propName = this.aras.getItemProperty(propDef, 'name');

			if ('item' != propDT) {
				var orTypeName = 'OR-' + transformedParentItemXPath + propName;

				parentItemDef.push(this._generatePropertyDefinition(propName, propDT));
				orGroupDefinition.push('<xs:element name="OR-' + propName + '" type="' + orTypeName + '"/>');

				this.schemaGlobalObjects.push(this._generatePropertyOR(orTypeName, propName, propDT));
			} else {
				parentItemDef.push(this.generateItemPropertyDefinition(parentItemXPath + '/' + propName, propName));
			}
		} else {
			propName = currentXPathArray[parentItemXPathDeep];
			parentItemDef.push(this.generateItemPropertyDefinition(parentItemXPath + '/' + propName, propName));
		}

		this.aras.deletePropertyFromObject(this.validationInfoObject, currentXPath);
	}

	orGroupDefinition.push('  </xs:choice>');
	orGroupDefinition.push('</xs:group>');

	this.schemaGlobalObjects.push(orGroupDefinition.join(''));

	parentItemDef.push('        <xs:group ref="' + orGroupName + '"/>');
	parentItemDef.push('      </xs:choice>');
};

AmlValidator.prototype.generateConditionDefinition = function AmlValidatorGenerateConditionDefinition(conditionNum, allowedValuesArr) {
	var conditionDefinitions = [];
	conditionDefinitions.push('  <xs:simpleType name="CONST-Condition-' + conditionNum + '">');
	conditionDefinitions.push('    <xs:restriction base="xs:string">');

	for (var i = 0; i < allowedValuesArr.length; i++) {
		conditionDefinitions.push('      <xs:enumeration value="' + allowedValuesArr[i] + '"/>');
	}

	conditionDefinitions.push('    </xs:restriction>');
	conditionDefinitions.push('  </xs:simpleType>');
	return conditionDefinitions.join('');
};

AmlValidator.prototype.generateItemAttributesDefinition = function AmlValidatorGenerateItemAttributesDefinition(typeOfItem, itemDefinition, isRoot) {
	itemDefinition.push('      <xs:attribute name="action" type="action-attribute" use="required"/>');
	if (typeOfItem) {
		itemDefinition.push('      <xs:attribute name="type" use="required">');
		itemDefinition.push('        <xs:simpleType>');
		itemDefinition.push('          <xs:restriction base="xs:string">');
		itemDefinition.push('            <xs:enumeration value="' + typeOfItem + '"/>');
		itemDefinition.push('          </xs:restriction>');
		itemDefinition.push('        </xs:simpleType>');
		itemDefinition.push('      </xs:attribute>');
	}

	if (isRoot) {
		itemDefinition.push('      <xs:attribute name="typeId" use="optional">');
		itemDefinition.push('        <xs:simpleType>');
		itemDefinition.push('          <xs:restriction base="xs:string">');
		itemDefinition.push('            <xs:length value="32"/>');
		itemDefinition.push('          </xs:restriction>');
		itemDefinition.push('        </xs:simpleType>');
		itemDefinition.push('      </xs:attribute>');
		itemDefinition.push('      <xs:attribute name="page" type="integer-as-string" use="optional" />');
		itemDefinition.push('      <xs:attribute name="pagesize" type="integer-as-string" use="optional" />');
		itemDefinition.push('      <xs:attribute name="maxRecords" type="integer-as-string" use="optional" />');
		itemDefinition.push('      <xs:attribute name="returnMode" type="xs:string" use="optional" />');
		itemDefinition.push('      <xs:attribute name="select" type="xs:string" use="optional"/>');
		itemDefinition.push('      <xs:attribute name="queryType" type="xs:string" use="optional"/>');
		itemDefinition.push('      <xs:attribute name="queryDate" type="xs:string" use="optional"/>');

		if (this.aras.getVariable('SortPages') == 'true') {
			itemDefinition.push('      <xs:attribute name="order_by" type="xs:string" use="optional"/>');
		}
	}
};

AmlValidator.prototype.generateConditions = function AmlValidatorGenerateConditions() {
	var res = '';
	res += this.generateConditionDefinition(0, new Array('eq', 'ne'));
	res += this.generateConditionDefinition(1, new Array('is null', 'is not null'));
	res += this.generateConditionDefinition(2, new Array('eq', 'ne', 'is null', 'is not null'));
	res += this.generateConditionDefinition(3, new Array('eq', 'ne', 'like', 'not like', 'is null', 'is not null'));
	res += this.generateConditionDefinition(4, new Array('eq', 'ne', 'lt', 'gt', 'le', 'ge', 'is null', 'is not null'));
	res += this.generateConditionDefinition(5, new Array('eq', 'ne', 'lt', 'gt', 'le', 'ge', 'is null', 'is not null', 'between', 'not between'));

	return res;
};

AmlValidator.prototype.generateTypeDefinition = function AmlValidatorGenerateTypeDefinition(innovatorTypesArr, typeName, conditionType, baseTypeName) {
	this.types[typeName] = conditionType;
	if ('integer' == typeName && !baseTypeName) {
		baseTypeName = 'integer-or-empty-string';
		innovatorTypesArr.push('  <xs:simpleType name="' + baseTypeName + '">');
		innovatorTypesArr.push('    <xs:restriction base="xs:string">');
		innovatorTypesArr.push('      <xs:pattern value="([-+]?[0-9]+)?"/>');
		innovatorTypesArr.push('    </xs:restriction>');
		innovatorTypesArr.push('  </xs:simpleType>');
	}

	if (!baseTypeName) {
		baseTypeName = 'xs:string';
	}

	innovatorTypesArr.push('  <xs:complexType name="CONST-' + typeName + '">');
	innovatorTypesArr.push('    <xs:simpleContent>');
	innovatorTypesArr.push('      <xs:extension base="' + baseTypeName + '">');
	innovatorTypesArr.push('        <xs:attribute name="condition" type="' + conditionType + '"/>');
	innovatorTypesArr.push('      </xs:extension>');
	innovatorTypesArr.push('    </xs:simpleContent>');
	innovatorTypesArr.push('  </xs:complexType>');
};

AmlValidator.prototype.generateTypes = function AmlValidatorGenerateTypes() {
	var typesArray = [];
	this.generateTypeDefinition(typesArray, 'boolean', 'CONST-Condition-0', 'xs:boolean');
	this.generateTypeDefinition(typesArray, 'color', 'CONST-Condition-3');
	this.generateTypeDefinition(typesArray, 'color-list', 'CONST-Condition-2');
	this.generateTypeDefinition(typesArray, 'date', 'CONST-Condition-5');
	this.generateTypeDefinition(typesArray, 'decimal', 'CONST-Condition-4', 'xs:decimal');
	this.generateTypeDefinition(typesArray, 'federated', 'CONST-Condition-3');
	this.generateTypeDefinition(typesArray, 'filter-list', 'CONST-Condition-2');
	this.generateTypeDefinition(typesArray, 'float', 'CONST-Condition-4', 'xs:float');
	this.generateTypeDefinition(typesArray, 'formatted-text', 'CONST-Condition-3');
	this.generateTypeDefinition(typesArray, 'image', 'CONST-Condition-3');
	this.generateTypeDefinition(typesArray, 'integer', 'CONST-Condition-4');
	this.generateTypeDefinition(typesArray, 'item', 'CONST-Condition-3');
	this.generateTypeDefinition(typesArray, 'list', 'CONST-Condition-2');
	this.generateTypeDefinition(typesArray, 'md5', 'CONST-Condition-2');
	this.generateTypeDefinition(typesArray, 'ml_string', 'CONST-Condition-3');
	this.generateTypeDefinition(typesArray, 'mv_list', 'CONST-Condition-4');
	this.generateTypeDefinition(typesArray, 'sequence', 'CONST-Condition-4');
	this.generateTypeDefinition(typesArray, 'string', 'CONST-Condition-3');
	this.generateTypeDefinition(typesArray, 'text', 'CONST-Condition-3');
	this.generateTypeDefinition(typesArray, 'global_version', 'CONST-Condition-5', 'unsigned-big-integer-or-empty-string');
	this.generateTypeDefinition(typesArray, 'ubigint', 'CONST-Condition-5', 'unsigned-big-integer-or-empty-string');

	return typesArray.join('');
};

AmlValidator.prototype.generateSchema = function AmlValidatorGenerateSchema(validationInfoObject) {
	this.validationInfoObject = validationInfoObject;

	this.schemaGlobalObjects = [];
	this.schemaGlobalObjects.push('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>');
	this.schemaGlobalObjects.push('<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema" elementFormDefault="qualified">');
	this.schemaGlobalObjects.push(this.generateTypes());
	this.schemaGlobalObjects.push(this.generateConditions());
	this.schemaGlobalObjects.push(this.generateItemDefinition(this.getRootItemXPath(), true, false));
	this.schemaGlobalObjects.push('  <xs:complexType name="CONST-OR-keyed_name">');
	this.schemaGlobalObjects.push('    <xs:choice maxOccurs="unbounded">');
	this.schemaGlobalObjects.push('      <xs:element name="keyed_name" type="CONST-string"/>');
	this.schemaGlobalObjects.push('      <xs:element name="OR-keyed_name" type="CONST-OR-keyed_name"/>');
	this.schemaGlobalObjects.push('    </xs:choice>');
	this.schemaGlobalObjects.push('  </xs:complexType>');
	this.schemaGlobalObjects.push('  <xs:simpleType name="integer-as-string">');
	this.schemaGlobalObjects.push('    <xs:restriction base="xs:string">');
	this.schemaGlobalObjects.push('      <xs:pattern value="|\\d*"/>');
	this.schemaGlobalObjects.push('    </xs:restriction>');
	this.schemaGlobalObjects.push('  </xs:simpleType>');
	this.schemaGlobalObjects.push('  <xs:simpleType name="unsigned-big-integer-or-empty-string">');
	this.schemaGlobalObjects.push('    <xs:restriction base="xs:string">');
	this.schemaGlobalObjects.push('      <xs:pattern value="([0-9]){0,20}"/>');
	this.schemaGlobalObjects.push('    </xs:restriction>');
	this.schemaGlobalObjects.push('  </xs:simpleType>');
	this.schemaGlobalObjects.push('  <xs:simpleType name="action-attribute">');
	this.schemaGlobalObjects.push('    <xs:restriction base="xs:string">');
	this.schemaGlobalObjects.push('      <xs:enumeration value="get"/>');
	this.schemaGlobalObjects.push('    </xs:restriction>');
	this.schemaGlobalObjects.push('  </xs:simpleType>');
	this.schemaGlobalObjects.push('</xs:schema>');

	return this.schemaGlobalObjects.join('');
};
/*@cc_on
@if (@register_classes == 1)
Type.registerNamespace("Aras");
Type.registerNamespace("Aras.Client");
Type.registerNamespace("Aras.Client.JS");

Aras.Client.JS.AmlValidator = AmlValidator;
Aras.Client.JS.AmlValidator.registerClass("Aras.Client.JS.AmlValidator");
@end
@*/
