﻿// (c) Copyright by Aras Corporation, 2004-2013.
var currQryItem;
var currItemType = null;
var itemTypeID = '';
var itemTypeName = '';
var itemTypeLabel = '';
var visiblePropNds;
var userMethodColumnCfgs = {}; //to support OnSearchDialog grid event
var searchLocation = '';

var xmlReadyFlag = false;
var promiseCountResult;
const INPUT_ROW_ID = 'input_row';

function initPage(isPopup) {
	var criteriaName;
	var criteriaValue;

	if (itemTypeID) {
		//itemTypeID has higher priority because of poly items
		criteriaName = 'id';
		criteriaValue = itemTypeID;
	} else if (itemTypeName) {
		criteriaName = 'name';
		criteriaValue = itemTypeName;
	} else {
		aras.AlertError(aras.getResource('', 'search.neither_input_item_type_name_nor_id_specified'));
		if (isPopup) {
			window.close();
		}
		return false;
	}

	var iomItemType = aras.getItemTypeForClient(criteriaValue, criteriaName);
	if (iomItemType.isError()) {
		if (isPopup) {
			window.close();
		}
		return false;
	}

	currItemType = iomItemType.node;
	itemTypeID = currItemType.getAttribute('id');
	itemTypeName = aras.getItemProperty(currItemType, 'name');
	itemTypeLabel = aras.getItemProperty(currItemType, 'label');
	if (!itemTypeLabel) {
		itemTypeLabel = itemTypeName;
	}

	currQryItem = aras.newQryItem(itemTypeName);

	visiblePropNds = [];

	visiblePropNds = aras.getvisiblePropsForItemType(currItemType);
	aras.uiInitItemsGridSetups(currItemType, visiblePropNds);
}

function initToolbar() {
	if (!window.searchbar) {
		return;
	}

	const searchToolbar = searchbar.getActiveToolbar();
	if (!multiselect) {
		searchToolbar.getItem('select_all').disable();
	}

	if (aras.isPolymorphic(currItemType)) {
		let cb = searchToolbar.getItem('implementation_type');
		cb.removeAll();
		cb.Add(itemTypeID, itemTypeLabel);
		const morphae = aras.getMorphaeList(currItemType);
		for (let i = 0; i < morphae.length; i++) {
			const m = morphae[i];
			cb.Add(m.id, m.label);
		}
		cb = null;
		searchToolbar.showItem('implementation_type');
	} else {
		searchToolbar.hideItem('implementation_type');
	}
}

function updatePagination() {
	pagination.updateControlsStateByDefault();
	const isAppend = (aras.getPreferenceItemProperty('Core_GlobalLayout', null, 'core_append_items') === 'true');
	if (!isAppend) {
		return Promise.resolve();
	}

	const nextButtonState = pagination.getItem('pagination_next_button').disabled;
	pagination.setItemEnabled('pagination_prev_button', false);
	pagination.setItemEnabled('pagination_next_button', !nextButtonState && pagination.itemsCount !== pagination.totalResults);

	return pagination.render();
}

function doSearch() {
	if (searchContainer) {
		searchContainer.runSearch();
	} else {
		setTimeout(function() {
			doSearch();
		}, 50);
	}
}

function doSelectAll() {
	statusId = aras.showStatusMessage('status', aras.getResource('', 'search.selecting_all'), '../images/Progress.gif');

	grid.selectAll();
	var ids = grid.GetSelectedItemIDs();
	if (ids.length > 0 && onSelectItem) {
		onSelectItem(ids[0]);
	}

	aras.clearStatusMessage(statusId);
}

function onSelectItem() {
}

function cacheValue(value, cacheKey) {
	if (value && !currentSearchMode.getCacheItem(cacheKey)) {
		currentSearchMode.setCacheItem(cacheKey, value);
	}
}

function setupPageNumber(anyItem) {
	if (!anyItem) {
		anyItem = currQryItem.getResponseDOM().selectSingleNode('//Item');
	}
	let pagemax = -1;
	let itemmax;
	const itemsWithNoAccesCount = currQryItem.getResponse().getMessageValue('items_with_no_access_count');
	if (itemsWithNoAccesCount) {
		currentSearchMode.setCacheItem('itemsWithNoAccessCount', parseInt(itemsWithNoAccesCount));
	}
	if (!anyItem) {
		return;
	}

	const pagesize = currQryItem.getPageSize();
	if (pagesize === '-1') {
		pagemax = 1;
		itemmax = currQryItem.getResultDOM().selectNodes('/' + SoapConstants.EnvelopeBodyXPath + '/Result/Item').length;
	} else {
		pagemax = anyItem.getAttribute('pagemax');
		itemmax = anyItem.getAttribute('itemmax');
	}
	currentSearchMode.setCacheItem('criteriesHash', ArasModules.utils.hashFromString(currQryItem.getCriteriesString()));
	cacheValue(itemmax, 'itemmax');
	cacheValue(pagemax, 'pagemax');
}

function setupGrid(isGridInitXml, doNotRenderRows) {
	var isRelationshipsGrid = (searchLocation === 'Relationships Grid');
	const isMainGrid = (searchLocation === 'Main Grid');
	const isSearchDialog = (searchLocation === 'Search Dialog');
	var resDom = currQryItem.getResultDOM();
	if (!resDom) {
		return Promise.resolve();
	}

	var itTypeId = '';
	if (isRelationshipsGrid) {
		itTypeId = aras.getRelationshipTypeId(window['RelType_Nm']);
		syncWithClient(resDom);
		if (window.pagination) {
			pagination.itemsCount = currQryItem.getResultDOM().selectNodes('/' + SoapConstants.EnvelopeBodyXPath + '/Result/Item').length;
		}
	} else {
		itTypeId = aras.getItemTypeId(itemTypeName);
		currQryItem.syncWithClient();
	}

	var gridXml = '';
	var params = aras.newObject();
	params['only_rows'] = !isGridInitXml;
	params.multiselect = isRelationshipsGrid || isMainGrid || window.multiselect;
	if (isRelationshipsGrid && aras.getVariable('infernoRelationshipGrid') !== 'true') {
		const columnObjects = aras.uiPrepareDOM4XSLT(resDom, itTypeId, 'RT_');
		params['enable_links'] = !isEditMode;
		params.columnObjects = columnObjects;
		params.enableFileLinks = true;
		params.bgInvert = true;
		if (window['RelatedItemType_ID']) {
			params[window['RelatedItemType_ID']] = '';
		}

		var tableNd = resDom.selectSingleNode(aras.XPathResult('/table'));
		tableNd.setAttribute('editable', (isEditMode ? 'true' : 'false'));

		gridXml = aras.uiGenerateRelationshipsGridXML(resDom, DescByVisibleProps, RelatedVisibleProps, window['DescByItemType_ID'], params, true);
	}

	if (isGridInitXml) {
		if (isMainGrid || isSearchDialog) {
			const userPreferencesItem = aras.getPreferenceItem('Core_ItemGridLayout', itemTypeID);
			return window.cuiGrid(grid._grid, {
				itemTypeId: itemTypeID,
				userPreferences: userPreferencesItem,
				gridWrapper: grid
			}).then(function() {
				if (window.setFrozenColumns) {
					window.setFrozenColumns.call(grid);
				}
				if (window.previewPane) {
					const userPreviewMode = aras.getItemProperty(userPreferencesItem, 'preview_state', 'Off');
					previewPane.setType(userPreviewMode);
				}
				xmlReadyFlag = true;
			});
		}

		if (aras.getVariable('infernoRelationshipGrid') === 'true') {
			const userPreferencesRelItem = aras.getPreferenceItem('Core_RelGridLayout', RelType_ID);
			const relationshipTypeNode = aras.getRelationshipType(RelType_ID).node;
			const relationshipTypeNodeJson = ArasModules.xmlToODataJson('<Result>' + relationshipTypeNode.xml + '</Result>').Result;

			return window.cuiGrid(grid._grid, {
				itemTypeId: relationshipTypeNodeJson.relationship_id,
				userPreferences: userPreferencesRelItem,
				relatedItemTypeId: relationshipTypeNodeJson.related_id,
				gridWrapper: grid
			}).then(function() {
				xmlReadyFlag = true;
			});
		}

		if (!gridXml) {
			gridXml = aras.uiGenerateItemsGridXML(resDom, visiblePropNds, itTypeId, params);
		}
		grid['InitXML_Experimental'](gridXml, doNotRenderRows);

	} else {
		if (isMainGrid || isSearchDialog || aras.getVariable('infernoRelationshipGrid') === 'true') {
			if (grid.getRowCount() > 0) {
				grid.removeAllRows();
			}
			const rowsInfo = window.adaptGridRows(currQryItem.getResult(), {
				headMap: grid._grid.head,
				indexHead: grid._grid.settings.indexHead
			});
			grid._grid.rows._store = rowsInfo.rowsMap;
			grid._grid.settings.indexRows = rowsInfo.indexRows;
			grid._grid.render();
			grid.gridXmlLoaded(true);
		} else {
			if (grid.getRowCount() === 0) {
				grid.addXMLRows(gridXml);
			} else {
				grid['InitXMLRows_Experimental'](gridXml);
			}
		}
	}

	xmlReadyFlag = true;
	return Promise.resolve();
}

function createEmptyResultDom() {
	var resDom = aras.createXMLDocument();
	resDom.loadXML(SoapConstants.EnvelopeBodyStart + '<Result/>' + SoapConstants.EnvelopeBodyEnd);
	return resDom;
}

onbeforeunload = function onbeforeunloadHandler() {
	window.isOnBeforeUnload = true;

	saveSetups();

	window.isOnBeforeUnload = false;
};

function saveSetups() {
	if (!xmlReadyFlag) {
		return;
	}

	if (searchContainer) {
		searchContainer.onEndSearchContainer();
	}

	const varsHash = {};
	const isRelationshipsGrid = (searchLocation === 'Relationships Grid');
	const currToolBar = (isRelationshipsGrid ? document.toolbar : currentSearchMode.searchToolbar);

	const maxResults = pagination.maxResults;
	const ps = pagination.pageSize;
	if (ps || ps === 0) {
		varsHash['page_size'] = ps || '';
	}
	if (maxResults || maxResults === 0) {
		varsHash['max_records'] = maxResults || '';
	}

	if (currToolBar && isMainGrid && isVersionableIT) {
		const queryType = currToolBar.data.get('searchview.commandbar.default.querytype').value;
		let queryDate = currToolBar.data.get('searchview.commandbar.default.querydate').value;
		queryDate = aras.convertToNeutral(queryDate, 'date', GetDatePattern(queryType));
		varsHash['query_type'] = queryType;
		if (queryType !== 'Current') {
			aras.setVariable(window['varName_queryDate'], queryDate);
		}
	}

	varsHash['col_widths'] = window.grid.GetColWidths();
	varsHash['col_order'] = window.grid.getLogicalColumnOrder();

	let tmpId;
	let tmpTypeNm;
	if (isRelationshipsGrid) {
		tmpId = aras.getRelationshipTypeId(window['RelType_Nm']);
		tmpTypeNm = 'Core_RelGridLayout';
	} else {
		if (window.previewPane) {
			varsHash['preview_state'] = window.previewPane.getType();
		}

		tmpId = aras.getItemTypeId(itemTypeName);
		tmpTypeNm = 'Core_ItemGridLayout';
		if (window.grid._grid && window.grid._grid.view.defaultSettings.freezableColumns) {
			varsHash['frozen_columns'] = String(window.grid._grid.settings.frozenColumns);
		}
	}
	aras.setPreferenceItemProperties(tmpTypeNm, tmpId, varsHash);
}

function onLink(itemTypeName, itemID) {
	aras.uiShowItem(itemTypeName, itemID);
}

function startCellEditCommon(rowId, field) {
	if (INPUT_ROW_ID === rowId) {
		setupFilteredListIfNeed(rowId, field);
	}
}

function applyCellEditCommon(rowId, field) {
	if (INPUT_ROW_ID === rowId && searchReady) {
		currentSearchMode.setPageNumber(1);

		const grid = this;
		const columnIndex = this.GetColumnIndex(field);
		const inputCell = this['grid_Experimental'].inputRowCollections[field] || {get: function() {},
			focus: function() {
				grid._grid.settings.focusedCell = {rowId: 'searchRow', headId: field};
				grid._oldSearchHeadId = field;
			}
		};
		const criteria = this.inputRow.get(field, 'value');
		const useWildcards = (aras.getPreferenceItemProperty('Core_GlobalLayout', null, 'core_use_wildcards') == 'true');
		const condition = (useWildcards ? 'like' : 'eq');

		const propDef = searchContainer.getPropertyDefinitionByColumnIndex(columnIndex);
		const propXpath = searchContainer.getPropertyXPathByColumnIndex(columnIndex);

		if (currentSearchMode.setSearchCriteria(propDef, propXpath, criteria, condition)) {
			inputCell._lastValueReported = inputCell.get('value');
		} else {
			inputCell._lastValueReported = '';
			if (aras.confirm(aras.getResource('', 'search.invalid_criteria'))) {
				inputCell.focus();
			} else {
				currentSearchMode.removeSearchCriteria(propXpath);
				this.inputRow.set(field, 'value', '');
			}
		}
	}
}

function setupFilteredListIfNeed(rowID, field) {
	if (INPUT_ROW_ID !== rowID) {
		return;
	}
	var col = window.grid['columns_Experimental'].get(field, 'index');

	var propNd = visiblePropNds[col - 1];
	if (!propNd || !propNd.xml) {
		return;
	}

	var propName = aras.getItemProperty(propNd, 'name');
	var propDataType = aras.getItemProperty(propNd, 'data_type');
	if (propDataType != 'filter list') {
		return;
	}

	var propPatternName = aras.getItemProperty(propNd, 'pattern');
	if (!propPatternName) {
		return;
	}

	var patternColIndexArr = [];
	for (var i = 0; j < visiblePropNds.length; i++) {
		var curPropName = aras.getItemProperty(visiblePropNds[i], 'name');
		if (curPropName == propPatternName) {
			patternColIndexArr.push(i);
		}
	}

	for (var j = 0; j < patternColIndexArr.length; j++) {
		var filterValue = window.grid.inputRow.get(patternColIndexArr[j] + 1, 'value') || '';
		var resObj = aras.uiGetFilteredObject4Grid(itemTypeID, propName, filterValue);
		if (resObj.hasError) {
			return;
		}

		window.grid.inputRow.set(col, 'comboList', resObj.labels, resObj.values);
	}
}

function removeFilterListValueIfNeed(rowID, field) {
	if (INPUT_ROW_ID !== rowID) {
		return;
	}
	var col = grid['columns_Experimental'].get(field, 'index');

	var propNd = visiblePropNds[col - 1];
	if (!propNd || !propNd.xml) {
		return;
	}

	var propName = aras.getItemProperty(propNd, 'name');
	var propDataType = aras.getItemProperty(propNd, 'data_type');
	if (propDataType != 'filter list') {
		return;
	}

	var filteredColIndexArr = [];
	var j;
	for (j = 0; j < visiblePropNds.length; j++) {
		var curPropPattern = aras.getItemProperty(visiblePropNds[j], 'pattern');
		if (curPropPattern == propName) {
			filteredColIndexArr.push(j);
		}
	}

	for (j = 0; j < filteredColIndexArr.length; j++) {
		grid.inputRow.set(filteredColIndexArr[j] + 1, 'value', '');
	}
}

function getAndUpdatePageSizeAndMaxRecords() {
	currentSearchMode.removeCacheItem('itemmax');
	currentSearchMode.removeCacheItem('pagemax');
	currentSearchMode.removeCacheItem('criteriesHash');
	const pagesize = currQryItem.getPageSize();
	const maxRecords = currQryItem.getMaxRecords();
	const item = aras.newIOMItem(currQryItem.itemTypeName, 'get');
	item.setAttribute('returnMode', 'countOnly');
	item.setAttribute('select', 'id');
	item.setAttribute('pagesize', pagesize);
	if (maxRecords) {
		item.setAttribute('maxRecords', maxRecords);
	}
	const criteries = currQryItem.dom.selectNodes('/Item/*');
	for (let i = 0; i < criteries.length; i++) {
		item.dom.firstChild.appendChild(criteries[i].cloneNode(true));
	}
	promiseCountResult = Promise.resolve(aras.soapSend('ApplyItem', item.node.xml).results);
	return promiseCountResult
		.then(parseAnswerGetCount);
}

function parseAnswerGetCount(res) {
	promiseCountResult = null;
	if (aras.hasFault(res)) {
		aras.AlertError(res);
		return;
	}
	res = aras.getMessageNode(res);
	const itemmax = res.selectSingleNode('event[@name="itemmax"]').getAttribute('value');
	const pagemax = res.selectSingleNode('event[@name="pagemax"]').getAttribute('value');
	currentSearchMode.setCacheItem('criteriesHash', ArasModules.utils.hashFromString(currQryItem.getCriteriesString()));
	currentSearchMode.setCacheItem('itemmax', itemmax);
	currentSearchMode.setCacheItem('pagemax', pagemax);
	currentSearchMode.setCacheItem('itemsWithNoAccessCount', parseInt(res.selectSingleNode('event[@name="items_with_no_access_count"]').getAttribute('value')));
	return Promise.resolve(itemmax);
}
