﻿using DeploymentProcedure.Components.Base;
using DeploymentProcedure.Components.Type;
using DeploymentProcedure.Logging;
using DeploymentProcedure.Packages;
using DeploymentProcedure.Utility;
using DeploymentProcedure.Utility.Base;
using Microsoft.Web.Administration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using System.Threading;

namespace DeploymentProcedure.Components
{
	[XmlType("web")]
	public class WebComponent : CodeTreeComponent
	{
		private string _url;

		public string VirtualDirectoryPath { get; set; }
		public string ApplicationPoolName { get; set; }
		public string ManagedRuntimeVersion { get; set; } = "v4.0";
		public string ManagedPipelineMode { get; set; } = "Integrated";
		public string SiteName { get; set; } = "Default web Site";
		public string Protocol { get; set; } = "http";
		public string ApplicationPoolUser { get; set; }
		public SecretString ApplicationPoolUserPassword { get; set; }
		public string Url
		{
			get { return _url ?? string.Format("{0}://{1}{2}", Protocol, ServerName, VirtualDirectoryPath); }
			set { _url = value; }
		}

		#region Implementing Setup logic
		public override void Setup()
		{
			base.Setup();

			SetupApplicationPool(ApplicationPoolName, ManagedRuntimeVersion, ManagedPipelineMode);
			SetupApplication(SiteName, VirtualDirectoryPath, ApplicationPoolName, InstallationPath);
		}

		public void SetupApplicationPool(string apppoolName, string managedRuntimeVersion, string managedPipelineMode)
		{
			using (ServerManager serverManager = ServerManager.OpenRemote(ServerName))
			{
				ApplicationPool appPool = serverManager.ApplicationPools[apppoolName];
				if (appPool == null)
				{
					Logger.Instance.Log(LogLevel.Info, "\tCreating new '{0}' application pool on '{1}'...", apppoolName, ServerName);

					appPool = serverManager.ApplicationPools.Add(apppoolName);
				}
				else
				{
					Logger.Instance.Log(LogLevel.Info, "\tRecycling '{0}' application pool on '{1}'...", apppoolName, ServerName);

					appPool.Recycle();
				}

				Logger.Instance.Log(LogLevel.Info, "\tSetting up '{0}' managed runtime version and '{1}' managed pipeline mode '{2}' application pool.",
					managedRuntimeVersion, managedPipelineMode, apppoolName);

				appPool.ManagedRuntimeVersion = managedRuntimeVersion;
				appPool.ManagedPipelineMode = ParseManagedPipelineMode(managedPipelineMode);


				if (!string.IsNullOrEmpty(ApplicationPoolUser) && ApplicationPoolUserPassword != null)
				{
					Logger.Instance.Log(LogLevel.Info, "\tSetting up '{0}' user for '{1}' application pool.", ApplicationPoolUser, apppoolName);

					appPool.ProcessModel.IdentityType = ProcessModelIdentityType.SpecificUser;
					appPool.ProcessModel.UserName = ApplicationPoolUser;
					appPool.ProcessModel.Password = ApplicationPoolUserPassword.Value;
				}

				serverManager.CommitChanges();

				Logger.Instance.Log(LogLevel.Info, "\tChanges were commited.");
			}
		}

		public void SetupApplication(string siteName, string virtualDirectoryName, string apppoolName, string physicalPath)
		{
			using (ServerManager serverManager = ServerManager.OpenRemote(ServerName))
			{
				Application app = serverManager.Sites[siteName].Applications[virtualDirectoryName];
				if (app != null)
				{
					Logger.Instance.Log(LogLevel.Info, "\tRemoving '{0}' application from '{1}'...", virtualDirectoryName, ServerName);

					serverManager.Sites[SiteName].Applications.Remove(app);
				}

				Logger.Instance.Log(LogLevel.Info, "\tCreaing new '{0}' application with '{1}' application pool.", virtualDirectoryName, apppoolName);

				app = serverManager.Sites[SiteName].Applications.Add(virtualDirectoryName, physicalPath);
				app.ApplicationPoolName = apppoolName;

				serverManager.CommitChanges();

				Logger.Instance.Log(LogLevel.Info, "\tChanges were commited.");
			}
		}

		protected void ConfigureInnovatorIISWebApplication(string locationPath)
		{
			using (ServerManager serverManager = ServerManager.OpenRemote(ServerName))
			{
				Configuration config = serverManager.GetApplicationHostConfiguration();

				ConfigurationSection aspSection = config.GetSection("system.webServer/asp", locationPath);
				aspSection.ChildElements["session"]["timeout"] = TimeSpan.FromMinutes(20); // "00:20:00"
				aspSection.ChildElements["limits"]["scriptTimeout"] = TimeSpan.FromSeconds(90); // "00:01:30"

				ConfigurationSection anonymousAuthenticationSection = config.GetSection("system.webServer/security/authentication/anonymousAuthentication", locationPath);
				anonymousAuthenticationSection["enabled"] = true;

				ConfigurationSection basicAuthenticationSection = config.GetSection("system.webServer/security/authentication/basicAuthentication", locationPath);
				basicAuthenticationSection["enabled"] = false;

				ConfigurationSection windowsAuthenticationSection = config.GetSection("system.webServer/security/authentication/windowsAuthentication", locationPath);
				windowsAuthenticationSection["enabled"] = false;

				ConfigurationSection httpLoggingSection = config.GetSection("system.webServer/httpLogging", locationPath);
				httpLoggingSection["dontLog"] = false;

				serverManager.CommitChanges();
			}
		}

		public void SetupWinAuth(string locationPath)
		{
			using (ServerManager serverManager = ServerManager.OpenRemote(ServerName))
			{
				Configuration config = serverManager.GetApplicationHostConfiguration();

				ConfigurationSection anonymousAuthenticationSection = config.GetSection("system.webServer/security/authentication/anonymousAuthentication", locationPath);
				anonymousAuthenticationSection["enabled"] = false;

				ConfigurationSection windowsAuthenticationSection = config.GetSection("system.webServer/security/authentication/windowsAuthentication", locationPath);
				windowsAuthenticationSection["enabled"] = true;

				serverManager.CommitChanges();
			}
		}

		private static ManagedPipelineMode ParseManagedPipelineMode(string managedPipelineModeString)
		{
			return (ManagedPipelineMode)Enum.Parse(typeof(ManagedPipelineMode), managedPipelineModeString);
		}
		#endregion

		#region Implementing Cleanup logic
		public override void Remove()
		{
			Logger.Instance.Log(LogLevel.Info, "\nRemoving component ({0}):\n", Id);

			RemoveApplicationPoolWithApplications(ApplicationPoolName);
			RemoveLocationFromIISApplicationHostConfigByPath(VirtualDirectoryPath);

			base.Remove();
		}

		public void RemoveLocationFromIISApplicationHostConfigByPath(string locationPath)
		{
			using (ServerManager serverManager = ServerManager.OpenRemote(ServerName))
			{
				Configuration config = serverManager.GetApplicationHostConfiguration();

				config.RemoveLocationPath(locationPath);
				serverManager.CommitChanges();
			}
		}

		public void RemoveApplicationPoolWithApplications(string apppoolName)
		{
			using (ServerManager serverManager = ServerManager.OpenRemote(ServerName))
			{
				foreach (Site site in serverManager.Sites)
				{
					List<Application> applicationsToRemoveList = site.Applications.Where(app => app.ApplicationPoolName.Equals(apppoolName)).ToList();
					foreach (Application applicationToRemove in applicationsToRemoveList)
					{
						Logger.Instance.Log(LogLevel.Info, "\tRemoving '{0}' application '{1}'...", applicationToRemove.Path, ServerName);

						site.Applications.Remove(applicationToRemove);

					}
				}

				List<ApplicationPool> applicationPoolsToRemoveList = serverManager.ApplicationPools.Where(apppool => apppool.Name.Equals(apppoolName)).ToList();
				foreach (ApplicationPool applicationPoolToRemove in applicationPoolsToRemoveList)
				{
					Logger.Instance.Log(LogLevel.Info, "\tRemoving '{0}' application pool '{1}'...", applicationPoolToRemove.Name, ServerName);

					serverManager.ApplicationPools.Remove(applicationPoolToRemove);

				}

				serverManager.CommitChanges();
			}
		}
		#endregion

		#region Implementing ApplyPackage
		public override void HealthCheck()
		{
			ValidationHelper.CheckHostAvailability(ServerName);
			ValidationHelper.CheckWebApplicationAvailability(this);
		}

		public override void ApplyPackage(Package package)
		{
			string pathToAppOfflineHtm = FileSystem.CombinePaths(InstallationPath, "app_offline.htm");
			if (string.IsNullOrEmpty(ManagedRuntimeVersion))
			{
				using (FileStream appOfflineHtm = TargetFileSystem.OpenFile(pathToAppOfflineHtm))
				{
					StreamWriter appOfflineHtmWriter = new StreamWriter(appOfflineHtm);
					appOfflineHtmWriter.WriteLine("This is temporary app_offline.htm generated by a deployment script to stop web application.");
					appOfflineHtmWriter.WriteLine("If you see this - please remove the file");
				}
			}

			StopAppPool();

			base.ApplyPackage(package);

			if (string.IsNullOrEmpty(ManagedRuntimeVersion) && TargetFileSystem.FileExists(pathToAppOfflineHtm))
			{
				TargetFileSystem.DeleteFile(pathToAppOfflineHtm);
			}

			StartAppPool();
		}
		#endregion

		private void StopAppPool()
		{
			using (ServerManager serverManager = ServerManager.OpenRemote(ServerName))
			{
				string apppoolName = !string.IsNullOrEmpty(ApplicationPoolName) ? ApplicationPoolName : serverManager.Sites[SiteName].Applications[VirtualDirectoryPath].ApplicationPoolName;
				ApplicationPool apppool = serverManager.ApplicationPools[apppoolName];

				Logger.Instance.Log(LogLevel.Info, "\t[iis] Stopping '{0}' application pool ...", apppool.Name);

				apppool.Stop();
				WaitForAppPoolStatus(apppool, ObjectState.Stopped, 10000);
			}
		}

		private void StartAppPool()
		{
			using (ServerManager serverManager = ServerManager.OpenRemote(ServerName))
			{
				string apppoolName = !string.IsNullOrEmpty(ApplicationPoolName) ? ApplicationPoolName : serverManager.Sites[SiteName].Applications[VirtualDirectoryPath].ApplicationPoolName;
				ApplicationPool apppool = serverManager.ApplicationPools[apppoolName];

				Logger.Instance.Log(LogLevel.Info, "\t[iis] Starting '{0}' application pool ...", apppool.Name);

				apppool.Start();
				WaitForAppPoolStatus(apppool, ObjectState.Started, 10000);
			}
		}

		private void WaitForAppPoolStatus(ApplicationPool apppool, ObjectState targetAppPoolState, int timeout)
		{
			const int waitTime = 100;
			int timeSpent = 0;
			while (apppool.State != targetAppPoolState)
			{
				Thread.Sleep(waitTime);
				timeSpent += waitTime;
				if (timeSpent >= timeout)
				{
					Logger.Instance.Log(LogLevel.Warning, "\t[iis] Waiting for '{0}' application pool to be {1} reached a timeout of {2} seconds!", apppool.Name, targetAppPoolState.ToString(), timeout / 1000);
					break;
				}
			}
			Logger.Instance.Log(LogLevel.Info, "\t[iis] Application pool {0} has been {1} in {2} seconds.", apppool.Name, targetAppPoolState.ToString(), waitTime / 1000);
		}
	}
}
