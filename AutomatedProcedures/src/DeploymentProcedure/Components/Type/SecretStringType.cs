﻿namespace DeploymentProcedure.Components.Type
{
	public enum SecretStringType
	{
		PlainText,
		EnvironmentVariable
	}
}
