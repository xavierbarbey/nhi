﻿using System;
using System.Xml.Serialization;

namespace DeploymentProcedure.Components.Type
{
	public class SecretString
	{
		private string _value;

		[XmlAttribute("type")]
		public SecretStringType Type { get; set; } = SecretStringType.PlainText;

		[XmlText]
		public string Value
		{
			get
			{
				switch (Type)
				{
					case SecretStringType.PlainText:
						return _value;
					case SecretStringType.EnvironmentVariable:
						return Environment.GetEnvironmentVariable(_value);
					default:
						throw new ArgumentException("Invalid type of secret string.");
				}
			}
			set
			{
				_value = value;
			}
		}
	}
}
