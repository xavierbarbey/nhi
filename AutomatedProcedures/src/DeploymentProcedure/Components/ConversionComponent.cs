﻿using DeploymentProcedure.Logging;
using DeploymentProcedure.Utility;
using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace DeploymentProcedure.Components
{
	[XmlType("conversion")]
	public class ConversionComponent : WebComponent
	{
		public string Name { get; set; } = "Default";
		public string ConversionServiceAsmxUrl => Url.TrimEnd('/') + "/ConversionService.asmx";
		public string PathToHoopsConverterDir { get; set; } = Environment.GetEnvironmentVariable("Path.To.Hoops.Converter.Dir");

		public ConversionComponent()
		{
			ManagedRuntimeVersion = string.Empty;
		}

		#region Overriding CodeTreeComponent properties
		private string _pathToConfig;
		public override string PathToConfig
		{
			get { return _pathToConfig ?? Path.Combine(InstallationPath, "..\\ConversionServerConfig.xml"); }
			set { _pathToConfig = value; }
		}
		public override string PathToConfigTemplate => Path.Combine(Settings.Default.PathToTemplates, "ConversionServerConfig.xml");
		public override string PathToBasicConfig => Path.Combine(InstallationPath, "ConversionServer.xml");
		public override string BaselineSourcePath { get; set; } = Path.Combine(Settings.Default.PathToCodeTree, "ConversionServer");
		public override string DeploymentPackageDirectoryName { get; set; } = "ConversionServer";
		#endregion


		#region Implementing Setup logic
		public override void Setup()
		{
			base.Setup();

			if (!string.IsNullOrEmpty(PathToHoopsConverterDir))
			{
				SetupHoopsForConversionServerConfig();
			}
		}
		#endregion

		#region Overriding ApplyPackage
		public override void HealthCheck()
		{
			ValidationHelper.CheckHostAvailability(ServerName);
		}
		#endregion

		private void SetupHoopsForConversionServerConfig()
		{
			XmlDocument conversionServerConfig = TargetFileSystem.XmlHelper.OpenDocument(PathToConfig);

			string converterPath = Path.Combine(PathToHoopsConverterDir, "bin\\hoops_converter.exe");
			foreach (XmlNode converterSettingsNode in conversionServerConfig.SelectNodes("/configuration/ConverterSettings/*"))
			{
				XmlNode converterArgumentsAttribute = converterSettingsNode.SelectSingleNode("Command/@arguments");
				XmlNode converterPathAttribute = converterSettingsNode.SelectSingleNode("Application/@converterPath");

				if (converterArgumentsAttribute != null)
				{
					string converterArguments = SubstituteHoopsPlaceholder(converterArgumentsAttribute.InnerText);

					Logger.Instance.Log(LogLevel.Info, "\tSetting the '{0}' arguments and '{1}' converter path for '{2}' converter.", converterArguments, converterPath, converterSettingsNode.Name);

					converterArgumentsAttribute.InnerText = converterArguments;
				}
				if (converterPathAttribute != null)
				{
					converterPathAttribute.InnerText = converterPath;
				}
			}

			TargetFileSystem.XmlHelper.SaveXmlDocument(conversionServerConfig);
		}

		private string SubstituteHoopsPlaceholder(string arguments)
		{
			return arguments.Replace("@Path.To.Hoops.Converter.Dir@", PathToHoopsConverterDir);
		}
	}
}
