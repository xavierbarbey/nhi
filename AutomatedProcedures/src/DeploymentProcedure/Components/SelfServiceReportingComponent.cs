﻿using System.IO;
using System.Xml.Serialization;

namespace DeploymentProcedure.Components
{
	[XmlType("ssr")]
	public class SelfServiceReportingComponent : WebComponent
	{
		#region Overriding CodeTreeComponent properties
		private string _pathToConfig;
		public override string PathToConfig
		{
			get { return _pathToConfig ?? Path.Combine(InstallationPath, "..\\SelfServiceReportConfig.xml"); }
			set { _pathToConfig = value; }
		}
		public override string PathToCodeTreeTemplates => Path.Combine(Settings.Default.PathToTemplates, "SelfServiceReporting");
		public override string PathToConfigTemplate => Path.Combine(Settings.Default.PathToTemplates, "SelfServiceReportConfig.xml");
		public override string PathToBasicConfig => Path.Combine(InstallationPath, "SelfServiceReport.xml");
		public override string BaselineSourcePath { get; set; } = Path.Combine(Settings.Default.PathToCodeTree, "SelfServiceReporting");
		public override string DeploymentPackageDirectoryName { get; set; } = "SelfServiceReporting";
		#endregion

		public override void Setup()
		{
			base.Setup();

			ConfigureInnovatorIISWebApplication(SiteName + VirtualDirectoryPath);
		}
	}
}
