﻿using Aras.IOM;
using DeploymentProcedure.Components;
using DeploymentProcedure.Components.Utility;
using DeploymentProcedure.Logging;
using DeploymentProcedure.Utility.Base;
using System;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.NetworkInformation;
using System.Net.Sockets;

namespace DeploymentProcedure.Utility
{
	internal static class ValidationHelper
	{
		private static readonly Ping ping = new Ping();

		static ValidationHelper()
		{
			if (!Settings.Default.DoVerifyCertificates)
			{
				ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;
			}
		}

		internal static void CheckAmlApplicability(DatabaseComponent databaseComponent)
		{
			Logger.Instance.Log(LogLevel.Info, "Checking that we can apply AML to '{0}' component", databaseComponent.Id);

			HttpServerConnection connection = ServerConnectionFactory.GetServerConnection(databaseComponent);
			Innovator innovator = new Innovator(connection);
			string aml = "<AML />";
			Item result = innovator.applyAML(aml);
			if (result.isError())
			{
				Logger.Instance.Log(LogLevel.Error, result.ToString());

				throw new Exception(string.Format("Failed to apply AML: {0}.", aml));
			}

			Logger.Instance.Log(LogLevel.Info, "OK");
		}

		internal static void CheckDirectoryExistence(IFileSystem fileSystem, string directoryPath)
		{
			Logger.Instance.Log(LogLevel.Info, "Checking '{0}' directory existence", fileSystem.GetFullPath(directoryPath));

			if (!fileSystem.DirectoryExists(directoryPath))
			{
				throw new Exception(string.Format("Directory '{0}' doesn't exist", fileSystem.GetFullPath(directoryPath)));
			}

			Logger.Instance.Log(LogLevel.Info, "OK");
		}

		internal static void CheckHostAvailability(string hostname)
		{
			Logger.Instance.Log(LogLevel.Info, "Checking '{0}' availability", hostname);

			PingReply reply = ping.Send(hostname);
			if (reply.Status != IPStatus.Success)
			{
				throw new Exception(string.Format("Host '{0}' is unreachable.", hostname));
			}

			Logger.Instance.Log(LogLevel.Info, "OK");
		}

		internal static void CheckSqlServerConnection(string connectionString)
		{
			Logger.Instance.Log(LogLevel.Info, "Checking SQL server connection through '{0}' connection string", connectionString);

			using (SqlConnection sqlConnection = new SqlConnection(connectionString))
			{
				sqlConnection.Open();

				Logger.Instance.Log(LogLevel.Info, "OK");
			}
		}

		internal static void CheckTcpPortAvailability(string hostname, int tcpPort)
		{
			Logger.Instance.Log(LogLevel.Info, "Checking '{0}:{1}' TCP port availability", hostname, tcpPort);

			using (TcpClient tcpClient = new TcpClient())
			{
				tcpClient.Connect(hostname, tcpPort);

				Logger.Instance.Log(LogLevel.Info, "OK");
			}
		}

		internal static void CheckWebApplicationAvailability(WebComponent webComponent)
		{
			Logger.Instance.Log(LogLevel.Info, "Checking '{0}' web application availability", webComponent.Id);

			const string healthCheckPageContent = "I'm alright";
			const string healthCheckPageName = "HealthCheck.aspx";
			string healthCheckPagePath = Path.Combine(webComponent.InstallationPath, healthCheckPageName);
			string healthCheckPageUrl = string.Format("{0}/{1}", webComponent.Url, healthCheckPageName);

			try
			{
				webComponent.TargetFileSystem.WriteAllTextToFile(healthCheckPagePath, string.Format("<%= \"{0}\" %>", healthCheckPageContent));

				using (HttpClient healthCheckClient = new HttpClient())
				{
					HttpResponseMessage healthCheckResponse = healthCheckClient.GetAsync(healthCheckPageUrl).Result;
					healthCheckResponse.EnsureSuccessStatusCode();

					Logger.Instance.Log(LogLevel.Info, "OK");
				}
			}
			finally
			{
				webComponent.TargetFileSystem.DeleteFile(healthCheckPagePath);
			}
		}

		internal static void CheckWritePermissionsToDirectory(IFileSystem fileSystem, string directoryPath)
		{
			Logger.Instance.Log(LogLevel.Info, "Checking write permissions to '{0}' directory", fileSystem.GetFullPath(directoryPath));

			directoryPath = directoryPath.TrimEnd(Path.DirectorySeparatorChar);
			while (!fileSystem.DirectoryExists(directoryPath))
			{
				Logger.Instance.Log(LogLevel.Warning, "Directory '{0}' doesn't exist", directoryPath);

				directoryPath = directoryPath.Substring(0, directoryPath.LastIndexOf(Path.DirectorySeparatorChar));

				Logger.Instance.Log(LogLevel.Warning, "Continue check for '{0}'", directoryPath);
			}

			string pathToTempFile = FileSystem.CombinePaths(directoryPath, Path.GetRandomFileName());
			try
			{
				fileSystem.CreateFile(pathToTempFile);

				Logger.Instance.Log(LogLevel.Info, "OK");
			}
			finally
			{
				fileSystem.DeleteFile(pathToTempFile);
			}
		}
	}
}
