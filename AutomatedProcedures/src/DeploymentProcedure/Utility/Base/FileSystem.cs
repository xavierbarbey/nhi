﻿using DeploymentProcedure.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;

namespace DeploymentProcedure.Utility.Base
{
	internal abstract class FileSystem : IFileSystem
	{
		public static string LocalServerName => "localhost";

		public abstract bool IsLocal { get; }
		public abstract string ServerName { get; }
		public XmlHelper XmlHelper { get; }

		public FileSystem()
		{
			XmlHelper = new XmlHelper(this);
		}

		public void CopyDirectory(string sourceDirectoryPath, IFileSystem targetFileSystem, string targetDirectoryPath, bool overwrite = true)
		{
			string robocopyPath = "robocopy";
			int robocopySearchExitCode = ProcessWrapper.Execute(LogLevel.None, "cmd", "/C where {0}", robocopyPath);
			bool useRobocopy = robocopySearchExitCode == 0;

			if (useRobocopy)
			{
				//robocopy options explanation:
				// /E :: copy subdirectories, including Empty ones.
				// /NP :: No Progress - don't display percentage copied.
				// /NFL::No File List -don't log file names.
				// /NDL::No Directory List -don't log directory names.
				// /NJH::No Job Header.
				ProcessWrapper.Execute(robocopyPath, "\"{0}\" \"{1}\" /E /NP /NJH /NFL /NDL", GetFullPath(sourceDirectoryPath), targetFileSystem.GetFullPath(targetDirectoryPath));
			}
			else
			{
				Logger.Instance.Log(LogLevel.Info, "\tCopying files from '{0}' to '{1}'...", GetFullPath(sourceDirectoryPath), targetFileSystem.GetFullPath(targetDirectoryPath));

				int copiedFilesAmount = CopyFilesRecursively(sourceDirectoryPath, targetFileSystem, targetDirectoryPath, overwrite);

				Logger.Instance.Log(LogLevel.Info, "\t{0} files were copied", copiedFilesAmount);
			}
		}

		public abstract void CopyFile(string sourceFilePath, IFileSystem targetFileSystem, string targetFilePath, bool overwrite);
		public abstract void CreateDirectory(string directoryPath);
		public abstract void CreateFile(string filePath);
		public abstract void DeleteDirectory(string directoryPath);
		public abstract void DeleteFile(string filePath);
		public abstract bool DirectoryExists(string directoryPath);
		public abstract IEnumerable<string> EnumerateDirectories(string path);
		public abstract IEnumerable<string> EnumerateFiles(string path);
		public abstract bool FileExists(string filePath);
		public abstract string GetFullPath(string path);
		public abstract FileStream OpenFile(string filePath);
		public abstract void WriteAllTextToFile(string filePath, string content);

		internal static string CombinePaths(string path, string childPath)
		{
			return path.TrimEnd(Path.DirectorySeparatorChar) + Path.DirectorySeparatorChar + childPath;
		}

		internal static bool IsLocalMachine(string machineName)
		{
			if (string.Equals(machineName, LocalServerName, StringComparison.InvariantCultureIgnoreCase))
			{
				return true;
			}

			IPAddress[] machineIPs = Dns.GetHostAddresses(machineName);
			IPAddress[] localMachineIPs = Dns.GetHostAddresses(Dns.GetHostName());

			return machineIPs.Any(machineIP => IPAddress.IsLoopback(machineIP) || localMachineIPs.Contains(machineIP));
		}

		protected int CopyFilesRecursively(string sourceDirectoryPath, IFileSystem targetFileSystem, string targetDirectoryPath, bool overwrite)
		{
			if (string.Equals(sourceDirectoryPath, targetDirectoryPath, StringComparison.InvariantCultureIgnoreCase))
			{
				Logger.Instance.Log(LogLevel.Warning, "\tCannot copy directory {0} to itself.", sourceDirectoryPath);

				return 0;
			}

			if (!targetFileSystem.DirectoryExists(targetDirectoryPath))
			{
				targetFileSystem.CreateDirectory(targetDirectoryPath);
			}

			int copiedFilesCount = 0;
			foreach (string sourceSubdirectoryPath in EnumerateDirectories(sourceDirectoryPath))
			{
				string targetSubdirectoryPath = CombinePaths(targetDirectoryPath, GetLastElementNameFromPath(sourceSubdirectoryPath));
				copiedFilesCount += CopyFilesRecursively(sourceSubdirectoryPath, targetFileSystem, targetSubdirectoryPath, overwrite);
			}
			foreach (string sourceFilePath in EnumerateFiles(sourceDirectoryPath))
			{
				string targetFilePath = CombinePaths(targetDirectoryPath, GetLastElementNameFromPath(sourceFilePath));
				CopyFile(sourceFilePath, targetFileSystem, targetFilePath, overwrite);
				copiedFilesCount++;
			}
			return copiedFilesCount;
		}

		private static string GetLastElementNameFromPath(string path)
		{
			return path.Substring(path.LastIndexOf(Path.DirectorySeparatorChar) + 1);
		}
	}
}
