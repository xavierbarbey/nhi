﻿using DeploymentProcedure.Utility.Base;
using System.Collections.Generic;
using System.IO;

namespace DeploymentProcedure.Utility
{
	internal partial class FileSystemFactory
	{
		private class ServerFileSystem : FileSystem
		{
			public override bool IsLocal { get; }
			public override string ServerName { get; }

			public ServerFileSystem(string serverName)
			{
				IsLocal = IsLocalMachine(serverName);
				ServerName = serverName;
			}

			public override void CopyFile(string sourceFilePath, IFileSystem targetFileSystem, string targetFilePath, bool overwrite)
			{
				File.Copy(GetFullPath(sourceFilePath), targetFileSystem.GetFullPath(targetFilePath), overwrite);
			}

			public override void CreateDirectory(string directoryPath)
			{
				Directory.CreateDirectory(GetFullPath(directoryPath));
			}

			public override void CreateFile(string filePath)
			{
				using (FileStream fileStream = File.Create(GetFullPath(filePath)))
				{
					// To correctly close file
				}
			}

			public override void DeleteDirectory(string directoryPath)
			{
				Directory.Delete(GetFullPath(directoryPath), true);
			}

			public override void DeleteFile(string filePath)
			{
				File.Delete(GetFullPath(filePath));
			}
			public override bool DirectoryExists(string directoryPath)
			{
				return Directory.Exists(GetFullPath(directoryPath));
			}
			public override IEnumerable<string> EnumerateDirectories(string path)
			{
				return Directory.EnumerateDirectories(GetFullPath(path));
			}

			public override IEnumerable<string> EnumerateFiles(string path)
			{
				return Directory.EnumerateFiles(GetFullPath(path));
			}

			public override bool FileExists(string filePath)
			{
				return File.Exists(GetFullPath(filePath));
			}

			public override string GetFullPath(string path)
			{
				return string.Format("\\\\{0}\\{1}", ServerName, path.Replace(':', '$'));
			}

			public override FileStream OpenFile(string filePath)
			{
				return File.Open(GetFullPath(filePath), FileMode.OpenOrCreate, FileAccess.ReadWrite);
			}

			public override void WriteAllTextToFile(string filePath, string content)
			{
				File.WriteAllText(GetFullPath(filePath), content);
			}
		}
	}
}
