@echo off

set PathToDeploymentProcedureExe="%~dp0%\AutomatedProcedures\tools\DeploymentProcedure\DeploymentProcedure.exe"
set PathToInstanceDeployConfig=InstanceTemplate.deploy.xml
set /P PathToInstanceDeployConfig=Enter PathToInstanceDeployConfig (It describes Innovator instance structure and deployment steps. Default value is '%PathToInstanceDeployConfig%'):

%PathToDeploymentProcedureExe% -t Deploy -c %PathToInstanceDeployConfig%

pause