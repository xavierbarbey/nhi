﻿using DeploymentProcedure.Components;
using DeploymentProcedure.Components.Base;
using DeploymentProcedure.Components.Type;
using DeploymentProcedure.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;

namespace DeploymentProcedure.Packages
{
	public class Package
	{
		[XmlAttribute("applyTo")]
		public string ApplyToComponents { get; set; } = string.Empty;
		public string PathToDeploymentPackage { get; set; }
		[XmlArray("featureLicenses")]
		[XmlArrayItem("featureLicense")]
		public List<SecretString> FeatureLicenses { get; set; }
		[XmlArray("variables")]
		[XmlArrayItem("variable", typeof(Variable))]
		public List<Variable> Variables { get; set; }

		public virtual void Import(IReadOnlyCollection<Component> instanceComponents)
		{
			Logger.Instance.Log(LogLevel.Info, "\nApplying package from the '{0}' folder...", Path.GetFullPath(PathToDeploymentPackage));

			foreach (Component component in GetFilteredInstanceComponents(instanceComponents))
			{
				component.ApplyPackage(this);
			}
		}

		private IReadOnlyCollection<Component> GetFilteredInstanceComponents(IReadOnlyCollection<Component> instanceComponents)
		{
			if (!string.IsNullOrEmpty(ApplyToComponents))
			{
				string[] applyToComponentsIds = ApplyToComponents.Split(new char[] { ',', ';', ' ' }, StringSplitOptions.RemoveEmptyEntries);
				return instanceComponents.Where(c => applyToComponentsIds.Contains(c.Id)).ToList();
			}
			else
			{
				return instanceComponents;
			}
		}
	}
}
