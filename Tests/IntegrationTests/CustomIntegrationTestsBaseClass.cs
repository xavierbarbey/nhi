﻿using Aras.Tests.Integration;

namespace IntegrationTests
{
	public class CustomIntegrationTestsBaseClass : Aras.Tests.Integration.InnovatorServerTests
	{
		public static string PathToTestsOutputFolder => ConnectionInfo.TestsOutputDirectory;

		protected override string RelativePathToIntegrationTestsDirectory => "..\\..\\IntegrationTests\\";
		protected override string CustomRelativePathToIntegrationTestsTestCasesDirectory => "IntegrationTests";
		protected override string DefaultRelativePathToIntegrationTestsTestCasesDirectory => "TestCases";
	}
}
