@echo off

SET PathToThisBatFileFolder=%~dp0

"%PathToThisBatFileFolder%AutomatedProcedures\tools\.nuget\NuGet.exe" restore AllTests.sln

start "" "%PathToThisBatFileFolder%packages\NUnit.Runners.2.6.4\tools\nunit.exe"^
 "%PathToThisBatFileFolder%CSharpMethods.UnitTests.nunit"
pause